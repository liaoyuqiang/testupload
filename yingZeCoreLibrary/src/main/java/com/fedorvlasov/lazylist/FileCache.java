package com.fedorvlasov.lazylist;

import java.io.File;

import com.yingzecorelibrary.utils.LogUtil;

import android.content.Context;
import android.util.Log;

public class FileCache {
    
    private File cacheDir;
    public static String downloadFolder = "/SmartHomeHelper/image";// 支持动态配置
    
    public FileCache(Context context){
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(), downloadFolder);
        
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
        Log.d("msg", "默认缓存文件夹路劲==>"+cacheDir.getAbsolutePath());
    }
    
    public FileCache(Context context,String FilePath){
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
            cacheDir=new File(FilePath);
        Log.d("msg", "获得了存储卡路劲==>cacheDir==>"+FilePath);
        }
        else{
            cacheDir=context.getCacheDir();
            Log.d("msg", "没有存储卡路劲==>cacheDir==>"+context.getCacheDir());
        }
        if(!cacheDir.exists()) {
			boolean mkdirs = cacheDir.mkdirs();
			Log.d("msg", "缓存创建文件夹==>"+mkdirs);
		}else{
			Log.d("msg", "缓存文件夹已存在");
		}
    }
    
    public File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
//        String filename=String.valueOf(url.hashCode());
    	String filename = clearPostfix(url.substring(url.lastIndexOf("/")+1, url.length()));
    	LogUtil.set(getClass().getSimpleName(),"filename=" + filename).info();
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    public static String clearPostfix(String image){
    	return image.substring(0, image.lastIndexOf(".")==-1?image.length():image.lastIndexOf("."));
	}
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        for(File f:files)
            f.delete();
    }

}