package com.fedorvlasov.lazylist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Stack;
import java.util.WeakHashMap;
import com.yingzecorelibrary.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

@SuppressLint("NewApi")
public class ImageLoader {
	
	public String imgUrl;
	
    public String getImgUrl() {
		return imgUrl;
	}

	private final int DEFAULT_EXPECT_WIDTH = -1;
    private final int DEFAULT_EXPECT_HEIGHT = -1;
     int stub_id = R.drawable.ic_nopic;
    private Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
   
    private FileCache fileCache;
    private MemoryCache memoryCache=new MemoryCache();
    private Handler handler = new Handler();

    private PhotosQueue photosQueue=new PhotosQueue();
    private PhotosLoader photoLoaderThread=new PhotosLoader();
    
    public ImageLoader(Context context) {
        photoLoaderThread.setPriority(Thread.NORM_PRIORITY-1);
        fileCache=new FileCache(context);
    }
    
    public ImageLoader(Context context,String dirPath) {
        photoLoaderThread.setPriority(Thread.NORM_PRIORITY-1);
        fileCache=new FileCache(context, dirPath);
    }
    
    public void DisplayImage(String url, Activity activity, ImageView imageView) {
        imageViews.put(imageView, url);
        Bitmap bitmap=memoryCache.get(url);
        if(bitmap!=null) {
            imageView.setImageBitmap(bitmap);
        } else {	
        	if("".equals(url)||url==null) {
        		 imageView.setImageResource(stub_id);
        	} else {
	            queuePhoto(url, activity, imageView);
	            imageView.setImageResource(stub_id);
        	}
        }    
    }
   public void setImage(int id){
	   stub_id = id;
   }
   
   public void DisplayImage(String url, Activity activity, ImageView imageView,int imageId) {
       imageViews.put(imageView, url);
       Bitmap bitmap=memoryCache.get(url);
       if(bitmap!=null) {
           imageView.setImageBitmap(bitmap);
       } else {	
       	if("".equals(url)||url==null) {
       		 imageView.setImageResource(imageId);
       	} else {
	            queuePhoto(url, activity, imageView);
	            imageView.setImageResource(imageId);
       	}
       }    
   }
   
    public void DisplayImage(String url, Activity activity, ImageView imageView, int expectWidth, int expectHeight) {
        imageViews.put(imageView, url);
        Bitmap bitmap=memoryCache.get(url);
        if(bitmap!=null) {
            imageView.setImageBitmap(bitmap);
            imgUrl = fileCache.getFile(url).getAbsolutePath();
            Log.e("ImageLoader", "display bitmap!=null:"+imgUrl);
        } else {	
        	if("".equals(url)||url==null) {
        		 imageView.setImageResource(stub_id);
        	} else {
	            queuePhoto(url, activity, imageView, expectWidth, expectHeight);
	            imageView.setImageResource(stub_id);
        	}
        }    
    }
    
    public void DisplayImage(String url, Activity activity, ImageView imageView, int expectWidth, int expectHeight,int imageId) {
    	stub_id = imageId;
    	Log.d("msg", "DisplayImage");
    	imageViews.put(imageView, url);
        Bitmap bitmap=memoryCache.get(url);
        if(bitmap!=null) {
        	Log.d("msg", "bitmap!=null");
            imageView.setImageBitmap(bitmap);
        } else {	
        	if("".equals(url)||url==null) {
        		Log.d("msg", "url==null");
        		 imageView.setImageResource(imageId);
        		 Log.d("msg", "设置了图片id为"+imageId);
        	} else {
        		Log.d("msg", "url!=null");
	            queuePhoto(url, activity, imageView, expectWidth, expectHeight);
	            imageView.setImageResource(imageId);
        	}
        }    
    }
    
    private void queuePhoto(String url, Activity activity, ImageView imageView) {
        photosQueue.Clean(imageView);
        PhotoToLoad p=new PhotoToLoad(url, imageView);
        synchronized(photosQueue.photosToLoad){
            photosQueue.photosToLoad.push(p);
            photosQueue.photosToLoad.notifyAll();
        }
        if(photoLoaderThread.getState()==Thread.State.NEW) {
            photoLoaderThread.start();
        }
    }
    
    private void queuePhoto(String url, Activity activity, ImageView imageView, int expectWidth, int exceptHeight) {
        photosQueue.Clean(imageView);
        PhotoToLoad p=new PhotoToLoad(url, imageView, expectWidth, exceptHeight);
        synchronized(photosQueue.photosToLoad){
            photosQueue.photosToLoad.push(p);
            photosQueue.photosToLoad.notifyAll();
        }
        if(photoLoaderThread.getState()==Thread.State.NEW) {
            photoLoaderThread.start();
        }
    }
    
    private Bitmap getBitmap(PhotoToLoad photoToLoad) {
    	String url = photoToLoad.url;
        File f=fileCache.getFile(url);
        Bitmap b;
        if(url.startsWith("http")) {
        	 b = decodeFile(f, photoToLoad.expectWidth, photoToLoad.expectHeight);
        } else {
	        //from SD cache, 用作处理当传过来的url为本地地址的时候
	        int fileNamePostfixIndex = url.lastIndexOf(".");
	        String fileNamePostfix = url.substring(fileNamePostfixIndex+1); // 文件后缀
	        if(fileNamePostfix.equalsIgnoreCase("jpg")){
	        	b=decodeFile(url, photoToLoad.expectWidth, photoToLoad.expectHeight);
	        } else if(fileNamePostfix.equalsIgnoreCase("png")){
	        	b=decodeFile(url, photoToLoad.expectWidth, photoToLoad.expectHeight);
	        } else {
	        	b = decodeFile(f, photoToLoad.expectWidth, photoToLoad.expectHeight);
	        }
        }
        
        if(b!=null) {
        	imgUrl =fileCache.getFile(url).getAbsolutePath(); 
        	Log.e("ImageLoader", "getbitmap from SD:"+imgUrl);
            return b;
        }
        
        //from web
        try {
            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            InputStream is=conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            bitmap = decodeFile(f, photoToLoad.expectWidth, photoToLoad.expectHeight);
            imgUrl =fileCache.getFile(url).getAbsolutePath(); 
            Log.e("ImageLoader", "getbitmap from web:"+imgUrl);
            return bitmap;
        } catch (Exception ex){
           ex.printStackTrace();
           return null;
        }
    }

    private Bitmap decodeFile(File f, int expectWidth, int expectHeight){
        try {
   		 	BitmapFactory.Options opt = new BitmapFactory.Options();
   		 	opt.inJustDecodeBounds = true;
   		 	BitmapFactory.decodeFile(f.getAbsolutePath(), opt);
        	
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            int scale=1;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            if(expectWidth!=DEFAULT_EXPECT_WIDTH && expectHeight!=DEFAULT_EXPECT_HEIGHT) {
            	if(width_tmp>expectWidth || height_tmp>expectHeight) {
                	float widthScale = width_tmp/expectWidth;
                	float heightScale = height_tmp/expectHeight;
                	float scale_tmp = widthScale>heightScale?widthScale:heightScale;
                	scale = (int) Math.ceil(scale_tmp);// 向上取整
            	}
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            if (bmp != null) {
            	Log.e("image-loader", "图片大小:"+bmp.getByteCount()+",缩放比例:"+scale);
            }
            return bmp;
        } catch (FileNotFoundException e) {}
        return null;
    }
    
    //decodes image .jpg
    private Bitmap decodeFile(String url, int expectWidth, int expectHeight) {
    	if(url!=null){
	   		 BitmapFactory.Options opt = new BitmapFactory.Options();
	   		 opt.inJustDecodeBounds = true;
	   		 BitmapFactory.decodeFile(url, opt);
	   		 
    		 BitmapFactory.Options o = new BitmapFactory.Options();
    		 o.inJustDecodeBounds = true;
    		 BitmapFactory.decodeFile(url, o);
             int scale=1;
             int width_tmp=o.outWidth, height_tmp=o.outHeight;
             Log.e("image-loader", "图片尺寸:"+width_tmp+","+height_tmp);
             if(expectWidth!=DEFAULT_EXPECT_WIDTH && expectHeight!=DEFAULT_EXPECT_HEIGHT) {
             	if(width_tmp>expectWidth || height_tmp>expectHeight) {
                 	float widthScale = width_tmp/expectWidth;
                 	float heightScale = height_tmp/expectHeight;
                 	float scale_tmp = widthScale>heightScale?widthScale:heightScale;
                 	scale = (int) Math.ceil(scale_tmp);// 向上取整
             	}
             }
             BitmapFactory.Options o2 = new BitmapFactory.Options();
             o2.inSampleSize=scale;
             Bitmap bmp = BitmapFactory.decodeFile(url, o2);
             if (bmp != null) {
            	 Log.e("image-loader", "图片大小:"+bmp.getByteCount()+",缩放比例:"+scale);
             }
             return bmp;
    	}
    	return null;
    }
    
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;
        public int expectWidth = -1;
        public int expectHeight = -1; 
        public PhotoToLoad(String u, ImageView i) {
            url=u; 
            imageView=i;
        }
        
        public PhotoToLoad(String u, ImageView i, int expectWidth, int expectHeight) {
            url=u; 
            imageView=i;
            this.expectWidth = expectWidth;
            this.expectHeight = expectHeight;
        }
    }
    
    public void stopThread() {
        photoLoaderThread.interrupt();
    }
    
    class PhotosQueue {
        private Stack<PhotoToLoad> photosToLoad=new Stack<PhotoToLoad>();
        public void Clean(ImageView image) {
            for(int j=0 ;j<photosToLoad.size();){
                if(photosToLoad.get(j).imageView==image) {
                    photosToLoad.remove(j);
                } else {
                    ++j;
                }
            }
        }
    }
    
    class PhotosLoader extends Thread {
        public void run() {
            try {
                while(true) {
                    if(photosQueue.photosToLoad.size()==0){
                        synchronized(photosQueue.photosToLoad){
                            photosQueue.photosToLoad.wait();
                        }
                    }
                    if(photosQueue.photosToLoad.size()!=0) {
                        PhotoToLoad photoToLoad;
                        synchronized(photosQueue.photosToLoad){
                            photoToLoad=photosQueue.photosToLoad.pop();
                        }
                        Bitmap bmp=getBitmap(photoToLoad);
                        memoryCache.put(photoToLoad.url, bmp);
                        String tag=imageViews.get(photoToLoad.imageView);
                        if(tag!=null && tag.equals(photoToLoad.url)){
                            BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad.imageView);
                            handler.post(bd);
                        }
                    }
                    if(Thread.interrupted()) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        ImageView imageView;
        
        public BitmapDisplayer(Bitmap b, ImageView i){
        	bitmap=b;imageView=i;
        }
        
        public void run() {
            if(bitmap!=null) {
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setImageResource(stub_id);
            }
        }
    }

    
    public String getImgUrlByString(String url){
    	
    	return fileCache.getFile(url).getAbsolutePath();
    }
    
    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

}
