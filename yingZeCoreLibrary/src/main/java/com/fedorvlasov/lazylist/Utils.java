package com.fedorvlasov.lazylist;

import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.BitmapFactory;
import android.widget.ImageView;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
    /**
     * 获取合适的缩放比例解码图片
     * @param pathName
     * @param imageView
     * @return
     */
    public static int countBitmapInSampleSize(String pathName ,ImageView imageView){
    	int inSampleSize = 1;
    	
    	int width_imageView = imageView.getWidth();
    	int height_imageView = imageView.getHeight();
    	
    	BitmapFactory.Options options = new BitmapFactory.Options();
    	options.inJustDecodeBounds = true;
    	BitmapFactory.decodeFile(pathName,options);
    	int width_bitmap = options.outWidth;
    	int height_bitmap = options.outHeight;
    	while (!(width_bitmap / 2 < width_imageView || height_bitmap / 2 < height_imageView)) {
			inSampleSize *= 2;
			width_bitmap /= 2;
			height_bitmap /= 2;
		}
    	return inSampleSize;
    }
    
}