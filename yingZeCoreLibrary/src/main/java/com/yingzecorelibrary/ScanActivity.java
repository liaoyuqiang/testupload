package com.yingzecorelibrary;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.yingzecorelibrary.qrcode.CameraManager;
import com.yingzecorelibrary.qrcode.CaptureActivityHandler;
import com.yingzecorelibrary.qrcode.InactivityTimer;
import com.yingzecorelibrary.qrcode.QRCodeListener;
import com.yingzecorelibrary.qrcode.ViewfinderView;
import com.yingzecorelibrary.utils.FieldUtil;
import com.yingzecorelibrary.utils.LogUtil;

/**
 * 初始化扫描界面
 * @author Ryan.Tang
 */
public class ScanActivity extends Activity implements Callback,QRCodeListener {
//	@SPResourceLayout(R.layout.activity_scan_camera)
//	private int layout;
//	@SPResourceView(R.id.viewfinder_view)
	private ViewfinderView viewfinderView;
//	@SPResourceView(R.id.btn_cancel_scan)
	private Button cancelScanButton;
	

	private CaptureActivityHandler handler;
	private boolean hasSurface;
	private Vector<BarcodeFormat> decodeFormats;
	private String characterSet;
	private InactivityTimer inactivityTimer;
	private boolean vibrate;
	
	private Application app;
	
	private int scanType;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		this.setContentView(R.layout.activity_scan_camera);
		viewfinderView = (ViewfinderView)findViewById(R.id.viewfinder_view);
		cancelScanButton = (Button)findViewById(R.id.btn_cancel_scan);
		
		CameraManager.init(getApplication());
		
		init();
	}

	private void init() {
		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);
		app = (Application) getApplication();
		scanType = getIntent().getIntExtra("scanType", -1);
		LogUtil.set(getClass().getSimpleName(), "scanType-->"+scanType);
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			initCamera(surfaceHolder);
		} else {
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		decodeFormats = null;
		characterSet = null;

		vibrate = true;//震动
		
		//为扫描二维码界面上的取消按钮添加监听
		cancelScanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ScanActivity.this.finish();
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		CameraManager.get().closeDriver();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		super.onDestroy();
	}
	
	/**
	 * Handler scan result,回调相应函数
	 * @param result
	 * @param barcode
	 */
	public void handleDecode(Result result, Bitmap barcode) {
		inactivityTimer.onActivity();
		playBeepSoundAndVibrate();
		String resultString = result.getText();
		LogUtil.set(getClass().getSimpleName(), "resultString-->" + resultString).info();
		//TODO 扫描出二维码进行逻辑处理
		Intent intent;
		switch (scanType) {
		case FieldUtil.SCAN_TYPE_REPAIR://自动填写报修信息
			intent = new Intent();
			intent.putExtra("scanInfo", resultString);
			setResult(-1,intent);
			finish();
			break;
		case FieldUtil.SCAN_TYPE_WIFI://连接wifi
//			intent = new Intent(getApplicationContext(), ScanResultActivity.class);
//			intent.putExtra("scanInfo", resultString);
//			intent.putExtra("scanType", scanType);
//			startActivity(intent);
			finish();
			break;
		case FieldUtil.SCAN_TYPE_BORROW_BIKE://慢行交通借车
//			intent = new Intent(getApplicationContext(), ScanResultActivity.class);
//			intent.putExtra("scanInfo", resultString);
//			intent.putExtra("scanType", scanType);
//			startActivity(intent);
			finish();
			break;
		default://没有目的，展示扫描结果
//			intent = new Intent(getApplicationContext(), ScanResultActivity.class);
//			intent.putExtra("scanInfo", resultString);
//			intent.putExtra("scanType", scanType);
//			startActivity(intent);
			break;
		}
		
//		if (scanType == FieldUtil.SCAN_TYPE_REPAIR) {//自动填写报修信息
//			intent = new Intent();
//			intent.putExtra("scanInfo", resultString);
//			setResult(-1,intent);
//			finish();
//		}else if (scanType == FieldUtil.SCAN_TYPE_WIFI) {//连接wifi
//			intent = new Intent(getApplicationContext(), ScanResultActivity.class);
//			intent.putExtra("scanInfo", resultString);
//			intent.putExtra("scanType", scanType);
//			startActivity(intent);
//			finish();
//		}else if(scanType == FieldUtil.SCAN_TYPE_BORROW_BIKE){//慢行交通借车
//			intent = new Intent(getApplicationContext(), ScanResultActivity.class);
//			intent.putExtra("scanInfo", resultString);
//			intent.putExtra("scanType", scanType);
//			startActivity(intent);
//			finish();
//		}else {//没有目的，展示扫描结果
//			intent = new Intent(getApplicationContext(), ScanResultActivity.class);
//			intent.putExtra("scanInfo", resultString);
//			intent.putExtra("scanType", scanType);
//			startActivity(intent);
//		}
		
	}
	
	private void initCamera(SurfaceHolder surfaceHolder) {
		try {
			CameraManager.get().openDriver(surfaceHolder);
		} catch (IOException ioe) {
			return;
		} catch (RuntimeException e) {
			return;
		}
		if (handler == null) {
			handler = new CaptureActivityHandler(this, decodeFormats,
					characterSet);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;

	}

	public ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}

	private static final long VIBRATE_DURATION = 200L;

	private void playBeepSoundAndVibrate() {
		if (vibrate) {
			Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}

	public String getQrCodeContent(String content) {
		String result = "";
		byte[] b = null;
		content = content.replace(" ", "");
		try {
			String[] temp = content.split(",");
			int size = temp.length;
			b = new byte[size];
			for (int i = 0; i < size; i++) {
				b[i] = Byte.parseByte(temp[size - i - 1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			result = new String(b , "gbk");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		return result;
	}
	
	@Override
	public void onBackPressed() {
		finish();
		super.onBackPressed();
	}

}