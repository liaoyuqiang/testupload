package com.yingzecorelibrary.qrcode;

import com.google.zxing.Result;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;

public interface QRCodeListener {

	public Handler getHandler();
	public void drawViewfinder();
	public void startActivity(Intent intent);
	public void handleDecode(Result result, Bitmap barcode);
	public void setResult(int resultCode, Intent data);
	public void finish();
	public ViewfinderView getViewfinderView();
}
