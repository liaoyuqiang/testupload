package com.yingzecorelibrary.unittest;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yingzecorelibrary.widget.connectmanage.BaseSocketController;
import com.yingzecorelibrary.widget.connectmanage.SocketClient;
import com.yingzecorelibrary.widget.connectmanage.SocketController;
import com.yingzecorelibrary.widget.connectmanage.SocketInfo;

public class FunctionTest {

	public Context mContext;
	public EditText text;
	public TextView networkTv;
	public TextView wifiTv;
	private int index=1;
	private final int TYPE_RESPONSECONTENT=0;
	private final int TYPE_NETWORKSTATE=1;
	private final int TYPE_WIFI=2;
	
	public FunctionTest(Context context, EditText text, TextView networkTv, TextView wifiTv) {
		this.mContext = context;
		this.text = text;
		this.networkTv = networkTv;
		this.wifiTv = wifiTv;
	}
	
	public void startSocketConnect() {
//		SocketInfo socketInfo=new SocketInfo("192.168.1.1", 8024);
//		SocketInfo socketInfo=new SocketInfo("192.168.1.31", 525);
		SocketInfo socketInfo=new SocketInfo(mContext);
		wifiTv.setText(socketInfo.getWifiName()+"  ,  "+socketInfo.getUrl()+":"+socketInfo.getPort());
		SocketController socketController = new BaseSocketController();
		SocketClient.SocketResponseListener socketResponseListener=new SocketClient.SocketResponseListener() {		
		
			@Override
			public void onConnectHandle(boolean connected) {
				Message message = Message.obtain();
				message.arg1 = TYPE_NETWORKSTATE;
				message.obj = connected ? "长连接保持中" : "长连接断开";
				handler.sendMessage(message);
			}
			
			@Override
			public void onResponseHandle(String content) {
				Message message = Message.obtain();
				message.arg1 = TYPE_RESPONSECONTENT;
				message.obj = content+"  ,  "+(index++);
				handler.sendMessage(message);
			}
			
			@Override
			public void updateWifiInfo(SocketInfo info,boolean isAvailable) {
				Toast.makeText(mContext, "wifi is "+info.getWifiName(), Toast.LENGTH_SHORT).show();
				wifiTv.setText(info.getWifiName()+"  ,   "+info.getUrl() + " : "+info.getPort());
			}
			
		};
		SocketClient client = SocketClient.getInstance();
		client.init(mContext, socketInfo, socketController, socketResponseListener);
		client.startConnect();
	}

	public void closeSocketConnect() {
		SocketClient.getInstance().closeConnect();
		boolean isClosed=SocketClient.getInstance().isClosed();
		if(isClosed) {
			Message message = Message.obtain();
			message.arg1 = TYPE_NETWORKSTATE;
			message.obj = "长连接断开";
			handler.sendMessage(message);
		}
		else{
			Message message = Message.obtain();
			message.arg1 = TYPE_NETWORKSTATE;
			message.obj = "长连接关闭失败";
			handler.sendMessage(message);
		}
		text.setText("");
		wifiTv.setText("");
	}
	
	Handler handler=new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String content = String.valueOf(msg.obj);
			switch(msg.arg1) {
				case TYPE_RESPONSECONTENT:
					text.setText(content);
					break;
				case TYPE_NETWORKSTATE:
					networkTv.setText(content);
					break;
				case TYPE_WIFI:
					wifiTv.setText(content);
					break;
			}
			
		}
	};
	
	
}
