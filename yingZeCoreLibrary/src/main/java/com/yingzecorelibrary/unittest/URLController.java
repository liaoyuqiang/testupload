/**
 * 此类用于管理各种路径，主要用于3g的演示管理
 */
package com.yingzecorelibrary.unittest;

public class URLController {

//	public static String ip = "10.0.0.106";
	public static String ip = "183.62.254.106";
	
	public static String url_catEye = getComplete(""); //猫眼ip
	public static String port_catEye = "554";//猫眼端口
	
	public static String url_shineiji = getComplete("");//室内机ip
	
	public static String url_frontCamera = getComplete("");//前景摄像头ip
	public static String port_frontCamera = "5002";//前景摄像头端口
	
	public static String url_panoramicCamera = getComplete("");//全景摄像头ip
	public static String port_panoramicCamera = "5004";//全景摄像头端口
	
	public static String url_voice_calls = getComplete("");//语音对话ip
	public static String port_voice_calls = "8000";//语音对话端口
	
	public static String url_voice_call_order = getComplete("");//语音对话前发送指令的ip
	public static String port_voice_call_order = "8026";//语音对话前发送指令的ip
	
	private static String getComplete(String str) {
		return ip;
	}
	
	/**
	 * 获取指定的猫眼路径
	 * @param catEyeUrl
	 * @return
	 */
	public static String getCatEyeUrl(String catEyeUrl) {
		return url_catEye!=null ? url_catEye:catEyeUrl;
	}
	
	/**
	 * 获取指定的室内机地址
	 * @param url
	 * @return
	 */
	public static String getShiNeiJiUrl(String url) {
		return url_shineiji!=null ? url_shineiji:url;
	}
	
	/**
	 * 前景摄像头地址
	 * @param url
	 * @return
	 */
	public static String getFrontCameraUrl(String url) {
		return url_frontCamera!=null ? url_frontCamera:url;
	}
	
	/**
	 * 全景摄像头地址
	 * @param url
	 * @return
	 */
	public static String getPanoramicCameraUrl(String url) {
		return url_panoramicCamera!=null ? url_panoramicCamera:url;
	}
	
	/**
	 * 语音对话地址
	 * @param url
	 * @return
	 */
	public static String getVoiceCallsUrl(String url) {
		return url_voice_calls!=null ? url_voice_calls:url;
	}
}
