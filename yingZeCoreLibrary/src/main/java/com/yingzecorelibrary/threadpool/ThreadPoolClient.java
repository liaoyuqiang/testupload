package com.yingzecorelibrary.threadpool;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import android.annotation.SuppressLint;
import android.content.Context;

@SuppressLint("NewApi")
public final class ThreadPoolClient {

	/**
	 * 因为3.0之后添加了SerialExecutor，单任务进行，再多的图片也不会崩溃，
	 * 如果要多任务进行，ok，自己动态加个线程池就可以了，故添加一个单例线
	 * 线程池
	 */
	private static ThreadPoolClient it;
	private final int mCorePoolSize=5; 
	private final int maximumPoolSize=200;
	private final long mKeepAliveTime=10; 
	private final TimeUnit mTimeUnit = TimeUnit.SECONDS;
	private LinkedBlockingQueue<Runnable> mWorkQueue = new LinkedBlockingQueue<Runnable>();
	Hashtable<String, BaseTask> matchingTable = new Hashtable<String, BaseTask>();
	private Executor exec = new ThreadPoolExecutor(
		mCorePoolSize, 
		maximumPoolSize, 
		mKeepAliveTime, 
		mTimeUnit,
		mWorkQueue);	
	
	public static ThreadPoolClient getInstance() {
		if (it == null) {
			it = new ThreadPoolClient();
		}
		return it;
	}
	
	public void download(Context context, String url, BaseTask.TaskActionListener taskListener) {
		String ID=getID(url);
		BaseTask it = findTask(ID);
		if (it == null) {
			BaseTask task = new BaseTask(
				url, 
				taskListener,
				new BaseTask.TaskStateListener() {
					@Override
					public void success(String ID) {
						removeTask(ID);
					}
					@Override
					public void fail(String ID) {
						removeTask(ID);
					}
				});
			addTask(ID, task);// 放入暂存队列中
			task.executeOnExecutor(exec, context);
		} else {
			it.setListener(taskListener);
		}
	}
	
	public void addTask(String ID, BaseTask task) {
		cancelTask(ID);
		matchingTable.put(ID, task);
	}

	public void removeTask(String ID) {
		matchingTable.remove(ID);
	}

	public BaseTask findTask(String ID) {
		return matchingTable.get(ID);
	}

	public void cancelTask(String ID) {
		BaseTask templateTask = findTask(ID);
		if (templateTask != null) {
			templateTask.stopLoading();
			templateTask.cancel(true);
			matchingTable.remove(ID);
		}
	}

	public Executor getExecutor() {
		return exec;
	}

	public String getID(String content) {
		return content;
	}
	
	/**
	 * 释放指定监听器
	 * @param IDArr
	 */
	 public void callbacokRelease(String[] IDArr) {	 
		 Iterator<Entry<String, BaseTask>> it = matchingTable.entrySet().iterator();
		 while(it.hasNext()) {
			 Entry<String, BaseTask> item=it.next();
			 BaseTask task=(BaseTask) item.getValue();
			 for(int i=0;i<IDArr.length;i++) {
				 String ID=task.getID();
				 if(ID.equals(IDArr[i])) {
					 task.setListener(null);
				 }
			 }
		 }
	 }
	
	 
	 
}
