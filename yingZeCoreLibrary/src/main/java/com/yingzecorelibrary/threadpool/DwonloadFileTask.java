//package com.download.task;
//
//import java.io.File;
//import java.io.IOException;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import com.download.Decode;
//import com.download.DownloadTaskManager;
//import com.download.HttpUtil;
//import com.download.MD5Handle;
//import com.download.ZipUtil;
//import com.download.classinterface.CallbackListener;
//import com.download.database.DBOpenHelper;
//import com.nmg.reader.ToolUtil;
//import com.nmg.util.Debug;
//import com.nmg.util.StaticValues;
//
//import android.content.Context;
//import android.os.AsyncTask;
//import android.util.Log;
//
////定义参数类型（doback）、进度（progressupdate）、返回结果（doback）的类型
//public class DwonloadFileTask extends AsyncTask<Context, Integer, String>
//		implements CallbackListener {
//
//	public final static int FINISH = -1;
//	public final static int CANCEL = -2;
//
//	HttpUtil.DwonLoadFile downLoadFile;
//	CallbackListener listener;
//	public String url;// 网络路径
//	public String filepath;// 文件下载保存路径
//	String data;
//	public String fileId;
//	Context context;
//
//	public float currentDownloadSize = 0;
//	public float lastDownloadSize = 0;
//	public float totalSize = 0;
//
//	int state = DownloadState.DOWNLOADBEFORE;
//
//	public static class DownloadState {
//
//		public final static int DOWNLOADBEFORE = 0;
//		public final static int DOWNLOADING = 1;
//		public final static int DOWNLOADFINISH = 2;
//		public final static int DOWNLOADERROR = 3;
//		public final static int DOWNLOADNETWORDERROR = 4;
//
//		public final static int DECODEBEFORE = 10;
//		public final static int DECODEING = 11;
//		public final static int DECODEFINISH = 12;
//		public final static int DECODEERROR = 13;
//
//		public final static int UNZIPBEFORE = 20;
//		public final static int UNZIPING = 21;
//		public final static int UNZIPFINISH = 22;
//		public final static int UNZIPERROR = 23;
//
//	}
//	
//	/**
//	 * @param listener
//	 *            回调接口
//	 * @param filePath
//	 *            下载路径
//	 * @param dirPath
//	 *            sd卡的保存路径
//	 */
//	public DwonloadFileTask(CallbackListener listener, Context context,
//			String fileId, String url, String filepath,String data) {
//		this.listener = listener;
//		this.url = url;
//		this.filepath = filepath;
//		this.fileId = fileId;
//		this.data=data;
//		JSONObject jsonObj = DBOpenHelper.getInstance(context).bookdown_find(
//				fileId, null);
//		/** 添加记录 */
//		if (jsonObj == null) {
//			DBOpenHelper.getInstance(context).bookdown_add(
//					fileId, url, ToolUtil.toLocalpathWithNotZipOrXZip(filepath),
//					data, DBOpenHelper.State.DOWNLOADING);
//		} else {
//			try {
//				int propertyStatus = jsonObj.getInt("status");
//				if (propertyStatus == DBOpenHelper.State.DOWNLOADERROR) {
//					DBOpenHelper.getInstance(context).bookdown_updateStatus(fileId, DBOpenHelper.State.DOWNLOADING);
//				}
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	public void setListener(CallbackListener listener) {
//		this.listener = listener;
//	}
//
//	@Override
//	protected String doInBackground(Context... params) {
//		Log.d("DwonloadFileTask", "filepath ===" + url);
//		context = params[0];
//		String localpath = null;
//		try {
//			/** 准备进行下载状态 */
//			state = DownloadState.DOWNLOADBEFORE;
//			/** 判断是否下载完成，如果已经下载、正在下载则返回，否则往数据库添加下载记录 */
//			JSONObject jsonObj = DBOpenHelper.getInstance(context)
//					.bookdown_find(fileId, null);
//			if (jsonObj != null) {
//				int propertyStatus = jsonObj.getInt("status");
//				switch (propertyStatus) {
//				case DBOpenHelper.State.DOWNLOADERROR:
//					/* 表示文件发生错误，要重新下载 */
//					DBOpenHelper.getInstance(context).bookdown_updateStatus(
//							fileId,
//							DBOpenHelper.State.DOWNLOADING);
//					break;
//				case DBOpenHelper.State.DOWNLOADING:
//					/* 正在下载的不会进入到这里，外面已经拦截了，断网的情况下， 状态仍为doing,可以不对数据库进行操作*/
//					break;
//				case DBOpenHelper.State.DOWNLOADFINISH:
//					/* 对本地文件作检测 */
//					File file = new File(ToolUtil.toLocalpathWithNotZipOrXZip(filepath));
//					if (file.exists()) {
//						/* 表示文件对应存在 */
//						state=DownloadState.UNZIPFINISH;
//						return ""+state;
//					} else {
//						/** 文件不存在，重新下载 */
//						DBOpenHelper.getInstance(context)
//								.bookdown_updateStatus(
//										fileId,
//										DBOpenHelper.State.DOWNLOADING);
//					}
//					break;
//				default:
//					break;
//				}
//			} else {
//				DBOpenHelper.getInstance(context).bookdown_add(
//						fileId, 
//						url, 
//						ToolUtil.toLocalpathWithNotZipOrXZip(filepath),
//						data,
//						DBOpenHelper.State.DOWNLOADING);
//			}
//			/** 开始进行下载状态 */
//			state = DownloadState.DOWNLOADING;
//			downLoadFile = new HttpUtil.DwonLoadFile(this, this);
//			localpath = downLoadFile.dwonLoadFile(context, url, filepath);
//			currentProgress=0;
//			// 根据返回的路径对状态进行判断
//			if (localpath == null || !localpath.equals(filepath)) {
//				if (downLoadFile.state == HttpUtil.State.NETWORKEXCEPTION) {
//					/** 下载网络异常状态 */
//					state = DownloadState.DOWNLOADNETWORDERROR;
//				} else {
//					/** 下载其他异常状态 */
//					state = DownloadState.DOWNLOADERROR;
//				}
//				return "" + state;
//			} else {
//				/** 下载结束状态 */
//				state = DownloadState.DOWNLOADFINISH;
//				/** 准备解密状态 */
//				state = DownloadState.DECODEBEFORE;
//			}
//			/** 开始进行解密状态 */
//			state = DownloadState.DECODEING;
//			/** 更新当前数据库记录状态为解密状态 */
//			DBOpenHelper.getInstance(context).bookdown_updateStatus(
//					fileId, DBOpenHelper.State.DECODEING);
//			String filepath2 = ToolUtil.toLocalpathWithNotZipOrXZip(filepath)
//					+ ".zip";
//			
//			boolean decodeResult = Decode.xor(filepath, filepath2);
//			if (!decodeResult) {
//				/** 解密失败状态 */
//				state = DownloadState.DECODEERROR;
//				return "" + state;
//			} else {
//				/** 解密结束状态 */
//				state = DownloadState.DECODEFINISH;
//				/**更新当前zip保存的路径*/
//				filepath=filepath2;
//			}
//
//			/** 准备解压状态 */
//			state = DownloadState.UNZIPBEFORE;
//			/** 开始进行解压状态 */
//			state = DownloadState.UNZIPING;
//			/** 更新当前数据库记录状态为解压缩状态 */
//			DBOpenHelper.getInstance(context).bookdown_updateStatus(
//					fileId, DBOpenHelper.State.DECOMPRESS);
//			Log.e("解压缩", "解压缩："+filepath);
//			ZipUtil z = new ZipUtil(2048, this);
//			boolean result = z.unZip(this, context, fileId,
//					filepath, ToolUtil.toLocalpathWithNotZipOrXZip(filepath));
//			if (!result) {
//				/** 解压异常状态 */
//				state = DownloadState.UNZIPERROR;
//				return state + "";
//			} else {
//				/** 解压结束状态 */
//				state = DownloadState.UNZIPFINISH;
//			}
//			return state + "";
//
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return "" + state;
//	}
//
//	/**
//	 * Runs on the UI thread after doInBackground. The specified result is the
//	 * value returned by doInBackground. This method won't be invoked if the
//	 * task was cancelled.
//	 */
//	@Override
//	protected void onPostExecute(String result) {
//		super.onPostExecute(result);
//
//		int curState = Integer.parseInt(result);
//		if (curState == DownloadState.DOWNLOADNETWORDERROR) {
//			/** 断网，保留当前下载中的状态，下次进行重新下载 */
//			setState(DBOpenHelper.State.DOWNLOADING);
//			if (listener != null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_DOWNLOAD,networkerrProgress);
//			}
//		} else if (curState == DownloadState.DOWNLOADERROR) {
//			/** 如果因为非网络原因导致，则设置为下载普通错误模式 */
//			setState(DBOpenHelper.State.DOWNLOADERROR);
//		} else if (curState == DownloadState.DECODEERROR) {
//			/** 解密异常模式,删除记录 */
////			setState(DBOpenHelper.State.DOWNLOADERROR);
//			DBOpenHelper.getInstance(context).bookdown_delete(fileId);
//			if(listener!=null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_DECODE,-1);
//			}
//		} else if (curState == DownloadState.UNZIPERROR) {
//			/** 解压缩异常模式,删除记录 */
////			setState(DBOpenHelper.State.DOWNLOADERROR);
//			DBOpenHelper.getInstance(context).bookdown_delete(fileId);
//			if(listener!=null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_UNZIP,-1);
//			}
//		} else if (curState == DownloadState.UNZIPFINISH) {
//			/** 解压完成 */
//			setState(DBOpenHelper.State.DOWNLOADFINISH);
//			if (!isCancelled() && listener != null) {
//				listener.finish();
//			}
//		}
//		cleanListener();
//		DownloadTaskManager.getInstance().removeTask(url);
//	}
//
//	public void cancelNetwork() {
//	}
//
//	@Override
//	protected void onCancelled() {
//		// TODO Auto-generated method stub
//		super.onCancelled();
//
//		int curState = state;
//		if (curState == DownloadState.DOWNLOADNETWORDERROR) {
//			/** 断网，保留当前下载中的状态，下次进行重新下载 */
//			setState(DBOpenHelper.State.DOWNLOADING);
//			if (listener != null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_DOWNLOAD,networkerrProgress);
//			}
//		} else if (curState == DownloadState.DOWNLOADERROR) {
//			/** 如果因为非网络原因导致，则设置为下载普通错误模式 */
//			setState(DBOpenHelper.State.DOWNLOADERROR);
//			if (listener != null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_DOWNLOAD,networkerrProgress);
//			}
//		} else if (curState == DownloadState.DECODEERROR) {
//			/** 解密异常模式,删除记录 */
////			setState(DBOpenHelper.State.DOWNLOADERROR);
//			DBOpenHelper.getInstance(context).bookdown_delete(fileId);
//			if(listener!=null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_DECODE,-1);
//			}
//		} else if (curState == DownloadState.UNZIPERROR) {
//			/** 解压缩异常模式,删除记录 */
////			setState(DBOpenHelper.State.DOWNLOADERROR);
//			DBOpenHelper.getInstance(context).bookdown_delete(fileId);
//			if(listener!=null) {
//				listener.errorCallback(StaticValues.ActionType.ACTION_TYPE_UNZIP,-1);
//			}
//		} else if (curState == DownloadState.UNZIPFINISH) {
//			/** 解压完成 */
//			setState(DBOpenHelper.State.DOWNLOADFINISH);
//			if (!isCancelled() && listener != null) {
//				listener.finish();
//			}
//		}
//
//		cleanListener();
//		DownloadTaskManager.getInstance().removeTask(url);
//		setState(DBOpenHelper.State.DOWNLOADERROR);
////		Log.i("thread", "thread-Cancel:"+url);
//	}
//
//	@Override
//	protected void onProgressUpdate(Integer... values) {
//		int progress = values[0];
//		int type=values[1];
////		Log.e("下载进度", "当前下载进度:"+progress+"%");
//		if (listener != null) {
//			listener.callback(progress,type);
//		}
//		super.onProgressUpdate(values);
//	}
//
//	int currentProgress=0;
//	@Override
//	public void callback(int obj,int type) {
//		// publishProgress(obj);
//		if(obj>currentProgress) {
//			currentProgress=obj;
//			this.publishProgress(obj,type);
//		}
//	}
//
//	public void cleanListener() {
//		listener = null;
//	}
//
//	public void setState(int state) {
//		DBOpenHelper.getInstance(context).bookdown_updateStatus(
//				fileId, state);
//	}
//
//	@Override
//	public void finish() {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void velocityReply(float velocity, float downloadSize,
//			float totalSize) {
//		// TODO Auto-generated method stub
//		this.currentDownloadSize = downloadSize;
//		this.totalSize = totalSize;
//	}
//
//	// 这个方法是让监听者知道当前速度的值
//	public void flushListenerVelocity(float timeGap) {
//		timeGap /= 1000;// 转化为秒计算
//		timeGap = 1;
//		float velocity = (currentDownloadSize - lastDownloadSize) / timeGap;
//		velocity /= 1024;
//		if (listener != null && state==DownloadState.DOWNLOADING)
//			listener.velocityReply(velocity, currentDownloadSize, totalSize);
//		lastDownloadSize = currentDownloadSize;
//		// Log.i("当前时钟显示",
//		// "下载率为:"+velocity+" - "+lastDownloadSize+" - "+currentDownloadSize);
//	}
//
//	@Override
//	public void decodeCallback() {
//		// TODO Auto-generated method stub
//		if (state == DownloadState.DECODEING && listener != null) {
//			listener.decodeCallback();
////			Log.e("解密", "解密："+filepath);
//		}
//	}
//
//	@Override
//	public void unzipCallback() {
//		// TODO Auto-generated method stub
//		if (state == DownloadState.UNZIPING && listener != null) {
//			listener.unzipCallback();
////			Log.e("解压缩", "解压缩："+filepath);
//		}
//	}
//
//	int networkerrProgress = 0;
//
//	@Override
//	public void errorCallback(int type,Object obj) {
//		// TODO Auto-generated method stub
//		if(type==StaticValues.ActionType.ACTION_TYPE_DOWNLOAD) {
//			networkerrProgress = (Integer) obj;
//		}
//	}
//
//}
