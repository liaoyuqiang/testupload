package com.yingzecorelibrary.threadpool;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

@SuppressLint("NewApi")
public class BaseTask extends AsyncTask<Context, Integer, String>{
	private String ID;
	private String mUrl;
	private TaskActionListener mTaskActionListener;
	private TaskStateListener mTaskStateListener;
	
	public BaseTask(
		String url, 
		TaskActionListener taskActionListener,
		TaskStateListener taskStateListener) {
		mUrl = url;
		mTaskActionListener = taskActionListener;
		mTaskStateListener = taskStateListener;
	}
	
	@Override
	protected String doInBackground(Context... params) {
		return null;
	}
	
	@Override
	protected void onCancelled(String result) {
		super.onCancelled(result);
		
	}
	
	public void setListener(TaskActionListener taskListener) {
		mTaskActionListener=taskListener;
	}
	
	public void stopLoading() {
		
	}
	
	public String getUrl() {
		return mUrl;
	}
	
	public String getID() {
		return ID;
	}
	
	public static interface TaskActionListener {
		
	}
	
	public static interface TaskStateListener {
		public void success(String ID);
		public void fail(String ID);
	}
	
}
