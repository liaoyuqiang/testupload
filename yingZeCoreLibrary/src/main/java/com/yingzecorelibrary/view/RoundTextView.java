package com.yingzecorelibrary.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.EditText;

@SuppressLint("NewApi")
public class RoundTextView extends EditText{

	private Paint mPaint;
	
	public RoundTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mPaint = new Paint();
		mPaint.setColor(Color.GRAY);
		mPaint.setDither(true);
		mPaint.setStyle(Style.STROKE);
		mPaint.setAlpha(200);
		mPaint.setDither(true);
		setBackground(null);
	}

	@SuppressLint("DrawAllocation")
	public void onDraw(Canvas canvas) {
		canvas.drawRoundRect(new RectF(2f,2f,(float)(getWidth()-2),(float)(getHeight()-2)), 8, 8, mPaint);
		super.onDraw(canvas);
	}
	
}
