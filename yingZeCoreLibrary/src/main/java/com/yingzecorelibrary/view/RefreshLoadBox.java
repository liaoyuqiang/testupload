//package com.yingzecorelibrary.view;
//
////import com.functest.R;
//import com.yingzecorelibrary.listener.InterceptTouchEventListener;
//import com.yingzecorelibrary.utils.LogUtil;
//import com.yingzecorelibrary.view.ListBox;
//
//import android.content.Context;
//import android.util.AttributeSet;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.TranslateAnimation;
//import android.view.animation.Animation.AnimationListener;
//import android.widget.BaseAdapter;
//import android.widget.FrameLayout;
//
///**
// * 
// * @author LittleBird
// */
//public class RefreshLoadBox extends FrameLayout implements InterceptTouchEventListener{
//	private FrameLayout mLeftSub;
//	private SubLayout mMiddleSub;
//	private FrameLayout mRightSub;
//	private LayoutInflater inflater;
//	private BehaviorStateController mBSController;
//	private AnimationController mAnimationController;
//	private InterceptTouchEventListener mInterceptTouchEventListener;
//	private int interval = 120;
//	private int durationMillis = 600;
//	private float mStatePointX,mStatePointY;
//	private int mEffectiveNumberY;
//	private String type;
//	
//	public RefreshLoadBox(Context context) {
//		super(context);
//		init();
//	}
//	
//	public RefreshLoadBox(Context context, AttributeSet attrs) {
//		super(context, attrs);
//		init();
//	}
//
//	public void init() {
//		mBSController = new BehaviorStateController();
//		mAnimationController = new AnimationController();
//		this.setClickable(true);
//		
//		mLeftSub = new FrameLayout(getContext());
//		mRightSub = new FrameLayout(getContext());
//		mMiddleSub = new SubLayout(getContext());
//		mMiddleSub.setClickable(false);
//		this.addView(mLeftSub);
//		this.addView(mRightSub);
//		this.addView(mMiddleSub);
//	}
//	
//	ListBox listview;
//	public void test() {
////		mLeftSub.setBackgroundColor(0x2fffff00);
////		mRightSub.setBackgroundColor(0x2fff00ff);
////		
////		inflater = LayoutInflater.from(getContext());
////		View page = inflater.inflate(R.layout.layout_main_page, null);
////		page.findViewById(R.id.lv_main_page_crossbar).setVisibility(GONE);
////		listview = (ListBox)page.findViewById(R.id.lv_main_page);
////		listview.setInterceptTouchEventListener(this);
////		BaseAdapter adapter = DataController.getInstance().getAdapter(getContext());
////		listview.setAdapter(adapter);
////		mMiddleSub.setContentView(page);
//	}
//	
//	boolean stateChange = false;
//	@Override
//	public boolean dispatchTouchEvent(MotionEvent event) {
//		if(mBSController.isInAnimationState()) {
//			return false;
//		}
//		if(event.getAction() == MotionEvent.ACTION_DOWN) {
//			mBSController.setNoInterceptTouchEventState();
//			stateChange = false;
//		}
//		if(!mBSController.isInterceptTouchEventState()) {
//			mBSController.setEffectivePoint(event.getX(), event.getY());
//			return listview.dispatchTouchEvent(event);
//		} else {
//			if(!stateChange) {
//				stateChange = true;
//				mStatePointX = event.getX();
//				mStatePointY = event.getY();
//			}
//			return onTouchEvent(event);
//		}
//		
//	}
//	
//	@Override
//	public boolean onTouchEvent(MotionEvent event) {
//		switch(event.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			mBSController.setEffectivePoint(event.getX(), event.getY());
//			break;
//		case MotionEvent.ACTION_MOVE:
//			if(type.contentEquals("refresh")) {
//				if(event.getY()<=mStatePointY) {
//					mEffectiveNumberY+=1;
//					if(mEffectiveNumberY>1) {
//						mBSController.reset();
//						mBSController.setNoInterceptTouchEventState();
//						listview.setVerticalSlideState();// 正常滑动模式
//					}
//				} else {
//					mEffectiveNumberY = 0;
//					mBSController.updateTrack(event.getX(), event.getY());
//				}
//			} else {
//				if(event.getY()>=mStatePointY) {
//					mEffectiveNumberY+=1;
//					if(mEffectiveNumberY>1) {
//						mBSController.reset();
//						mBSController.setNoInterceptTouchEventState();
//						listview.setVerticalSlideState();// 正常滑动模式
//					}
//				} else {
//					mEffectiveNumberY = 0;
//					mBSController.updateTrack(event.getX(), event.getY());
//				}
//			}
//			break;
//		case MotionEvent.ACTION_CANCEL:
//		case MotionEvent.ACTION_UP:
//			LogUtil.set("Refreshload", "action_up").info();
//			mBSController.upHandle();
//			break;
//		}
//		requestLayout();
//		return super.onTouchEvent(event);
//	}
//	
//	@Override
//	public void setInterceptTouchEventHandle(boolean onInterceptTouchEvent, String type) {
//		mEffectiveNumberY = 0;
//		stateChange = false;
//		this.type = type;
//		if(type.contentEquals("intecept_fast_slide_horizontal") && mInterceptTouchEventListener!=null) {
//			mInterceptTouchEventListener.setInterceptTouchEventHandle(onInterceptTouchEvent, type);
//		} else {
//			if(onInterceptTouchEvent) {
//				mBSController.setInterceptTouchEventState();
//			} else {
//				mBSController.setNoInterceptTouchEventState();
//			}
//		}
//	}
//	
//	@Override
//	protected void onLayout(boolean changed, int left, int top, int right,
//			int bottom) {
//		int[] mideleStartEdteArr = mBSController.getMiddleStartEdge();
//		mMiddleSub.layout(0 , - SubLayout.EDGE_WIDTH + mideleStartEdteArr[1], right - left, 
//				bottom - top + SubLayout.EDGE_WIDTH + mideleStartEdteArr[1]);
//		mLeftSub.layout(0, 0, right-left, bottom - top);
//		mRightSub.layout(0, 0, right-left, bottom - top);
//	}
//	
//	public void toBottom() {
//		if(mBSController.isInAnimationState()){
//			return;
//		}
//		mBSController.setInAnimationState();
//		mLeftSub.setVisibility(View.VISIBLE);
//		mRightSub.setVisibility(View.GONE);
//		float timeRatio = Math.abs(((float)getHeight()-interval-
//				mBSController.getMiddleStartEdgeY())/(getHeight()-interval));
//		mAnimationController
//			.setTrack(0, 0, 0, getHeight()-interval-mBSController.getMiddleStartEdgeY())
//			.setDurationMillis((int)(timeRatio * durationMillis))
//			.setContentView(mMiddleSub)
//			.setAnimationListener(new ViewAnimationListener() {
//				@Override
//				public void feedback() {
//					mMiddleSub.clearAnimation();
//					mBSController.setMiddleStartEdgeY(getHeight()-interval);
//					mBSController.setInNoAnimationState();
//					requestLayout();
//				}
//		}).startAnimation();
//	}
//	
//	public void toCenter() {
//		if(mBSController.isInAnimationState()){
//			return;
//		}
//		mBSController.setInAnimationState();
//		float timeRatio = Math.abs(((float)-mBSController.getMiddleStartEdgeY())/(getHeight()-interval));
//		mAnimationController
//			.setTrack(0, 0, 0, -mBSController.getMiddleStartEdgeY())
//			.setDurationMillis((int)(timeRatio * durationMillis))
//			.setContentView(mMiddleSub)
//			.setAnimationListener(new ViewAnimationListener() {
//				@Override
//				public void feedback() {
//					mMiddleSub.clearAnimation();
//					mBSController.setMiddleStartEdgeY(0);
//					mBSController.setInNoAnimationState();
//					requestLayout();
//				}
//		}).startAnimation();
//	}
//
//	public void toTop() {
//		if(mBSController.isInAnimationState()){
//			return;
//		}
//		mBSController.setInAnimationState();
//		mLeftSub.setVisibility(View.GONE);
//		mRightSub.setVisibility(View.VISIBLE);
//		float timeRatio = Math.abs(((float)-getHeight()+interval-
//				mBSController.getMiddleStartEdgeY())/(getHeight()-interval));
//		mAnimationController
//			.setTrack(0, 0, 0, -getHeight()+interval-mBSController.getMiddleStartEdgeY())
//			.setDurationMillis((int)(timeRatio * durationMillis))
//			.setContentView(mMiddleSub)
//			.setAnimationListener(new ViewAnimationListener() {
//				@Override
//				public void feedback() {
//					mMiddleSub.clearAnimation();
//					mBSController.setMiddleStartEdgeY(-getHeight()+interval);
//					mBSController.setInNoAnimationState();
//					requestLayout();
//			}
//		}).startAnimation();
//	}
//	
//	public void setInterceptTouchEventListener(InterceptTouchEventListener interceptTouchEventListener) {
//		mInterceptTouchEventListener = interceptTouchEventListener;
//	}
//	
//	public InterceptTouchEventListener getInterceptTouchEventListener() {
//		return mInterceptTouchEventListener;
//	}
//	
//	/**
//	 * 行为状态控制
//	 * @author dell
//	 */
//	class BehaviorStateController {
//		private boolean isInterceptTouchEventState = false;
//		private boolean isInAnimationState = false;
//		private float effectivePoint[];
//		private int mMiddleStartEdge[];
//
//		private BehaviorStateController() {
//			effectivePoint = new float[2];
//			mMiddleStartEdge = new int[2];
//		}
//		
//		public void setInterceptTouchEventState() {
//			isInterceptTouchEventState = true;
//		}
//		
//		public void setNoInterceptTouchEventState() {
//			isInterceptTouchEventState = false;
//		}
//		
//		public boolean isInterceptTouchEventState() {
//			return isInterceptTouchEventState;
//		}
//		
//		public void setInAnimationState() {
//			isInAnimationState = true;
//		}
//		
//		public void setInNoAnimationState() {
//			isInAnimationState = false;
//		}
//		
//		public boolean isInAnimationState() {
//			return isInAnimationState;
//		}
//		
//		public float[] getEffectivePoint() {
//			return effectivePoint;
//		}
//
//		public void setEffectivePoint(float effectivePointX, float effectivePointY) {
//			effectivePoint[0] = effectivePointX;
//			effectivePoint[1] = effectivePointY;
//		}
//		
//		public void updateTrack(float x, float y) {
//			int xGap = (int)(x - effectivePoint[0]);
//			int yGap = (int)(y - effectivePoint[1]);
//			if(xGap != 0) {
//				if(-getWidth()+interval<mMiddleStartEdge[0]+xGap &&
//					mMiddleStartEdge[0]+xGap<getWidth()-interval){
//					mMiddleStartEdge[0] += xGap;
//					requestLayout();
//				}
//				effectivePoint[0] = x;
//			}
//			if(yGap != 0) {
//				if(-getHeight()+interval<mMiddleStartEdge[1]+yGap &&
//					mMiddleStartEdge[1]+yGap<getHeight()-interval){
//					mMiddleStartEdge[1] += yGap;
//					requestLayout();
//				}
//				effectivePoint[1] = y;
//			}
//		}
//
//		public int[] getMiddleStartEdge() {
//			return mMiddleStartEdge;
//		}
//
//		public void setMiddleStartEdge(int[] mMiddleStartEdge) {
//			this.mMiddleStartEdge = mMiddleStartEdge;
//		}
//		
//		public int getMiddleStartEdgeX() {
//			return mMiddleStartEdge[0];
//		}
//		
//		public void setMiddleStartEdgeX(int middleStartEdgeX) {
//			mMiddleStartEdge[0] = middleStartEdgeX;
//		}
//		
//		public int getMiddleStartEdgeY() {
//			return mMiddleStartEdge[1]; 
//		}
//		
//		public void setMiddleStartEdgeY(int middleStartEdgeY) {
//			mMiddleStartEdge[1] = middleStartEdgeY;
//		}
//		
//		public void upHandle() {
//			int height = getHeight();
//			if(mMiddleStartEdge[1] > 0) {
//				if(mMiddleStartEdge[1] > height/2) {
//					toBottom();
//				} else {
//					toCenter();
//				}
//			} else if(mMiddleStartEdge[1] < 0) {
//				if(mMiddleStartEdge[1] > -height/2) {
//					toCenter();
//				} else {
//					toTop();
//				}
//			} else {
//				mMiddleStartEdge[0] = 0;
//				mMiddleStartEdge[1] = 0;
//			}
//			requestLayout();
//		}
//
//		public void reset() {
//			mMiddleStartEdge[0] = 0;
//			mMiddleStartEdge[1] = 0;
//		}
//		
//	}
//	
//	/**
//	 * 动画控制类
//	 * @author dell
//	 */
//	class AnimationController {
//		private int durationMillis = 600;
//		private float fromXDelta;
//		private float toXDelta;
//		private float fromYDelta;
//		private float toYDelta;
//		private View contentView;
//		private ViewAnimationListener viewAnimationListener;
//		
//		public AnimationController setTrack(float fromXDelta, float toXDelta,
//				float fromYDelta, float toYDelta) {
//			this.fromXDelta = fromXDelta;
//			this.toXDelta = toXDelta;
//			this.fromYDelta = fromYDelta;
//			this.toYDelta = toYDelta;
//			return this;
//		}
//		
//		public AnimationController setAnimationListener(ViewAnimationListener viewAnimationListener) {
//			this.viewAnimationListener = viewAnimationListener;
//			return this;
//		}
//		
//		public AnimationController setContentView(View contentView) {
//			this.contentView = contentView;
//			return this;
//		}
//		
//		public AnimationController setDurationMillis(int durationMillis) {
//			this.durationMillis = durationMillis;
//			return this;
//		}
//		
//		public void startAnimation() {
//			TranslateAnimation animation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta, toYDelta);
//			animation.setDuration(durationMillis);
//			animation.setAnimationListener(new AnimationListener() {
//				@Override
//				public void onAnimationEnd(Animation animation) {
//					if(viewAnimationListener != null) {
//						viewAnimationListener.feedback();
//					}
//				}
//				@Override
//				public void onAnimationRepeat(Animation animation) {}
//				@Override
//				public void onAnimationStart(Animation animation) {}
//			});
//			contentView.startAnimation(animation);
//		}
//		
//
//	}
//	
//	interface ViewAnimationListener {
//		public void feedback();
//	}
//	
//	/**
//	 * 主布局
//	 * @author littlebird
//	 */
//	class SubLayout extends FrameLayout{
//		private View leftEdgeView;
//		private View rightEdgeView;
//		private View contentView;
//		private final static int EDGE_WIDTH = 100;
//		
//		public SubLayout(Context context) {
//			super(context);
//			leftEdgeView = new View(context);
//			rightEdgeView = new View(context);
////			leftEdgeView.setBackgroundResource(R.drawable.shade_gradient_top);
////			rightEdgeView.setBackgroundResource(R.drawable.shade_gradient_bottom);
//			addView(leftEdgeView);
//			addView(rightEdgeView);
//		}
//		
//		public void setContentView(View view) {
//			contentView = view;
//			this.addView(contentView);
//		}
//		
//		@Override
//		protected void onLayout(boolean changed, int left, int top, int right,
//				int bottom) {
//			leftEdgeView.layout(0, 0, right-left, EDGE_WIDTH);
//			rightEdgeView.layout(0, bottom-top-EDGE_WIDTH, right-left, bottom-top);
//			if(contentView!=null){
//				contentView.layout(0,EDGE_WIDTH, right-left, bottom-top-EDGE_WIDTH);
//			}
//		}
//	}
//	
//	public void setCriticalCoordinate(int type, float[] coordinates, Object tag){
//		
//	}
//}