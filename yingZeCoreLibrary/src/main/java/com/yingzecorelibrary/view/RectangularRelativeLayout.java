/**
 * 等比例缩放图片控件
 */
package com.yingzecorelibrary.view;

import com.yingzecorelibrary.R;
import com.yingzecorelibrary.utils.LogUtil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class RectangularRelativeLayout extends RelativeLayout {
	public float scale = 1.0f;
	
	public RectangularRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.retangular_layout);
		scale=ta.getFloat(R.styleable.retangular_layout_measureScale, 1);
		LogUtil.set("^^^^^^^^^", "^^^^^^^^^^^^^^^  : "+scale).verbose();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);  
		
		int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int)(sizeWidth*scale), MeasureSpec.EXACTLY));
	}
	
}
