package com.yingzecorelibrary.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;


/**
 * 等比例缩放控件
 * @author Administrator
 *
 */
public class RectangularBox extends View{

	public float scale = 1.0f;
	
	public RectangularBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);  
		int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (sizeWidth * scale), modeHeight));
	}
	
}
