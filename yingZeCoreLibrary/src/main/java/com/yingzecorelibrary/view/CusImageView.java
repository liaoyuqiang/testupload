package com.yingzecorelibrary.view;

import com.yingzecorelibrary.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CusImageView extends ImageView{
	public float scale = 1.0f;
	
	public CusImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray  typedArray = context.obtainStyledAttributes(attrs, R.styleable.retangular_layout);
		scale = typedArray.getFloat(R.styleable.retangular_layout_measureScale, 1.0f);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);  
		int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (sizeWidth * scale), modeHeight));
	}
	
}
