package com.yingzecorelibrary.view;

import com.yingzecorelibrary.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class RatioButton extends LinearLayout {
	
	public static int widthMeasureSpec = -1;
	
	/**
	 * 圆角矩形背景色
	 */
	private int background;
	/**
	 * 圆角矩形默认背景色
	 */
	private int backgroundDefault;
	/**
	 * 圆角矩形按下背景色
	 */
	private int backgroundClick;
	
	public RatioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setWillNotDraw(false);//设置为false， ViewGroup强制调用onDraw（）方法
//		String backgroundStr = attrs.getAttributeValue("http://schemas.android.com/apk/res/android","background");
		String backgroundStr = (String) getTag();
		backgroundDefault = Color.parseColor(backgroundStr);
		backgroundClick = getResources().getColor(R.color.app_main_color);
		background = backgroundDefault;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
//			setBackgroundColor(getResources().getColor(R.color.app_main_color));
			background = backgroundClick;
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
//			setBackgroundColor(Color.parseColor(background));
			background = backgroundDefault;
			invalidate();
			break;
		case MotionEvent.ACTION_CANCEL:
//			setBackgroundColor(Color.parseColor(background));
			background = backgroundDefault;
			invalidate();
			break;
		}
		return super.dispatchTouchEvent(event);
	}
	
	@Override
	public void setOnClickListener(OnClickListener l) {
		super.setOnClickListener(l);
	}
	
//	@Override
//	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {//修改高度为宽度的1.5倍
//		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);  
//		int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
//        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) (sizeWidth*1.34), modeHeight));
//	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint p = new Paint();
		p.setStyle(Paint.Style.FILL);//充满  
		p.setColor(background);
		p.setAntiAlias(true);// 设置画笔的锯齿效果  
		RectF rectF = new RectF(0, 0, getMeasuredWidth(), getMeasuredHeight());
		int angle = getResources().getDimensionPixelOffset(R.dimen.app_default_right_margin);
		canvas.drawRoundRect(rectF, angle, angle, p);
		
//		p.setStyle(Paint.Style.FILL);//充满  
//        p.setColor(Color.BLUE);  
//        p.setAntiAlias(true);// 设置画笔的锯齿效果  
//        RectF oval3 = new RectF(0, 0, getMeasuredWidth(), getMeasuredHeight());// 设置个新的长方形  
////        int angle = getResources().getDimensionPixelOffset(R.dimen.default_margin);
//		canvas.drawRoundRect(oval3, 30, 30, p);//第二个参数是x半径，第三个参数是y半径
	}
	
//	@Override
//	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//		System.out.println("widthMeasureSpec-->"+widthMeasureSpec);
//		System.out.println("heightMeasureSpec-->"+heightMeasureSpec);
//		
//		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);  
//        int sizeHeight = MeasureSpec.getSize(heightMeasureSpec);  
//        int modeWidth = MeasureSpec.getMode(widthMeasureSpec);  
//        int modeHeight = MeasureSpec.getMode(heightMeasureSpec); 
//        
//        System.out.println("sizeWidth-->"+sizeWidth);
//        System.out.println("sizeHeight-->"+sizeHeight);
//        System.out.println("modeWidth-->"+modeWidth);
//        System.out.println("modeHeight-->"+modeHeight);
//        
//        
//        setMeasuredDimension(sizeWidth, (int) (sizeWidth*1.5));
//	}
	

}
