//package com.yingzecorelibrary.view;
//
//import android.annotation.SuppressLint;
//import android.annotation.TargetApi;
//import android.content.Context;
//import android.os.Build;
//import android.util.AttributeSet;
//import android.util.StateSet;
//import android.view.MotionEvent;
//import android.view.ViewConfiguration;
//import android.widget.ListView;
//import com.yingzecorelibrary.listener.InterceptTouchEventListener;
//import com.yingzecorelibrary.utils.LogUtil;
//
//@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
//@SuppressLint("ClickableViewAccessibility")
//public class ListBox extends ListView{
//	private long downtime;
//	private ViewConfiguration config;
//	private InterceptTouchEventListener mInterceptTouchEventListener;
//	private BehaviorStateController mBSController;
//	private float mTouchY;
//	private int mTopEffectiveNumber = 0;// 顶部滑动次数
//	private int mBottomEffectiveNumber = 0;// 底部滑动次数
//	private final int FLAG_EFFECTIVE_LIMIT = 1;// 滑动临界值
//	
//	// 恢复测试
//	
//	public ListBox(Context context, AttributeSet attrs) {
//		super(context, attrs);
//		config = ViewConfiguration.get(context);
//		mBSController = new BehaviorStateController();
//	}
//	
//	public InterceptTouchEventListener getInterceptTouchEventListener() {
//		return mInterceptTouchEventListener;
//	}
//
//	public void setInterceptTouchEventListener(
//			InterceptTouchEventListener mInterceptTouchEventListener) {
//		this.mInterceptTouchEventListener = mInterceptTouchEventListener;
//	}
//
//	@Override
//	public boolean dispatchTouchEvent(MotionEvent event) {	
//		if(event.getAction() == MotionEvent.ACTION_DOWN) {
//			downtime = System.currentTimeMillis();
//			mBSController.setGetFoucusState();
//			mBSController.setEffectivePoint(event.getX(), event.getRawY());
//		} else {
//			long currentTime = System.currentTimeMillis();
//			if(!mBSController.isOverTime(currentTime)) {
//				if(mBSController.isInGetFocusState()) {
//					mBSController.updateTrackY(event.getRawY(), currentTime);
//				}
//				if(mBSController.isInGetFocusState()) {
//					mBSController.updateTrackX(event.getX(), currentTime);
//				}
//				if(mBSController.isInHorizontalSlideState() &&
//						mInterceptTouchEventListener != null) {
//					mInterceptTouchEventListener.setInterceptTouchEventHandle(
//							true, "intecept_fast_slide_horizontal");
//					mBSController.setLostFocusState();
//					cancelHighLight();
//					return false;
//				}
//				if(mBSController.isInVerticalSlideState()) {
//					cancelHighLight();
//				}
//			} 
//		}
//		return super.dispatchTouchEvent(event);
//	}
//	
//	@Override
//	public boolean onTouchEvent(MotionEvent event) {
//		switch(event.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			mTouchY = event.getRawY();
//			break;
//		case MotionEvent.ACTION_MOVE:
//			LogUtil.set("listbox-ontouch", ""+mTopEffectiveNumber+","+
//					getFirstVisiblePosition()+","+getChildAt(0).getTop()).warm();
//			/* 如果当前操作滑动到顶部，则执行<数据刷新状态>的判断 */
//            if (getFirstVisiblePosition() == 0 && getChildAt(0).getTop()>=0) {
//				if(event.getRawY()>mTouchY) {
//					mTopEffectiveNumber+=1;
//				} else {
//					mTopEffectiveNumber = 0;
//				}
//				if(mTopEffectiveNumber>FLAG_EFFECTIVE_LIMIT) {
//					LogUtil.set("listbox-ontouch", "////////listbox-ontouch : "+mTopEffectiveNumber).warm();
//					mBSController.setDataRefreshState();
//					cancelHighLight();
//					mInterceptTouchEventListener.setCriticalCoordinate(
//							1, new float[]{event.getX(), event.getRawY()}, null);
//					mInterceptTouchEventListener.setInterceptTouchEventHandle(true, "refresh");
//				}
//            } else if (getLastVisiblePosition() == (getCount() - 1) &&  
//            		getChildAt(getChildCount()-1).getBottom()<=getHeight()) {// 判断滚动到底部
//				if(event.getRawY()<mTouchY) {
//					mBottomEffectiveNumber+=1;
//				} else {
//					mBottomEffectiveNumber = 0;
//				}
//				if(mBottomEffectiveNumber > FLAG_EFFECTIVE_LIMIT) {
//					mInterceptTouchEventListener.setInterceptTouchEventHandle(true, "load");
//					mInterceptTouchEventListener.setCriticalCoordinate(1, 
//							new float[]{event.getX(), event.getRawY()}, null);
//					mBSController.setDataLoadState();
//					cancelHighLight();
//				}
//            }
//            mTouchY = event.getRawY();
//			break;
//		}
//		return super.onTouchEvent(event);
//	}
//	
//	/**
//	 * 设置当前控件为正常的滑动状态
//	 */
//	public void setVerticalSlideState() {
//		mTopEffectiveNumber = 0;
//		mBSController.setVerticalSlideState();
//	}
//	
//	public void cancelHighLight() {
//		this.cancelLongPress();
//		this.setPressed(false);
//		getSelector().setState(StateSet.NOTHING);
//	}
//	
//	class BehaviorStateController {
//		private final int STATE_FOCUS_GET = 0;// 获得焦点
//		private final int STATE_FOCUS_LOST = 1;// 失去焦点
//		private final int STATE_SLIDE_HORIZONTAL = 2;// 左右滑动
//		private final int STATE_SLIDE_VERTICAL = 3;// 上下滑动
//		private final int STATE_DATA_REFRESH = 4;// 滑动刷新
//		private final int STATE_DATA_LOAD = 5;// 滑动加载
//		private int state = STATE_FOCUS_LOST;
//		private float effectivePoint[];// 临界坐标
//		
//		private BehaviorStateController() {
//			effectivePoint = new float[2];
//		}
//		
//		public void setGetFoucusState() {
//			state = STATE_FOCUS_GET;
//		}
//		public void setLostFocusState() {
//			state = STATE_FOCUS_LOST;
//		}
//		public void setHorizontalSlideState() {
//			state = STATE_SLIDE_HORIZONTAL;
//		}
//		public void setVerticalSlideState() {
//			state = STATE_SLIDE_VERTICAL;
//		}
//		public void setDataRefreshState() {
//			state = STATE_DATA_REFRESH;
//		}
//		public void setDataLoadState() {
//			state = STATE_DATA_LOAD;
//		}
//		public boolean isInGetFocusState() {
//			return state == STATE_FOCUS_GET;
//		}
//		public boolean isInLostFocusState() {
//			return state == STATE_FOCUS_LOST;
//		}
//		public boolean isInHorizontalSlideState() {
//			return state == STATE_SLIDE_HORIZONTAL;
//		}
//		public boolean isInVerticalSlideState() {
//			return state == STATE_SLIDE_VERTICAL;
//		}
//		public boolean isInDataRefreshState() {
//			return state == STATE_DATA_REFRESH;
//		}
//		public boolean isInDataLoadState() {
//			return state == STATE_DATA_LOAD;
//		}
//		
//		public boolean isOverTime(long currentTime) {
//			return currentTime-downtime >= ViewConfiguration.getGlobalActionKeyTimeout();
//		}
//		
//		public void setEffectivePoint(float effectivePointX, float effectivePointY) {
//			effectivePoint[0] = effectivePointX;
//			effectivePoint[1] = effectivePointY;
//		}
//		
//		public void updateTrackX(float x, long currentTime) {
//			if(Math.abs(x - effectivePoint[0])>config.getScaledTouchSlop() &&
//					currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
//				setHorizontalSlideState();
//			}
//		}
//		
//		public void updateTrackY(float y, long currentTime) {
//			if(Math.abs(y - effectivePoint[1])>config.getScaledTouchSlop() &&
//					currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
//				setVerticalSlideState();
//			}
//		}
//	}
//}
