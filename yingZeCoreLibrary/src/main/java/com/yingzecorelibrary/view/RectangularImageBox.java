package com.yingzecorelibrary.view;

import android.content.Context;
import android.util.AttributeSet;

public class RectangularImageBox extends RectangularBox{
	
	public RectangularImageBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		scale=0.5f;
	}
	
}
