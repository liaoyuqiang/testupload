package com.yingzecorelibrary.view;

import com.yingzecorelibrary.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class RotateableView extends ImageView{
	Handler mHandler = new Handler();
    /** 
     * 命名区域 
     */  
    private final String namespace = "http://com.bill.cn";   
    /** 
     * 保存创建旋转角度 
     */  
    private float mRotateDegrees;   
    /** 
     * 保存创建背景图片的ID 
     */  
    private int mBackGroudDrawableId;  
    
    private Bitmap bmp;
    
    /** 
     * 利用图片ID加载图片 
     */  
    private Drawable mBackGroudDrawable;  
    public RotateableView(Context context, AttributeSet attrs) {  
        super(context, attrs);  
        mBackGroudDrawableId=attrs.getAttributeResourceValue(namespace, "background", R.drawable.ic_loading);  
        mBackGroudDrawable = context.getResources().getDrawable(mBackGroudDrawableId);        
        mRotateDegrees=attrs.getAttributeFloatValue(namespace, "rotateDegrees",0.0f);  
        this.setWillNotDraw(false);
    } 
    
    @Override  
    public void draw(Canvas canvas) {
    	Log.e("draw", "++++++++++++++++ : "+getWidth()+","+getHeight());
        canvas.rotate(mRotateDegrees, getWidth()/2, getHeight()/2);
        super.draw(canvas);
//        mBackGroudDrawable.setBounds(0, 0, getWidth(), getHeight());  
//        mBackGroudDrawable.draw(canvas);  
    }  
    
//    @Override  
//    protected void onDraw(Canvas canvas) {  
//        super.onDraw(canvas);  
//        /** 
//         * 旋转画布 
//         */  
//        if (mRotateDegrees==90.0f) {  
//            canvas.rotate(mRotateDegrees, 0, 0);  
//            canvas.translate(0, -mBackGroundHeight);  
//        } else {  
//            canvas.rotate(mRotateDegrees, mBackGroundWidth/2, mBackGroundHeight/2);  
//        }  
//        /** 
//         * 执行draw 
//         */  
//        mBackGroudDrawable.setBounds(0, 0, mBackGroundWidth, mBackGroundHeight);  
//        mBackGroudDrawable.draw(canvas);  
//    }  
    
//    @Override  
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {  
//        // TODO Auto-generated method stub  
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);  
//        /** 
//         * 设定View显示区域 
//         */  
//        mBackGroundHeight=mBackGroudDrawable.getMinimumHeight();  
//        mBackGroundWidth=mBackGroudDrawable.getMinimumWidth();  
//        if (mRotateDegrees==90.0f) {  
//            setMeasuredDimension(mBackGroundHeight, mBackGroundWidth);  
//        } else {  
//            setMeasuredDimension(mBackGroundWidth, mBackGroundHeight);  
//        }         
//    }  
    
    public void stopAnimation() {
    	mHandler.removeCallbacksAndMessages(null);
    }
    
    public void startAnimation() {
    	mHandler.post(new AnimationRun());
    }
    
    class AnimationRun implements Runnable {
		@Override
		public void run() {
			mRotateDegrees += 30;
			postInvalidate();
			mHandler.postDelayed(this, 100);
		}
    }
}
