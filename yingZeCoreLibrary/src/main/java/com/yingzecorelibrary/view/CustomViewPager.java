package com.yingzecorelibrary.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 定制viewpage
 * 1、可以设置是否启动左右滑动
 * @author owm
 *
 */
public class CustomViewPager extends ViewPager{
	
	private boolean CanScroll = true;

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomViewPager(Context context) {
		super(context);
	}

	public void setCanScroll(boolean canScroll) {
		CanScroll = canScroll;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (ev.getAction() == MotionEvent.ACTION_DOWN ||
				ev.getAction() == MotionEvent.ACTION_MOVE) {
			this.getParent().requestDisallowInterceptTouchEvent(true);
		} else if (ev.getAction() == MotionEvent.ACTION_CANCEL ||
				ev.getAction() == MotionEvent.ACTION_UP) {
			this.getParent().requestDisallowInterceptTouchEvent(false);
		}
		return super.dispatchTouchEvent(ev);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (CanScroll) {//通过消除事件禁止滑动
			return super.onTouchEvent(ev);
		}
		return false;
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (CanScroll) {//通过消除事件禁止滑动
			return super.onInterceptTouchEvent(ev);
		}
		return false;
	}
	

}
