/**
 * 日志辅助类
 */
package com.yingzecorelibrary.utils;

import android.util.Log;

public class LogUtil {

	public static LogUtil it;
	private String mTag;
	private String mContent;
	
	public synchronized static LogUtil getInstance() {
		if(it == null) {
			it = new LogUtil();
		}
		return it;
	}
	
	public static LogUtil set(String tag, String content) {
		LogUtil it = getInstance();
		it.mTag = tag;
		it.mContent = content;
		return it;
	}
	
	public void warm() {
		Log.w(mTag, mContent);
	}
	
	public void info() {
		Log.i(mTag, mContent);
	}
	
	public void debugInfo() {
		Log.d(mTag, mContent);
	}
	
	public void error() {
		Log.e(mTag, mContent);
	}
	
	public void verbose() {
		Log.v(mTag, mContent);
	}
}
