package com.yingzecorelibrary.utils;

import com.yingzecorelibrary.R;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;


public class OSdetailsUtils {
	public static boolean isSDKHigherKITKAT(){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
			return true;
		}
		return false;
	}
	public void setSystemBar(Activity activity){
		if(isSDKHigherKITKAT()){
			setTranslucentStatus(true,activity);
		}
		SystemBarTintManager tintManager = new SystemBarTintManager(activity);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintResource(R.color.statusbar_bg);
	}
	@TargetApi(19)
	private void setTranslucentStatus(boolean on , Activity activity) {
		Window win = activity.getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
		if (on) {
			winParams.flags |= bits;
		} else {
			winParams.flags &= ~bits;
		}
		win.setAttributes(winParams);
	}
}
