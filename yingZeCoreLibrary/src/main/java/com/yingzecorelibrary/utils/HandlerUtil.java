package com.yingzecorelibrary.utils;

import android.os.Handler;
import android.os.Message;

public class HandlerUtil {

	private Handler mHandler;
	
	public static HandlerUtil getInstance(Handler handler) {
		return new HandlerUtil(handler);
	}
	
	private HandlerUtil(Handler handler) {
		this.mHandler=handler;
	}
	
	public MessageController getMessageController() {
		return new MessageController();
	}
	
	public void cleanMessage() {
		mHandler.removeCallbacksAndMessages(null);
	}
	
	public class MessageController {
		
		private boolean mFliter = true;
		private long mTime = 0;
		private int mType;
		
		public MessageController setFliter(boolean fliter) {
			this.mFliter=fliter;
			return this;
		}
		
		public MessageController setType(int type) {
			this.mType=type;
			return this;
		}
		
		public MessageController setDelayTime(long time) {
			this.mTime=time;
			return this;
		}
		
		public void send() {
			if(mFliter) {
				Message message = Message.obtain();
				message.arg1 = mType;
				if(mTime>0) {
					mHandler.sendMessageDelayed(message, mTime);
				}
				else{
					mHandler.sendMessage(message);
				}
			}
		}
	}
	
}
