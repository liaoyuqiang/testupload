package com.yingzecorelibrary.utils;

import android.content.Context;
import android.widget.Toast;

public final class ToastUtil {

	public static ToastUtil it;
	private Context mContext;
	private String mContent;
	private int duration = Toast.LENGTH_SHORT;
	
	public synchronized static ToastUtil getInstance() {
		if(it == null) {
			it = new ToastUtil();
		}
		return it;
	}
	
	public static ToastUtil set(Context context,String content) {
		ToastUtil it = getInstance();
		it.mContext = context;
		it.mContent = content;
		return it;
	}
	
	public void show() {
		Toast.makeText(mContext, mContent, duration).show();
	}
	
//	http://blog.csdn.net/chanccie/article/details/8314951
	
}
