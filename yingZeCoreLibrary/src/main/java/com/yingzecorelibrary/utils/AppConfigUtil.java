/**
 * app配置文件
 * 设计思路：将属性写入到json里面，然后通过读取json的数据，达到动态配置属性的效果
 */

package com.yingzecorelibrary.utils;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;

public class AppConfigUtil {

	public static int titleBackgroundColor = 0xffffffff;
	public static int titleTextColor = 0xff000000;
	public static int titleTextSize = 16;
	
	//todo...
	// json creator is develop
	public static String getPropertyJson() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("title_background_color", "0xffffffff");
		obj.put("title_text_color", "#ff000000");
		obj.put("title_text_size", "24");
		String content = obj.toString();
		LogUtil.set("config", content).verbose();
		return content;
	}
	
	// and json parser will be develop
	public static void parsePropertyContent(String content) throws JSONException {
		JSONObject obj = new JSONObject(content);
		titleBackgroundColor = getColor(obj, "title_background_color", "#ffffffff");
		titleTextColor = getColor(obj, "title_text_color", "#ff000000");
		titleTextSize = obj.optInt("title_text_size", 16);
	}
	
	public static void init() {
		try {
			parsePropertyContent(getPropertyJson());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取颜色
	 * @param obj
	 * @param name
	 * @param defaultValue
	 * @return
	 */
	public static int getColor(JSONObject obj, String name, String defaultValue) {
		String color = obj.optString(name, defaultValue);
		return Color.parseColor(colorFromat(color));
	}
	
	public static String colorFromat(String color){
		if(color.startsWith("0x")) {
			color = color.replace("0x", "#");
		}
		return color;
	}
	
}
