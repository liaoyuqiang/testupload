package com.yingzecorelibrary.utils;

public class FieldUtil {
	
	public final static String SETTING_FILE_NAME = "setting";
	public final static String LOCAL_PORT = "LOCALPORT";
	public final static String REMOTEIP= "REMOTEIP";
	
	
	public static String routerIP = "192.168.1.1"; //路由器IP，即手机端的网关  首次启动和wifi改变时会重新赋值
	public static final int routerPort = 8028;  //路由器命令发送端口
	
	public static String localIP = ""; // 手机端wifi IP地址    首次启动和wifi改变时会重新赋值
	public static int localPort;  // 手机端语音通话接受端口 ip第4位数*2+6000   首次启动和wifi改变时会重新赋值认证wifi连接正确时复制
	
	public static String outMachineIP = ""; //室外机IP地址， 首次启动和wifi改变时会重新赋值
	public static final int outMachinePort = 8000; // 室外机  语音发送端口
	
	public static String visitOutMachineIP = ""; // 来访室外机IP地址，在监听socket成功连接后去发送第二个命令获取
	public static String deviceNum = ""; //设备号
	
	public static String room = ""; //房间号
	
	
	public static boolean isConnect4Wifi = false;
	
	//可视对讲的ip和端口
	public static final String ACCESS_IP="access_ip";
	public static final String ACCESS_PORT="access_port";
	
	//手机端发送开门指令
	public static final String openDoorMsg = "1,router please open the door.";
	public static final String openDoorMsg_resultSuccess ="0,wait to open the door.";
	
	public static final String getCallingIP = "2,check the status.";
	public static final String callingIP_result = "0,192.168.10.141";
	
	//手机端发送语音通话IP映射指令
	public static final String mapIP = "4,client ip:";
	public static final String mapIP_resultSuccess ="0,port:";
	
	//手机端发送获取室外机指令
	public static final String getOutMachineIP = "5,get out machine ip.";
	public static final String getOutMachineIP_resultSuccess = "0,ip:";
	
	//手机端通话中，手机端主动挂断，通知室外机挂断指令
	public static final String phoneDisconnect = "6,phone disconnect.";
	
	//手机端获取所在房间号指令
	public static final String getRoomOrder = "7,get the room num.";
	
	//手机语音通话结束指令
	public static final String  voiceDisconnect= "8,router voice disconnect.";
	public static final String  voiceDisconnect_resultSuccess= "0,voice disconnect OK.";
	
	//开启室内机铃声
	public static final String OPEN_RING = "9,set ring on.";
	//关闭室内机铃声
	public static final String CLOSE_RING = "9,set ring off.";
	//设置铃声关闭或开启成功
	public static final String CHANGE_RING_SUCCESS = "0,set OK.";
	//获取室内机铃声状态
	public static final String RING_STATE = "10,get ring status.";
	public static final String RING_STATE_ON = "0,on.";
	public static final String RING_STATE_OFF = "0,off.";
	
	//设置布防开关或关闭
	public static final String SET_PROTECTIOIN = "11,set guard on";
	public static final String CANCEL_PROTECTIOIN = "11,set guard off";
	//获取布防状态
	public static final String GET_SECURITY="12,get guard status.";
	public static final String SECURITY_ON="0,on.";
	public static final String SECURITY_OFF="0,off.";
	
	//布防结束指令
	public static final String SECURITY__resultSuccess="0,set OK.";
	
	public static String toStrings(){
		return "网关：" + routerIP + "\n" + 
				"本机IP：" + localIP + "\n" +
				"本机语音接受端口：" + localPort + "\n" +
				"室外机IP：" + outMachineIP + "\n" + 
				"isConnect4Wifi：" + isConnect4Wifi + "  room=" + room;
	}
	
	
	public static final int SCAN_TYPE_WIFI = 0x1;
	public static final int SCAN_TYPE_BORROW_BIKE = 0x2;
	public static final int SCAN_TYPE_REPAIR = 0x3;
	
	
	//水煤电  begin
	public static final int WATER = 0x1;
	public static final int COMBUSTION = 0x2; 
	public static final int ELECTRICITY = 0x3; 
	
	public static final String downloadFolder = "/SmartHomeHelper/image";
	
	public static final String WATER_FOLDER = "/waterRecord/";
	public static final String COMBUSTION_FOLDER = "/gasRecord/";
	public static final String ELECTRICITY_FOLDER = "/electricityRecord/";
	
	public static int pageSize = 5; // listView上拉加载一页有多少条记录，该参数为空，服务器默认为12条
	
	//水煤电  end
	
	//报修平台  begin
	
	//报修平台  end
	
}
