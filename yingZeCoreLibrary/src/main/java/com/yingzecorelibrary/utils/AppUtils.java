/**
 * 跟App相关的辅助类
 */
package com.yingzecorelibrary.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;

@SuppressLint("NewApi")
public class AppUtils
{

	private AppUtils()
	{
		/* cannot be instantiated */
		throw new UnsupportedOperationException("cannot be instantiated");

	}

	/**
	 * 获取应用程序名称
	 */
	public static String getAppName(Context context)
	{
		try
		{
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(
					context.getPackageName(), 0);
			int labelRes = packageInfo.applicationInfo.labelRes;
			return context.getResources().getString(labelRes);
		} catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * [获取应用程序版本名称信息]
	 * 
	 * @param context
	 * @return 当前应用的版本名称
	 */
	public static String getVersionName(Context context)
	{
		try
		{
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(
					context.getPackageName(), 0);
			return packageInfo.versionName;

		} catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	public static boolean isActivityEffectived(Activity activity) {
//		if (activity==null || activity.isFinishing() || activity.isDestroyed()) {
//			return false;
//		} else {
//			return true;
//		}
		
		if (activity == null) {
			return false;
		}
		
		int sdk = android.os.Build.VERSION.SDK_INT; 
		if(sdk>=android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {     
			if (activity.isFinishing()) {
				return false;
			}
			if (activity.isDestroyed()) {
				return false;
			}
			return true;
		} else {    
			return true;
		}
	}
	
}

