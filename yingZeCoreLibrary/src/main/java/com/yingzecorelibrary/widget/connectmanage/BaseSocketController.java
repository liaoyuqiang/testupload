package com.yingzecorelibrary.widget.connectmanage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import com.yingzecorelibrary.utils.LogUtil;

public class BaseSocketController implements SocketController{

	private final int mConnectTimeOut = 5000;
	
	private Socket mSocket;
	private SocketInfo mSocketInfo;
	private BufferedOutputStream mBos = null;
	private BufferedInputStream mBis = null;
	private SocketListener mSocketListner;
	private String msg="pong";
	
	@Override
	public void init(SocketInfo socketInfo) {
		this.mSocketInfo = socketInfo;
	}
	
	@Override
	public void openConnection(){
		try {
			InetSocketAddress socketAddress = new InetSocketAddress(mSocketInfo.getUrl(), 
					mSocketInfo.getPort());
			LogUtil.set("socket 长连接", mSocketInfo.getUrl()+" \\ "+mSocketInfo.getPort()).error();
			String url = mSocketInfo.getUrl();
			int port = mSocketInfo.getPort();
			mSocket = new Socket();
			mSocket.connect(socketAddress, mConnectTimeOut);
			if(mSocketListner!=null) {
				mSocketListner.onConnectInform(true);
			}
		} catch (IOException e) {
			if(mSocketListner!=null) {
				mSocketListner.onConnectInform(false);
			}
		}
	}
	
	@Override
	public void requestToSelvert() {
		try {
			if (!mSocket.isClosed() && !mSocket.isOutputShutdown()) {
				mBos = new BufferedOutputStream(mSocket.getOutputStream());
				String message = msg + "\r\n";
				mBos.write(message.getBytes());
				mBos.flush();
			}
			if(mSocketListner!=null) {
				mSocketListner.onRequestInform(true);
			}
		} catch (IOException e) {
			if(mSocketListner!=null) {
				mSocketListner.onRequestInform(false);
			}
		}
	}
	
	@Override
	public void getResponseContent() {
		int length = 0;
		byte[] buffer = new byte[1024];
		try {
			mBis = new BufferedInputStream(mSocket.getInputStream());
			while(mSocket.isConnected() && !mSocket.isInputShutdown()) {
				length = mBis.read(buffer);
				if(length == -1)continue;
				if (length > 0) {
					String responseContent = new String(buffer, 0, length);
					if(mSocketListner != null) {
						mSocketListner.getResponseContent(responseContent);
					}
				}
			}
		} catch (IOException e) {
			if(mSocketListner!=null) {
				mSocketListner.onResponseInform(false);
			}
		}
	}
	
	@Override
	public void close() {	
		if(mSocketListner != null) {
			mSocketListner = null;
		}
		try {
			if(mBos != null) {
				mBos.close();
			}
			if(mBis != null) {
				mBis.close();
			}
			if(mSocket != null) {
				mSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			if(mSocketListner!=null) {
				mSocketListner.onCloseInform();
			}
		}
	}

	@Override
	public boolean isConnected() {
		return mSocket == null ? false : mSocket.isConnected();
	}

	@Override
	public boolean isClosed() {
		return mSocket == null ? true : mSocket.isClosed();
	}
	
	@Override
	public void setSocketListener(SocketListener socketListner) {
		this.mSocketListner=socketListner;
	}

	@Override
	public void setRequestContent(String content) {
		msg = content;
	}
	
}
