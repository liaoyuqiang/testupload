package com.yingzecorelibrary.widget.connectmanage;

public interface SocketController {
	
	public boolean isConnected();
	public boolean isClosed();
	public void init(SocketInfo socketInfo);
	public void openConnection();
	public void requestToSelvert();
	public void getResponseContent();
	public void close();
	public void setSocketListener(SocketListener socketResponseListner);
	public void setRequestContent(String content);
}
