package com.yingzecorelibrary.widget.connectmanage;

import com.yingzecorelibrary.unittest.URLController;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

public class SocketInfo {
	
	private WifiManager wifiManager;
	private WifiInfo wifiInfo;
	private DhcpInfo dhcpInfo;
	private int port = 8024;
	private String ip="";
	private String wifiName="";
	private boolean isTested=false;
	private Context mContext;
	
	public SocketInfo(Context context) {
		this.mContext = context;
		updateWifiInfo();
	}
	
	public SocketInfo(String ip, int port) {
		this.ip=ip;
		this.port=port;
		isTested=true;
	}
	
	public boolean updateWifiInfo() {
		if(!isTested) {
			wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
			wifiInfo=wifiManager.getConnectionInfo();
			dhcpInfo = wifiManager.getDhcpInfo();
			String currentIp = getGateway();
//			currentIp = URLController.getShiNeiJiUrl(currentIp);
			String currentWifiName = getWifiSSID();
			if(currentIp.equalsIgnoreCase(ip) && currentWifiName.equalsIgnoreCase(wifiName)) {
				return false;
			}
			else {
				ip = currentIp;
				wifiName = currentWifiName;
				return true;
			}
		}

		return true;
	}
	
	private String getWifiSSID() {
		return checkWifiEnabled() ? wifiInfo.getSSID() : "";
	}
	
	private String getGateway(){
		return checkWifiEnabled() ? Formatter.formatIpAddress(dhcpInfo.gateway) : "";
	}
	
	private boolean checkWifiEnabled(){
		return wifiManager.isWifiEnabled();
	}
	
	public String getUrl() {
		return ip;
	}

	public void setUrl(String url) {
		this.ip = url;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}	
	
	public String getWifiName() {
		return wifiName;
	}
	
	public void setWifiName(String wifiName) {
		this.wifiName=wifiName;
	}
	
}
