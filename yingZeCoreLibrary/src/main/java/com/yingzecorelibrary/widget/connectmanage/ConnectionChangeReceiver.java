package com.yingzecorelibrary.widget.connectmanage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ConnectionChangeReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		/* 因为在回调的过程中，必须要确保socketclient系非空的，并且不能在执行以下逻辑的过程中发生变化，需要确保同步，故对 */
		synchronized (SocketClient.class) {
			if(SocketClient.isInstanceExist() && SocketClient.getInstance().isEffective()) {
				SocketClient sclient = SocketClient.getInstance();
				if(sclient != null) {
					if(ConnectUtils.isWifiConnected(context)) {
						sclient.onNetStateChanged(true);
					} else {
						sclient.onNetStateChanged(false);
					}
				}
			}
		}
	}
}
