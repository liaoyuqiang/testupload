package com.yingzecorelibrary.widget.connectmanage;

public interface SocketListener {
	public void onConnectInform(boolean onConnected);
	public void onRequestInform(boolean onRequested);
	public void onResponseInform(boolean onResponsed);
	public void onCloseInform();
	public void getResponseContent(String content);
}
