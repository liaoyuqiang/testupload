package com.yingzecorelibrary.widget.connectmanage;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.yingzecorelibrary.utils.HandlerUtil;
import com.yingzecorelibrary.utils.LogUtil;
import com.yingzecorelibrary.utils.ToastUtil;

/*****************************************************************
 * 	SocketClient.java
 *****************************************************************
 *
 *	功能概述 ：
 *      	1、创建代理实例
 *      	2、销毁代理实例
 *			3、正常开启长连接
 *			4、正常关闭长连接
 *
 *  基本情况处理：
 *			1、wifi网络连接状态变为无效的时候，如果长连接没有关闭，则关闭长连接
 *			2、wifi网络连接状态变为有效的时候，如果长连接没有开启，则开启长连接
 *			3、在网络连接正常的情况下，如果本地连接(request/response)抛出
 *		  	     异常，则重新开启长连接，如果检测远程端口关闭，也重新开启长连接（按规则）
 *
 *  业务逻辑情况处理:
 *			1、遇到disconnect的时候，重启长连接
 *			2、当收到发送过来的数据包时，需要进行回复
 *			3、当遇到success的时候，通知监听接口回调
 *
 *****************************************************************
 * @author LittleBird, created date:08/10/2015, city:guangzhou
 * ****************************************************************
 */
public final class SocketClient {
	private final String tag = SocketClient.class.getSimpleName();
	private final int TYPE_CONNECT_RESTART=0;
	private final int TYPE_CONNECT_CLOSE=1;		
	private boolean isEffective = false;
	private boolean isNetAvailable = false;
	private Context mContext;
	private HandlerUtil mHandlerUtil;
	private HeartBeatRunb mHeartBeatRunb;
	private ResponseRunb mResponseRunb;
	private SocketController mSocketController;
	private SocketInfo mSocketInfo;
	private SocketResponseListener mSocketResponseListener;

	private static SocketClient mClient;
	
	Handler handler=new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.arg1) {
			case TYPE_CONNECT_RESTART:
				closeConnect();
				startConnect();
				break;
			case TYPE_CONNECT_CLOSE:
				closeConnect();
				break;
			}

		}
	};
	
	public synchronized static boolean isInstanceExist() {
		return mClient!=null;
	}
	
	public synchronized static SocketClient getInstance() {
		if(mClient==null) {
			mClient=new SocketClient();
		}
		return mClient;
	}
	
	public synchronized static void destroyInstance() {
		if(mClient != null) {
			mClient.closeConnect();
			mClient.mContext = null;
			mClient.mSocketController = null;
			mClient.mSocketResponseListener = null;
			mClient.isEffective = false;
			mClient.handler.removeCallbacksAndMessages(null);
			mClient.handler = null;
			mClient = null;
		}
	}
	
	private SocketClient() {
		mHandlerUtil = HandlerUtil.getInstance(handler);
	}
	
	public void init(Context context, SocketInfo socketInfo,
		SocketController socketController,
		SocketResponseListener socketListener) {
		this.mContext = context;
		this.mSocketController = socketController;
		this.mSocketResponseListener = socketListener;
		this.mSocketInfo = socketInfo;
		this.isEffective=true;
	}
	
	public void startConnect() {
		new Thread(){
			@Override
			public void run() {
				buildConnect();
			}
		}.start();
	}
	
	public void closeConnect() {
		if(mSocketController!=null) {
			mSocketController.close();
		}
		if(mHeartBeatRunb!=null) {
			mHeartBeatRunb.close();
		}
		if(mResponseRunb!=null) {
			mResponseRunb.close();
		}
	}
	
	private void buildConnect() {
		mSocketController.setSocketListener(
			new SocketListener() {
				@Override
				public void onConnectInform(boolean onConnected) {
					LogUtil.set(tag, String.format("######## SocketClient show : " +
						"current wifi is [%s]  @@  " + "socket connected result is [%b]", 
						mSocketInfo.getWifiName(), onConnected)).verbose();
					mSocketResponseListener.onConnectHandle(onConnected);
					mHandlerUtil.getMessageController()
						.setFliter(!onConnected && ConnectUtils.isWifiConnected(mContext))
						.setType(TYPE_CONNECT_RESTART).setDelayTime(4000).send();
				}
				
				@Override
				public void onRequestInform(boolean onRequested) {
					LogUtil.set(tag, String.format("######## SocketClient show : " +
						"current wifi is [%s]  @@  " + "socket requested result is [%b]", 
						mSocketInfo.getWifiName(), onRequested)).verbose();
				}
				
				@Override
				public void onResponseInform(boolean onResponsed) {
					LogUtil.set(tag, String.format("######## SocketClient show : " +
						"current wifi is [%s]  @@  " + "socket get responsed result is [%b]", 
						mSocketInfo.getWifiName(), onResponsed)).verbose();
					mHandlerUtil.getMessageController()
						.setFliter(!onResponsed && ConnectUtils.isWifiConnected(mContext))
						.setType(TYPE_CONNECT_RESTART).send();
				}
				
				@Override
				public void onCloseInform() {
					LogUtil.set(tag, String.format("######## SocketClient show : " +
						"current wifi is [%s]  @@  " + "socket connection is closed !!", 
						mSocketInfo.getWifiName())).verbose();
				}
				
				@Override
				public void getResponseContent(String content) {					
					LogUtil.set(tag, String.format("######## SocketClient show : " +
						"current wifi is [%s]  @@  " + "socket get responsed content is [%s]", 
						mSocketInfo.getWifiName(), content)).verbose();
					mSocketResponseListener.onResponseHandle(content);
					mSocketController.setRequestContent(content);
					mHeartBeatRunb.sendHeartBeat();
				}
			}
		);
		mSocketController.init(mSocketInfo);
		mSocketController.openConnection();
		if(mSocketController.isConnected()) {
			mHeartBeatRunb = new HeartBeatRunb();
			mResponseRunb = new ResponseRunb();
			new Thread(mHeartBeatRunb).start();
			new Thread(mResponseRunb).start();
		}
	}

	class HeartBeatRunb implements Runnable {
		public boolean mLooped = true;
		@Override
		public void run() {
			try {
				while(mLooped) {
					synchronized(this) {
						mSocketController.requestToSelvert();
						wait();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			LogUtil.set(tag, String.format("######## SocketClient show : " +
				"current wifi is [%s]  @@  " + "Request thread is finished!!", 
				mSocketInfo.getWifiName())).verbose();
		}
		public void sendHeartBeat() {
			synchronized(this) {notify();}
		}
		public void close() {
			mLooped = false;
			synchronized(this) {notify();}
		}
	};
	
	class ResponseRunb implements Runnable {
		@Override
		public void run() {
			mSocketController.getResponseContent();
			LogUtil.set(tag, String.format("######## SocketClient show : " +
				"current wifi is [%s]  @@  " + "Response thread is finished!!", 
				mSocketInfo.getWifiName())).verbose();
		}
		public void close() {}
	};
	
	public synchronized void onNetStateChanged(boolean isNetAvailable) {
		if(isEffective) {			
			boolean isWifiInfoChanged = mSocketInfo.updateWifiInfo();
			if(!isWifiInfoChanged && this.isNetAvailable == isNetAvailable) {
				return;
			}
			this.isNetAvailable = isNetAvailable;			
			mSocketResponseListener.updateWifiInfo(mSocketInfo, isEffective);
			
			if(isNetAvailable) {
				ToastUtil.set(mContext, "wifi is available").show();
				LogUtil.set(tag, String.format("######## SocketClient show : " +
					"current wifi is [%s]  @@  " + "wifi is available!!", 
					mSocketInfo.getWifiName())).verbose();
				mHandlerUtil.getMessageController()
					.setFliter(true).setType(TYPE_CONNECT_RESTART).send();
			} else {	
				ToastUtil.set(mContext, "wifi is unavailable").show();
				LogUtil.set(tag, String.format("######## SocketClient show : " +
					"current wifi is [%s]  @@  " + "wifi is unavailable!!", 
					mSocketInfo.getWifiName())).verbose();
				mHandlerUtil.getMessageController()
					.setFliter(true).setType(TYPE_CONNECT_CLOSE).send();
			}
		}
	}

	public boolean isEffective() {
		return isEffective;
	}
	
	public void updateSocketResponseListener(SocketResponseListener socketResponseListener) {
		this.mSocketResponseListener = socketResponseListener;
	}
	
	/**
	 * 长连接是否断开
	 * 
	 * @return 
	 *   	socket connect result
	 */
	public boolean isConnected() {
		return mSocketController == null ? false : mSocketController.isConnected();
	}
	
	/**
	 * 长连接是否关闭
	 * 
	 * @return 
	 * 		socket connect result
	 */
	public boolean isClosed() {
		return mSocketController == null ? false : mSocketController.isClosed();
	}

	/**
	 * 连接响应过滤器
	 */
	public static class SocketResponseFilter {}
	
	/**
	 * socket 连接回调监听器
	 */
	public static interface SocketResponseListener {
		public void updateWifiInfo(SocketInfo info, boolean isAvailable);
		public void onConnectHandle(boolean connected);
		public void onResponseHandle(String content);
	}

}
