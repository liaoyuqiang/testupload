/**
 * 上传多文件
 */
package com.yingze.corelibrary.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;

import com.yingze.corelibrary.network.NetworkClient.NetworkClientCallback;
import com.yingze.corelibrary.network.NetworkController;
import com.yingzecorelibrary.utils.LogUtil;

public class UploadMultipartController implements NetworkController {
	protected final int mConnectTimeout = 1000 * 40;
	protected final int mReadTimeout = 1000 * 40;
	protected static final String CHARSET = "utf-8"; //设置编码
	protected String CONTENT_TYPE = "multipart/form-data";//内容类型
	protected String BOUNDARY =  UUID.randomUUID().toString();//边界标识   随机生成
	protected String PREFIX = "--";//分割符
	protected String LINEEND = "\r\n"; //回车换行

	protected OutputStream os = null;//从服务器获取的写入流
	protected HttpURLConnection conn = null;//网络连接类
	protected ByteArrayOutputStream opt = new ByteArrayOutputStream();
	
	protected NetworkClientCallback mCallback;//回调接口
//	protected String mLocalFilePath;// 本地文件路径 
	protected String mRemoteSubmitPath;//server的文件处理地址 
	protected Map<String,String> mParams;
	protected ArrayList<Annex> mAnnexArr;
	
	public UploadMultipartController(ArrayList<Annex> annexArr, String remoteSubmitPath, 
			NetworkClientCallback callback, Map<String,String> params) {
		mAnnexArr = annexArr;
		if (mAnnexArr == null) {
			mAnnexArr = new ArrayList<Annex>();
		}
		mRemoteSubmitPath = remoteSubmitPath;
		mCallback = callback;
		mParams = params;
	}
	
	@Override
	public void setNetworkCallback(NetworkClientCallback callback) {
		mCallback = callback;
	}

	@Override
	public NetworkClientCallback getNetworkCallback() {
		return mCallback;
	}

	@Override
	public void setRequestType(String type) {

	}

	@Override
	public void openConnect() throws IOException {
		URL url = new URL(mRemoteSubmitPath);
		conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);  //不允许使用缓存
		conn.setChunkedStreamingMode(512); // 参数0代表使用默认的chunk长度。
		conn.setConnectTimeout(mConnectTimeout);
		conn.setReadTimeout(mReadTimeout);
	}

	@Override
	public void requestServer() throws IOException {
		conn.setRequestMethod("POST");
        conn.setRequestProperty("Charset", CHARSET);
        conn.setRequestProperty("connection", "keep-alive"); 
        conn.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary=" + BOUNDARY); 
        
        DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
        
        /* 首先拼接文本类型的参数 */
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : mParams.entrySet()) {
            sb.append(PREFIX);
            sb.append(BOUNDARY);
            sb.append(LINEEND);
            sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEEND);
            sb.append("Content-Type: application/x-www-form-urlencoded; charset=" + CHARSET + LINEEND);
            sb.append("Content-Transfer-Encoding: 8bit" + LINEEND);
            sb.append(LINEEND);
            sb.append(entry.getValue());
            sb.append(LINEEND);
        }
        dos.write(sb.toString().getBytes());
        
        for (int z=0;z<mAnnexArr.size();z++) {
        	Annex annex = mAnnexArr.get(z);
        	String type = annex.getAnnexType();
        	if (type.equals(Annex.TYPE_FILE)) { // 属于File类型
                File file = new File(String.valueOf(annex.value));
    	        /* 判断上传的文件是否存在  */
    	        if (file.exists()) {
    	            /* 然后拼接上传的附件参数属性 */
    	            sb = new StringBuilder();
    	            sb.append(PREFIX);
    	            sb.append(BOUNDARY);
    	            sb.append(LINEEND);
//    	            sb.append("Content-Disposition: form-data; name=\"files\"; filename=\""+annex.name+"\""+LINEEND); 
    	            sb.append("Content-Disposition: form-data; name=\""+ annex.fileCategory +"\"; filename=\""+annex.name+"\""+LINEEND); 
    	            sb.append("Content-Type: application/octet-stream; charset="+CHARSET+LINEEND);
    	            sb.append(LINEEND);
    	            dos.write(sb.toString().getBytes());
    	            
    	            /* 最后，上传附件到服务器 */
    	    		FileInputStream fileInputStream = new FileInputStream(file);
    	    		int contentLength = fileInputStream.available();
    	    		int sum = 0;
    	    		int length = 0;
    	    		byte[] buffer = new byte[512];
    	    		while((length=fileInputStream.read(buffer))!=-1) {
    	    			dos.write(buffer, 0, length);
    	    			sum += length;
    	    			LogUtil.set( "upload", "file 已经上传:" + length + "," + (sum*100/contentLength) + "%"
    	    					+ contentLength).error();
    	    			mCallback.onUploadProcess(length, sum, contentLength);
    	    		}
    	    		fileInputStream.close();
    	    		dos.write(LINEEND.getBytes());
    	        }
        	} else if(type.equals(Annex.TYPE_BITMAP)) { // 属于bitmap类型
        		
	            /* 然后拼接上传的附件参数属性 */
	            sb = new StringBuilder();
	            sb.append(PREFIX);
	            sb.append(BOUNDARY);
	            sb.append(LINEEND);
	            sb.append("Content-Disposition: form-data; name=\""+ annex.fileCategory +"\"; filename=\""+annex.name+"\""+LINEEND); 
	            sb.append("Content-Type: application/octet-stream; charset="+CHARSET+LINEEND);
	            sb.append(LINEEND);
	            dos.write(sb.toString().getBytes());
        		
	            Bitmap bitmap = (Bitmap) annex.value;
	            
        		try {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bitmap.compress(CompressFormat.PNG, 100, out);
					byte[] arr = out.toByteArray();
					int blockLength = 512;
					int blockIndex = 0;
					int blockSize = arr.length/blockLength;
					int quyu = arr.length%blockLength;
					int sum = 0;
					while(blockIndex<blockSize) {
						dos.write(arr, blockIndex*blockLength, blockLength);
						sum += blockLength;
						mCallback.onUploadProcess(blockLength, sum, arr.length);
						LogUtil.set( "upload", "bitmap 已经上传:" + (sum*100/arr.length) + "%"
								+ "图片总长:" +arr.length).verbose();
						blockIndex++;
					}
					if(quyu>0) {
						dos.write(arr, blockIndex*blockLength, quyu);
						mCallback.onUploadProcess(quyu, arr.length, arr.length);
						LogUtil.set( "upload", "已经上传:" + "100%"
								+ "图片总长:" +arr.length).verbose();
					}
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        		dos.write(LINEEND.getBytes());
        	}
        }
        
        /* 拼接结束上传的边界字符 */
		byte[] end_data = (PREFIX+BOUNDARY+PREFIX+LINEEND).getBytes();
		dos.write(end_data);
		dos.flush();
	}

	@Override
	public void uploadPostContent(OutputStream outputStream) {

	}

	@Override
	public boolean isConnectEffectived() throws IOException {
		int responseCode = conn.getResponseCode();
		Log.e("upload-multipart-controller", "@@@@@@@@@@@@@@@@ response-code : " + responseCode);
		return conn.getResponseCode()==200;
	}

	@Override
	public void processingDataFromServer() throws IOException {
		int length = 0;
		byte[] buffer = new byte[512];
		InputStream inputStream = conn.getInputStream();
		while ((length = inputStream.read(buffer)) != -1) {
			opt.write(buffer, 0, length);
		}
		opt.flush();
	}

	@Override
	public void disconnect() {
		if (conn != null) {
			conn.disconnect();
		}
	}

	@Override
	public void close() throws IOException {
		if (conn != null) {
			conn.disconnect();
		}
		if (os != null) {
			os.close();
		}
		if(opt!=null) {
			opt.close();
		}
	}

	@Override
	public String getData() {
		return opt.toString();
	}

	public static class Annex {
		public static final String TYPE_FILE = "file";
		public static final String TYPE_BITMAP = "bitmap";
		private String annexType="";// 附件类型
		private String name;
		private Object value;
		private String fileCategory="files";// 上传的文件类型

		public Annex(String type, String fileCategory) {
			this.annexType = type;
			this.fileCategory = fileCategory;
		}
		
		public String getAnnexType(){
			return annexType;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}
		
		public String getFileCategory() {
			return fileCategory;
		}

		public void setFileCategory(String fileCategory) {
			this.fileCategory = fileCategory;
		}
	}
	
}
