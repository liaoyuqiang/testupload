package com.yingze.corelibrary.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.UUID;
import com.yingze.corelibrary.network.NetworkClient.NetworkClientCallback;
import com.yingzecorelibrary.utils.LogUtil;

public class UploadFileController implements NetworkController {
	protected final int mConnectTimeout = 1000 * 40;
	protected final int mReadTimeout = 1000 * 40;
	protected static final String CHARSET = "utf-8"; //设置编码
	protected String CONTENT_TYPE = "multipart/form-data";//内容类型
	protected String BOUNDARY =  UUID.randomUUID().toString();//边界标识   随机生成
	protected String PREFIX = "--";//分割符
	protected String LINEEND = "\r\n"; //回车换行

	protected OutputStream os = null;//从服务器获取的写入流
	protected HttpURLConnection conn = null;//网络连接类
	protected ByteArrayOutputStream opt = new ByteArrayOutputStream();
	
	protected NetworkClientCallback mCallback;//回调接口
	protected String mLocalFilePath;// 本地文件路径 
	protected String mRemoteSubmitPath;//server的文件处理地址 
	protected Map<String,String> mParams;
	
	public UploadFileController(String localFilePath, String remoteSubmitPath, 
			NetworkClientCallback callback, Map<String,String> params) {
		mLocalFilePath = localFilePath;
		mRemoteSubmitPath = remoteSubmitPath;
		mCallback = callback;
		mParams = params;
	}
	
	@Override
	public void setNetworkCallback(NetworkClientCallback callback) {
		mCallback = callback;
	}

	@Override
	public NetworkClientCallback getNetworkCallback() {
		return mCallback;
	}

	@Override
	public void setRequestType(String type) {

	}

	@Override
	public void openConnect() throws IOException {
		URL url = new URL(mRemoteSubmitPath);
		conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);  //不允许使用缓存
		conn.setChunkedStreamingMode(512); // 参数0代表使用默认的chunk长度。
		conn.setConnectTimeout(mConnectTimeout);
		conn.setReadTimeout(mReadTimeout);
	}

	@Override
	public void requestServer() throws IOException {
		conn.setRequestMethod("POST");
        conn.setRequestProperty("Charset", CHARSET);
        conn.setRequestProperty("connection", "keep-alive"); 
        conn.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary=" + BOUNDARY); 
        
        DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
        File file = new File(mLocalFilePath);
        
        /* 首先拼接文本类型的参数 */
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : mParams.entrySet()) {
            sb.append(PREFIX);
            sb.append(BOUNDARY);
            sb.append(LINEEND);
            sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEEND);
            sb.append("Content-Type: application/x-www-form-urlencoded; charset=" + CHARSET + LINEEND);
            sb.append("Content-Transfer-Encoding: 8bit" + LINEEND);
            sb.append(LINEEND);
            sb.append(entry.getValue());
            sb.append(LINEEND);
        }
        dos.write(sb.toString().getBytes());
        
        /* 判断上传的文件是否存在  */
        if (file.exists()) {
        	
            /* 然后拼接上传的附件参数属性 */
            sb = new StringBuilder();
            sb.append(PREFIX);
            sb.append(BOUNDARY);
            sb.append(LINEEND);
            sb.append("Content-Disposition: form-data; name=\"file\"; filename=\""+file.getName()+"\""+LINEEND); 
            sb.append("Content-Type: application/octet-stream; charset="+CHARSET+LINEEND);
            sb.append(LINEEND);
            dos.write(sb.toString().getBytes());
            
            /* 最后，上传附件到服务器 */
    		FileInputStream fileInputStream = new FileInputStream(file);
    		int contentLength = fileInputStream.available();
    		int sum = 0;
    		int length = 0;
    		byte[] buffer = new byte[512];
    		while((length=fileInputStream.read(buffer))!=-1) {
    			dos.write(buffer, 0, length);
    			sum += length;
    			LogUtil.set( "upload", "已经上传:" + length + "," + (sum*100/contentLength) + "%"
    					+ contentLength).error();
    			mCallback.onUploadProcess(length, sum, contentLength);
    		}
    		fileInputStream.close();
    		dos.write(LINEEND.getBytes());
        }
        
        /* 拼接结束上传的边界字符 */
		byte[] end_data = (PREFIX+BOUNDARY+PREFIX+LINEEND).getBytes();
		dos.write(end_data);
		dos.flush();
	}

	@Override
	public void uploadPostContent(OutputStream outputStream) {

	}

	@Override
	public boolean isConnectEffectived() throws IOException {
		return conn.getResponseCode()==200;
	}

	@Override
	public void processingDataFromServer() throws IOException {
		int length = 0;
		byte[] buffer = new byte[512];
		InputStream inputStream = conn.getInputStream();
		while ((length = inputStream.read(buffer)) != -1) {
			opt.write(buffer, 0, length);
		}
		opt.flush();
	}

	@Override
	public void disconnect() {
		if (conn != null) {
			conn.disconnect();
		}
	}

	@Override
	public void close() throws IOException {
		if (conn != null) {
			conn.disconnect();
		}
		if (os != null) {
			os.close();
		}
		if(opt!=null) {
			opt.close();
		}
	}

	@Override
	public String getData() {
		return opt.toString();
	}

}
