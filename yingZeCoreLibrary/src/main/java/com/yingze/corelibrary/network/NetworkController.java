/**
 * 网络请求控制器，定义为接口，供子类实现
 */
package com.yingze.corelibrary.network;

import java.io.IOException;
import java.io.OutputStream;

import com.yingze.corelibrary.network.NetworkClient.NetworkClientCallback;

public interface NetworkController {
	public final static String GET = "get";
	public final static String POST = "post";
	
	public void setNetworkCallback(NetworkClientCallback callback);
	public NetworkClientCallback getNetworkCallback();
	public void setRequestType(String type);
	public void openConnect()  throws IOException;
	public void requestServer() throws IOException;
	public void uploadPostContent(OutputStream outputStream) throws IOException;
	public boolean isConnectEffectived() throws IOException;
	public void processingDataFromServer() throws IOException;
	public void disconnect();
	public void close() throws IOException;
	public String getData();
	
	public static NetworkController NULL = new NetworkController() {
		@Override
		public void setNetworkCallback(NetworkClientCallback callback) {
		}
		@Override
		public NetworkClientCallback getNetworkCallback() {
			return NetworkClientCallback.NULL;
		}
		@Override
		public void setRequestType(String type) {
		}
		@Override
		public void openConnect() throws IOException {
		}
		@Override
		public void requestServer() throws IOException {
		}
		@Override
		public void uploadPostContent(OutputStream outputStream)
				throws IOException {
		}
		@Override
		public boolean isConnectEffectived() throws IOException {
			return false;
		}
		@Override
		public void processingDataFromServer() throws IOException {
		}
		@Override
		public void disconnect() {	
		}
		@Override
		public void close() throws IOException {
		}
		@Override
		public String getData() {
			return "";
		}
		
	};
}
