/**
 * 基本的请求网络控制器，适用于大多数业务请求，可根据实际情况进行扩展
 */
package com.yingze.corelibrary.network;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import com.yingze.corelibrary.network.NetworkClient.NetworkClientCallback;
import com.yingzecorelibrary.utils.LogUtil;

public abstract class BaseNetworkController implements NetworkController {
	protected final int mConnectTimeout = 1000 * 20;
	protected final int mReadTimeout = 1000 * 20;
	protected String requestUrl;
	protected String requestType = NetworkController.GET;
	protected HttpURLConnection serverConnect = null;// http 连接器
	protected OutputStream serverOutputStream;// 写入到服务器的流
	protected InputStream serverInputStream = null;// 读取服务器返回的数据流
	protected ByteArrayOutputStream writer = new ByteArrayOutputStream();// 用于写内存流
	protected FileOutputStream LocalFileWriter = null;// 用于写文件流
	protected NetworkClientCallback callback;
	protected boolean isServerConnectEffectived = true;
	
	public BaseNetworkController(String requestUrl, NetworkClientCallback callback) {
		this.requestUrl = requestUrl;
		this.callback = callback;
	}
	
	public BaseNetworkController(String requestUrl, FileOutputStream fileOutputStream, 
			NetworkClientCallback callback) {
		this.requestUrl = requestUrl;
		this.LocalFileWriter = fileOutputStream;
		this.callback = callback;
	}
	
	@Override
	public void setRequestType(String type) {
		this.requestType = type;
	}
	
	@Override
	public void setNetworkCallback(NetworkClientCallback callback) {
		this.callback = callback;
	}
	
	@Override
	public NetworkClientCallback getNetworkCallback() {
		return callback;
	}
	
	@Override
	public void openConnect() throws IOException{
		if (isServerConnectEffectived()) {
			System.setProperty("http.keepalive","false");
			URL url = new URL(requestUrl);
			serverConnect = (HttpURLConnection) url.openConnection();
			serverConnect.setDoInput(true);
			serverConnect.setUseCaches(false);  //不允许使用缓存
			serverConnect.setChunkedStreamingMode(512); // 参数0代表使用默认的chunk长度。
			serverConnect.setConnectTimeout(mConnectTimeout);
			serverConnect.setReadTimeout(mReadTimeout);
		} else {
			LogUtil.set("network", "openConnect 主动抛出IO异常").error();
			close();
			throw new IOException();
		}
	}
	
	@Override
	public void requestServer() throws IOException{
		if (isServerConnectEffectived()) {
			if(NetworkController.GET.equalsIgnoreCase(requestType)) {
				serverConnect.setRequestMethod("GET");
				serverConnect.connect();
			} else if (NetworkController.POST.equalsIgnoreCase(requestType)) {
				serverConnect.setDoOutput(true);
				serverConnect.setRequestMethod("POST");
				serverOutputStream = serverConnect.getOutputStream();
				uploadPostContent(serverOutputStream);
				serverOutputStream.flush();
				serverOutputStream.close();
			}
//			switch(requestType) {
//			case NetworkController.GET:
//				serverConnect.setRequestMethod("GET");
//				serverConnect.connect();
//				break;
//			case NetworkController.POST:
//				serverConnect.setDoOutput(true);
//				serverConnect.setRequestMethod("POST");
//				serverOutputStream = serverConnect.getOutputStream();
//				uploadPostContent(serverOutputStream);
//				serverOutputStream.flush();
//				serverOutputStream.close();
//				break;
//			}
		} else {
			LogUtil.set("network", "requestServer 主动抛出IO异常").error();
			close();
			throw new IOException();
		}
	}
	
	@Override
	public boolean isConnectEffectived() throws IOException {
		if (isServerConnectEffectived()) {
			return serverConnect.getResponseCode()==200;
		} else {
			LogUtil.set("network", "isConnectEffectived 主动抛出IO异常").error();
			close();
			throw new IOException();
		}
	}
	
	@Override
	public void processingDataFromServer() throws IOException {
		if (isServerConnectEffectived()) {
			byte[] buffer = new byte[1024];
			int bufferSize = 0;
			int bufferSum = 0;
			int contentLength = serverConnect.getContentLength();
			serverInputStream = serverConnect.getInputStream();
			while((bufferSize = serverInputStream.read(buffer)) != -1) {
				if(LocalFileWriter!=null){
					LocalFileWriter.write(buffer, 0, bufferSize);
				} else {
					writer.write(buffer, 0, bufferSize);
				}
				bufferSum += bufferSize;
				callback.onDownloadProcess(bufferSize, bufferSum, contentLength);
			}
		} else {
			LogUtil.set("network", "processingDataFromServer 主动抛出IO异常").error();
			close();
			throw new IOException();
		}
	}
	
	@Override
	public String getData() {
		return writer.toString();
	}
	
	@Override
	public void close() throws IOException {
		if (serverConnect != null) {
			serverConnect.disconnect();
		}
		if (serverOutputStream != null) {
			serverOutputStream.close();
		}
		if (serverInputStream != null) {
			serverInputStream.close();
		}
		if(LocalFileWriter!=null) {
			LocalFileWriter.close();
		}
		writer.close();
	}
	
	@Override
	public void disconnect() {
		LogUtil.set("network", "-------BN-------请求中断 http 连接").error();
		isServerConnectEffectived = false;
		if(serverConnect!=null) {
			serverConnect.disconnect();
			LogUtil.set("network", "-------BN-------中断 http 连接").error();
		}
		
		if (serverOutputStream != null) {
			try {
				serverOutputStream.close();
				LogUtil.set("network", "-------BN-------中断 http_outstream").error();
			} catch (IOException e) {
				e.printStackTrace();
				LogUtil.set("network", "-------BN-------中断 http_outstream_IO_exception").error();
			}
		}
	}
	
	public boolean isServerConnectEffectived() {
		return isServerConnectEffectived;
	}
	
//	public void testSleep() {
//		try {
//			Thread.sleep(6000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}
}
