package com.yingze.corelibrary.network;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import com.yingze.corelibrary.network.NetworkClient.NetworkClientCallback;

public class UploadServerController extends UploadFileController {
	private String mFileName;
	
	public UploadServerController(String fileName, String remoteSubmitPath, NetworkClientCallback callback, Map<String, String> params) {
		super(null, remoteSubmitPath, callback, params);
		mFileName = fileName;
	}

	@Override
	public void requestServer() throws IOException {
		conn.setRequestMethod("POST");
        conn.setRequestProperty("Charset", CHARSET);
        conn.setRequestProperty("connection", "keep-alive"); 
        conn.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary=" + BOUNDARY); 
        
        DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
//        File file = new File(mLocalFilePath);
        
        /* 首先组拼文本类型的参数 */
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : mParams.entrySet()) {
            sb.append(PREFIX);
            sb.append(BOUNDARY);
            sb.append(LINEEND);
            sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEEND);
            sb.append("Content-Type: application/x-www-form-urlencoded; charset=" + CHARSET + LINEEND);
            sb.append("Content-Transfer-Encoding: 8bit" + LINEEND);
            sb.append(LINEEND);
            sb.append(entry.getValue());
            sb.append(LINEEND);
        }
        dos.write(sb.toString().getBytes());
        
        /* 然后拼接上传的附件参数属性 */
        sb = new StringBuilder();
        sb.append(PREFIX);
        sb.append(BOUNDARY);
        sb.append(LINEEND);
        sb.append("Content-Disposition: form-data; name=\"file\"; filename=\""+mFileName+"\""+LINEEND); 
        sb.append("Content-Type: application/octet-stream; charset="+CHARSET+LINEEND);
        sb.append(LINEEND);
        dos.write(sb.toString().getBytes());
		
        /* 最后，上传附件到服务器 */
        uploadPostContent(dos);
        
//		FileInputStream fileInputStream = new FileInputStream(file);
//		int contentLength = fileInputStream.available();
//		int sum = 0;
//		int length = 0;
//		byte[] buffer = new byte[512];
//		while((length=fileInputStream.read(buffer))!=-1) {
//			dos.write(buffer, 0, length);
//			sum += length;
//			LogUtil.set( "upload", "已经上传:" + length + "," + (sum*100/contentLength) + "%"
//							+ contentLength).verbose();
//			mCallback.onDownloadProcess(length, sum, contentLength);
//		}
//		fileInputStream.close();
		dos.write(LINEEND.getBytes());
		byte[] end_data = (PREFIX+BOUNDARY+PREFIX+LINEEND).getBytes();
		dos.write(end_data);
		dos.flush();
	}

}
