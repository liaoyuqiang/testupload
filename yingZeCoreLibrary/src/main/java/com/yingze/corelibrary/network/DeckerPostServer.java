/**
 * 请求服务器的类型为post方式
 */
package com.yingze.corelibrary.network;

import com.yingze.corelibrary.network.ThreadPoolController.TaskController;

public class DeckerPostServer implements TaskController {
	private NetworkClient mClient;
	
	public DeckerPostServer(NetworkClient client) {
		mClient = client;
		mClient.setRequestType(NetworkController.POST);
	}
	
	@Override
	public void execute() {
		mClient.connect();
	}

	@Override
	public void cancel() {
		new Thread() {
			public void run() {
				mClient.disConnect();
			};
		}.start();
	}

}
