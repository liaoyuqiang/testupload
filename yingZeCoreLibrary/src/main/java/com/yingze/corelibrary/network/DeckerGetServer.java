/**
 * 请求服务器的类型为get方式
 */
package com.yingze.corelibrary.network;

import com.yingze.corelibrary.network.ThreadPoolController.TaskController;

public class DeckerGetServer implements TaskController {
	private NetworkClient mClient;

	public DeckerGetServer(NetworkClient client) {
		mClient = client;
		mClient.setRequestType(NetworkController.GET);
	}
	
	@Override
	public void execute() {
		mClient.connect();
	}

	@Override
	public void cancel() {
		new Thread() {
			public void run() {
				mClient.disConnect();
			};
		}.start();
	}

}
