package com.yingze.corelibrary.network;
/**
 * 对外管理器，用于封装网络请求的动作，供外部调用
 */
import java.io.IOException;
import com.yingzecorelibrary.utils.LogUtil;

public class NetworkClient {
	private String mRequestType = NetworkController.GET;
	private NetworkController mNetworkController = NetworkController.NULL;
	private NetworkClientCallback mNetworkClientCallback = NetworkClientCallback.NULL;
	
	public static NetworkClient newInstance() {
		return new NetworkClient();
	}
	
	public NetworkClient setNetworkController(NetworkController networkController) {
		mNetworkController = networkController;
		return this;
	}
	
	protected void connect() {
		mNetworkClientCallback = mNetworkController.getNetworkCallback();
		try {
			mNetworkController.openConnect();
			mNetworkController.setRequestType(mRequestType);
			mNetworkController.requestServer();
			if (mNetworkController.isConnectEffectived()) {
				mNetworkController.processingDataFromServer();
				mNetworkClientCallback.onFinish(mNetworkController.getData());
			} else {
				mNetworkClientCallback.onFail(null, "服务器处理异常，下载失败");
			}
			mNetworkController.close();
		} catch (IOException e) {
			LogUtil.set("network", "-------NC-------中断 ").error();
			mNetworkClientCallback.onFail(null, "网络连接超时，请检查网络！");
		}
	}
	
	protected void disConnect() {
		mNetworkClientCallback = NetworkClientCallback.NULL;
		mNetworkController.setNetworkCallback(NetworkClientCallback.NULL);
		mNetworkController.disconnect();
	}

	protected String getRequestType() {
		return mRequestType;
	}

	protected void setRequestType(String requestType) {
		this.mRequestType = requestType;
	}
	
	public static interface NetworkClientCallback {
		public void onDownloadProcess(int readbufferLength, int downloadFileLength, int totalFileLength);
		public void onUploadProcess(int readbufferLength, int uploadFileLength, int totalFileLength);
		public void onFinish(String content);
		public void onFail(Exception e, String message);
		
		public static NetworkClientCallback NULL = new NetworkClientCallback() {
			@Override
			public void onDownloadProcess(int readbufferLength,
					int downloadFileLength, int totalFileLength) {
			}
			@Override
			public void onUploadProcess(int readbufferLength,
					int uploadFileLength, int totalFileLength) {
			}
			@Override
			public void onFinish(String content) {
			}
			@Override
			public void onFail(Exception e, String message) {
			}
		};
	}
	
	public static abstract class DownloadCallback implements NetworkClientCallback{
		public void onUploadProcess(int readbufferLength, int uploadFileLength, int totalFileLength) {
			// 可以不用显示在子类中
		}
	}
	
	public static abstract class UploadCallback implements NetworkClientCallback{
		public void onDownloadProcess(int readbufferLength, int uploadFileLength, int totalFileLength) {
			// 可以不用显示在子类中
		}
	}
	
}
