/**
 * 线程池控制器
 */
package com.yingze.corelibrary.network;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.util.Log;

public final class ThreadPoolController {
	private final int mThreadNum = 5;
	private ExecutorService executorService = Executors.newFixedThreadPool(mThreadNum); // 固定五个线程来执行任务
	private static ThreadPoolController it;
	
	public static synchronized ThreadPoolController getInstance() {
		if(it==null) {
			it = new ThreadPoolController();
		}
		return it;
	}
	
	public void submit(TaskController taskController) {
		if(taskController!=null) {
			RequestServerTask task = new RequestServerTask(taskController); 
			executorService.submit(task);
		}
	}
	
	public void cancel(TaskController taskController) {
		if(taskController!=null) {
			taskController.cancel();
		}
	}
	
	public static class RequestServerTask implements Runnable{
		private TaskController taskController;
		public RequestServerTask(TaskController taskController) {
			this.taskController=taskController;
		}
		@Override
		public void run() {
			Log.e("RequestServerTask", "---------------- 开始执行网络请求操作");
			taskController.execute();
		}
	}
	
	public static interface TaskController {
		public void execute();
		public void cancel();
	}

}
