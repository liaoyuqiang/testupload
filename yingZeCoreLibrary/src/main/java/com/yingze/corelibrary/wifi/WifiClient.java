/**
 * wifi 客户端控制器
 * ------------------------------------------------------
 * 功能描述：
 * 		自动接收并分析wifi的状态变化，通过对应的命令驱动状态转换，自动重连等
 * ------------------------------------------------------
 * @author LittleBird
 * create at guangzhou, 2015/10/20 14:30
 */
package com.yingze.corelibrary.wifi;

import java.util.ArrayList;

import com.yingzecorelibrary.utils.LogUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Parcelable;
import android.widget.Toast;

public class WifiClient {
	
	public final int COMMAND_WIFI_ENABLE = 1;
	public final int COMMAND_WIFI_DISABLED = 2;
	
	private boolean isCompulsoryConnected = false; // 是否强制连接指定wifi
	private String mWifiName;
	private String mWifiPassword;
	
	private Context mContext;
	private WifiController mWifiController;
	private WifiStateController mWifiStateController;
	private Handler mHandler = new Handler();
	private Handler mShowMessage = new Handler();
	private ArrayList<WifiStateCallback> mWifiStateCallback = new ArrayList<WifiStateCallback>();
	
	/**
	 * 获取当前已经连接的wifi的名称
	 * @param context
	 * @return
	 */
	public static String getCurrentWifiSSID(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		if (wifiInfo!=null && wifiInfo.getSSID()!=null) {
			return wifiInfo.getSSID();
		}
		return "SSID is null";
	}
	
	public WifiClient(Context context, String wifiName, String wifiPassword, boolean isCompulsoryConnected) {
		mContext = context;
		mWifiName = wifiName;
		mWifiPassword = wifiPassword;
		this.isCompulsoryConnected = isCompulsoryConnected;
	}

	public void addWifiStateCallback(WifiStateCallback wifiStateCallback) {
		mWifiStateCallback.add(wifiStateCallback);
	}
	
	/**
	 * 注册wifi广播接收器
	 */
	public void registerWifiCommandReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		mContext.registerReceiver(wifiCommandReceiver, filter);
	}
	
	/**
	 * 注销wifi广播接收器
	 */
	public void unRegisterWifiCommandReceiver() {
		mWifiStateCallback.clear();
		mContext.unregisterReceiver(wifiCommandReceiver);
	}
	
	/**
	 * 开启wifi自动连接
	 */
	public void startWifiAutoConnect() {
		if(mWifiName==null || mWifiName.length() ==0 || 
				mWifiPassword==null || mWifiPassword.length()==0) {
			return;
		}
		mWifiController = new WifiController(mContext);
		mWifiStateController = new WifiStateController();
		mWifiStateController.initState(mWifiStateController.TYPE_CONNECT);
	}

	/**
	 * 关闭wifi自动连接
	 */
	public void closeWifiAutoConnect() {
		mWifiController = null;
		mWifiStateController = null;
		mHandler.removeCallbacksAndMessages(null);
	}
	
	public void setCompulsoryConnected(boolean isCompulsoryConnected) {
		this.isCompulsoryConnected = isCompulsoryConnected;
	}
	
	public void showInfo(final String str) {
		LogUtil.set("Wifi Client : ", str).verbose();
		mShowMessage.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	/**
	 * wifi命令广播接收器
	 * --------------------------------------
	 * 作用描述:
	 * 		每隔一段时间，主动连接设定好的wifi
	 * --------------------------------------
	 * @author LittleBird
	 * created at guangzhou, 2015/10/20 14:30
	 */
	BroadcastReceiver wifiCommandReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// 这个监听wifi的打开与关闭，与wifi的连接无关
			if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {
				int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
				switch (wifiState) {
				case WifiManager.WIFI_STATE_DISABLED:
					showInfo("broadcast : wifi is disabled");
					if(mWifiStateController!=null) {
						mWifiStateController.handleCommand(COMMAND_WIFI_DISABLED, intent);
					}
                	for(WifiStateCallback item : mWifiStateCallback) {
                		item.onConnectFailed(context, intent);
                	}
					break;
				}
			}
			// 这个是连接状态改变的监听
			if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {  
	            Parcelable parcelableExtra = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);  
	            if (null != parcelableExtra) {  
	                NetworkInfo networkInfo = (NetworkInfo) parcelableExtra;  
	                State state = networkInfo.getState();  
	                boolean isConnected = state == State.CONNECTED;// 当然，这边可以更精确的确定状态  
	                if (isConnected) { 
	                	showInfo("broadcast network : " + getCurrentWifiSSID(mContext) +"is connect");
	                	if(mWifiStateController!=null) {
	                		mWifiStateController.handleCommand(COMMAND_WIFI_ENABLE, intent);
	                	}
	                } else {  
	                	showInfo("broadcast network : " + getCurrentWifiSSID(mContext) +"is not connect");
	                	for(WifiStateCallback item : mWifiStateCallback) {
	                		item.onConnectFailed(context, intent);
	                	}
	                }  
	            }  
	        }  
		}
	};
	
	/**
	 * wifi 事件状态接口
	 * ------------------------------------------------------
	 * 功能描述：
	 * 		通过wifi的命令驱动状态的转换及执行对应的操作
	 * ------------------------------------------------------
	 * @author Little
	 * created at guangzhou, 2015/10/20 14:30
	 */
	public interface StateInterface {
		public void handleCommand(int command, Intent intent);
		public void execute();
	}
	
	/**
	 * wifi 的状态控制器
	 * ------------------------------------------------------
	 * 功能描述：
	 * 		控制当前wifi的状态控制，通过系统的wifi命令作驱动，进行状态间的转换
	 * ------------------------------------------------------
	 * @author Little
	 * created at guangzhou, 2015/10/20 14:30
	 */
	class WifiStateController {
		
		public final int TYPE_READY = 0;
		public final int TYPE_CONNECT = 1;
		public final int TYPE_CHECKOUT = 2;
		public StateInterface stateInterface;
		
		public void initState(int type) {
			switch(type) {
			case TYPE_READY:
				stateInterface = new StateReady();
				break;
			case TYPE_CONNECT:
				stateInterface = new StateConnect();
				break;
			case TYPE_CHECKOUT:
				stateInterface = new StateChcekOut();
				break;
			}
			if(stateInterface!=null) {
				stateInterface.execute();
			}
		}
		
		public synchronized void handleCommand(int command, Intent intent) {
			if(stateInterface==null)return;
			stateInterface.handleCommand(command, intent);
			stateInterface.execute();
		}
		
		/**
		 * wifi连接等待状态类
		 * ---------------------
		 * 作用描述:
		 * 		等待主动连接wifi
		 * ---------------------
		 */
		class StateReady implements StateInterface {
			@Override
			public void handleCommand(int command, Intent intent) {
				switch(command) {
				case COMMAND_WIFI_DISABLED:
					stateInterface = new StateConnect();
					stateInterface.execute();
					break;
				case COMMAND_WIFI_ENABLE:
					/* 
					 * 在指定wifi已经连接的情况下，如果切换其他wifi，且设置了强制
					 * 则会重新连接到指定的wifi中去
					 */
					if(isCompulsoryConnected) {
						String currentWifiSSID = getCurrentWifiSSID(mContext);
						if (!(currentWifiSSID.equals("\""+mWifiName+"\"") || currentWifiSSID.equals(mWifiName))) {
							LogUtil.set("WifiClient", "wifi 温馨提示: 当前已连接的wifi不是指定wifi,当前连接的wifi的ssid是 " + 
									currentWifiSSID + ", 指定连接的wifi的ssid是 "+mWifiName).verbose();
							stateInterface = new StateConnect();
							stateInterface.execute();
						}
					}
					break;
				}
			}
			@Override
			public void execute() {
				LogUtil.set("WifiClient", "wifi 温馨提示: 当前wifi进入等待连接状态 ").verbose();
			}
		}
		
		/**
		 * wifi连接状态类
		 * ------------------
		 * 作用描述:
		 * 		主动连接wifi
		 * ------------------
		 */
		class StateConnect implements StateInterface {
			@Override
			public void handleCommand(int command, Intent intent) {
				switch(command) {
				case COMMAND_WIFI_DISABLED:
					break;
				case COMMAND_WIFI_ENABLE:
					mHandler.removeCallbacksAndMessages(null);
					stateInterface = new StateChcekOut();
					stateInterface.execute();
					break;
				}
			}
			@Override
			public void execute() {
				LogUtil.set("WifiClient", "wifi 温馨提示: 当前wifi进入自动连接状态 ").verbose();
				mHandler.post(new WifiAutoConnectRunner());
			}
		}
		
		/**
		 * wifi状态检查类
		 * ---------------------
		 * 作用描述:
		 * 		综合处理wifi的数据
		 * ---------------------
		 */
		class StateChcekOut implements StateInterface {
			@Override
			public void handleCommand(int command, Intent intent) {
			}
			
			@Override
			public void execute() {
				LogUtil.set("WifiClient", "wifi 温馨提示: 当前wifi进入checkout状态 ").verbose();
				if(!isCompulsoryConnected) {
					stateInterface = new StateReady();
					stateInterface.execute();
                	for(WifiStateCallback item : mWifiStateCallback) {
                		item.onConnectSuccessed(mContext, null);
                	}
				} else {
					String currentWifiSSID = getCurrentWifiSSID(mContext);
					if (currentWifiSSID.equals("\""+mWifiName+"\"") || currentWifiSSID.equals(mWifiName)) {
						LogUtil.set("WifiClient", "wifi 温馨提示: 当前已连接指定wifi,ssid是 "+currentWifiSSID).verbose();
						stateInterface = new StateReady();
						stateInterface.execute();
	                	for(WifiStateCallback item : mWifiStateCallback) {
	                		item.onConnectSuccessed(mContext, null);
	                	}
					} else {
						LogUtil.set("WifiClient", "wifi 温馨提示: 当前已连接的wifi不是指定wifi,当前连接的wifi的ssid是 " + 
								currentWifiSSID + ", 指定连接的wifi的ssid是 "+mWifiName).verbose();
						stateInterface = new StateConnect();
						stateInterface.execute();
	                	for(WifiStateCallback item : mWifiStateCallback) {
	                		item.onConnectFailed(mContext, null);
	                	}
					}
				}
			}
		}
	}

	/**
	 * wifi重连接口
	 * ----------------------------------
	 * 作用描述:
	 * 		每隔一段时间，主动连接设定好的wifi
	 * ----------------------------------
	 * @author LittleZi
	 * created at guangzhou, 2015/10/20 14:30
	 */
	class WifiAutoConnectRunner implements Runnable {
		@Override
		public void run() {
			mWifiController.openWifi(); 
			mWifiController.addNetwork(mWifiController.CreateWifiInfo(mWifiName, mWifiPassword, 3)); 
			mHandler.postDelayed(new WifiAutoConnectRunner(), 20000);
		}
	}
	
	public boolean isConnected(Intent intent) {
		Parcelable parcelableExtra = intent.getParcelableExtra(
		WifiManager.EXTRA_NETWORK_INFO);
		if (null != parcelableExtra) {
			NetworkInfo networkInfo = (NetworkInfo) parcelableExtra;
			State state = networkInfo.getState();
			boolean isConnected = state == State.CONNECTED ? true:false;
			showInfo("LB tips : " + getCurrentWifiSSID(mContext) + " is "+isConnected);
			return isConnected;
		}
		return false;
	}

	public static interface WifiStateCallback {
		public void onConnectSuccessed(Context context, Intent intent);
		public void onConnectFailed(Context context, Intent intent);
	}
	
}
