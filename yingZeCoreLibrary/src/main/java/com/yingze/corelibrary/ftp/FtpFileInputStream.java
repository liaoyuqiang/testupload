package com.yingze.corelibrary.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.yingze.corelibrary.ftp.FTPController.FtpClientCallback;

public class FtpFileInputStream extends FileInputStream {
	private int sum = 0;
	private int length;
	private FtpClientCallback mFtpClientCallback;
	
	public FtpFileInputStream(File file) throws FileNotFoundException {
		super(file);
		try {
			length = available();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setFtpClientCallback(FtpClientCallback ftpClientCallback) {
		mFtpClientCallback = ftpClientCallback;
	}
	
	@Override
	public int read(byte[] buffer, int byteOffset, int byteCount)
			throws IOException {
		int length = super.read(buffer, byteOffset, byteCount);
		printLog("read3", length);
		return length;
	}
	
	public void printLog(String type, int bufferSize) {
		if(bufferSize!=-1) {
			sum += bufferSize;
//			int precent = sum * 100 /length;
			mFtpClientCallback.onUploadProcess(bufferSize, sum, length);
//			LogUtil.set("文件读取流", type + "," + length + "," + bufferSize + ", "+ 
//					precent + "%").verbose();
		}
	}
	
}