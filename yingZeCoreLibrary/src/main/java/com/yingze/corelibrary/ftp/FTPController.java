package com.yingze.corelibrary.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.yingzecorelibrary.utils.LogUtil;

import android.util.Log;

public class FTPController {
	private String hostName;// 服务器名
	private int serverPort;// 端口
	private String userName;// 用户名
	private String password;// 密码
	private FTPClient ftpClient;// FTP连接

//	public FTPController() {
//		this.hostName = "192.168.1.101";
//		this.serverPort = 21;
//		this.userName = "admin";
//		this.password = "1234";
//		this.ftpClient = new FTPClient();
//	}

	public FTPController(String hostName, int port, String userName, String password) {
		this.hostName = hostName;
		this.serverPort = port;
		this.userName = userName;
		this.password = password;
		this.ftpClient = new FTPClient();
	}

	public FTPFile[] scanFtpFolder(String path) {
		FTPClient ftp = new FTPClient();
		FTPFile[] ftpFileArr = null;
		try {
			ftp.setControlEncoding("UTF-8");
			ftp.connect(hostName, serverPort);// 连接FTP服务器
			
			ftp.login(userName, password);// 登录
			int reply = ftp.getReplyCode();
			
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return null;
			}
			// 二进制文件支持
			ftp.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
			ftp.changeWorkingDirectory(path);	
			ftpFileArr = ftp.listFiles();
			ftp.logout();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		return ftpFileArr;
	}
	
	/**
	 * 调用此方法即可
	 * @param url
	 * @param port
	 * @param username
	 * @param password
	 * @param path
	 * @param filename
	 * @param input
	 * @return
	 */
	public boolean uploadFile(String path, String filename, FtpFileInputStream input,
			FtpClientCallback ftpClientCallback) {
		boolean success = false;
		input.setFtpClientCallback(ftpClientCallback);
		FTPClient ftp = new FTPClient();

		try {
			ftp.setControlEncoding("UTF-8");
			ftp.connect(hostName, serverPort);// 连接FTP服务器
			
			ftp.login(userName, password);// 登录
			int reply = ftp.getReplyCode();
			
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}

			// 二进制文件支持
			ftp.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
			ftp.changeWorkingDirectory(path);
			
			boolean result = ftp.storeFile(filename, input);
			if(result) {
				success = true;
			} else {
				success = false;
			}
			input.close();
			ftp.logout();
			ftpClientCallback.onFinish();
		} catch (IOException e) {
			e.printStackTrace();
			ftpClientCallback.onFail(e, "上传异常");
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		return success;
	}

	// -------------------------------------------------------文件上传方法------------------------------------------------

	/**
	 * 上传单个文件.
	 * 
	 * @param localFile
	 *            本地文件
	 * @param remotePath
	 *            FTP目录
	 * @param listener
	 *            监听器
	 * @throws IOException
	 */
	public void uploadSingleFile(File singleFile, String remotePath,
			UploadProgressListener listener) throws IOException {

		// 上传之前初始化
		this.uploadBeforeOperate(remotePath, listener);

		boolean flag;
		flag = uploadingSingle(singleFile, listener);
		if (flag) {
			listener.onUploadProgress(FtpStates.FTP_UPLOAD_SUCCESS, 0,
					singleFile);
		} else {
			listener.onUploadProgress(FtpStates.FTP_UPLOAD_FAIL, 0,
					singleFile);
		}

		// 上传完成之后关闭连接
		this.uploadAfterOperate(listener);
	}

	/**
	 * 上传多个文件.
	 * 
	 * @param localFile
	 *            本地文件
	 * @param remotePath
	 *            FTP目录
	 * @param listener
	 *            监听器
	 * @throws IOException
	 */
	public void uploadMultiFile(LinkedList<File> fileList, String remotePath,
			UploadProgressListener listener) throws IOException {

		// 上传之前初始化
		this.uploadBeforeOperate(remotePath, listener);

		boolean flag;

		for (File singleFile : fileList) {
			flag = uploadingSingle(singleFile, listener);
			if (flag) {
				listener.onUploadProgress(FtpStates.FTP_UPLOAD_SUCCESS, 0,
						singleFile);
			} else {
				listener.onUploadProgress(FtpStates.FTP_UPLOAD_FAIL, 0,
						singleFile);
			}
		}

		// 上传完成之后关闭连接
		this.uploadAfterOperate(listener);
	}

	/**
	 * 上传单个文件.
	 * 
	 * @param localFile
	 *            本地文件
	 * @return true上传成功, false上传失败
	 * @throws IOException
	 */
	private boolean uploadingSingle(File localFile,
			UploadProgressListener listener) throws IOException {
		boolean flag = true;
		// 不带进度的方式
		// // 创建输入流
		// InputStream inputStream = new FileInputStream(localFile);
		// // 上传单个文件
		// flag = ftpClient.storeFile(localFile.getName(), inputStream);
		// // 关闭文件流
		// inputStream.close();

		// 带有进度的方式
		// BufferedInputStream buffIn = new BufferedInputStream(
		// new FileInputStream(localFile));
		// ProgressInputStream progressInput = new ProgressInputStream(buffIn,
		// listener, localFile);
		// flag = ftpClient.storeFile(localFile.getName(), progressInput);
		// buffIn.close();
		FileInputStream stream = new FileInputStream(localFile);
		flag = ftpClient.storeFile(localFile.getName(), stream);
		stream.close();

		return flag;
	}

	/**
	 * 上传文件之前初始化相关参数
	 * 
	 * @param remotePath
	 *            FTP目录
	 * @param listener
	 *            监听器
	 * @throws IOException
	 */
	private void uploadBeforeOperate(String remotePath,
			UploadProgressListener listener) throws IOException {
		// 打开FTP服务
		try {
			this.openConnect();
			listener.onUploadProgress(FtpStates.FTP_CONNECT_SUCCESSS, 0, null);
		} catch (IOException e1) {
			e1.printStackTrace();
			listener.onUploadProgress(FtpStates.FTP_CONNECT_FAIL, 0, null);
			return;
		}

		// 设置模式
		ftpClient
				.setFileTransferMode(org.apache.commons.net.ftp.FTP.STREAM_TRANSFER_MODE);
		// FTP下创建文件夹
		boolean createRes = ftpClient.makeDirectory(remotePath);
		// 改变FTP目录
		boolean changeRes = ftpClient.changeWorkingDirectory(remotePath);

		// boolean changeParent = ftpClient.changeToParentDirectory();
		// 上传单个文件
		Log.e("ftp", "++++++++++++++++++++ : " + createRes + "," + changeRes);

		// Log.e("ftp", "++++++++++++++++++++ : " + changeParent);
	}

	/**
	 * 上传完成之后关闭连接
	 * 
	 * @param listener
	 * @throws IOException
	 */
	private void uploadAfterOperate(UploadProgressListener listener)
			throws IOException {
		this.closeConnect();
		listener.onUploadProgress(FtpStates.FTP_DISCONNECT_SUCCESS, 0, null);
	}

	/**
	 * 下载单个文件，可实现断点下载.
	 * 
	 * @param serverPath
	 *            Ftp目录及文件路径
	 * @param localPath
	 *            本地目录
	 * @param fileName
	 *            下载之后的文件名称
	 * @param listener
	 *            监听器
	 * @throws IOException
	 */
	public void downloadSingleFile(String serverPath, String localPath,
			String fileName, DownLoadProgressListener listener)
			throws Exception {

		// 打开FTP服务
		try {
			this.openConnect();
			listener.onDownLoadProgress(FtpStates.FTP_CONNECT_SUCCESSS, 0,
					null);
		} catch (IOException e1) {
			e1.printStackTrace();
			listener.onDownLoadProgress(FtpStates.FTP_CONNECT_FAIL, 0, null);
			return;
		}

		// 先判断服务器文件是否存在
		FTPFile[] files = ftpClient.listFiles(serverPath);
		if (files.length == 0) {
			listener.onDownLoadProgress(FtpStates.FTP_FILE_NOTEXISTS, 0, null);
			return;
		}

		// 创建本地文件夹
		File mkFile = new File(localPath);
		if (!mkFile.exists()) {
			mkFile.mkdirs();
		}

		localPath = localPath + fileName;
		// 接着判断下载的文件是否能断点下载
		long serverSize = files[0].getSize(); // 获取远程文件的长度
		File localFile = new File(localPath);
		long localSize = 0;
		if (localFile.exists()) {
			localSize = localFile.length(); // 如果本地文件存在，获取本地文件的长度
			if (localSize >= serverSize) {
				File file = new File(localPath);
				file.delete();
			}
		}

		// 进度
		long step = serverSize / 100;
		long process = 0;
		long currentSize = 0;
		
		// 开始准备下载文件
		OutputStream out = new FileOutputStream(localFile, true);
		ftpClient.setRestartOffset(localSize);
		InputStream input = ftpClient.retrieveFileStream(serverPath);
		byte[] b = new byte[1024];
		int length = 0;
		
		while ((length = input.read(b)) != -1) {
			out.write(b, 0, length);
			currentSize = currentSize + length;
			
			if (currentSize / step != process) {
				process = currentSize / step;
				
				if (process % 5 == 0) { // 每隔%5的进度返回一次
					listener.onDownLoadProgress(FtpStates.FTP_DOWN_LOADING,
							process, null);
				}
				
			}
			
		}
		out.flush();
		out.close();
		input.close();

		// 此方法是来确保流处理完毕，如果没有此方法，可能会造成现程序死掉
		if (ftpClient.completePendingCommand()) {
			listener.onDownLoadProgress(FtpStates.FTP_DOWN_SUCCESS, 0,
					new File(localPath));
		} else {
			listener.onDownLoadProgress(FtpStates.FTP_DOWN_FAIL, 0, null);
		}

		// 下载完成之后关闭连接
		this.closeConnect();
		listener.onDownLoadProgress(FtpStates.FTP_DISCONNECT_SUCCESS, 0, null);
		return;
	}

	/**
	 * 删除Ftp下的文件.
	 * 
	 * @param serverPath
	 *            Ftp目录及文件路径
	 * @param listener
	 *            监听器
	 * @throws IOException
	 */
	public void deleteSingleFile(String serverPath,
			DeleteFileProgressListener listener) throws Exception {

		// 打开FTP服务
		try {
			this.openConnect();
			listener.onDeleteProgress(FtpStates.FTP_CONNECT_SUCCESSS);
		} catch (IOException e1) {
			e1.printStackTrace();
			listener.onDeleteProgress(FtpStates.FTP_CONNECT_FAIL);
			return;
		}

		// 先判断服务器文件是否存在
		FTPFile[] files = ftpClient.listFiles(serverPath);
		if (files.length == 0) {
			listener.onDeleteProgress(FtpStates.FTP_FILE_NOTEXISTS);
			return;
		}

		// 进行删除操作
		boolean flag = true;
		flag = ftpClient.deleteFile(serverPath);
		if (flag) {
			listener.onDeleteProgress(FtpStates.FTP_DELETEFILE_SUCCESS);
		} else {
			listener.onDeleteProgress(FtpStates.FTP_DELETEFILE_FAIL);
		}

		// 删除完成之后关闭连接
		this.closeConnect();
		listener.onDeleteProgress(FtpStates.FTP_DISCONNECT_SUCCESS);

		return;
	}

	/**
	 * 打开FTP服务.
	 * 
	 * @throws IOException
	 */
	public void openConnect() throws IOException {
		ftpClient.setBufferSize(1024);
		// 中文转码
		ftpClient.setControlEncoding("UTF-8");
		int reply; // 服务器响应值
		// 连接至服务器
		ftpClient.connect(hostName, serverPort);
		// 获取响应值
		reply = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			// 断开连接
			ftpClient.disconnect();
			throw new IOException("connect fail: " + reply);
		}
		// 登录到服务器
		boolean loginRes = ftpClient.login(userName, password);
		// 获取响应值
		reply = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			// 断开连接
			ftpClient.disconnect();
			throw new IOException("connect fail: " + reply);
		} else {
			// 获取登录信息
//			FTPClientConfig config = new FTPClientConfig(ftpClient
//					.getSystemType().split(" ")[0]);
//			config.setServerLanguageCode("zh");
			// ftpClient.setControlEncoding("UTF-8");
//			ftpClient.configure(config);
//			// 使用被动模式设为默认
//			ftpClient.enterLocalPassiveMode();
			// 二进制文件支持
			ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
		}
	}

	/**
	 * 关闭FTP服务.
	 * 
	 * @throws IOException
	 */
	public void closeConnect() throws IOException {
		if (ftpClient != null) {
			// 退出FTP
			ftpClient.logout();
			// 断开连接
			ftpClient.disconnect();
		}
	}

	/**
	 * 上传进度监听
	 */
	public interface UploadProgressListener {
		public void onUploadProgress(String currentStep, long uploadSize,
				File file);
	}

	/**
	 * 下载进度监听
	 */
	public interface DownLoadProgressListener {
		public void onDownLoadProgress(String currentStep, long downProcess,
				File file);
	}

	/**
	 * 文件删除监听
	 */
	public interface DeleteFileProgressListener {
		public void onDeleteProgress(String currentStep);
	}

	public static interface FtpClientCallback {
		public void onDownloadProcess(int readbufferLength, int downloadFileLength, int totalFileLength);
		public void onUploadProcess(int readbufferLength, int uploadFileLength, int totalFileLength);
		public void onFinish();
		public void onFail(Exception e, String message);
	}
	
}
