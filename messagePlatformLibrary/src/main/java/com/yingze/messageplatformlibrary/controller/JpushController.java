package com.yingze.messageplatformlibrary.controller;

import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.yingze.messageplatformlibrary.R;

import android.app.Application;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import cn.jpush.android.api.BasicPushNotificationBuilder;
import cn.jpush.android.api.CustomPushNotificationBuilder;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public final class JpushController implements JPushBroadcastReceiver.JPushListener {
	private static JpushController instance;
//	private JPushBroadcastReceiver mJPushBroadcastReceiver = new JPushBroadcastReceiver(this);
	private IntentFilter filter = new IntentFilter();
	private JPushBroadcastReceiver.JPushListener mJPushListener;
	
	public synchronized static JpushController getInstance() {
		if (instance == null) {
			instance = new JpushController();
		}
		return instance;
	}
	
	private JpushController() {
		mJPushListener = new JPushBroadcastReceiver.JPushListener(){
			@Override
			public void onJPushRegistrationIdCallback(String registionId) {
			}
			@Override
			public void onJPushReceiveCustomMessageCallback(
					String messageContent) {
			}
			@Override
			public void onJPushReceiveNotificationCallback(Intent intent) {
			}
			@Override
			public void onJPushUserOpenNotificationCallback(Intent intent) {
			}
		};
	}
	
	public void setJPushListener(JPushBroadcastReceiver.JPushListener jPushListener) {
		mJPushListener = jPushListener;
	}
	
	public void init(boolean debugMode,Application application){
		JPushInterface.setDebugMode(debugMode);
		JPushInterface.init(application);
	}
	
	/**
	 * 设置别名，返回0的时候代表设置有效
	 * @param application
	 * @param alias
	 * @return
	 */
	public void setAlias(Application application, String alias) {
		JPushInterface.setAlias(application, alias, new TagAliasCallback() {
			@Override
			public void gotResult(int resultCode, String arg1, Set<String> arg2) {
				if(resultCode == 0){
					Log.v("我的e校园", "我的e校园提示:别名设置成功");
				} else {
					Log.v("我的e校园", "我的e校园提示:别名设置错误，响应码是:"+resultCode);
				}
			}
		});
	}
	
	/**
	 * 设置别名，返回0代表设置有效
	 * @param context
	 * @param alias
	 */
	public void setAlias(Context context, String alias) {
		JPushInterface.setAliasAndTags(context, alias, null, new TagAliasCallback() {
			@Override
			public void gotResult(int resultCode, String arg1, Set<String> arg2) {
				if(resultCode == 0){
					Log.v("我的e校园", "++ 我的e校园提示:别名设置成功");
				} else {
					Log.v("我的e校园", "++ 我的e校园提示:别名设置错误，响应码是:"+resultCode);
				}
			}
		});
	}
	
	/**
	 * 配置filter
	 * @param context
	 */
	public void configFilter(Context context) {
		/*
		 * 用户注册SDK的intent
		 */
		filter.addAction("cn.jpush.android.intent.REGISTRATION");
		/*
		 * 用户注销SDK的intent
		 */
		filter.addAction("cn.jpush.android.intent.UNREGISTRATION");
		/*
		 * 用户接收SDK消息的intent
		 */
		filter.addAction("cn.jpush.android.intent.MESSAGE_RECEIVED");
		/*
		 * 用户接收SDK通知栏信息的intent
		 */
		filter.addAction("cn.jpush.android.intent.NOTIFICATION_RECEIVED");
		/*
		 * 用户打开自定义通知栏的intent
		 */
		filter.addAction("cn.jpush.android.intent.NOTIFICATION_OPENED");
		/*
		 * 用户接受Rich Push Javascript 回调函数的intent
		 */
		filter.addAction("cn.jpush.android.intent.ACTION_RICHPUSH_CALLBACK");
		/*
		 * 接收网络变化 连接/断开 since 1.6.3
		 */
		filter.addAction("cn.jpush.android.intent.CONNECTION");
		/*
		 * 通过以包名的方式设置指定的Category
		 */
		filter.addCategory(context.getPackageName());
		
//		filter.addCategory("com.yingze.maintenanceplatform.build");
		
//		filter.addDataScheme(scheme)
	}
	
	/**
	 *设置通知提示方式 - 基础属性
	 */
	private void setStyleBasic(Context context){
		BasicPushNotificationBuilder builder = new BasicPushNotificationBuilder(context);
//		builder.statusBarDrawable = R.drawable.ic_launcher;
		builder.notificationFlags = Notification.FLAG_AUTO_CANCEL;  //设置为点击后自动消失
		builder.notificationDefaults = Notification.DEFAULT_SOUND;  //设置为铃声（ Notification.DEFAULT_SOUND）或者震动（ Notification.DEFAULT_VIBRATE）  
		JPushInterface.setPushNotificationBuilder(1, builder);
		Toast.makeText(context, "Basic Builder - 1", Toast.LENGTH_SHORT).show();
	}
	
	
	/**
	 *设置通知栏样式 - 定义通知栏Layout
	 */
	private void setStyleCustom(Context context) {
		CustomPushNotificationBuilder builder = new CustomPushNotificationBuilder(
				context,R.layout.layout_jpush_notication, R.id.icon, R.id.title, R.id.text);
//		builder.layoutIconDrawable = R.drawable.ic_launcher;
		builder.developerArg0 = "developerArg2";
		JPushInterface.setPushNotificationBuilder(2, builder);
		Toast.makeText(context,"Custom Builder - 2", Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * 注册回调监听接口
	 * @param context
	 */
	public void registJPushReceiver(Context context) {
//		configFilter(context);
//		context.registerReceiver(mJPushBroadcastReceiver, filter);
	}
	
	/**
	 * 注销回调监听接口
	 * @param context
	 */
	public void unRegistJPushReceiver(Context context) {
//		context.unregisterReceiver(mJPushBroadcastReceiver);
	}

	@Override
	public void onJPushRegistrationIdCallback(String registionId) {
		mJPushListener.onJPushRegistrationIdCallback(registionId);
	}

	@Override
	public void onJPushReceiveCustomMessageCallback(String messageContent) {
		mJPushListener.onJPushReceiveCustomMessageCallback(messageContent);
	}

	@Override
	public void onJPushReceiveNotificationCallback(Intent intent) {
		mJPushListener.onJPushReceiveNotificationCallback(intent);
	}

	@Override
	public void onJPushUserOpenNotificationCallback(Intent intent) {
		mJPushListener.onJPushUserOpenNotificationCallback(intent);
	}
	
}
