//package com.yingze.messageplatformlibrary.activity;
//
//import android.content.Context;
//import android.telephony.TelephonyManager;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import cn.smssdk.EventHandler;
//import cn.smssdk.SMSSDK;
//import com.yingze.businessframework.activity.AppActivity;
//import com.yingze.businessframework.controller.TitleController;
//import com.yingze.messageplatformlibrary.R;
//import com.yingzecorelibrary.utils.ToastUtil;
//
//public class MessagePlatformActivity extends AppActivity implements View.OnClickListener {
//	// 默认使用中国区号
//	private static final String DEFAULT_COUNTRY_ID = "42";
//	
//	private TextView tv_send_sms_test;
//	private Button btn_get_verification_code_test;
//	private EditText edit_verification_code_test;
//	private Button btn_submit_verification_code_test;
//
//	private EventHandler eh;
//	
//	@Override
//	protected void onDestroy() {
//		SMSSDK.unregisterEventHandler(eh);
//		super.onDestroy();
//	}
//
//	@Override
//	public void initWindowFeatureOnCreate() {
//		SMSSDK.initSDK(this, "fb497f1493a2", "40eec0ea8169eea9e24ba072769d2d47");
//		eh = new EventHandler() {
//			@Override
//			public void afterEvent(int event, int result, Object data) {
//				if (result == SMSSDK.RESULT_COMPLETE) {// 回调完成
//					if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {// 提交验证码成功
//						ToastUtil.set(MessagePlatformActivity.this, "提交验证码成功"+String.valueOf(data)).show();
//					} else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {// 获取验证码成功
//						ToastUtil.set(MessagePlatformActivity.this, "获取验证码成功"+String.valueOf(data)).show();
//					} else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {// 返回支持发送验证码的国家列表
//						
//					}
//				} else {
//					((Throwable) data).printStackTrace();
//				}
//			}
//		};
//		SMSSDK.registerEventHandler(eh); // 注册短信回调
//	}
//
//	@Override
//	public void initResourceViewOnCreate(View contentView) {
//		tv_send_sms_test = (TextView) findViewById(R.id.tv_send_sms_test);
//		btn_get_verification_code_test = (Button) findViewById(R.id.btn_get_verification_code_test);
//		edit_verification_code_test =  (EditText) findViewById(R.id.edit_verification_code_test);
//		btn_submit_verification_code_test = (Button) findViewById(R.id.btn_submit_verification_code_test);
//	}
//
//	@Override
//	public void initTransactionOnCreate() {
//		btn_get_verification_code_test.setOnClickListener(this);
//		btn_submit_verification_code_test.setOnClickListener(this);
//	}
//
//	@Override
//	public void initListenerOnCreate() {
//
//	}
//
//	@Override
//	public int getLayoutResID() {
//		return R.layout.activity_main;
//	}
//
//	@Override
//	public TitleController createTitleController() {
//		return new TitleController() {
//			@Override
//			public void initTransactionOnCreate() {
//
//			}
//
//			@Override
//			public void initListenerOnCreate() {
//
//			}
//			
//		};
//	}
//	
//	@Override
//	public void onClick(View v) {
//		if(v.getId() == R.id.btn_get_verification_code_test) {
//			SMSSDK.getVerificationCode("86", "13632456468");
//		} else if (v.getId() == R.id.btn_submit_verification_code_test) {
//			SMSSDK.submitVerificationCode("86", "13632456468",
//					edit_verification_code_test.getText().toString());
//		}
//	}
//
//	private String[] getCurrentCountry() {
//		String mcc = getMCC();
//		String[] country = null;
//		if (!(mcc == null || mcc.length()==0)) {
//			country = SMSSDK.getCountryByMCC(mcc);
//		}
//		if (country == null) {
//			Log.w("SMSSDK", "no country found by MCC: " + mcc);
//			country = SMSSDK.getCountry(DEFAULT_COUNTRY_ID);
//		}
//		return country;
//	}
//
//	private String getMCC() {
//		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//		// 返回当前手机注册的网络运营商所在国家的MCC+MNC. 如果没注册到网络就为空.
//		String networkOperator = tm.getNetworkOperator();
//		if (!TextUtils.isEmpty(networkOperator)) {
//			return networkOperator;
//		}
//		// 返回SIM卡运营商所在国家的MCC+MNC. 5位或6位. 如果没有SIM卡返回空
//		return tm.getSimOperator();
//	}
//
//}
