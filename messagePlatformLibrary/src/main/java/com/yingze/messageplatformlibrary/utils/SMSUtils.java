package com.yingze.messageplatformlibrary.utils;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

public final class SMSUtils {
	private EventHandler eh;
	private static SMSUtils instance;
	private SMSCallback mSMSCallback;
	
	public static SMSUtils getInstance() {
		synchronized (SMSUtils.class) {
			if(instance == null) {
				instance = new SMSUtils();
			}
			return instance;
		}
	}
	
	public void setSMSCallback(SMSCallback SMSCallback) {
		this.mSMSCallback = SMSCallback;
	}
	
	public static void onDestroy() {
		if(instance!=null) {
			SMSSDK.unregisterEventHandler(instance.eh);
		}
	}
	
	public void init(final Context context, String appKey, String appsecret) {
		SMSSDK.initSDK(context, appKey, appsecret);
		eh = new EventHandler() {
			@Override
			public void afterEvent(int event, int result, Object data) {
//				Looper.prepare();

				if (result == SMSSDK.RESULT_COMPLETE) {
					/*
					 * 回调完成
					 */
					if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
						/*
						 * 返回支持发送验证码的国家列表
						 */
					} else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
						/*
						 * 获取验证码成功
						 */
						Boolean isIntelligentCheck = (Boolean)data;
						if(isIntelligentCheck) {
							/*
							 * 智能校验
							 */
							mSMSCallback.onIntelligentCheck();
						} else {
							/*
							 * 下发短信
							 */
							mSMSCallback.onSuccessGetVerificationCode();
						}
					} else if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
						/*
						 * 提交验证码成功(校验验证码)
						 */
						mSMSCallback.onSuccessSubmitVerificationCode();
					}
				} else {
					((Throwable) data).printStackTrace();
					mSMSCallback.onFailSubmitVerificationCode(String.valueOf(data));
				
				}
			}
		};
		SMSSDK.registerEventHandler(eh); // 注册短信回调
	}
	
	/**
	 * 获取验证码
	 * @param phoneNum 接收短信的电话号码
	 */
//	public void getVerificationCode(Context context,String phoneNum) {
//		SMSSDK.getVerificationCode("86", phoneNum);
//		Toast.makeText(context, "phoneNum==>"+phoneNum, Toast.LENGTH_LONG).show();
//	}
	public void getVerificationCode(String phoneNum) {
		SMSSDK.getVerificationCode("86", phoneNum);
//		Toast.makeText(context, "phoneNum==>"+phoneNum, Toast.LENGTH_LONG).show();
	}
	
	public void submitVerificationCode(String phoneNum, String smsCode) {
		SMSSDK.submitVerificationCode("86", phoneNum, smsCode);
	}

	public static interface SMSCallback {
		public void onSuccessGetVerificationCode();
		public void onFailGetVerificationCode();
		public void onSuccessSubmitVerificationCode();
		public void onFailSubmitVerificationCode(String content);
		public void onIntelligentCheck();
	}
}
