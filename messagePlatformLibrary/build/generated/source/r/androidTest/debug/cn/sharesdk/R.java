/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package cn.sharesdk;

public final class R {
	public static final class drawable {
		public static final int ssdk_auth_title_back = 0x7f020004;
		public static final int ssdk_back_arr = 0x7f020005;
		public static final int ssdk_logo = 0x7f020006;
		public static final int ssdk_title_div = 0x7f020033;
	}
	public static final class string {
		public static final int ssdk_alipay = 0x7f06005d;
		public static final int ssdk_alipay_client_inavailable = 0x7f06005e;
		public static final int ssdk_baidutieba = 0x7f0600b1;
		public static final int ssdk_baidutieba_client_inavailable = 0x7f0600b2;
		public static final int ssdk_bluetooth = 0x7f06005f;
		public static final int ssdk_douban = 0x7f060060;
		public static final int ssdk_dropbox = 0x7f060061;
		public static final int ssdk_email = 0x7f060062;
		public static final int ssdk_evernote = 0x7f060063;
		public static final int ssdk_facebook = 0x7f060064;
		public static final int ssdk_facebookmessenger = 0x7f060065;
		public static final int ssdk_facebookmessenger_client_inavailable = 0x7f060066;
		public static final int ssdk_flickr = 0x7f060067;
		public static final int ssdk_foursquare = 0x7f060068;
		public static final int ssdk_google_plus_client_inavailable = 0x7f060069;
		public static final int ssdk_googleplus = 0x7f06006a;
		public static final int ssdk_instagram = 0x7f06006b;
		public static final int ssdk_instagram_client_inavailable = 0x7f06006c;
		public static final int ssdk_instapager_email_or_password_incorrect = 0x7f06006d;
		public static final int ssdk_instapager_login_html = 0x7f0600b3;
		public static final int ssdk_instapaper = 0x7f06006e;
		public static final int ssdk_instapaper_email = 0x7f06006f;
		public static final int ssdk_instapaper_login = 0x7f060070;
		public static final int ssdk_instapaper_logining = 0x7f060071;
		public static final int ssdk_instapaper_pwd = 0x7f060072;
		public static final int ssdk_kaixin = 0x7f060073;
		public static final int ssdk_kakaostory = 0x7f060074;
		public static final int ssdk_kakaostory_client_inavailable = 0x7f060075;
		public static final int ssdk_kakaotalk = 0x7f060076;
		public static final int ssdk_kakaotalk_client_inavailable = 0x7f060077;
		public static final int ssdk_laiwang = 0x7f060078;
		public static final int ssdk_laiwang_client_inavailable = 0x7f060079;
		public static final int ssdk_laiwangmoments = 0x7f06007a;
		public static final int ssdk_line = 0x7f06007b;
		public static final int ssdk_line_client_inavailable = 0x7f06007c;
		public static final int ssdk_linkedin = 0x7f06007d;
		public static final int ssdk_mingdao = 0x7f06007e;
		public static final int ssdk_mingdao_share_content = 0x7f06007f;
		public static final int ssdk_neteasemicroblog = 0x7f060080;
		public static final int ssdk_pinterest = 0x7f06008d;
		public static final int ssdk_pinterest_client_inavailable = 0x7f06008e;
		public static final int ssdk_pocket = 0x7f06008f;
		public static final int ssdk_qq = 0x7f060090;
		public static final int ssdk_qq_client_inavailable = 0x7f060091;
		public static final int ssdk_qzone = 0x7f060092;
		public static final int ssdk_renren = 0x7f060093;
		public static final int ssdk_share_to_baidutieba = 0x7f0600b4;
		public static final int ssdk_share_to_mingdao = 0x7f060094;
		public static final int ssdk_share_to_qq = 0x7f060095;
		public static final int ssdk_share_to_qzone = 0x7f060096;
		public static final int ssdk_share_to_qzone_default = 0x7f060097;
		public static final int ssdk_shortmessage = 0x7f060098;
		public static final int ssdk_sinaweibo = 0x7f060099;
		public static final int ssdk_sohumicroblog = 0x7f06009a;
		public static final int ssdk_sohusuishenkan = 0x7f06009b;
		public static final int ssdk_tencentweibo = 0x7f06009c;
		public static final int ssdk_tumblr = 0x7f06009d;
		public static final int ssdk_twitter = 0x7f06009e;
		public static final int ssdk_use_login_button = 0x7f06009f;
		public static final int ssdk_vkontakte = 0x7f0600a0;
		public static final int ssdk_website = 0x7f0600a1;
		public static final int ssdk_wechat = 0x7f0600a2;
		public static final int ssdk_wechat_client_inavailable = 0x7f0600a3;
		public static final int ssdk_wechatfavorite = 0x7f0600a4;
		public static final int ssdk_wechatmoments = 0x7f0600a5;
		public static final int ssdk_weibo_oauth_regiseter = 0x7f0600a6;
		public static final int ssdk_weibo_upload_content = 0x7f0600a7;
		public static final int ssdk_whatsapp = 0x7f0600a8;
		public static final int ssdk_whatsapp_client_inavailable = 0x7f0600a9;
		public static final int ssdk_yixin = 0x7f0600aa;
		public static final int ssdk_yixin_client_inavailable = 0x7f0600ab;
		public static final int ssdk_yixinmoments = 0x7f0600ac;
		public static final int ssdk_youdao = 0x7f0600ad;
	}
}
