package com.sp.activity;

import android.app.Activity;
import android.os.Bundle;

import com.sp.ui.inject.SPResource;

public abstract class SPActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		onPreActivityCreate();
		super.onCreate(savedInstanceState);
		onAfterActivityCreate();
		initActivity();
	}

	private void initActivity() {
		initResource();
	}

	protected void onPreActivityCreate() {

	}

	protected void onAfterActivityCreate() {

	}
	
	private void initResource() {
		SPResource spResource = SPResource.getInstance();
		spResource.injectLayout(this);
		spResource.injectAll(this);
	}
	
}
