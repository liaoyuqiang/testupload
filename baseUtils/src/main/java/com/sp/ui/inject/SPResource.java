package com.sp.ui.inject;

import java.lang.reflect.Field;

import com.sp.ui.inject.annotated.SPResourceLayout;
import com.sp.ui.inject.annotated.SPResourceView;

import android.app.Activity;

public class SPResource {

	private static SPResource instance;

	public SPResource() {
	}

	public static SPResource getInstance() {
		if (instance == null) {
			instance = new SPResource();
		}
		return instance;
	}

	public void injectLayout(Activity activity) {
		Field[] fields = activity.getClass().getDeclaredFields();
		if (fields != null && fields.length > 0) {
			for (Field field : fields) {
				if (field.isAnnotationPresent(SPResourceLayout.class)) {
					injectLayout(activity, field);
				}
			}
		}
	}

	public void injectLayout(Activity activity, Field field) {
		if (field.isAnnotationPresent(SPResourceLayout.class)) {
			SPResourceLayout spl = field.getAnnotation(SPResourceLayout.class);
			int layoutId = spl.value();
			try {
				activity.setContentView(layoutId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void injectAll(Activity activity) {
		Field[] fields = activity.getClass().getDeclaredFields();
		if (fields != null && fields.length > 0) {
			for (Field field : fields) {
				if (field.isAnnotationPresent(SPResourceView.class)) {
					injectView(activity, field);
				}
			}
		}
	}

	private void injectView(Activity activity, Field field) {
		if (field.isAnnotationPresent(SPResourceView.class)) {
			SPResourceView spResourceView = field
					.getAnnotation(SPResourceView.class);
			int viewId = spResourceView.value();
			try {
				field.setAccessible(true);
				field.set(activity, activity.findViewById(viewId));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
