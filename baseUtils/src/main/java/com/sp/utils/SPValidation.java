package com.sp.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextUtils;

/**
 * 提供一些常用的校验
 * 
 * @author Shawn Poon
 * 
 */
public class SPValidation {

	/**
	 * 验证手机号码
	 * 
	 * @param number
	 * @return
	 */
	public static boolean isMobile(String number) {
		boolean flag = false;
		try {
//			Pattern p = Pattern
//					.compile("^((13[0-9])|(15[0-9])|(18[0-9]))\\d{8}$");
			Pattern p = Pattern
					.compile("[1]\\d{10}");
			Matcher m = p.matcher(number);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 检验EMAIL
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 检验是否数字
	 * 
	 * @param number
	 * @return
	 */
	public static boolean isNumericIn5(String number) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("^[0-9]{5}$");
			Matcher m = p.matcher(number);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		if (TextUtils.isEmpty(str)) {
			return false;
		}
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		//注意
		Pattern pattern1 = Pattern.compile("^0\\d*$");
		Matcher is0 = pattern1.matcher(str);
		if(is0.matches()){
			return false;
		}
		return true;
	}

	/**
	 * 校验是否中文字
	 * 
	 * @param chineseStr
	 * @return
	 */
	public boolean isChinese(String chineseStr) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("^[\u4e00-\u9fa5]{0,}$");
			Matcher m = p.matcher(chineseStr);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 校验是否有%&',;@#=?$\这些符号
	 * 
	 * @param str
	 * @return
	 */
	public  static boolean isHavingSymbol(String str) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("[^%&',;!#=?$\\x22]+");
			Matcher m = p.matcher(str);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 校验是否身份证
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isIdNumber(String str) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("^\\d{15}|\\d{18}|\\d{17}[\\d,x,X]{1}$");
			Matcher m = p.matcher(str);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	/**
	 * 校验是否车牌号
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isCarNumber(String str) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("^[京,津,冀,晋,蒙,辽,吉,黑,沪,苏,浙,皖,闽,赣,鲁,豫,鄂,湘,粤,桂,琼,渝,川,贵,云,藏,陕,甘,青,宁,新]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fa5]$");
			Matcher m = p.matcher(str);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	/**
	 * 检测智能家居登录用户名
	 * @param str
	 * @return
	 */
	public static boolean checkUserName(String str){
		Pattern pattern = Pattern.compile("[a-z0-9A-Z_]{6,18}");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}
	
	/**
	 * 检测智能家居登录密码
	 * @param str
	 * @return
	 */
	public static boolean checkPassword(String str){
		Pattern pattern = Pattern.compile("[a-z0-9A-Z_]{6,18}");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}
	
	/**
	 * 验证手机号码
	 * 
	 * @param number
	 * @return
	 */
	public static boolean checkTelephone(String str) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
			Matcher m = p.matcher(str);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	/**
	 * 检验EMAIL
	 * 
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String str) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(str);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	/**
	 * 从位数检验是否是IP地址
	 * 
	 * @param address
	 * @return
	 */
	public static boolean isIPAddress(String address) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
			Matcher m = p.matcher(address);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	/**
	 * 判断给定字符串是否空白串。 空白串是指由空格、制表符、回车符、换行符组成的字符串 若输入字符串为null或空字符串，返回true
	 * 
	 * @param input
	 * @return boolean 若输入字符串为null或空字符串，返回true
	 */
	public static boolean isEmpty(String input) {
		if (input == null || "".equals(input)) {
			return true;
		}
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
				return false;
			}
		}
		return true;
	}
	
}
