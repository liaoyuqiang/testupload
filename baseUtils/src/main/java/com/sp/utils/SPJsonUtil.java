package com.sp.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * JSON澶勭悊甯哥敤鏂规硶
 * 
 * @author Shawn Poon
 * 
 */
public class SPJsonUtil {

	private SPJsonUtil() {
	}

	protected static Object parseString(String str) throws JSONException {
		Object result = null;
		str = str.trim();
		if (str.startsWith("{") || str.startsWith("[")) {
			result = new JSONTokener(str).nextValue();
		}
		if (result == null) {
			result = str;
		}
		return result;
	}

	public static JSONArray getJSONArray(String str) {
		try {
			Object result = parseString(str);
			if (result instanceof JSONArray) {
				return (JSONArray) result;
			} else {
				return null;
			}
		} catch (JSONException e) {
			return null;
		}
	}

	public static JSONObject getJSONObject(String str) {
		try {
			Object result = parseString(str);
			if (result instanceof JSONObject) {
				return (JSONObject) result;
			} else {
				return null;
			}
		} catch (JSONException e) {
			return null;
		}
	}

	public static boolean isJSONArray(String str) {
		try {
			Object result = parseString(str);
			if (result instanceof JSONArray) {
				return true;
			} else {
				return false;
			}
		} catch (JSONException e) {
			return false;
		}
	}

	public static boolean isJSONObject(String str) {
		try {
			Object result = parseString(str);
			if (result instanceof JSONObject) {
				return true;
			} else {
				return false;
			}
		} catch (JSONException e) {
			return false;
		}
	}

}
