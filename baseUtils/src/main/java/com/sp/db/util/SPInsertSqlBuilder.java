package com.sp.db.util;

import java.lang.reflect.Field;

import org.apache.http.NameValuePair;

import com.sp.db.SPDbException;
import com.sp.db.annotation.SPPrimaryKey;
import com.sp.db.entity.SPArrayList;
import com.sp.utils.SPValidation;

public class SPInsertSqlBuilder extends SPSqlBuilder {

	@Override
	public void onPreGetStatement() throws SPDbException, IllegalArgumentException, IllegalAccessException {
		if (getUpdateFields() == null) {
			setUpdateFields(getFieldsAndValue(entity));
		}
		super.onPreGetStatement();
	}

	/**
	 * 从实体加载,更新的数据
	 * 
	 * @return
	 * @throws TADBException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static SPArrayList getFieldsAndValue(Object entity) throws SPDbException, IllegalArgumentException, IllegalAccessException {
		SPArrayList arrayList = new SPArrayList();
		if (entity == null) {
			throw new SPDbException("没有加载实体类！");
		}
		Class<?> clazz = entity.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (!SPDbUtils.isTransient(field)) {
				if (SPDbUtils.isBaseDateType(field)) {
					SPPrimaryKey annotation = field.getAnnotation(SPPrimaryKey.class);
					if (annotation != null && annotation.autoIncrement()) {

					} else {
						String columnName = SPDbUtils.getColumnByField(field);
						field.setAccessible(true);
						arrayList.add((columnName != null && !columnName.equals("")) ? columnName : field.getName(), field.get(entity) == null ? null
								: field.get(entity).toString());
					}
				}
			}
		}
		return arrayList;
	}

	@Override
	public String buildSql() throws SPDbException, IllegalArgumentException, IllegalAccessException {
		StringBuilder columns = new StringBuilder(256);
		StringBuilder values = new StringBuilder(256);
		columns.append("INSERT INTO ");
		columns.append(tableName).append(" (");
		values.append("(");
		SPArrayList updateFields = getUpdateFields();
		if (updateFields != null) {
			for (int i = 0; i < updateFields.size(); i++) {
				NameValuePair nameValuePair = updateFields.get(i);
				columns.append(nameValuePair.getName());
				values.append(SPValidation.isNumeric(nameValuePair.getValue() != null ? nameValuePair.getValue().toString() : "") ? nameValuePair
						.getValue() : "'" + nameValuePair.getValue() + "'");
				if (i + 1 < updateFields.size()) {
					columns.append(", ");
					values.append(", ");
				}
			}
		} else {
			throw new SPDbException("插入数据有误！");
		}
		columns.append(") values ");
		values.append(")");
		columns.append(values);
		return columns.toString();
	}

}
