package com.sp.db.util;

import java.lang.reflect.Field;

import com.sp.db.SPDbException;
import com.sp.db.entity.SPArrayList;

public class SPDeleteSqlBuilder extends SPSqlBuilder {

	@Override
	public String buildSql() throws SPDbException, IllegalArgumentException, IllegalAccessException {
		StringBuilder stringBuilder = new StringBuilder(256);
		stringBuilder.append("DELETE FROM ");
		stringBuilder.append(tableName);
		if (entity == null) {
			stringBuilder.append(buildConditionString());
		} else {
			stringBuilder.append(buildWhere(buildWhere(this.entity)));
		}
		return stringBuilder.toString();
	}

	/**
	 * 创建Where语句
	 * 
	 * @param entity
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws SPDbException
	 */
	public SPArrayList buildWhere(Object entity) throws IllegalArgumentException, IllegalAccessException, SPDbException {
		Class<?> clazz = entity.getClass();
		SPArrayList whereArrayList = new SPArrayList();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if (!SPDbUtils.isTransient(field)) {
				if (SPDbUtils.isBaseDateType(field)) {
					// 如果ID不是自动增加的
					if (!SPDbUtils.isAutoIncrement(field)) {
						String columnName = SPDbUtils.getColumnByField(field);
						if (null != field.get(entity) && field.get(entity).toString().length() > 0) {
							whereArrayList.add((columnName != null && !columnName.equals("")) ? columnName : field
									.getName(), field.get(entity).toString());
						}
					}
				}
			}
		}
		if (whereArrayList.isEmpty()) {
			throw new SPDbException("不能创建Where条件，语句");
		}
		return whereArrayList;
	}

}
