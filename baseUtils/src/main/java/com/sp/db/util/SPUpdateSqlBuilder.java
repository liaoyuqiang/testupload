package com.sp.db.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import org.apache.http.NameValuePair;

import com.sp.db.SPDbException;
import com.sp.db.annotation.SPPrimaryKey;
import com.sp.db.entity.SPArrayList;
import com.sp.utils.SPStringUtil;
import com.sp.utils.SPValidation;

public class SPUpdateSqlBuilder extends SPSqlBuilder {

	@Override
	public void onPreGetStatement() throws SPDbException, IllegalArgumentException, IllegalAccessException {
		if (getUpdateFields() == null) {
			setUpdateFields(getFieldsAndValue(entity));
		}
		super.onPreGetStatement();
	}

	@Override
	public String buildSql() throws SPDbException, IllegalArgumentException, IllegalAccessException {
		StringBuilder stringBuilder = new StringBuilder(256);
		stringBuilder.append("UPDATE ");
		stringBuilder.append(tableName).append(" SET ");

		SPArrayList needUpdate = getUpdateFields();
		for (int i = 0; i < needUpdate.size(); i++) {
			NameValuePair nameValuePair = needUpdate.get(i);
			stringBuilder.append(nameValuePair.getName()).append(" = ")
					.append(SPValidation.isNumeric(nameValuePair.getValue().toString()) ? nameValuePair.getValue() : "'" + nameValuePair.getValue()
							+ "'");
			if (i + 1 < needUpdate.size()) {
				stringBuilder.append(", ");
			}
		}
		if (!SPStringUtil.isEmpty(this.where)) {
			stringBuilder.append(buildConditionString());
		} else {
			stringBuilder.append(buildWhere(buildWhere(this.entity)));
		}
		return stringBuilder.toString();
	}

	/**
	 * 创建Where语句
	 * 
	 * @param entity
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws SPDbException
	 */
	public SPArrayList buildWhere(Object entity) throws IllegalArgumentException, IllegalAccessException, SPDbException {
		Class<?> clazz = entity.getClass();
		SPArrayList whereArrayList = new SPArrayList();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if (!SPDbUtils.isTransient(field)) {
				if (SPDbUtils.isBaseDateType(field)) {
					Annotation annotation = field.getAnnotation(SPPrimaryKey.class);
					if (annotation != null) {
						String columnName = SPDbUtils.getColumnByField(field);
						whereArrayList.add((columnName != null && !columnName.equals("")) ? columnName : field.getName(), field.get(entity)
								.toString());
					}
				}
			}
		}
		if (whereArrayList.isEmpty()) {
			throw new SPDbException("不能创建Where条件，语句");
		}
		return whereArrayList;
	}

	/**
	 * 从实体加载,更新的数据
	 * 
	 * @return
	 * @throws SPDbException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static SPArrayList getFieldsAndValue(Object entity) throws SPDbException, IllegalArgumentException, IllegalAccessException {
		SPArrayList arrayList = new SPArrayList();
		if (entity == null) {
			throw new SPDbException("没有加载实体类！");
		}
		Class<?> clazz = entity.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (!SPDbUtils.isTransient(field)) {
				if (SPDbUtils.isBaseDateType(field)) {
					SPPrimaryKey annotation = field.getAnnotation(SPPrimaryKey.class);
					if (annotation == null || !annotation.autoIncrement()) {
						String columnName = SPDbUtils.getColumnByField(field);
						field.setAccessible(true);
						arrayList.add((columnName != null && !columnName.equals("")) ? columnName : field.getName(),
								field.get(entity) == null ? null : field.get(entity).toString());
					}
				}
			}
		}
		return arrayList;
	}

}
