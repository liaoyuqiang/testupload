package com.sp.db.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;

import com.sp.db.SPDbException;
import com.sp.db.entity.SPPKProperyEntity;
import com.sp.db.entity.SPProperyEntity;
import com.sp.db.entity.SPTableInfoEntity;

public class SPTableInfofactory {
	/**
	 * 表名为键，表信息为值的HashMap
	 */
	private static final HashMap<String, SPTableInfoEntity> tableInfoEntityMap = new HashMap<String, SPTableInfoEntity>();

	public SPTableInfofactory() {
	}

	private static SPTableInfofactory instance;

	/**
	 * 获得数据库表工厂
	 * 
	 * @return 数据库表工厂
	 */
	public static SPTableInfofactory getInstance() {
		if (instance == null) {
			instance = new SPTableInfofactory();
		}
		return instance;
	}

	/**
	 * 获得表信息
	 * 
	 * @param clazz
	 *            实体类型
	 * @return 表信息
	 * @throws TADBException
	 */
	public SPTableInfoEntity getTableInfoEntity(Class<?> clazz) throws SPDbException {
		if (clazz == null) {
			throw new SPDbException("表信息获取失败，应为class为null");
		}
		SPTableInfoEntity tableInfoEntity = tableInfoEntityMap.get(clazz.getName());
		if (tableInfoEntity == null) {
			tableInfoEntity = new SPTableInfoEntity();
			tableInfoEntity.setTableName(SPDbUtils.getTableName(clazz));
			tableInfoEntity.setClassName(clazz.getName());
			Field idField = SPDbUtils.getPrimaryKeyField(clazz);
			if (idField != null) {
				SPPKProperyEntity pkProperyEntity = new SPPKProperyEntity();
				pkProperyEntity.setColumnName(SPDbUtils.getColumnByField(idField));
				pkProperyEntity.setName(idField.getName());
				pkProperyEntity.setType(idField.getType());
				pkProperyEntity.setAutoIncrement(SPDbUtils.isAutoIncrement(idField));
				tableInfoEntity.setPkProperyEntity(pkProperyEntity);
			} else {
				tableInfoEntity.setPkProperyEntity(null);
			}
			List<SPProperyEntity> propertyList = SPDbUtils.getPropertyList(clazz);
			if (propertyList != null) {
				tableInfoEntity.setPropertieArrayList(propertyList);
			}

			tableInfoEntityMap.put(clazz.getName(), tableInfoEntity);
		}
		if (tableInfoEntity == null || tableInfoEntity.getPropertieArrayList() == null || tableInfoEntity.getPropertieArrayList().size() == 0) {
			throw new SPDbException("不能创建+" + clazz + "的表信息");
		}
		return tableInfoEntity;
	}
}
