package com.sp.db.util;

/**
 * 
 * @author shawn-pat
 * 
 */
public class SPSqlBuilderFactory {

	private static SPSqlBuilderFactory instance;

	/**
	 * 调用getSqlBuilder(int operate)返回插入sql语句构建器传入的参数
	 */
	public static final int INSERT = 0;
	/**
	 * 调用getSqlBuilder(int operate)返回查询sql语句构建器传入的参数
	 */
	public static final int SELECT = 1;
	/**
	 * 调用getSqlBuilder(int operate)返回删除sql语句构建器传入的参数
	 */
	public static final int DELETE = 2;
	/**
	 * 调用getSqlBuilder(int operate)返回更新sql语句构建器传入的参数
	 */
	public static final int UPDATE = 3;

	/**
	 * 单例模式获得Sql构建器工厂
	 * 
	 * @return sql构建器
	 */
	public static SPSqlBuilderFactory getInstance() {
		if (instance == null) {
			instance = new SPSqlBuilderFactory();
		}
		return instance;
	}

	/**
	 * 获得sql构建器
	 * 
	 * @param operate
	 * @return 构建器
	 */
	public synchronized SPSqlBuilder getSqlBuilder(int operate)
	{
		SPSqlBuilder sqlBuilder = null;
		switch (operate)
		{
		case INSERT:
			sqlBuilder = new SPInsertSqlBuilder();
			break;
		case SELECT:
			sqlBuilder = new SPQuerySqlBuilder();
			break;
		case DELETE:
			sqlBuilder = new SPDeleteSqlBuilder();
			break;
		case UPDATE:
			sqlBuilder = new SPUpdateSqlBuilder();
			break;
		default:
			break;
		}
		return sqlBuilder;
	}
}
