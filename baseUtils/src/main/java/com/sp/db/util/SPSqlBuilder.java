package com.sp.db.util;

import org.apache.http.NameValuePair;

import android.text.TextUtils;

import com.sp.db.SPDbException;
import com.sp.db.entity.SPArrayList;
import com.sp.utils.SPValidation;

public abstract class SPSqlBuilder {

	protected Boolean distinct;
	protected String where;
	protected String groupBy;
	protected String having;
	protected String orderBy;
	protected String limit;
	protected Class<?> clazz = null;
	protected String tableName = null;
	protected Object entity;
	protected SPArrayList updateFields;

	public SPSqlBuilder() {
	}

	public SPSqlBuilder(Object entity) {
		this.entity = entity;
		setClazz(entity.getClass());
	}

	public SPSqlBuilder(Class<?> clazz) {
		setTableName(clazz);
	}

	/**
	 * 获取sql语句
	 * 
	 * @return
	 * @throws SPDbException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public String getSqlStatement() throws SPDbException,
			IllegalArgumentException, IllegalAccessException
	{
		onPreGetStatement();
		return buildSql();
	}

	/**
	 * 构建sql语句前执行方法
	 * 
	 * @return
	 * @throws SPDbException
	 */
	public void onPreGetStatement() throws SPDbException,
			IllegalArgumentException, IllegalAccessException
	{

	}

	/**
	 * 构建sql语句
	 * 
	 * @return
	 * @throws SPDbException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public abstract String buildSql() throws SPDbException, IllegalArgumentException, IllegalAccessException;

	/**
	 * 创建条件字句
	 * 
	 * @return 返回条件Sql
	 */
	protected String buildConditionString()
	{
		StringBuilder query = new StringBuilder(120);
		appendClause(query, " WHERE ", where);
		appendClause(query, " GROUP BY ", groupBy);
		appendClause(query, " HAVING ", having);
		appendClause(query, " ORDER BY ", orderBy);
		appendClause(query, " LIMIT ", limit);
		return query.toString();
	}

	protected void appendClause(StringBuilder s, String name, String clause)
	{
		if (!TextUtils.isEmpty(clause))
		{
			s.append(name);
			s.append(clause);
		}
	}

	/**
	 * 构建where子句
	 * 
	 * @param conditions
	 * @return 返回where子句
	 */
	public String buildWhere(SPArrayList conditions) {
		StringBuilder stringBuilder = new StringBuilder(256);
		if (conditions != null) {
			stringBuilder.append(" WHERE ");
			for (int i = 0; i < conditions.size(); i++) {
				NameValuePair nameValuePair = conditions.get(i);
				stringBuilder.append(nameValuePair.getName()).append(" = ")
						.append(SPValidation.isNumeric(nameValuePair.getValue().toString()) ? nameValuePair.getValue() : "'"
								+ nameValuePair.getValue() + "'");
				if (i + 1 < conditions.size()) {
					stringBuilder.append(" AND ");
				}
			}
		}
		return stringBuilder.toString();
	}

	public void setCondition(boolean distinct, String where, String groupBy, String having, String orderBy, String limit) {
		this.distinct = distinct;
		this.where = where;
		this.groupBy = groupBy;
		this.having = having;
		this.orderBy = orderBy;
		this.limit = limit;
	}

	public Boolean getDistinct() {
		return distinct;
	}

	public void setDistinct(Boolean distinct) {
		this.distinct = distinct;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	public String getHaving() {
		return having;
	}

	public void setHaving(String having) {
		this.having = having;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		setTableName(clazz);
		this.clazz = clazz;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setTableName(Class<?> clazz) {
		this.tableName = SPDbUtils.getTableName(clazz);
	}

	public Object getEntity() {
		return entity;
	}

	public void setEntity(Object entity) {
		this.entity = entity;
		setClazz(entity.getClass());
	}

	public SPArrayList getUpdateFields() {
		return updateFields;
	}

	public void setUpdateFields(SPArrayList updateFields) {
		this.updateFields = updateFields;
	}

}
