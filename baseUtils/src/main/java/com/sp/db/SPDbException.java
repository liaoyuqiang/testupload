package com.sp.db;

public class SPDbException extends Exception {

	private static final long serialVersionUID = 1L;

	public SPDbException()
	{
		super();
	}

	public SPDbException(String detailMessage)
	{
		super(detailMessage);
	}
}
