package com.sp.db;

public class SPDbNotOpenException extends Exception {

	private static final long serialVersionUID = 1L;

	public SPDbNotOpenException() {
		super();
	}

	public SPDbNotOpenException(String detailMessage) {
		super(detailMessage);
	}
}
