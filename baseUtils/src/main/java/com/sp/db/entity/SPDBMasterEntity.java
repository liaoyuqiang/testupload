package com.sp.db.entity;

import com.sp.utils.SPBaseEntity;

public class SPDBMasterEntity extends SPBaseEntity {

	private static final long serialVersionUID = -2522639507545906963L;
	private String type;
	private String name;
	private String tbl_name;
	private String sql;
	private int rootpage;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTbl_name() {
		return tbl_name;
	}

	public void setTbl_name(String tbl_name) {
		this.tbl_name = tbl_name;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public int getRootpage() {
		return rootpage;
	}

	public void setRootpage(int rootpage) {
		this.rootpage = rootpage;
	}
}
