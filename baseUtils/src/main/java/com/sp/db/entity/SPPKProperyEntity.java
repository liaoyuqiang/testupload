package com.sp.db.entity;

public class SPPKProperyEntity extends SPProperyEntity {

	public SPPKProperyEntity() {
	}

	public SPPKProperyEntity(String name, Class<?> type, Object defaultValue, boolean primaryKey, boolean isAllowNull, boolean autoIncrement,
			String columnName) {
		super(name, type, defaultValue, primaryKey, isAllowNull, autoIncrement, columnName);
	}
}
