package com.sp.db.entity;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.sp.utils.SPStringUtil;

public class SPArrayList extends ArrayList<NameValuePair> {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean add(NameValuePair nameValuePair) {
		if (!SPStringUtil.isEmpty(nameValuePair.getValue())) {
			return super.add(nameValuePair);
		} else {
			return false;
		}
	}

	/**
	 * 添加数据
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean add(String key, String value) {
		return add(new BasicNameValuePair(key, value));
	}
}
