package com.sp.db.entity;

import java.util.ArrayList;
import java.util.List;

import com.sp.utils.SPBaseEntity;

public class SPTableInfoEntity extends SPBaseEntity {

	private static final long serialVersionUID = -133039097162794984L;
	private String tableName = "";
	private String className = "";
	private SPPKProperyEntity pkProperyEntity = null;
	ArrayList<SPProperyEntity> propertieArrayList = new ArrayList<SPProperyEntity>();

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public SPPKProperyEntity getPkProperyEntity() {
		return pkProperyEntity;
	}

	public void setPkProperyEntity(SPPKProperyEntity pkProperyEntity) {
		this.pkProperyEntity = pkProperyEntity;
	}

	public ArrayList<SPProperyEntity> getPropertieArrayList() {
		return propertieArrayList;
	}

	public void setPropertieArrayList(List<SPProperyEntity> propertieArrayList) {
		this.propertieArrayList = (ArrayList<SPProperyEntity>) propertieArrayList;
	}

}
