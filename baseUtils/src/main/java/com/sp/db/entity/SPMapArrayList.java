package com.sp.db.entity;

import java.util.ArrayList;

public class SPMapArrayList<T extends Object> extends ArrayList<SPHashMap<T>> {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean add(SPHashMap<T> spHashMap) {
		if (spHashMap != null) {
			return super.add(spHashMap);
		} else {
			return false;
		}
	}
}
