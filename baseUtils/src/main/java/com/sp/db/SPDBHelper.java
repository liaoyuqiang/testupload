package com.sp.db;

import com.sp.db.SPSQLiteDatabase.SPDBUpdateListener;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SPDBHelper extends SQLiteOpenHelper {

	private SPDBUpdateListener mTadbUpdateListener;

	public SPDBHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	public SPDBHelper(Context context, String name, CursorFactory factory, int version, SPDBUpdateListener tadbUpdateListener) {
		super(context, name, factory, version);
		this.mTadbUpdateListener = tadbUpdateListener;
	}

	public void setOndbUpdateListener(SPDBUpdateListener tadbUpdateListener) {
		this.mTadbUpdateListener = tadbUpdateListener;
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.e("TAG", "*************检查到数据升级**************");
		if (mTadbUpdateListener != null) {
			Log.e("TAG", "*************不为null**************");
			mTadbUpdateListener.onUpgrade(db, oldVersion, newVersion);
		}
	}

}
