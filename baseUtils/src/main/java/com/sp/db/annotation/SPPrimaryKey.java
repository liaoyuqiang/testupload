package com.sp.db.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({ METHOD, FIELD })
@Retention(RUNTIME)
public @interface SPPrimaryKey {

	/**
	 * 设置主键名
	 * 
	 * @return
	 */
	public String name() default "";

	/**
	 * 字段默认值
	 * 
	 * @return
	 */
	public String defaultValue() default "";

	/**
	 * 是否自动自增
	 * 
	 * @return
	 */
	boolean autoIncrement() default false;

}
