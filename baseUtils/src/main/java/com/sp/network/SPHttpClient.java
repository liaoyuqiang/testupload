package com.sp.network;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Future;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.SyncBasicHttpContext;

import android.content.Context;

/**
 * 
 * @author Shawn Poon
 */
public class SPHttpClient {

	private static final int DEFAULT_MAX_CONNECTIONS = 10;
	private static final int DEFAULT_SOCKET_TIMEOUT = 10 * 1000;
	private static final int DEFAULT_SOCKET_BUFFER_SIZE = 8192;

	private static int socketTimeout = DEFAULT_SOCKET_TIMEOUT;

	private final HttpClient httpClient;
	private final HttpContext httpContext;

	private final Map<Context, List<WeakReference<Future<?>>>> requestMap;
	private final Map<String, String> clientHeaderMap;

	public SPHttpClient() {
		HttpParams httpParams = new BasicHttpParams();

		ConnManagerParams.setTimeout(httpParams, socketTimeout);
		ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRouteBean(DEFAULT_MAX_CONNECTIONS));
		ConnManagerParams.setMaxTotalConnections(httpParams, DEFAULT_MAX_CONNECTIONS);

		HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
		HttpConnectionParams.setConnectionTimeout(httpParams, socketTimeout);
		HttpConnectionParams.setTcpNoDelay(httpParams, true);
		HttpConnectionParams.setSocketBufferSize(httpParams, DEFAULT_SOCKET_BUFFER_SIZE);

		HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(httpParams, HTTP.UTF_8);
		HttpProtocolParams.setUseExpectContinue(httpParams, true);

		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

		ClientConnectionManager conManager = new ThreadSafeClientConnManager(httpParams, schemeRegistry);
		httpContext = new SyncBasicHttpContext(new BasicHttpContext());
		httpClient = new DefaultHttpClient(conManager, httpParams);
		requestMap = new WeakHashMap<Context, List<WeakReference<Future<?>>>>();
		clientHeaderMap = new HashMap<String, String>();

	}

	/**
	 * 取得httpClient instance
	 * 
	 * @return
	 */
	public HttpClient getHttpClient() {
		return this.httpClient;
	}

	public void setTimeOut(int timeout) {
		final HttpParams httpParams = this.httpClient.getParams();
		ConnManagerParams.setTimeout(httpParams, timeout);
		HttpConnectionParams.setSoTimeout(httpParams, timeout);
		HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
	}

	public void addHeader(String header, String value) {
		clientHeaderMap.put(header, value);
	}

	/**
	 * 通过android context 中断请求
	 * 
	 * @param context
	 * @param mayInterruptIfRunning
	 */
	public void cancelRequests(Context context, boolean mayInterruptIfRunning) {
		List<WeakReference<Future<?>>> requestList = requestMap.get(context);
		if (requestList != null) {
			for (WeakReference<Future<?>> requestRef : requestList) {
				Future<?> request = requestRef.get();
				if (request != null) {
					request.cancel(mayInterruptIfRunning);
				}
			}
		}
		requestMap.remove(context);
	}

	/**
	 * 执行一个无参HTTP GET请求
	 * 
	 * @param url
	 * @param responseHandler
	 */
	public void get(String url, SPHttpResponseHandler responseHandler) {
		get(null, url, null, responseHandler);
	}

	/**
	 * 执行一个带参HTTP GET请求
	 * 
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public void get(String url, SPRequestParams params, SPHttpResponseHandler responseHandler) {
		get(null, url, params, responseHandler);
	}

	/**
	 * 执行一个带参HTTP GET请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param responseHandler
	 */
	public void get(Context context, String url, SPHttpResponseHandler responseHandler) {
		get(context, url, null, responseHandler);
	}

	/**
	 * 执行一个带参HTTP GET请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public void get(Context context, String url, SPRequestParams params, SPHttpResponseHandler responseHandler) {
		sendRequest(httpClient, httpContext, new HttpGet(getUrlWithQueryString(url, params)), null, responseHandler, context);
	}

	/**
	 * 执行一个带参HTTP GET请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param headers
	 * @param params
	 * @param responseHandler
	 */
	public void get(Context context, String url, Header[] headers, SPRequestParams params, SPHttpResponseHandler responseHandler) {
		HttpUriRequest request = new HttpGet(getUrlWithQueryString(url, params));
		if (headers != null)
			request.setHeaders(headers);
		sendRequest(httpClient, httpContext, request, null, responseHandler,
				context);
	}

	/**
	 * 执行一个无参HTTP POST请求
	 * 
	 * @param url
	 * @param responseHandler
	 */
	public void post(String url, SPHttpResponseHandler responseHandler) {
		post(null, url, null, responseHandler);
	}

	/**
	 * 执行一个带参HTTP POST请求
	 * 
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public void post(String url, SPRequestParams params, SPHttpResponseHandler responseHandler) {
		post(null, url, params, responseHandler);
	}

	/**
	 * 执行一个带参HTTP POST请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public void post(Context context, String url, SPRequestParams params, SPHttpResponseHandler responseHandler) {
		post(context, url, paramsToEntity(params), null, responseHandler);
	}

	/**
	 * 执行一个带参HTTP POST请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param entity
	 * @param contentType
	 * @param responseHandler
	 */
	public void post(Context context, String url, HttpEntity entity, String contentType, SPHttpResponseHandler responseHandler) {
		sendRequest(httpClient, httpContext, addEntityToRequestBase(new HttpPost(url), entity), contentType, responseHandler, context);
	}

	/**
	 * 执行一个带参HTTP POST请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param headers
	 * @param params
	 * @param contentType
	 * @param responseHandler
	 */
	public void post(Context context, String url, Header[] headers, SPRequestParams params, String contentType, SPHttpResponseHandler responseHandler) {
		HttpEntityEnclosingRequestBase request = new HttpPost(url);
		if (params != null) {
			request.setEntity(paramsToEntity(params));
		}
		if (headers != null) {
			request.setHeaders(headers);
		}
		sendRequest(httpClient, httpContext, request, contentType, responseHandler, context);
	}

	/**
	 * 执行一个带参HTTP POST请求，并通过Android的CONTEXT进行跟踪
	 * 
	 * @param context
	 * @param url
	 * @param headers
	 * @param entity
	 * @param contentType
	 * @param responseHandler
	 */
	public void post(Context context, String url, Header[] headers, HttpEntity entity, String contentType, SPHttpResponseHandler responseHandler) {
		HttpEntityEnclosingRequestBase request = addEntityToRequestBase(new HttpPost(url), entity);
		if (headers != null) {
			request.setHeaders(headers);
		}
		sendRequest(httpClient, httpContext, request, contentType, responseHandler, context);

	}

	protected void sendRequest(HttpClient httpClient, HttpContext httpContext, HttpUriRequest uriRequest, String contentType,
			SPHttpResponseHandler responseHandler, Context context) {
		if (contentType != null) {
			uriRequest.addHeader("Content-Type", contentType);
		}

		Future<?> request = SPNetworkThreadPool.execute(new SPHttpRequest(httpClient, httpContext, uriRequest, responseHandler));
		if (context != null) {
			List<WeakReference<Future<?>>> requestList = requestMap.get(context);
			if (requestList == null) {
				requestList = new LinkedList<WeakReference<Future<?>>>();
				requestMap.put(context, requestList);
			}

			requestList.add(new WeakReference<Future<?>>(request));

		}
	}

	private HttpEntityEnclosingRequestBase addEntityToRequestBase(HttpEntityEnclosingRequestBase requestBase, HttpEntity entity) {
		if (entity != null) {
			requestBase.setEntity(entity);
		}
		return requestBase;
	}

	private HttpEntity paramsToEntity(SPRequestParams params) {
		HttpEntity entity = null;

		if (params != null) {
			entity = params.getEntity();
		}

		return entity;
	}

	public static String getUrlWithQueryString(String url, SPRequestParams params) {
		if (params != null) {
			String paramString = params.getParamString();
			if (url.indexOf("?") == -1) {
				url += "?" + paramString;
			} else {
				url += "&" + paramString;
			}
		}
		return url;
	}
}
