package com.sp.network;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

public class SPHttpRequest implements Runnable {
	private final HttpClient client;
	private final HttpContext context;
	private final HttpUriRequest request;
	private final SPHttpResponseHandler responseHandler;

	public SPHttpRequest(HttpClient client, HttpContext context,
			HttpUriRequest request, SPHttpResponseHandler responseHandler) {
		this.client = client;
		this.context = context;
		this.request = request;
		this.responseHandler = responseHandler;
	}

	@Override
	public void run() {
		if (responseHandler != null) {
			responseHandler.sendStartMessage();
		}
		makeRequest();
		if (responseHandler != null) {
			responseHandler.sendFinishMessage();
		}
	}

	private void makeRequest() {
		try {
			if (!Thread.currentThread().isInterrupted()) {
				HttpResponse response = client.execute(request, context);
				if (!Thread.currentThread().isInterrupted()) {
					if (responseHandler != null) {
						responseHandler.sendResponseMessage(response);
					}
				}
			}
		} catch (UnknownHostException e) {
			if (responseHandler != null) {
				responseHandler.sendFailureMessage(e, "can't resolve host");
			}
		} catch (SocketException e) {
			if (responseHandler != null) {
				responseHandler.sendFailureMessage(e, "can't resolve host");
			}
		} catch (SocketTimeoutException e) {
			if (responseHandler != null) {
				responseHandler.sendFailureMessage(e, "socket time out");
			}
		} catch (IOException e) {
			if (responseHandler != null) {
				responseHandler.sendFailureMessage(e, "IO exception");
			}
		} catch (NullPointerException e) {
			if (responseHandler != null) {
				responseHandler.sendFailureMessage(e, "httpClient bug");
			}
		} catch (Exception e) {
			if (responseHandler != null) {
				responseHandler.sendFailureMessage(e, "unknow exception");
			}
		}
	}
}
