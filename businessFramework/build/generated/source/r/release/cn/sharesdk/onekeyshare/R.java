/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package cn.sharesdk.onekeyshare;

public final class R {
	public static final class drawable {
		public static int ssdk_auth_title_back = 0x7f020089;
		public static int ssdk_back_arr = 0x7f02008a;
		public static int ssdk_logo = 0x7f02008b;
		public static int ssdk_oks_classic_alipay = 0x7f02008c;
		public static int ssdk_oks_classic_bluetooth = 0x7f02008d;
		public static int ssdk_oks_classic_check_checked = 0x7f02008e;
		public static int ssdk_oks_classic_check_default = 0x7f02008f;
		public static int ssdk_oks_classic_douban = 0x7f020090;
		public static int ssdk_oks_classic_dropbox = 0x7f020091;
		public static int ssdk_oks_classic_email = 0x7f020092;
		public static int ssdk_oks_classic_evernote = 0x7f020093;
		public static int ssdk_oks_classic_facebook = 0x7f020094;
		public static int ssdk_oks_classic_facebookmessenger = 0x7f020095;
		public static int ssdk_oks_classic_flickr = 0x7f020096;
		public static int ssdk_oks_classic_foursquare = 0x7f020097;
		public static int ssdk_oks_classic_googleplus = 0x7f020098;
		public static int ssdk_oks_classic_instagram = 0x7f020099;
		public static int ssdk_oks_classic_instapaper = 0x7f02009a;
		public static int ssdk_oks_classic_kaixin = 0x7f02009b;
		public static int ssdk_oks_classic_kakaostory = 0x7f02009c;
		public static int ssdk_oks_classic_kakaotalk = 0x7f02009d;
		public static int ssdk_oks_classic_laiwang = 0x7f02009e;
		public static int ssdk_oks_classic_laiwangmoments = 0x7f02009f;
		public static int ssdk_oks_classic_line = 0x7f0200a0;
		public static int ssdk_oks_classic_linkedin = 0x7f0200a1;
		public static int ssdk_oks_classic_mingdao = 0x7f0200a2;
		public static int ssdk_oks_classic_pinterest = 0x7f0200a3;
		public static int ssdk_oks_classic_platform_cell_back = 0x7f0200a4;
		public static int ssdk_oks_classic_platfrom_cell_back_nor = 0x7f0200cc;
		public static int ssdk_oks_classic_platfrom_cell_back_sel = 0x7f0200cd;
		public static int ssdk_oks_classic_pocket = 0x7f0200a5;
		public static int ssdk_oks_classic_progressbar = 0x7f0200a6;
		public static int ssdk_oks_classic_qq = 0x7f0200a7;
		public static int ssdk_oks_classic_qzone = 0x7f0200a8;
		public static int ssdk_oks_classic_renren = 0x7f0200a9;
		public static int ssdk_oks_classic_shortmessage = 0x7f0200aa;
		public static int ssdk_oks_classic_sinaweibo = 0x7f0200ab;
		public static int ssdk_oks_classic_tencentweibo = 0x7f0200ac;
		public static int ssdk_oks_classic_tumblr = 0x7f0200ad;
		public static int ssdk_oks_classic_twitter = 0x7f0200ae;
		public static int ssdk_oks_classic_vkontakte = 0x7f0200af;
		public static int ssdk_oks_classic_wechat = 0x7f0200b0;
		public static int ssdk_oks_classic_wechatfavorite = 0x7f0200b1;
		public static int ssdk_oks_classic_wechatmoments = 0x7f0200b2;
		public static int ssdk_oks_classic_whatsapp = 0x7f0200b3;
		public static int ssdk_oks_classic_yixin = 0x7f0200b4;
		public static int ssdk_oks_classic_yixinmoments = 0x7f0200b5;
		public static int ssdk_oks_classic_youdao = 0x7f0200b6;
		public static int ssdk_oks_ptr_ptr = 0x7f0200b7;
		public static int ssdk_title_div = 0x7f0200b8;
	}
	public static final class string {
		public static int ssdk_alipay = 0x7f07006a;
		public static int ssdk_alipay_client_inavailable = 0x7f07006b;
		public static int ssdk_baidutieba = 0x7f0700cb;
		public static int ssdk_baidutieba_client_inavailable = 0x7f0700cc;
		public static int ssdk_bluetooth = 0x7f07006c;
		public static int ssdk_douban = 0x7f07006d;
		public static int ssdk_dropbox = 0x7f07006e;
		public static int ssdk_email = 0x7f07006f;
		public static int ssdk_evernote = 0x7f070070;
		public static int ssdk_facebook = 0x7f070071;
		public static int ssdk_facebookmessenger = 0x7f070072;
		public static int ssdk_facebookmessenger_client_inavailable = 0x7f070073;
		public static int ssdk_flickr = 0x7f070074;
		public static int ssdk_foursquare = 0x7f070075;
		public static int ssdk_google_plus_client_inavailable = 0x7f070076;
		public static int ssdk_googleplus = 0x7f070077;
		public static int ssdk_instagram = 0x7f070078;
		public static int ssdk_instagram_client_inavailable = 0x7f070079;
		public static int ssdk_instapager_email_or_password_incorrect = 0x7f07007a;
		public static int ssdk_instapager_login_html = 0x7f0700cd;
		public static int ssdk_instapaper = 0x7f07007b;
		public static int ssdk_instapaper_email = 0x7f07007c;
		public static int ssdk_instapaper_login = 0x7f07007d;
		public static int ssdk_instapaper_logining = 0x7f07007e;
		public static int ssdk_instapaper_pwd = 0x7f07007f;
		public static int ssdk_kaixin = 0x7f070080;
		public static int ssdk_kakaostory = 0x7f070081;
		public static int ssdk_kakaostory_client_inavailable = 0x7f070082;
		public static int ssdk_kakaotalk = 0x7f070083;
		public static int ssdk_kakaotalk_client_inavailable = 0x7f070084;
		public static int ssdk_laiwang = 0x7f070085;
		public static int ssdk_laiwang_client_inavailable = 0x7f070086;
		public static int ssdk_laiwangmoments = 0x7f070087;
		public static int ssdk_line = 0x7f070088;
		public static int ssdk_line_client_inavailable = 0x7f070089;
		public static int ssdk_linkedin = 0x7f07008a;
		public static int ssdk_mingdao = 0x7f07008b;
		public static int ssdk_mingdao_share_content = 0x7f07008c;
		public static int ssdk_neteasemicroblog = 0x7f07008d;
		public static int ssdk_oks_cancel = 0x7f07008e;
		public static int ssdk_oks_confirm = 0x7f07008f;
		public static int ssdk_oks_contacts = 0x7f070090;
		public static int ssdk_oks_multi_share = 0x7f070091;
		public static int ssdk_oks_pull_to_refresh = 0x7f070092;
		public static int ssdk_oks_refreshing = 0x7f070093;
		public static int ssdk_oks_release_to_refresh = 0x7f070094;
		public static int ssdk_oks_share = 0x7f070095;
		public static int ssdk_oks_share_canceled = 0x7f070096;
		public static int ssdk_oks_share_completed = 0x7f070097;
		public static int ssdk_oks_share_failed = 0x7f070098;
		public static int ssdk_oks_sharing = 0x7f070099;
		public static int ssdk_pinterest = 0x7f07009a;
		public static int ssdk_pinterest_client_inavailable = 0x7f07009b;
		public static int ssdk_pocket = 0x7f07009c;
		public static int ssdk_qq = 0x7f07009d;
		public static int ssdk_qq_client_inavailable = 0x7f07009e;
		public static int ssdk_qzone = 0x7f07009f;
		public static int ssdk_renren = 0x7f0700a0;
		public static int ssdk_share_to_baidutieba = 0x7f0700ce;
		public static int ssdk_share_to_mingdao = 0x7f0700a1;
		public static int ssdk_share_to_qq = 0x7f0700a2;
		public static int ssdk_share_to_qzone = 0x7f0700a3;
		public static int ssdk_share_to_qzone_default = 0x7f0700a4;
		public static int ssdk_shortmessage = 0x7f0700a5;
		public static int ssdk_sinaweibo = 0x7f0700a6;
		public static int ssdk_sohumicroblog = 0x7f0700a7;
		public static int ssdk_sohusuishenkan = 0x7f0700a8;
		public static int ssdk_tencentweibo = 0x7f0700a9;
		public static int ssdk_tumblr = 0x7f0700aa;
		public static int ssdk_twitter = 0x7f0700ab;
		public static int ssdk_use_login_button = 0x7f0700ac;
		public static int ssdk_vkontakte = 0x7f0700ad;
		public static int ssdk_website = 0x7f0700ae;
		public static int ssdk_wechat = 0x7f0700af;
		public static int ssdk_wechat_client_inavailable = 0x7f0700b0;
		public static int ssdk_wechatfavorite = 0x7f0700b1;
		public static int ssdk_wechatmoments = 0x7f0700b2;
		public static int ssdk_weibo_oauth_regiseter = 0x7f0700b3;
		public static int ssdk_weibo_upload_content = 0x7f0700b4;
		public static int ssdk_whatsapp = 0x7f0700b5;
		public static int ssdk_whatsapp_client_inavailable = 0x7f0700b6;
		public static int ssdk_yixin = 0x7f0700b7;
		public static int ssdk_yixin_client_inavailable = 0x7f0700b8;
		public static int ssdk_yixinmoments = 0x7f0700b9;
		public static int ssdk_youdao = 0x7f0700ba;
	}
}
