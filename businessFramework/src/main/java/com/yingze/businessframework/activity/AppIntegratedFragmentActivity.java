/**
 * 集成了标题控制、标题栏下内容布局控制、网络加载、弹出进度框控制的综合界面
 */
package com.yingze.businessframework.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.yingze.businessframework.R;
import com.yingze.businessframework.component.CompatScrollView;
import com.yingze.businessframework.component.ScrollLayout;
import com.yingze.businessframework.controller.AppIntegratedActivityController;
import com.yingze.businessframework.controller.AppNetworkController;
import com.yingze.businessframework.controller.LayoutInflaterController;
import com.yingze.businessframework.controller.LoadingController;
import com.yingze.businessframework.controller.ScrollLayoutController;
import com.yingze.businessframework.controller.SubLayoutController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;
import com.yingze.businessframework.controller.AppNetworkController.NetworkCallback;
import com.yingze.businessframework.listener.ViewScrollCallback;

public abstract class AppIntegratedFragmentActivity extends BaseFragmentActivity implements ViewController, 
		AppNetworkController.DialogCallback, ScrollLayoutController.OnScrollLayoutControllerStateCallback {
	protected View mContentView;
	protected AppIntegratedActivityController mActivityController = AppIntegratedActivityController.NULL;
	protected LoadingController mLoadingController = new LoadingController();// 数据加载控制器
	protected LayoutInflaterController mLayoutInflaterController = new LayoutInflaterController(this);
	protected AppNetworkController mAppNetworkController = new AppNetworkController(this);
	
	private ScrollLayout scrollLayout;
	private ScrollLayout.ScrollContentLayout layoutController;
	private ScrollLayoutController mScrollLayoutController;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//竖屏
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		/*
		 * 生成窗口主布局控件,并取出根布局容器
		 */
		scrollLayout = new ScrollLayout(this);
		setContentView(scrollLayout);
		layoutController = scrollLayout.getScrollContentLayout();
		
		/*
		 * 生成根布局控件
		 */
		mContentView = mLayoutInflaterController.inflateRootLayout(R.layout.template_app_activity_facade);
		layoutController.addView(mContentView, new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		
		/*
		 * 将子布局动态绑定到根布局中 
		 */
		View subLayout = mLayoutInflaterController.findViewById(mContentView, R.id.fl_sub_layout);
		View subContent = mLayoutInflaterController.inflateRootLayout(getLayoutResID());
		mLayoutInflaterController.addSubLayoutChild(this, (FrameLayout)subLayout, subContent, getViewScrollCallback());
		
		/*
		 * 执行预定的初始化方法
		 */
		mActivityController = AppIntegratedActivityController.newInstance(this, mContentView);
		mActivityController.initOnCreate();
		
		/*
		 * 执行
		 */
		mScrollLayoutController = new ScrollLayoutController(this, scrollLayout, this,
				ScrollLayoutController.getIntentInjectProperty(getIntent()), String.valueOf(hashCode()),
				getActivityAnimationController(), getActivityActionController());
		mScrollLayoutController.onActivityCreateHandle();
	}
	
	/**
	 * 钩子方法，用于网络请求
	 */
	public void doNetworkAction(NetworkCallback networkCallback) {
		mAppNetworkController.initLoadingController(mLoadingController);
		mAppNetworkController.initDialogCallback(this);
		mAppNetworkController.initHttpRequestCallback(networkCallback);
		mAppNetworkController.initHttpResponseCallback(networkCallback);
		mAppNetworkController.openConnect();
	}
	
	@Override
	public ViewGroup getRootLayout() {
		return (ViewGroup) mContentView.findViewById(R.id.fl_sub_layout);
	}
	
    @Override
	public void onScrollLayoutControllerFinish() {
		super.finish();
	}
	
    @Override
    public void startActivity(Intent intent) {
    	mScrollLayoutController.injectIntentProperty(intent);
    	super.startActivity(intent);
    };
    
    @Override
    public void onResume() {
    	mScrollLayoutController.resetCurrentScrollOffsetHorizontal();
    	super.onResume();
    }
    
    @Override
    protected void onRestart() {
    	mScrollLayoutController.resetCurrentScrollOffsetHorizontal();
    	super.onRestart();
    }
    
	@Override
	public void finish() {
		mScrollLayoutController.onActivityFinishHandle();
	};
	
	@Override
	protected void onDestroy() {
		mLoadingController.closeProgress();
		mAppNetworkController.closeConnect();
		super.onDestroy();
	}
	
	/**
	 * 动态添加控制器
	 * @param controller
	 */
	public void addController(ViewController controller) {
		mActivityController.getControllerArr().add(controller);
	}
	
	/**
	 * 清空控制器
	 */
	public void cleanController() {
		mActivityController.getControllerArr().clear();
	}
	
	/**
	 * 返回标题控制器
	 * @return
	 */
	public TitleController getTitleController() {
		return mActivityController.getTitleController();
	}
	
	/**
	 * 返回页面内容布局控制器
	 * @return
	 */
	public SubLayoutController getSubLayoutController() {
		return mActivityController.getSubLayoutController();
	}
	
	/**
	 * 是否需要滑动刷新
	 * @return
	 */
	public ViewScrollCallback getViewScrollCallback() {
		return new ViewScrollCallback.NullCallback();
	}
	
	/**
	 * 获取自定义滑动刷新框架控件
	 * @return
	 */
	public CompatScrollView getCompatScrollView() {
		return mLayoutInflaterController.getCompatScrollView();
	}
	
	/**
	 * 获取activity动画控制器
	 * @return
	 */
	public ScrollLayoutController.ActivityAnimationController getActivityAnimationController() {
		return ScrollLayoutController.ActivityAnimationController.NULL;
	}
	
	/**
	 * 返回当前页面动作控制器
	 * @return
	 */
	public ScrollLayoutController.ActivityActionController getActivityActionController() {
		return ScrollLayoutController.ActivityActionController.NULL;
	}
	
	/**
	 * 返回activity的子布局
	 * @return
	 */
	public ViewGroup getSubContentView() {
		return (ViewGroup) mLayoutInflaterController.findViewById(mContentView, R.id.fl_sub_layout);
	}
}
