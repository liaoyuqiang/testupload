/**
 * 集成下拉刷新，上推加载的activity界面框架
 */
package com.yingze.businessframework.activity.branch;

import java.io.OutputStream;
import java.util.ArrayList;
import android.view.View;
import com.yingze.businessframework.controller.LoadingController;
import com.yingze.businessframework.controller.ViewController;
import com.yingze.corelibrary.network.BaseNetworkController;
import com.yingze.corelibrary.network.DeckerPostServer;
import com.yingze.corelibrary.network.NetworkClient;
import com.yingze.corelibrary.network.ThreadPoolController;
import com.yingzecorelibrary.utils.ToastUtil;
import com.yingzecorelibrary.view.XListView;

public class ActionListBusinessActivity extends ListBusinessActivity {
	private ManageBoxPageRequestController mManageBoxPageRequestController = new ManageBoxPageRequestController();
	private MessageController mMessageHandle = new MessageController();
	// 设想，通过状态控制器，有机的将 mManageBoxPageRequestController 和 mLoadingController 结合起身处理，封装行为状态同埋动作；
	
	@Override
	public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {
	}
	
	@Override
	public void initTransactionOnCreate() {
		super.initTransactionOnCreate();
		xlv_layer.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						//xlv_page_content.stopRefresh();
						xlv_layer.setPullLoadEnable(false);
						mManageBoxPageRequestController.setRefreshState();
						requestServer();
					}
				}, 500);
			}

			@Override
			public void onLoadMore() {
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
//						xlv_page_content.stopLoadMore();
						mManageBoxPageRequestController.setLoadState();
						mManageBoxPageRequestController.addPageIndex();
						requestServer();
					}
				}, 500);
			}
		});
	}
	
	@Override
	protected ListBusinessAdapterCallback createListBusinessAdapterCallback() {
		return null;
	}

	@Override
	protected String getTitleContent() {
		return null;
	}

	@Override
	protected String getServerUrl() {
		return null;
	}

	@Override
	protected void submitDataToServer(OutputStream outputStream) {
		
	}

	@Override
	protected void parsePageData(String content) {
		
	}

	@Override
	public void requestServer() {
		if(mManageBoxPageRequestController.isInitState()) {
			mLoadingController.showProgress(this, getRootLayout());
		}
		
		NetworkClient client = NetworkClient.newInstance();
		client.setNetworkController(new BaseNetworkController(
				getServerUrl(),
				new NetworkClient.DownloadCallback() {
					@Override
					public void onFinish(String content) {
						parsePageData(content);
						if (mManageBoxPageRequestController.isInitState()) {
							mLoadingController.closeProgress();
						} else if(mManageBoxPageRequestController.isRefreshState()) {
							mMessageHandle.stopRefresh();
							mMessageHandle.show("数据刷新成功");
						} else if(mManageBoxPageRequestController.isLoadState()) {
							mMessageHandle.stopLoadMore();
							mMessageHandle.show("数据加载成功");
						}
						mManageBoxPageRequestController.setInitState();
					}
					@Override
					public void onFail(Exception e, String message) {
						if(mManageBoxPageRequestController.isInitState()) {
							mLoadingController.showFailPrompts(ActionListBusinessActivity.this, 
									getRootLayout(), 
									new LoadingController.OnRefreshListener() {
										@Override
										public void onRefresh(View view) {
											requestServer();
										}
									});
						} else if(mManageBoxPageRequestController.isRefreshState()) {
							mMessageHandle.stopRefresh();
							mMessageHandle.show("网络连接异常，数据刷新失败");
						} else if(mManageBoxPageRequestController.isLoadState()) {
							mMessageHandle.stopLoadMore();
							mMessageHandle.show("网络连接异常，数据加载失败");
						}
						mManageBoxPageRequestController.setInitState();
					}
					@Override
					public void onDownloadProcess(int readbufferLength, int downloadFileLength,
							int totalFileLength) {
					}
					
				}){
			@Override
			public void uploadPostContent(OutputStream outputStream) {
				submitDataToServer(outputStream);
			}
		});
		mDeckerPostServer = new DeckerPostServer(client);
		ThreadPoolController.getInstance().submit(mDeckerPostServer);
	}
	
	class ManageBoxPageRequestController {
		final int STATE_INIT = 0;
		final int STATE_REFRESH = 1;
		final int STATE_LOAD = 2;
		
		final int DEFAULT_PAGE_INDEX=1;
		final int DEFAULG_PAGE_RECORD_SIZE = 20;
		
		int state = STATE_INIT;
		int totalSize;
		int pageSize=1;
		int pageRecordSize = DEFAULG_PAGE_RECORD_SIZE;
		int currentPage = DEFAULT_PAGE_INDEX;
		
		void setInitState() {
			state = STATE_INIT;
		}
		void setRefreshState() {
			state = STATE_REFRESH;
			resetPageIndex();
		}
		void setLoadState() {
			state = STATE_LOAD;
		}
		boolean isInitState() {
			return state == STATE_INIT;
		}
		boolean isRefreshState() {
			return state==STATE_REFRESH;
		}
		boolean isLoadState() {
			return state==STATE_LOAD;
		}
		public void resetPageIndex() {
			currentPage = DEFAULT_PAGE_INDEX;
		}
		public void addPageIndex() {
			currentPage++;
		}
		public void setTotalSize(int totalSize) {
			this.totalSize = totalSize;
			if(totalSize%pageRecordSize==0) {
				pageSize = totalSize/pageRecordSize;
			} else {
				pageSize = totalSize/pageRecordSize+1;
			}
		}
	}
	
	class MessageController {
		public void show(final String data) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					ToastUtil.set(ActionListBusinessActivity.this, data).show();
				}
			});
		}
		public void stopRefresh() {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					xlv_layer.stopRefresh();
				}
			});
		}
		public void stopLoadMore() {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					xlv_layer.stopLoadMore();
				}
			});
		}
	}
}