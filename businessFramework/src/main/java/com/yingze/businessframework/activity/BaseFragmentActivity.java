/**
 * 顶层的FragmentActivity，一般用于第三方功能嵌套或一些比较普遍的行为的定义
 */
package com.yingze.businessframework.activity;

import cn.jpush.android.api.JPushInterface;

import com.umeng.analytics.MobclickAgent;

import android.support.v4.app.FragmentActivity;

public class BaseFragmentActivity extends FragmentActivity {
	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		JPushInterface.onResume(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		JPushInterface.onPause(this);
	}
}
