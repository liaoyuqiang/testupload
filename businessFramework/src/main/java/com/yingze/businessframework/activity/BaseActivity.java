/**
 * 顶层Activity页面框架，一般用于第三方功能嵌套或定义一些执行步骤方法供调用
 */
package com.yingze.businessframework.activity;

import cn.jpush.android.api.JPushInterface;
import com.umeng.analytics.MobclickAgent;
import android.app.Activity;

public abstract class BaseActivity extends Activity {
	
	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		JPushInterface.onResume(this);
	}
	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		JPushInterface.onPause(this);
	}
}
