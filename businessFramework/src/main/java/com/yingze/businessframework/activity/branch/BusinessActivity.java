/**
 * 用于一般物品详情显示的业务框架界面
 */
package com.yingze.businessframework.activity.branch;

import java.io.OutputStream;
import java.util.ArrayList;

import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import com.yingze.businessframework.R;
import com.yingze.businessframework.activity.AppActivity;
import com.yingze.businessframework.controller.AppNetworkController;
import com.yingze.businessframework.controller.LoadingController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;
import com.yingze.corelibrary.network.BaseNetworkController;
import com.yingze.corelibrary.network.DeckerPostServer;
import com.yingze.corelibrary.network.NetworkClient;
import com.yingze.corelibrary.network.ThreadPoolController;
import com.yingzecorelibrary.utils.ToastUtil;

public abstract class BusinessActivity extends AppActivity implements View.OnClickListener {
	protected Handler mHandler = new Handler();
	protected DeckerPostServer mDeckerPostServer;
	protected NetworkController mNetworkController = new NetworkController();
	protected LoadingController mLoadingController = new LoadingController();// 数据加载控制器
	protected AppNetworkController mAppNetworkController = new AppNetworkController(this);
	
	/**
	 * 返回当前界面title的值
	 * @return title字符串的值
	 */
	protected abstract String getTitleContent();
	
	/**
	 * 获取当前界面的根布局，只要是包含了全部内容即可，但必须是相对布局类型
	 * @return 属于相对布局类型的根目录布局
	 */
	protected abstract RelativeLayout getRootLayout();
	
	/**
	 * 返回请求的服务器的url地址
	 * @return url的字符值
	 */
	protected abstract String getServerUrl();
	
	/**
	 * 收集提交到服务器的数据，根据不同需求上传对应的内容
	 * @param outputStream 从远程获取的写入流引用
	 */
	protected abstract void submitDataToServer(OutputStream outputStream);
	
	/**
	 * 获取服务器返回的界面数据并进行处理
	 * @param content 服务器返回的界面内容
	 */
	protected abstract void parsePageData(String content);
	
	@Override
	public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {
	}
	
	@Override
	public void initWindowFeatureOnCreate() {
	}

	@Override
	public TitleController initTitleOnCreate() {
		return new TitleController() {
			@Override
			public void initTransactionOnCreate() {
				tv_title_middle.setText(getTitleContent());
				tv_title_middle.setTextColor(Color.WHITE);
			}
			@Override
			public void initListenerOnCreate() {
				ll_title_left.setOnClickListener(BusinessActivity.this);
			}
			@Override
			public void injectControllerOnCreate(
					ArrayList<ViewController> controllerArr) {
			}
		};
	}

	@Override
	public void initHooksOnCreate() {
		if(mNetworkController.requestServerIsEffectived) {
			requestServer();
		}
	}

	public void setNetworkEffectived(boolean isEffectived) {
		mNetworkController.requestServerIsEffectived = isEffectived;
	}
	
	public void requestServer() {
		mLoadingController.showProgress(this, getRootLayout());
		NetworkClient client = NetworkClient.newInstance();
		client.setNetworkController(new BaseNetworkController(
				getServerUrl(),
				new NetworkClient.DownloadCallback() {
					@Override
					public void onFinish(String content) {
						parsePageData(content);
						mLoadingController.closeProgress();
					}
					@Override
					public void onFail(Exception e, String message) {
						mLoadingController.showFailPrompts(
							BusinessActivity.this, getRootLayout(), 
							new LoadingController.OnRefreshListener() {
								@Override
								public void onRefresh(View view) {
									requestServer();
								}
							});
					}
					@Override
					public void onDownloadProcess(int readbufferLength, int downloadFileLength,
							int totalFileLength) {
					}
					
				}){
			@Override
			public void uploadPostContent(OutputStream outputStream) {
				submitDataToServer(outputStream);
			}
		});
		mDeckerPostServer = new DeckerPostServer(client);
		ThreadPoolController.getInstance().submit(mDeckerPostServer);
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.ll_title_left) {
			finish();
		}
	}

	@Override
	protected void onDestroy() {
		ThreadPoolController.getInstance().cancel(mDeckerPostServer);
		super.onDestroy();
	}
	
	public void showMessage(final String data) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				ToastUtil.set(BusinessActivity.this, data).show();
			}
		});
	}

	class NetworkController {
		boolean requestServerIsEffectived = true;
	}
	
}