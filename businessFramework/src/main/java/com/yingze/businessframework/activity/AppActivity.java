package com.yingze.businessframework.activity;
/**
 * 基本activity的业务框架
 * <p> 通过继承BaseActivity的基本界面，通过mActivityController对逻辑进行控制</p>
 * @author LiangZH
 * @version v0.0.1
 * @date 2016/7/19 8:48
 */
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.yingze.businessframework.controller.AppNetworkController;
import com.yingze.businessframework.controller.AppActivityController;
import com.yingze.businessframework.controller.LoadingController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;

public abstract class AppActivity extends BaseActivity implements ViewController {
	protected View mContentView;
	protected AppActivityController mActivityController = AppActivityController.NULL;
	protected LoadingController mLoadingController = new LoadingController();// 数据加载控制器
	protected AppNetworkController mAppNetworkController = new AppNetworkController(this);
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//默认竖屏
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		mContentView = LayoutInflater.from(this).inflate(getLayoutResID(), null);
		setContentView(mContentView);
		mActivityController = AppActivityController.newInstance(this, mContentView);
		mActivityController.initOnCreate();
		
	}
	
	@Override
	protected void onDestroy() {
		cleanController();
		super.onDestroy();
	}
	
	/**
	 * 动态添加控制器
	 * <p> 通过动态注入的方式，为界面提供多个控制器,方便对功能进行扩展 </p>
	 * @param controller 动态注入的控制器
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void addController(ViewController controller) {
		mActivityController.getControllerArr().add(controller);
	}
	
	/**
	 * 清空控制器
	 * <p> 将界面原有的控制器逐个清除 </p>
	 * @param controller
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void cleanController() {
		mActivityController.getControllerArr().clear();
	}
	

	
	/**
	 * 返回标题控制器
	 * <p> 返回标题控制器 </p>
	 * @param controller
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public TitleController getTitleController() {
		return mActivityController.getTitleController();
	}
	
}

