/**
 * 列表业务框架界面
 */
package com.yingze.businessframework.activity.branch;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.yingze.businessframework.R;
import com.yingzecorelibrary.view.XListView;

public abstract class ListBusinessActivity extends BusinessActivity {
	protected RelativeLayout rl_main_layer;
	protected XListView xlv_layer;
	protected BaseAdapter mListBusinessAdapter;
	
	protected abstract ListBusinessAdapterCallback createListBusinessAdapterCallback();
	
	@Override
	public void initResourceViewOnCreate(View contentView) {
		rl_main_layer = (RelativeLayout) contentView.findViewById(R.id.rl_main_layer);
		xlv_layer = (XListView) contentView.findViewById(R.id.xlv_layer);
	}

	@Override
	public void initTransactionOnCreate() {
		mListBusinessAdapter = new ListBusinessAdapter(createListBusinessAdapterCallback());
		xlv_layer.setAdapter(mListBusinessAdapter);
		xlv_layer.setPullLoadEnable(false);
		xlv_layer.setPullRefreshEnable(false);
	}

	@Override
	public void initListenerOnCreate() {
		
	}

	@Override
	public int getLayoutResID() {
		return R.layout.template_activity_list;
	}

	@Override
	protected RelativeLayout getRootLayout() {
		return rl_main_layer;
	}
	
	class ListBusinessAdapter extends BaseAdapter{
		ListBusinessAdapterCallback listBusinessAdapterCallback;
		
		public ListBusinessAdapter(ListBusinessAdapterCallback listBusinessAdapterCallback) {
			this.listBusinessAdapterCallback = listBusinessAdapterCallback;
		}
		@Override
		public int getCount() {
			return listBusinessAdapterCallback.getCount();
		}
		@Override
		public Object getItem(int position) {
			return position;
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewCursor vCursor;
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(ListBusinessActivity.this);
				convertView = inflater.inflate(listBusinessAdapterCallback.getLayoutResID(), null);
				vCursor = listBusinessAdapterCallback.createViewCursor();
				listBusinessAdapterCallback.initResourceView(vCursor, convertView);
				convertView.setTag(vCursor);
			} else {
				vCursor = (ViewCursor) convertView.getTag();
			}
			listBusinessAdapterCallback.initTransaction(vCursor, position);
			listBusinessAdapterCallback.initListener(vCursor, position);
			return convertView;
		}
	}
	
	public static interface ListBusinessAdapterCallback {
		public int getCount();
		public int getLayoutResID();
		public ViewCursor createViewCursor();
		public void initResourceView(ViewCursor vCursor, View contentView);
		public void initTransaction(ViewCursor vCursor, int position);
		public void initListener(ViewCursor vCursor, int position);
	}
	
	/**
	 * 视图引用类
	 * @author LittleBird
	 * 描述：界面控件的视图引用
	 */
	public static class ViewCursor {
	}
	
	/**
	 * 列表业务控制器
	 * @author LittleBird
	 * 作用：用于对列表类型的业务界面提供封装view与model交互的操作，主要复写了几个方法，
	 *     然后在对外提供新的抽象方法代替原来方法的功能,子类可根据自身情况进行扩展
	 */
	protected abstract class ListBusinessController<V extends ViewCursor,M> implements ListBusinessAdapterCallback{
		private ArrayList<M> businessModeArr = new ArrayList<M>();
		
		public abstract void initResourceViewOnCreate(V viewCursor, View convertView);
		public abstract void initTransactionOnCreate(V viewCursor, M mode, int position);
		public abstract void initListenerOnCreate(V viewCursor, M mode, int position);
		
		@Override
		public int getCount() {
			return businessModeArr.size();
		}
		@Override
		public void initResourceView(ViewCursor vCursor, View contentView) {
			V viewCursor = changeType(vCursor);
			initResourceViewOnCreate(viewCursor, contentView);
		}
		@Override
		public void initTransaction(ViewCursor vCursor, int position) {
			V viewCursor = changeType(vCursor);
			M mode = businessModeArr.get(position);
			initTransactionOnCreate(viewCursor, mode, position);
		}
		@Override
		public void initListener(ViewCursor vCursor, int position) {
			V viewCursor = changeType(vCursor);
			M mode = businessModeArr.get(position);
			initListenerOnCreate(viewCursor, mode, position);
		}
		@SuppressWarnings("unchecked")
		public V changeType(ViewCursor vCursor) {
			return (V)vCursor;
		}
		public void notifyDataSetChanged(final ArrayList<M> businessModeArr) {
			if(mListBusinessAdapter!=null && businessModeArr!=null) {
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						setBusinessModeArr(businessModeArr);
						mListBusinessAdapter.notifyDataSetChanged();
					}
				});
			}
		}
		public ArrayList<M> getBusinessModeArr() {
			return businessModeArr;
		}
		public void setBusinessModeArr(ArrayList<M> businessModeArr) {
			this.businessModeArr = businessModeArr;
		}	
	}
}
