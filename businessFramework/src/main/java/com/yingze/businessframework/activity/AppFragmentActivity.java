/**
 * 基本fragmentactivity的业务框架
 */
package com.yingze.businessframework.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.yingze.businessframework.controller.AppActivityController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;

public abstract class AppFragmentActivity extends BaseFragmentActivity implements ViewController {
	protected View mContentView;
	protected AppActivityController mActivityController = AppActivityController.NULL;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//默认竖屏
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		mContentView = LayoutInflater.from(this).inflate(getLayoutResID(), null);
		setContentView(mContentView);
		mActivityController = AppActivityController.newInstance(this, mContentView);
		mActivityController.initOnCreate();
	}
	
	@Override
	protected void onDestroy() {
		cleanController();
		super.onDestroy();
	}
	
	/**
	 * 动态添加控制器
	 * @param controller
	 */
	public void addController(ViewController controller) {
		mActivityController.getControllerArr().add(controller);
	}
	
	/**
	 * 清空控制器
	 */
	public void cleanController() {
		mActivityController.getControllerArr().clear();
	}
	
	/**
	 * 返回标题控制器
	 * @return
	 */
	public TitleController getTitleController() {
		return mActivityController.getTitleController();
	}
}
