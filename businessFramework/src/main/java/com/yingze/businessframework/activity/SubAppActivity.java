package com.yingze.businessframework.activity;

import android.os.Bundle;



import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yingze.businessframework.R;


public abstract  class SubAppActivity extends AppActivity{
	protected ImageLoader imageLoader;
	protected  DisplayImageOptions options;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.ic_nopic)
		.showImageForEmptyUri(R.drawable.ic_nopic)
		.showImageOnFail(R.drawable.ic_nopic)
		.imageScaleType(ImageScaleType.EXACTLY)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)		
		.build();
		super.onCreate(savedInstanceState);

	}

}
