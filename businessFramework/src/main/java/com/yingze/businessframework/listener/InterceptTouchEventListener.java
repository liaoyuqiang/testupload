package com.yingze.businessframework.listener;

import android.view.MotionEvent;

public interface InterceptTouchEventListener {
	/**
	 * 设置事件拦截控制处理
	 * 
	 * @param onInterceptTouchEvent
	 *            是否进行事件拦截
	 * @param type
	 *            拦截事件的类型
	 */
	public void onInterceptTouchEventHandle(boolean onInterceptTouchEvent,
			String type, MotionEvent event);
}
