package com.yingze.businessframework.listener;

public interface OnScrollLayoutControllerActionCallback {
	public static int STATE_ACTIVITY_BACKGROUND = 0;
	public static int STATE_ACTIVITY_FOREGROUND = 1;
	
	public void injectOnScrollLayoutControllerActionCallback(Object itself);
	
	public void onCreateHandle();
	public void onStopHandle();
	public void onRestartHandle();
	public void onFinishHandle();
	public void onRecoverHandle(int activityState);
	
	public int getLayoutOffsetHorizontal();
	public void setLayoutOffsetHorizontal(int offset);
	public int getCurrentScrollOffsetHorizontal();
	public void setCurrentScrollOffsetHorizontal(int offset);
	public void resetCurrentScrollOffsetHorizontal();
	public void setScrollEffectived(boolean isScrollEffectived);
	
	public static OnScrollLayoutControllerActionCallback NULL = new OnScrollLayoutControllerActionCallback() {
		@Override
		public void injectOnScrollLayoutControllerActionCallback(Object itself) {
		}
		@Override
		public void onCreateHandle() {
		}
		@Override
		public void onStopHandle() {
		}
		@Override
		public void onRestartHandle() {
		}
		@Override
		public void onFinishHandle() {
		}
		@Override
		public void onRecoverHandle(int activityState) {
		}
		@Override
		public int getLayoutOffsetHorizontal() {
			return 0;
		}
		@Override
		public void setLayoutOffsetHorizontal(int offset) {
		}
		@Override
		public int getCurrentScrollOffsetHorizontal() {
			return 0;
		}
		@Override
		public void setCurrentScrollOffsetHorizontal(int offset) {
		}
		@Override
		public void setScrollEffectived(boolean isScrollEffectived) {
		}
		@Override
		public void resetCurrentScrollOffsetHorizontal() {
		}
	};
}
