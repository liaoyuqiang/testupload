/**
 * 控件滑动回调监听器
 */
package com.yingze.businessframework.listener;

import com.yingze.businessframework.component.CompatScrollView;

public interface ViewScrollCallback extends CompatScrollView.ScrollCallback {
	public boolean isPullEffectived();
	public boolean isLoadEffectived();
	
//	public static ViewScrollCallback getNullInstance() {
//		return new NullCallback();
//	}
	
	public static class NullCallback implements ViewScrollCallback {
		@Override
		public void onStartRefresh() {
		}
		@Override
		public void onStopRefresh() {
		}
		@Override
		public void onStartLoad() {
		}
		@Override
		public void onStopLoad() {
		}
		@Override
		public boolean isPullEffectived() {
			return false;
		}
		@Override
		public boolean isLoadEffectived() {
			return false;
		}
	}
}
