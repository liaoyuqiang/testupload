/**
 * 进度加载控制器
 */
package com.yingze.businessframework.controller;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.yingze.businessframework.R;

public class LoadingController {
	protected boolean isEffectived = true;
	protected String mLoadingDescriptions = "加载中...";
	protected LayoutInflater mLayoutInflater;
	protected View mLoadingView;
	protected ViewGroup mGroup;
	protected Handler mHandler = new Handler();
	
	public void showProgress(final Context context, final ViewGroup group) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived || context==null) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = mLayoutInflater.inflate(R.layout.template_layout_loading_data, null);
				TextView tv_loading_label = (TextView) mLoadingView.findViewById(R.id.tv_loading_label);
				tv_loading_label.setText(mLoadingDescriptions);
				if(group!=null&&mLoadingView!=null){
					group.addView(mLoadingView,new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				}
			}
		});
	}
	
	public void showProgress(final Context context, final ViewGroup group, final int resLayoutId) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived || context==null) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = mLayoutInflater.inflate(resLayoutId, null);
				group.addView(mLoadingView,new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			}
		});
	}
	
	public void showProgress(final Context context, final ViewGroup group, final View layout) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived || context==null) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = layout;
				group.addView(mLoadingView,new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			}
		});
	}
	
	public void cancelProgress() {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
			}
		});
	}
	
	public void closeProgress() {
		isEffectived = false;
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
			}
		});
	}
	
	public void showFailPrompts(final Context context, final ViewGroup group, 
			final OnRefreshListener onRefreshListener) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived || context==null) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = mLayoutInflater.inflate(R.layout.template_layout_loading_data_fail, null);
				group.addView(mLoadingView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

				mLoadingView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onRefreshListener.onRefresh(v);	
					}
				});
			}
		});
	}
	
	public void showFailPrompts(final Context context, final ViewGroup group, final int resLayoutId,
			final OnRefreshListener onRefreshListener) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived || context==null) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = mLayoutInflater.inflate(resLayoutId, null);
				group.addView(mLoadingView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

				mLoadingView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onRefreshListener.onRefresh(v);	
					}
				});
			}
		});
	}
	
	public void showFailPrompts(final Context context, final ViewGroup group, final View layout,
			final OnRefreshListener onRefreshListener) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived || context==null) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = layout;
				group.addView(mLoadingView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

				mLoadingView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onRefreshListener.onRefresh(v);	
					}
				});
			}
		});
	}
	
	public String getLoadingDescriptions() {
		return mLoadingDescriptions;
	}

	public void setLoadingDescriptions(String loadingDescriptions) {
		this.mLoadingDescriptions = loadingDescriptions;
	}
	
	public static interface OnRefreshListener{
		public void onRefresh(View view);
	}
	
}
