package com.yingze.businessframework.controller;
/**
 * 连接activity scrolllayout之间的中介者
 * @author LiangZH
 * @version v0.0.1
 * @date 2016/7/19 8:48
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.yingze.businessframework.component.ScrollLayout;
import com.yingze.businessframework.listener.IntentRegister;
import com.yingze.businessframework.listener.OnScrollLayoutControllerActionCallback;

@SuppressLint("HandlerLeak")
public class ScrollLayoutController implements ScrollLayout.OnScrollLayoutCallback {
	public final static String FLAG_ACTIVITY_CODE = "flag_activity_code";
	public final static String FLAG_HANDLER_CODE = "flag_handler_code";
	private boolean isFinished = false;
	private String mLastActivityFilterAction="";// 上一个activity的广播接收器拦截条件
	private String mPresentActivityFilterAction="";// 当前activity的广播接收器拦截条件
	
	private Context mContext;
	private ActivityAnimationController mActivityAnimationFilter;
	private ActivityActionController mActivityActionController;
	private OnScrollLayoutControllerActionCallback mCurrentOnScrollLayoutControllerActionCallback;
	private OnScrollLayoutControllerActionCallback mLastOnScrollLayoutControllerActionCallback;
	private OnScrollLayoutControllerStateCallback mOnScrollLayoutControllerStateCallback;
	
	public ScrollLayoutController(Context context, OnScrollLayoutControllerActionCallback scrollLayout, 
			OnScrollLayoutControllerStateCallback scrollLayoutControllerCallback,
			String lastActivityFilterAction, String presentActivityFilterAction) {
		mContext = context;
		mCurrentOnScrollLayoutControllerActionCallback = scrollLayout;
		mLastActivityFilterAction = lastActivityFilterAction;
		mPresentActivityFilterAction = presentActivityFilterAction;
		mOnScrollLayoutControllerStateCallback = scrollLayoutControllerCallback;

		mCurrentOnScrollLayoutControllerActionCallback.injectOnScrollLayoutControllerActionCallback(this);
		mLastOnScrollLayoutControllerActionCallback = IntentRegister.scroll;
		if (mLastOnScrollLayoutControllerActionCallback == null) {
			mLastOnScrollLayoutControllerActionCallback = OnScrollLayoutControllerActionCallback.NULL;
		}
		IntentRegister.scroll = null;
		
		mActivityAnimationFilter = ActivityAnimationController.NULL;
		mActivityActionController = ActivityActionController.NULL;
		
		mCurrentOnScrollLayoutControllerActionCallback.setScrollEffectived(
				mActivityActionController.isActivityScrollEffectived());
	}
	
	public ScrollLayoutController(Context context, OnScrollLayoutControllerActionCallback scrollLayout, 
			OnScrollLayoutControllerStateCallback scrollLayoutControllerCallback,
			String lastActivityFilterAction, String presentActivityFilterAction,
			ActivityAnimationController animationFilter, ActivityActionController activityActionController) {
		mContext = context;
		mCurrentOnScrollLayoutControllerActionCallback = scrollLayout;
		mLastActivityFilterAction = lastActivityFilterAction;
		mPresentActivityFilterAction = presentActivityFilterAction;
		mOnScrollLayoutControllerStateCallback = scrollLayoutControllerCallback;

		mCurrentOnScrollLayoutControllerActionCallback.injectOnScrollLayoutControllerActionCallback(this);
		mLastOnScrollLayoutControllerActionCallback = IntentRegister.scroll;
		if (mLastOnScrollLayoutControllerActionCallback == null) {
			mLastOnScrollLayoutControllerActionCallback = OnScrollLayoutControllerActionCallback.NULL;
		}
		IntentRegister.scroll = null;
		
		mActivityAnimationFilter = animationFilter;
		if (mActivityAnimationFilter == null) {
			mActivityAnimationFilter = ActivityAnimationController.NULL;
		}
		
		mActivityActionController = activityActionController;
		if (mActivityActionController == null) {
			mActivityActionController = ActivityActionController.NULL;
		}
		mCurrentOnScrollLayoutControllerActionCallback.setScrollEffectived(
				mActivityActionController.isActivityScrollEffectived());
	}
	
	/**
	 * 滑动回调
	 * <p> 手指所在scrolllayout滑动的时候，通知当前滑动的偏移距离 </p>
	 * <ul>
	 *  <li> mLastOnScrollLayoutControllerActionCallback 进行联动滑动的回调接口 </li>
	 *  <li> mLastOnScrollLayoutControllerActionCallback.setCurrentScrollOffsetHorizontal
	 *       设置联动滑动接口的当前视屏滑动偏移距离</li> 
	 * </ul>
	 * @param gap 联动的滑动距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	@Override
	public void onScroll(int gap) {
		if (mLastOnScrollLayoutControllerActionCallback != null) {
			Log.e("scroll", "@@@@@@@@@@@@@ "+mLastOnScrollLayoutControllerActionCallback.getLayoutOffsetHorizontal()+","+gap);
			mLastOnScrollLayoutControllerActionCallback.setCurrentScrollOffsetHorizontal(-mLastOnScrollLayoutControllerActionCallback.getLayoutOffsetHorizontal() + gap);
		}
	}
	
	/**
	 * 滑动事件的当前状态回调
	 * <p> 手指所在scrolllayout滑动的时候，通知当前状态的变化 </p>
	 * <ul>
	 *  <li> mLastOnScrollLayoutControllerActionCallback 进行联动滑动的回调接口 </li>
	 *  <li> mLastOnScrollLayoutControllerActionCallback.setCurrentScrollOffsetHorizontal
	 *       设置联动滑动接口的当前水平滑动偏移距离</li> 
	 * </ul>
	 * @param gap 联动的滑动距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	@Override
	public void onEventActionState(int actionType) {
		if (actionType == ScrollLayout.EVENT_STATE_KEEP_CURRENT_ACTIVITY) {
			onActivityRecoverHandle();
		} else {
			onActivityFinishHandle();
		}
	}
	
	/**
	 * 滑动事件结束状态回调
	 * <p> 手指所在scrolllayout滑动结束的时候，执行回调 </p>
	 * @param gap 联动的滑动距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	@Override
	public void onScrollLayoutFinish() {
		if (mOnScrollLayoutControllerStateCallback != null) {
			mOnScrollLayoutControllerStateCallback.onScrollLayoutControllerFinish();
		}
		mLastOnScrollLayoutControllerActionCallback = null;
		mCurrentOnScrollLayoutControllerActionCallback = null;
	}
	
	/**
	 * 设置上一个滑动接口的偏移
	 * <p> 设置上一个滑动接口的偏移 </p>
	 * @param gap 联动的滑动距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void setLastScrollLayoutGap(int gap) {
		if (mLastOnScrollLayoutControllerActionCallback != null) {
			Log.e("scroll", "^^^^^^^^^^^^^^ "+mLastOnScrollLayoutControllerActionCallback.getLayoutOffsetHorizontal()+","+gap);
			mLastOnScrollLayoutControllerActionCallback.setCurrentScrollOffsetHorizontal(gap);
		}
	}
	
	/**
	 * 恢复当前滑动接口的水平偏移位置
	 * <p> 复位当前滑动接口位置，多用于activity的onResume及onRestart调用</p>
	 * @param gap 联动的滑动距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void resetCurrentScrollOffsetHorizontal() {
		mCurrentOnScrollLayoutControllerActionCallback.resetCurrentScrollOffsetHorizontal();
	}
	
	/**
	 * 注入指定页面的监听器拦截条件
	 * <p> 当activity进行跳转的时候，执行此方法，动态注入条件标识 </p>
	 * <ul>
	 *  <li> mPresentActivityFilterAction 指定的跳转标识 </li>
	 *  <li> IntentRegister.scroll = mCurrentOnScrollLayoutControllerActionCallback  
	 *  	 动态指定当前滑动的组件，主要供后续跳转后的引用需要</li>
	 * </ul>
	 * @param intent activity跳转的意图
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void injectIntentProperty(Intent intent) {
		intent.putExtra(FLAG_ACTIVITY_CODE, mPresentActivityFilterAction);
		IntentRegister.scroll = mCurrentOnScrollLayoutControllerActionCallback;
	}
	
	/**
	 * 获取intent中绑定的前一个页面的监听器拦截条件
	 * <p> 动态注入activity的跳转标识条件 </p>
	 * @param gap 联动的滑动距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public static String getIntentInjectProperty(Intent intent) {
		return intent.getStringExtra(FLAG_ACTIVITY_CODE);
	}
	
	/**
	 * 在activity的onCreate方法执行处理
	 * <p> 当前activity执行oncreate方法的时候，执行对应的动作处理 </p>
	 * <ul>
	 *  <li> mLastOnScrollLayoutControllerActionCallback 上一个滑动组件回调接口</li>
	 *  <li> mCurrentOnScrollLayoutControllerActionCallback 当前滑动组件回调接口 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void onActivityCreateHandle() {
		if (mActivityAnimationFilter.isAnimationInActivityCreateEffectived()) {
			if (mLastOnScrollLayoutControllerActionCallback!=null) {
				mLastOnScrollLayoutControllerActionCallback.onStopHandle();
			}
			if (mCurrentOnScrollLayoutControllerActionCallback!=null) {
				mCurrentOnScrollLayoutControllerActionCallback.onCreateHandle();
			}
		}
	}
	
	/**
	 * 在activity的onStop方法执行处理
	 * <p> 当前activity执行onStop方法的时候，执行对应的动作处理 </p>
	 * <ul>
	 *  <li> mCurrentOnScrollLayoutControllerActionCallback 当前滑动组件回调接口 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void onActivityStopHandle() {
		if (mActivityAnimationFilter.isAnimationInActivityStopEffectived()) {
			if (mCurrentOnScrollLayoutControllerActionCallback!=null) {
				mCurrentOnScrollLayoutControllerActionCallback.onStopHandle();
			}
		}
	}

	/**
	 * 在activity的onRestart方法执行处理
	 * <p> 当前activity执行onRestart方法的时候，执行对应的动作处理 </p>
	 * <ul>
	 *  <li> mCurrentOnScrollLayoutControllerActionCallback 当前滑动组件回调接口 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void onActivityRestartHandle() {
		if (mActivityAnimationFilter.isAnimationInActivityRestartEffectived()) {
			if (mCurrentOnScrollLayoutControllerActionCallback!=null) {
				mCurrentOnScrollLayoutControllerActionCallback.onRestartHandle();
			}
		}
	}
	
	/**
	 * 在activity的finish方法执行处理
	 * <p> 当前activity执行finish方法的时候，执行对应的动作处理 </p>
	 * <ul>
	 *  <li> mLastOnScrollLayoutControllerActionCallback 上一个滑动组件回调接口</li>
	 *  <li> mCurrentOnScrollLayoutControllerActionCallback 当前滑动组件回调接口 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public synchronized void onActivityFinishHandle() {
		if (isFinished)return;
		isFinished = true;
		if (mActivityAnimationFilter.isAnimationInActivityFinishEffectived()) {
			if (mLastOnScrollLayoutControllerActionCallback!=null) {
				mLastOnScrollLayoutControllerActionCallback.onRestartHandle();
			}
 			if (mCurrentOnScrollLayoutControllerActionCallback!=null) {
 				mCurrentOnScrollLayoutControllerActionCallback.onFinishHandle();
 			}
		} else {
			onScrollLayoutFinish();
		}
	}
	
	/**
	 * 在activity的recover状态中执行处理
	 * <p> 当前activity处于recover状态的时候，执行对应的动作处理 </p>
	 * <ul>
	 *  <li> mLastOnScrollLayoutControllerActionCallback 上一个滑动组件回调接口</li>
	 *  <li> mCurrentOnScrollLayoutControllerActionCallback 当前滑动组件回调接口 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void onActivityRecoverHandle() {
		if (mActivityAnimationFilter.isAnimationInActivityRecoverEffectived()) {
			if (mLastOnScrollLayoutControllerActionCallback!=null) {
				mLastOnScrollLayoutControllerActionCallback.onRecoverHandle(OnScrollLayoutControllerActionCallback.STATE_ACTIVITY_BACKGROUND);
			}
			if (mCurrentOnScrollLayoutControllerActionCallback!=null) {
				mCurrentOnScrollLayoutControllerActionCallback.onRecoverHandle(OnScrollLayoutControllerActionCallback.STATE_ACTIVITY_FOREGROUND);
			}
		}
	}

	/**
	 * 设置滚动回调监听
	 * @param scrollLayoutControllerCallback
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void setScrollLayoutControllerCallback(OnScrollLayoutControllerStateCallback scrollLayoutControllerCallback) {
		mOnScrollLayoutControllerStateCallback = scrollLayoutControllerCallback;
	}  

    public static interface OnScrollLayoutControllerStateCallback {
    	public void onScrollLayoutControllerFinish();
    }

    public static interface ActivityAnimationController {
    	public boolean isAnimationInActivityCreateEffectived();
    	public boolean isAnimationInActivityStopEffectived();
    	public boolean isAnimationInActivityRestartEffectived();
    	public boolean isAnimationInActivityFinishEffectived();
    	public boolean isAnimationInActivityRecoverEffectived();
    	
    	public static ActivityAnimationController NULL = new ActivityAnimationController() {
			@Override
			public boolean isAnimationInActivityCreateEffectived() {
				return true;
			}
			@Override
			public boolean isAnimationInActivityStopEffectived() {
				return true;
			}
			@Override
			public boolean isAnimationInActivityRestartEffectived() {
				return true;
			}
			@Override
			public boolean isAnimationInActivityFinishEffectived() {
				return true;
			}
			@Override
			public boolean isAnimationInActivityRecoverEffectived() {
				return true;
			}
    	};
    }
    
    public static interface ActivityActionController {
    	public boolean isActivityScrollEffectived();
    	
    	public static ActivityActionController NULL = new ActivityActionController() {
			@Override
			public boolean isActivityScrollEffectived() {
				return true;
			}
    	};
    };
    
}
