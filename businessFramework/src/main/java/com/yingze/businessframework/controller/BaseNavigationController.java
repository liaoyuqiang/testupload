/**
 * 导航栏控制，用于不同种类项目点击及筛选的点击处理
 * @author LiangZiHao
 */
package com.yingze.businessframework.controller;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.yingze.businessframework.R;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;
import com.yingzecorelibrary.utils.LogUtil;
import com.yingzecorelibrary.view.HorizontalListView;
import com.yingzecorelibrary.view.HorizontalListView.OnScrollStateChangedListener;

@SuppressLint("NewApi")
public class BaseNavigationController implements ViewController,View.OnClickListener,OnItemClickListener,OnScrollStateChangedListener{
	private Context context;
	private ImageView iv_guide_left;
	private ImageView iv_guide_right;
	private View view_bottom_line;
	private LinearLayout navigation_filter;
//	private LinearLayout ll_content;
	private HorizontalListView hlv_navigation;
	private FieldController fieldController;
	private NavigationCallback mNavigationCallback;
	private ArrayList<FieldController> fieldControllerArr = new ArrayList<FieldController>();
	
	private NavigationAdapter mNavigationAdapter;
	
	public BaseNavigationController(Context context, NavigationCallback navigationCallback) {
		this.context = context;
		mNavigationCallback = navigationCallback;
	}
	
	@Override
	public void injectControllerOnCreate(
			ArrayList<ViewController> controllerInfoArr) {
	}
	
	@Override
	public void initWindowFeatureOnCreate() {
	}
	
	@Override
	public void initResourceViewOnCreate(View contentView) {
		navigation_filter = (LinearLayout) contentView.findViewById(R.id.navigation_filter);
		hlv_navigation = (HorizontalListView) contentView.findViewById(R.id.hlv_navigation);
//		ll_content = (LinearLayout) contentView.findViewById(R.id.ll_content);
		iv_guide_left = (ImageView) contentView.findViewById(R.id.iv_guide_left);
		iv_guide_right = (ImageView) contentView.findViewById(R.id.iv_guide_right);
		view_bottom_line = contentView.findViewById(R.id.view_bottom_line);
	}
	
	@Override
	public void initTransactionOnCreate() {
		mNavigationAdapter = new NavigationAdapter(context);
		hlv_navigation.setAdapter(mNavigationAdapter);
	}
	
	@Override
	public void initListenerOnCreate() {
		navigation_filter.setOnClickListener(this);
		hlv_navigation.setOnItemClickListener(this);
		hlv_navigation.setOnScrollStateChangedListener(this);
	}
	
	@Override
	public int getLayoutResID() {
		return 0;
	}
	
	@Override
	public TitleController initTitleOnCreate() {
		return null;
	}
	
	@Override
	public void initHooksOnCreate() {
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.navigation_filter) {
			mNavigationCallback.onNavigationFilterHandle();
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		LogUtil.set("hlist", "+++++++++ : "+position).verbose();
		if(fieldController==null) {
			fieldController = fieldControllerArr.get(position);
			fieldController.setFocus();
		} else {
			if(fieldController==fieldControllerArr.get(position)) {
				fieldController.clickHandle();
			} else {
				fieldController.resetState();
				fieldController=fieldControllerArr.get(position);
				fieldController.setFocus();
			}
		}
		mNavigationAdapter.notifyDataSetChanged();
		mNavigationCallback.onNavigationCategoryChangedHandle(fieldControllerArr.indexOf(fieldController));
	}
	
	@Override
	public void onScrollStateChanged(ScrollState scrollState) {
		int FirstVisiblePositionInAdapter = hlv_navigation.getFirstVisiblePosition();
		LogUtil.set("hlistview", "FirstVisiblePositionInAdapter : "+FirstVisiblePositionInAdapter).verbose();
		if(scrollState==ScrollState.SCROLL_STATE_FLING || 
				scrollState==ScrollState.SCROLL_STATE_TOUCH_SCROLL){
			updateNavigationGuide();
		} else if(scrollState==ScrollState.SCROLL_STATE_IDLE) {
			updateNavigationGuide();
		}
	}
	
	public void setFilterVisible(boolean isVisible) {
		navigation_filter.setVisibility(
				isVisible ? View.VISIBLE:View.GONE);
	}
	
	public void setBottomLineVisible(boolean isVisible) {
		view_bottom_line.setVisibility(
				isVisible ? View.VISIBLE:View.GONE);
	}
	
	public void updateNavigationGuide() {
		updateNavigationGuide(hlv_navigation.getFirstVisiblePosition());
	}
	
	public void updateNavigationGuide(int FirstVisiblePositionInAdapter) {
		if(FirstVisiblePositionInAdapter<0 || FirstVisiblePositionInAdapter>=fieldControllerArr.size()){ 
			return;
		}
		Bitmap ic_guide_left = BitmapFactory.decodeResource(context.getResources(), R.drawable.template_ic_guide_left);
		Bitmap ic_guide_left_unvisible = BitmapFactory.decodeResource(context.getResources(), R.drawable.template_ic_guide_left_unvisible);
		Bitmap ic_guide_right = BitmapFactory.decodeResource(context.getResources(), R.drawable.template_guide_right);
		Bitmap ic_guide_right_unvisible = BitmapFactory.decodeResource(context.getResources(), R.drawable.template_ic_guide_right_unvisible);
		
		if(FirstVisiblePositionInAdapter==0) {
			iv_guide_left.setBackground(new BitmapDrawable(context.getResources(), ic_guide_left_unvisible));
		} else {
			iv_guide_left.setBackground(new BitmapDrawable(context.getResources(), ic_guide_left));
		}
		
		if(FirstVisiblePositionInAdapter+hlv_navigation.getChildCount()==fieldControllerArr.size()) {
			iv_guide_right.setBackground(new BitmapDrawable(context.getResources(), ic_guide_right_unvisible));
		} else {
			iv_guide_right.setBackground(new BitmapDrawable(context.getResources(), ic_guide_right));
		}
	}
	
	/**
	 * 动态添加表头的域值
	 * @param name 表头域的名称
	 * @param field 表头域的域名
	 */
	public void addField(String name, String field) {
		FieldController fieldController = new FieldController(context, name, field);
		fieldControllerArr.add(fieldController);
	}
	
	/**
	 * 动态添加表头的域值
	 * @param name 表头域的名称
	 * @param field 表头域的域名
	 */
	public void addField(String name, String field, int selectBg, int unSelectBg, boolean isShowArrow) {
		FieldController fieldController = new FieldController(context, name, field, selectBg, unSelectBg, isShowArrow);
		fieldControllerArr.add(fieldController);
	}
	
	/**
	 * 收集提交的数据
	 * @param obj
	 * @throws JSONException 
	 */
	public void collectData(JSONObject obj) throws JSONException {
		String field = fieldController!=null ? fieldController.field:"";
		String order = fieldController!=null ? fieldController.order:"";
		obj.put("field", field);
		obj.put("order", order);
	}
	
	/**
	 * 获取域的名称，用于提交到服务器
	 * @return
	 */
	public String getField() {
		return fieldController!=null ? fieldController.field:"";
	}
	
	/**
	 * 获取域是否倒序，用于提交到服务器
	 * @return
	 */
	public String getOrder() {
		return fieldController!=null ? fieldController.order:"";
	}
	
	public void setSelection(int index) {
		if(index<0 || index>=fieldControllerArr.size()){ 
			return;
		}
		updateNavigationGuide(index);
		FieldController selector = fieldControllerArr.get(index);
		
		if(fieldController==null) {
			fieldController = selector;
			fieldController.setFocus();
		} else {
			if(fieldController==selector) {
				fieldController.clickHandle();
			} else {
				fieldController.resetState();
				fieldController=selector;
				fieldController.setFocus();
			}
		}
		mNavigationAdapter.notifyDataSetChanged();
		
	}
	
	public void setNavigationCallback(NavigationCallback navigationCallback) {
		mNavigationCallback = navigationCallback;
	}
	
	public void setGuideVisible(int visibility) {
		iv_guide_left.setVisibility(visibility);
		iv_guide_right.setVisibility(visibility);
	}
	
	class NavigationAdapter extends BaseAdapter {
		LayoutInflater layoutInflater;
		public NavigationAdapter(Context context) {
			layoutInflater = LayoutInflater.from(context);
		}
		@Override
		public int getCount() {
			return fieldControllerArr.size();
		}
		@Override
		public Object getItem(int position) {
			return position;
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewCursor vCursor;
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.template_layout_navigation_sort, null);
				vCursor = new ViewCursor();
				vCursor.ll_content = (LinearLayout)convertView.findViewById(R.id.ll_content);
				vCursor.iv_sort_thumb = (ImageView)convertView.findViewById(R.id.iv_sort_thumb);
				vCursor.tv_sort_title = (TextView) convertView.findViewById(R.id.tv_sort_title);
				convertView.setTag(vCursor);
			} else {
				vCursor = (ViewCursor) convertView.getTag();
			}
			
			if(fieldControllerArr.get(position).unSelectBg != FieldController.BG_VALUE_NULL) {
				vCursor.tv_sort_title.setBackgroundResource(fieldControllerArr.get(position).unSelectBg);
			}
			
			if(fieldControllerArr.get(position).isShowArrow) {
				vCursor.iv_sort_thumb.setVisibility(View.VISIBLE);
			} else {
				vCursor.iv_sort_thumb.setVisibility(View.GONE);
			}
			
			if(fieldController==null || fieldControllerArr.indexOf(fieldController)!=position) {
				vCursor.tv_sort_title.setTextColor(Color.BLACK);
				vCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_desc_default);
			} else {
				vCursor.tv_sort_title.setTextColor(Color.RED);
				if(fieldControllerArr.get(position).selectBg != FieldController.BG_VALUE_NULL) {
					vCursor.tv_sort_title.setBackgroundResource(fieldControllerArr.get(position).selectBg);
				}
				if(fieldController.order.equals(FieldController.ORDER_ASC)) {
					vCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_asc);
				} else {
					vCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_desc);
				}
			}

			vCursor.tv_sort_title.setText(fieldControllerArr.get(position).name);
			return convertView;
		}
		
	}
	
	class ViewCursor {
		LinearLayout ll_content;
		TextView tv_sort_title;
		ImageView iv_sort_thumb;
	}
	
	class FieldController {
		final static String ORDER_DESC = "0";// 降序，从大到小
		final static String ORDER_ASC = "1";// 升序,从小到大
		final static int BG_VALUE_NULL = -1;
		String name;
		String field = "";// 查找的域
		String order = ORDER_DESC;// 升序/降序
		
		int selectBg = BG_VALUE_NULL;
		int unSelectBg = BG_VALUE_NULL;
		boolean isShowArrow = true;
		
		public FieldController(Context context, String name, String field) {
			this.name = name;
			this.field = field;
		}
		
		public FieldController(Context context, String name, String field,
				int selectBg, int unSelectBg, boolean isShowArrow) {
			this.name = name;
			this.field = field;
			this.selectBg = selectBg;
			this.unSelectBg = unSelectBg;
			this.isShowArrow = isShowArrow;
		}
		
		public void resetState() {
			int position = fieldControllerArr.indexOf(this);
			int firstVisiblePositionInAdapter = hlv_navigation.getFirstVisiblePosition();
			int index = position - firstVisiblePositionInAdapter;
			if(index>=0 && index<hlv_navigation.getChildCount()) {
				View view = hlv_navigation.getChildAt(index);
				ViewCursor viewCursor = (ViewCursor) view.getTag();
				order = ORDER_DESC;
				viewCursor.tv_sort_title.setTextColor(Color.BLACK);
				if(unSelectBg != BG_VALUE_NULL) {
					viewCursor.tv_sort_title.setBackgroundResource(unSelectBg);
				}
				viewCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_desc_default);
				if(isShowArrow) {
					viewCursor.iv_sort_thumb.setVisibility(View.VISIBLE);
				} else {
					viewCursor.iv_sort_thumb.setVisibility(View.GONE);
				}
			}
		}
		
		public void setFocus() {
			int position = fieldControllerArr.indexOf(this);
			int firstVisiblePositionInAdapter = hlv_navigation.getFirstVisiblePosition();
			int index = position - firstVisiblePositionInAdapter;
			if(index>=0 && index<hlv_navigation.getChildCount()) {
				View view = hlv_navigation.getChildAt(index);
				ViewCursor viewCursor = (ViewCursor) view.getTag();
				order = ORDER_DESC;
				viewCursor.tv_sort_title.setTextColor(Color.RED);
				if(selectBg != BG_VALUE_NULL) {
					viewCursor.tv_sort_title.setBackgroundResource(selectBg);
				}
				viewCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_desc);
				if(isShowArrow) {
					viewCursor.iv_sort_thumb.setVisibility(View.VISIBLE);
				} else {
					viewCursor.iv_sort_thumb.setVisibility(View.GONE);
				}
			}
		}

		public void clickHandle() {
			int position = fieldControllerArr.indexOf(this);
			int firstVisiblePositionInAdapter = hlv_navigation.getFirstVisiblePosition();
			int index = position - firstVisiblePositionInAdapter;
			if(index>=0 && index<hlv_navigation.getChildCount()) {
				View view = hlv_navigation.getChildAt(index);
				ViewCursor viewCursor = (ViewCursor) view.getTag();
				viewCursor.tv_sort_title.setTextColor(Color.RED);
				if(selectBg != BG_VALUE_NULL) {
					viewCursor.tv_sort_title.setBackgroundResource(selectBg);
				}
				if(order.equals(ORDER_DESC)){
					order = ORDER_ASC;
					viewCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_asc);
				} else {
					order = ORDER_DESC;
					viewCursor.iv_sort_thumb.setImageResource(R.drawable.template_sort_desc);
				}
				if(isShowArrow) {
					viewCursor.iv_sort_thumb.setVisibility(View.VISIBLE);
				} else {
					viewCursor.iv_sort_thumb.setVisibility(View.GONE);
				}
			}
		}
	}
	
	public static interface NavigationCallback {
		public void onNavigationCategoryChangedHandle(int index);
		public void onNavigationFilterHandle();
	}

}