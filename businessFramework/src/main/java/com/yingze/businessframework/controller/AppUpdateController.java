package com.yingze.businessframework.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.yingze.businessframework.R;
import com.yingze.businessframework.dialog.IDialog;
import com.yingze.businessframework.utils.ActivityUtils;
import com.yingze.businessframework.utils.TypefaceUtils;
import com.yingzecorelibrary.utils.LogUtil;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class AppUpdateController implements
		AppNetworkController.NetworkCallback,
		AppNetworkController.DialogCallback {
	private final int SHOW_NOTICE_DIALOG = 0;
	private final int DOWNLOAD = 1;// 下载中
	private final int DOWNLOAD_FINISH = 2;// 下载结束
	private final int CLOSE_PROGRESS_DIALOG = 3;
	private int fileSize = 0;
	private boolean isActivityFinish = false;// activity 是否结束
	private boolean cancelUpdate = false;// 是否取消更新

	private Context mContext;
	private Dialog noticeDialog = null;
	private AlertDialog mDownloadDialog;
	// private AlertDialog.Builder builder = null;
	// private AlertDialog.Builder builderDownload = null;

	protected HttpURLConnection conn;
	protected DownloadApkThread dApkThread;
	protected AppNetworkController mAppNetworkController;
	protected AppUpdateCallback mAppUpdateCallback = AppUpdateCallback.NULL;

	private String getDownloadVersionPath;// 获取app版本路径，待传入参数
	private String getDownloadAppPath;// 获取下载app安装包路径
	private String requestParamsFileName = "myschool.apk";// 请求参数文件名
	private String mSavePath;// 下载保存路径(父文件夹)
	private String fileName;// 文件名
	protected NoticeController mNoticeController = new NoticeController();
	protected DownloadDialogController mDownloadDialogController = new DownloadDialogController();

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		@SuppressLint("DefaultLocale")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SHOW_NOTICE_DIALOG:
				showNoticeDialog();
				break;
			case CLOSE_PROGRESS_DIALOG:
				Log.d("msg", "CLOSE_PROGRESS_DIALOG");
				mDownloadDialogController.getDialog().cancel();
				break;
			case DOWNLOAD:
				// 设置进度条位置
				String value = String.valueOf(msg.obj);
				float progress_f = Float.parseFloat(value);
				int progress_d = (int) progress_f;
				String strProgress = String.format("%d%s", progress_d, "%");
				mDownloadDialogController.updateProgress(strProgress);
				break;
			case DOWNLOAD_FINISH:
				// 安装文件
				installApk();
				break;
			default:
				break;
			}
		};
	};
	private String ver;

	public AppUpdateController(Context context, String downloadVersionPath,
			String downloadAppPath, String paramsFileName, String savePath,
			String appName) {
		this.mContext = context;
		getDownloadVersionPath = downloadVersionPath;
		getDownloadAppPath = downloadAppPath;
		requestParamsFileName = paramsFileName;
		mSavePath = savePath;
		fileName = appName;
	}

	/**
	 * 在线检测更新
	 */
	public void checkUpdateOnLine() {
		mAppNetworkController = new AppNetworkController(mContext);
		mAppNetworkController.initLoadingController(null);
		mAppNetworkController.initDialogCallback(this);
		mAppNetworkController.initHttpRequestCallback(this);
		mAppNetworkController.initHttpResponseCallback(this);
		if (isActivityEffectived()) {
			mAppNetworkController.openConnect();
		}
	}

	@Override
	public ViewGroup getRootLayout() {
		return null;
	}

	@Override
	public String getServerUrl() {
		return getDownloadVersionPath;
	}

	@Override
	public String getRequestType() {
		return AppNetworkController.NetworkCallback.POST;
	}

	@Override
	public void submitDataToServer(Map<String, String> property) {
		property.put("fileName", requestParamsFileName);
	}

	@Override
	public void parsePageData(String content) {
		if (!content.equals(1) && !content.equals(2) && content != null) {
			/* 获取当前软件版本 */
			final String versionCode = getVersionCode(mContext);
			try {
				Log.d("msg", "检测版本返回的Josn:" + content);
				JSONObject json = new JSONObject(content);
				ver = json.optString("versions");
				if (ver == null || ver.isEmpty() || ver.length() == 0) {
					JSONObject contentJson = json.optJSONObject("content");
					ver = contentJson.optString("version");
					fileSize = contentJson.optInt("length");
				} else {
					fileSize = json.getInt("length");
				}
				Log.d("msg", "ver:" + ver);
				Log.d("msg", "fileSize:" + fileSize);
				String[] serverVersion = ver.split("\\.");
				String[] localVersion = versionCode.split("\\.");
				int s1 = Integer.parseInt(serverVersion[0]);
				int s2 = Integer.parseInt(serverVersion[1]);
				int s3 = 0 ;
				if(serverVersion.length>2){
					s3 = Integer.parseInt(serverVersion[2]);
				}
				
				int l1 = Integer.parseInt(localVersion[0]);
				int l2 = Integer.parseInt(localVersion[1]);
				int l3 = 0;
					
				
				if (localVersion.length >= 3) {
					l3 = Integer.parseInt(localVersion[2]);
				}

				if (s1 > l1) {
					Log.d("msg", "s1 > l1");
					mHandler.sendEmptyMessage(SHOW_NOTICE_DIALOG);
					mAppUpdateCallback
							.onSuccess(AppUpdateCallback.TYPE_LOW_VERSION);
					return;
				} else if (s1 == l1) {
					if (s2 > l2) {
						Log.d("msg", "s2 > l2");
						mHandler.sendEmptyMessage(SHOW_NOTICE_DIALOG);
						mAppUpdateCallback
								.onSuccess(AppUpdateCallback.TYPE_LOW_VERSION);
						return;
					} else if (s2 == l2) {
						if (s3 > l3) {
							Log.d("msg", "s3 > l3)");
							mHandler.sendEmptyMessage(SHOW_NOTICE_DIALOG);
							mAppUpdateCallback
									.onSuccess(AppUpdateCallback.TYPE_LOW_VERSION);
							return;
						}
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		mAppUpdateCallback.onSuccess(AppUpdateCallback.TYPE_TOP_VERSION);
	}

	@Override
	public void onFail(Exception e, String message) {
		LogUtil.set("check_update_fail", message).info();
		mAppUpdateCallback.onFail(e, message);
	}

	/**
	 * 判断当前activity是否有效
	 * 
	 * @return
	 */
	private boolean isActivityEffectived() {
		// int sdk = android.os.Build.VERSION.SDK_INT;
		// if(sdk>=android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
		// Activity activity = (Activity) mContext;
		// if (isActivityFinish || activity.isFinishing() ||
		// activity.isDestroyed()) {
		// return false;
		// } else {
		// return true;
		// }
		// } else {
		// return true;
		// }

		if (isActivityFinish) {
			return false;
		}

		if (!ActivityUtils.isActivityEffectived(mContext)) {
			return false;
		}

		return true;
	}

	/**
	 * 获取软件版本号
	 * 
	 * @param context
	 * @return
	 */
	private String getVersionCode(Context context) {
		String versionCode = "";
		String pkName = context.getPackageName();
		try {
			// 获取软件版本号，对应AndroidManifest.xml下android:versionCode
			versionCode = context.getPackageManager().getPackageInfo(pkName, 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionCode;
	}

	/**
	 * 显示软件更新对话框
	 */
	private void showNoticeDialog() {
		if (isActivityEffectived()) {
			mNoticeController.descriptions = mContext.getResources().getString(
					R.string.soft_update_info);
			IDialog.Builder builder = new IDialog.Builder(mContext);
			builder.setBaseDialogController(mNoticeController).create().show();
		}
	}

	/**
	 * 显示软件下载对话框
	 */
	private void showDownloadDialog() {
		if (isActivityFinish)
			return;
		// 构造软件下载对话框
		IDialog.Builder builder = new IDialog.Builder(mContext);
		builder.setBaseDialogController(mDownloadDialogController).create()
				.show();
		// 下载文件
		downloadApk();
	}

	/**
	 * 下载apk文件
	 */
	private void downloadApk() {
		// 启动新线程下载软件
		dApkThread = new DownloadApkThread();
		dApkThread.start();
	}

	/**
	 * 下载文件线程
	 * 
	 */
	private class DownloadApkThread extends Thread {
		@Override
		public void run() {
			try {
				// 判断SD卡是否存在，并且是否具有读写权限
				if (Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					URL url = new URL(String.format("%s%s%s%s",
							getDownloadAppPath, "?", "fileName=",
							requestParamsFileName));

					// 创建连接
					conn = (HttpURLConnection) url.openConnection();
					conn.setRequestProperty("Accept-Encoding", "identity");
					conn.connect();

					// 创建输入流
					InputStream is = conn.getInputStream();

					File file = new File(mSavePath);
					// 判断文件目录是否存在
					if (!file.exists()) {
						file.mkdirs();
					}
					File apkFile = new File(mSavePath, fileName);
					if (!apkFile.exists()) {
						apkFile.createNewFile();
					}
					FileOutputStream fos = new FileOutputStream(apkFile);
					// 获取文件大小
					// int length = conn.getContentLength();
					int count = 0;
					// 缓存
					byte buf[] = new byte[1024];
					// 写入到文件中
					do {
						int numread = is.read(buf);
						count += numread;
						// 计算进度条位置
						float progress = ((float) count / fileSize) * 100;
						// 更新进度
						// mProgress.setProgress(progress);
						Message msg = Message.obtain();
						msg.obj = progress;
						msg.what = DOWNLOAD;
						mHandler.sendMessage(msg);
						if (numread == -1) {
							// 下载完成
							mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
							Log.d("msg", "下载完成");
							break;
						}
						// 写入文件
						fos.write(buf, 0, numread);
					} while (!cancelUpdate);// 点击取消就停止下载.
					fos.close();
					is.close();
				} else {
					Log.d("msg", "没有SD卡");
					System.out.println("app download is finish!!");
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// // 取消下载对话框显示
			mHandler.sendEmptyMessage(CLOSE_PROGRESS_DIALOG);
		}
	};

	public void close() {
		Log.d("msg", "appUpload__close");
		isActivityFinish = true;
		if (noticeDialog != null) {
			noticeDialog.cancel();
		}
		if (mDownloadDialog != null) {
			mDownloadDialog.cancel();
		}

		cancelUpdate = true;
		new Thread() {
			public void run() {
				if (conn != null) {
					conn.disconnect();
				}
			};
		}.start();

		if (dApkThread != null) {
			dApkThread.interrupt();
			dApkThread = null;
		}
	}

	/**
	 * 安装APK文件
	 */
	private void installApk() {
		File apkfile = new File(mSavePath, fileName);
		if (!apkfile.exists()) {
			return;
		}
		// 通过Intent安装APK文件
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()),
				"application/vnd.android.package-archive");
		mContext.startActivity(i);
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public AppUpdateCallback getAppUpdateCallback() {
		return mAppUpdateCallback;
	}

	public void setAppUpdateCallback(AppUpdateCallback appUpdateCallback) {
		mAppUpdateCallback = appUpdateCallback;
	}

	/**
	 * 提示框控制器
	 * 
	 * @author LittleBird
	 *
	 */
	class NoticeController extends IDialog.BaseDialogController implements
			OnClickListener {
		LinearLayout ll_regist_area_select;
		TextView tv_notice;
		TextView bt_sure;
		TextView bt_cancel;
		String descriptions = "";
		int select = -1;

		@Override
		public int getLayoutResID() {
			return R.layout.dialog_update_app_notice;
		}

		@Override
		public TitleController initTitleOnCreate() {
			return new TitleController() {
				@Override
				public void initTransactionOnCreate() {
					ll_title_left.setVisibility(View.GONE);
					ll_title_right.setVisibility(View.GONE);
					// TypefaceUtils.getInstance().resetTextTypeface(
					// mContext, tv_title_middle, "fonts/VINERITC.TTF");
					// TypefaceUtils.getInstance().resetTextTypeface(
					// mContext, tv_title_middle,
					// "fonts/DroidSerif-BoldItalic.ttf");
					// TypefaceUtils.getInstance().resetTextTypeface(
					// mContext, tv_title_middle, "fonts/MNJLX.TTF");
					tv_title_middle.setText(getTitle());
					TextPaint tp = tv_title_middle.getPaint();
					tp.setFakeBoldText(true);
					tv_title_middle.setTextColor(0xff000000);
					rl_title_root.setBackgroundColor(0x00ffffff);
					v_title_bottomline.setVisibility(View.GONE);
				}

				@Override
				public void initListenerOnCreate() {

				}
			};
		}

		@Override
		public void initResourceViewOnCreate(View contentView) {
			ll_regist_area_select = (LinearLayout) contentView
					.findViewById(R.id.ll_regist_area_select);
			tv_notice = (TextView) contentView
					.findViewById(R.id.tv_update_notice);
			bt_sure = (TextView) contentView.findViewById(R.id.bt_sure);
			bt_cancel = (TextView) contentView.findViewById(R.id.bt_cancel);
		}

		@Override
		public void initTransactionOnCreate() {
			tv_notice.setText(descriptions);
			bt_sure.setText("更新");
			bt_cancel.setText("稍后更新");
		}

		@Override
		public void initListenerOnCreate() {
			bt_sure.setOnClickListener(this);
			bt_cancel.setOnClickListener(this);
		}

		@Override
		protected String getTitle() {
			return "app版本更新提示";
		}

		@Override
		protected String getContent() {
			return null;
		}

		@Override
		public void onClick(View v) {
			super.onClick(v);
			if (v.getId() == R.id.bt_sure) {
				dialog.cancel();
				// 显示下载对话框
				showDownloadDialog();
			} else if (v.getId() == R.id.bt_cancel) {
				dialog.cancel();
			}
		}

		@Override
		protected void onPositiveButtonClick(Dialog dialog, View view) {

		}

		@Override
		protected void onNegativeButtonClick(Dialog dialog, View view) {

		}

		@Override
		public void onClick(DialogInterface dialog, int which) {

		}
	}

	/**
	 * 提示框控制器
	 * 
	 * @author LittleBird
	 *
	 */
	class DownloadDialogController extends IDialog.BaseDialogController
			implements OnClickListener, DialogInterface.OnDismissListener {
		LinearLayout ll_regist_area_select;
		TextView tv_update_progress;
		TextView bt_sure;
		int select = -1;

		@Override
		public int getLayoutResID() {
			return R.layout.appframework_dialog_downlaod;
		}

		@Override
		public TitleController initTitleOnCreate() {
			return new TitleController() {
				@Override
				public void initTransactionOnCreate() {
					ll_title_left.setVisibility(View.GONE);
					ll_title_right.setVisibility(View.GONE);
					tv_title_middle.setText(getTitle());
					TextPaint tp = tv_title_middle.getPaint();
					tp.setFakeBoldText(true);
					tv_title_middle.setTextColor(0xff000000);
					rl_title_root.setBackgroundColor(0xffffffff);
					v_title_bottomline.setVisibility(View.GONE);
				}

				@Override
				public void initListenerOnCreate() {

				}
			};
		}

		@Override
		public void initResourceViewOnCreate(View contentView) {
			ll_regist_area_select = (LinearLayout) contentView
					.findViewById(R.id.ll_regist_area_select);
			tv_update_progress = (TextView) contentView
					.findViewById(R.id.tv_update_progress);
			TypefaceUtils.getInstance().resetTextTypeface(mContext,
					tv_update_progress, "fonts/DroidSerif-BoldItalic.ttf");
			bt_sure = (TextView) contentView.findViewById(R.id.bt_sure);
		}

		@Override
		public void initTransactionOnCreate() {
			dialog.setCancelable(false);
			bt_sure.setText("取消下载");
		}

		@Override
		public void initListenerOnCreate() {
			bt_sure.setOnClickListener(this);
			dialog.setOnDismissListener(this);
		}

		@Override
		protected String getTitle() {
			return "app更新提示";
		}

		@Override
		protected String getContent() {
			return null;
		}

		@Override
		public void onClick(View v) {
			super.onClick(v);
			if (v.getId() == R.id.bt_sure) {
				dialog.cancel();
				cancelUpdate = true;
				new Thread() {
					public void run() {
						if (conn != null) {
							conn.disconnect();
						}
					};
				}.start();
			}
		}

		@Override
		protected void onPositiveButtonClick(Dialog dialog, View view) {

		}

		@Override
		protected void onNegativeButtonClick(Dialog dialog, View view) {

		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			Log.d("msg", "dialog.onDismiss");
			cancelUpdate = true;
			new Thread(new Runnable() {

				@Override
				public void run() {
					if (conn != null) {
						conn.disconnect();
					}
				}
			});

		}

		public void updateProgress(String progress) {
			tv_update_progress.setText(progress);
		}
	}

	public static interface AppUpdateCallback {
		public final static int TYPE_TOP_VERSION = 1;
		public final static int TYPE_LOW_VERSION = 2;

		public void onSuccess(int type);

		public void onFail(Exception e, String message);

		public static AppUpdateCallback NULL = new AppUpdateCallback() {
			@Override
			public void onSuccess(int type) {
			}

			@Override
			public void onFail(Exception e, String message) {
			}
		};
	}
}
