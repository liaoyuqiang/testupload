package com.yingze.businessframework.controller;

import android.os.Handler;

import com.yingzecorelibrary.view.XListView;

/**
 * 网络状态控制器
 * 
 * @author LittleBird
 */
public class AppNetworkStateController {
	private boolean isNetworkRequestLoaded = false;// 是否正在执行网络请求
	private boolean isLoad = false;// 是否在刷新
	private boolean isRefresh = false;// 是否在加载
	private boolean isGetHistoryRecord = false;// 同步
	private int totalSize = 0;// 总记录条数
	private int currentPage = 1;// 当前页数
	private int pageSize = 12; // 一页有多少条记录，该参数为空，服务器默认为12条
	private XListView lv_content;
	private Handler mHandler = new Handler();
	
	public AppNetworkStateController(XListView lv_content) {
		this.lv_content = lv_content;
	}

	/**
	 * 停止加载
	 */
	public void stopLoad() {
		if (lv_content != null) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if (totalSize > pageSize) {
						lv_content.setPullLoadEnable(true);
					} else {
						lv_content.setPullLoadEnable(false);
					}
					if (isRefresh) {
						lv_content.stopRefresh();
						isRefresh = false;
					} else if (isLoad) {
						lv_content.stopLoadMore();
						isLoad = false;
					}
				}
			});
		} else {
			if (isRefresh) {
				isRefresh = false;
			} else if (isLoad) {
				isLoad = false;
			}
		}
	}

	public boolean isLoad() {
		return isLoad;
	}

	public void setLoad(boolean isLoad) {
		this.isLoad = isLoad;
	}

	public boolean isRefresh() {
		return isRefresh;
	}

	public void setRefresh(boolean isRefresh) {
		this.isRefresh = isRefresh;
	}

	public boolean isGetHistoryRecord() {
		return isGetHistoryRecord;
	}

	public void setGetHistoryRecord(boolean isGetHistoryRecord) {
		this.isGetHistoryRecord = isGetHistoryRecord;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isNetworkRequestLoaded() {
		return isNetworkRequestLoaded;
	}

	public void setNetworkRequestLoaded(boolean isNetworkRequestLoaded) {
		this.isNetworkRequestLoaded = isNetworkRequestLoaded;
	}
}

