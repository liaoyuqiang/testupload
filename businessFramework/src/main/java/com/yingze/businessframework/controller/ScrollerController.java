/**
 * 滑动控制器
 * @author LiangZH
 */
package com.yingze.businessframework.controller;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class ScrollerController {
	private int duration = 600;
	private boolean isScrolling = false;
	private Scroller mScroller;
	private View mTarget;
	private ScrollerControllerCallback mScrollerControllerCallback = ScrollerControllerCallback.NULL;
	
	/**
	 * ScrollerController的构造方法
	 * @param context 上下文
	 * @param target 目标view
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public ScrollerController(Context context, View target) {
		mScroller = new Scroller(context);
		mTarget = target;
	}
	
	/**
	 * ScrollerController的构造方法
	 * @param context 上下文
	 * @param target 目标view
	 * @param interpolator 动画插值器
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public ScrollerController(Context context, View target, Interpolator interpolator) {
		mScroller = new Scroller(context, interpolator);
		mTarget = target;
	}
	
	/**
	 * 快速滚动
	 * <p> 执行scroller快速滚动的处理   </p>
	 * <ul>
	 *  <li> mScroller.fling(startX, startY, dx, dy, duration); 
	 *  	   执行快速滚动处理 </li>
	 *  <li> setScrolling(true) 当前为滚动状态 </li>
	 *  <li> mTarget.invalidate(); 调用invalidate()才能保证computeScroll()会被调用 </li>
	 * </ul>
	 * @param startX 开始的x坐标
	 * @param startY 开始的y坐标
	 * @param velocityX 结束的x坐标
	 * @param velocityY 结束的y坐标
	 * @param minX 结束的x坐标
	 * @param maxX 结束的y坐标
	 * @param minY 结束的x坐标
	 * @param maxY 结束的y坐标
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void fling(int startX, int startY, int velocityX, int velocityY, int minX,
			int maxX, int minY, int maxY) {
    	setScrolling(true);
        mScroller.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
        mTarget.invalidate();
	}
	
	/**
	 * 设置滚动的相对偏移
	 * <p> 计算出结束的x和y的坐标，调用方法smoothScrollBy </p>
	 * @param startX 开始的x坐标
	 * @param startY 开始的y坐标
	 * @param fx x轴滑动的距离
	 * @param fy y轴滑动的距离
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
    public void smoothScrollTo(int startX, int startY, int fx, int fy) {  
        int dx = fx - startX;  
        int dy = fy - startY;  
        smoothScrollBy(startX, startY, dx, dy);
    }  
  
	/**
	 * 平滑滚动
	 * <p> 执行scroller平滑滚动的处理   </p>
	 * <ul>
	 *  <li> mScroller.startScroll(startX, startY, dx, dy, duration); 
	 *  	   设置mScroller的滚动偏移量 </li>
	 *  <li> setScrolling(true) 当前为滚动状态 </li>
	 *  <li> mTarget.invalidate(); 调用invalidate()才能保证computeScroll()会被调用 </li>
	 * </ul>
	 * @param startX 开始的x坐标
	 * @param startY 开始的y坐标
	 * @param dx 结束的x坐标
	 * @param dy 结束的y坐标
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
    public void smoothScrollBy(int startX, int startY, int dx, int dy) {  
    	setScrolling(true);
        mScroller.startScroll(startX, startY, dx, dy, duration);  
        mTarget.invalidate();
    }  
    
	/**
	 * 平滑滚动
	 * <p> 执行scroller平滑滚动的处理   </p>
	 * <ul>
	 *  <li> mScroller.computeScrollOffset() 用于判断当前是否滑动完成</li>
	 *  <li> 必须在每次调用scroller的scrolling之后调用mTarget.postInvalidate()，
	 *  	   否则有可能不执行滑动计算 </li>
	 *  <li> mScrollerControllerCallback 回调设置的监听接口以返回当前滑动的坐标和状态 </li>
	 *  <li> setScrolling(false) 设置当前滑动状态为停止 </li>
	 *  <li> setScrollerControllerCallback(null); 清空当前回调接口</li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
    public void computeScroll() {  
        if (mScroller.computeScrollOffset()) {  
        	mScrollerControllerCallback.scrolling(mScroller.getCurrX(), mScroller.getCurrY());
        	mTarget.postInvalidate();  
        	Log.e("scroller-controller", "**************** scrolling : "+mScroller.getCurrX()+","+mScroller.getCurrY());
        } else {
        	setScrolling(false);
        	mScrollerControllerCallback.scrollFinish();
        	setScrollerControllerCallback(null);
        }
    }  
	
	/**
	 * 中断当前滑动动画的处理
	 * <p> 直接中断当前的滑动计算，并会在computeScroll的时候返回结束状态 </p>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
    public void interrupt() {
    	mScroller.abortAnimation();
    }
        
	public ScrollerControllerCallback getScrollerControllerCallback() {
		return mScrollerControllerCallback;
	}

	public void setScrollerControllerCallback(ScrollerControllerCallback scrollerControllerCallback) {
		this.mScrollerControllerCallback = scrollerControllerCallback!=null ?
				scrollerControllerCallback:ScrollerControllerCallback.NULL;
	}
    
	public boolean isScrolling() {
		return isScrolling;
	}

	public void setScrolling(boolean isScrolling) {
		this.isScrolling = isScrolling;
	}
	
	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
    public static interface ScrollerControllerCallback {
    	public void scrolling(int currentX,int currentY);
    	public void scrollFinish();
    	public static ScrollerControllerCallback NULL = new ScrollerControllerCallback() {
			@Override
			public void scrolling(int currentX,int currentY) {
			}
    		@Override
			public void scrollFinish() {
			}
    	};
    }
    
}
