/**
 * AppIntegratedActivity控制器，控制集成化功能的逻辑处理
 */
package com.yingze.businessframework.controller;

import com.yingze.businessframework.R;
import android.view.View;

public class AppIntegratedActivityController extends AppActivityController {
	/*
	 * 已经指定布局的xml文件，需要添加到任务链中
	 */
	protected SubLayoutController mSubLayoutController = new SubLayoutController(R.id.fl_sub_layout);

	public static AppIntegratedActivityController newInstance(ViewController viewController, View contentView) {
		return new AppIntegratedActivityController(viewController, contentView);
	}
	
	protected AppIntegratedActivityController(ViewController viewController, View contentView) {
		super(viewController, contentView);
		mControllerArr.add(mSubLayoutController);
	}

	public SubLayoutController getSubLayoutController() {
		return mSubLayoutController;
	}

	public void setSubLayoutController(SubLayoutController mSubLayoutController) {
		this.mSubLayoutController = mSubLayoutController;
	}
	
	public static AppIntegratedActivityController NULL = new AppIntegratedActivityController(null, null) {
		public void initOnCreate() {
		};
	};
	
}
