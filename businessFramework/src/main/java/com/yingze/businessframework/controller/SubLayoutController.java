/**
 * 子布局控制器
 */
package com.yingze.businessframework.controller;

import java.util.ArrayList;
import com.yingze.businessframework.utils.SubLayoutConfigUtils;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;

public class SubLayoutController implements ViewController {
	private FrameLayout fl_sub_layout;
	private int mLayoutResID = -1;
	
	public SubLayoutController(int layoutResID) {
		mLayoutResID = layoutResID;
	}
	
	@Override
	public int getLayoutResID() {
		return 0;
	}

	@Override
	public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {

	}

	@Override
	public TitleController initTitleOnCreate() {
		return null;
	}

	@Override
	public void initWindowFeatureOnCreate() {
	}

	@Override
	public void initResourceViewOnCreate(View contentView) {
		fl_sub_layout = (FrameLayout) contentView.findViewById(mLayoutResID);
	}

	@Override
	public void initTransactionOnCreate() {
		fl_sub_layout.setPadding(
				SubLayoutConfigUtils.getInstance().getPaddingLeft(), 
				SubLayoutConfigUtils.getInstance().getPaddingTop(), 
				SubLayoutConfigUtils.getInstance().getPaddingRight(), 
				SubLayoutConfigUtils.getInstance().getPaddingBottom());
		fl_sub_layout.setBackgroundColor(SubLayoutConfigUtils.getInstance().getBackgroundColor());
		if (SubLayoutConfigUtils.getInstance().getBackgroundResourceId()>=0) {
			fl_sub_layout.setBackgroundResource(SubLayoutConfigUtils.getInstance().getBackgroundResourceId());
		}
	}

	@Override
	public void initListenerOnCreate() {
	}

	@Override
	public void initHooksOnCreate() {
	}

	public void setPaddingLeft(int paddingLeft) {
		fl_sub_layout.setPadding(paddingLeft, fl_sub_layout.getPaddingTop(), 
				fl_sub_layout.getPaddingRight(), fl_sub_layout.getPaddingBottom());
	}
	
	public void setPaddingRight(int paddingRight) {
		fl_sub_layout.setPadding(fl_sub_layout.getPaddingLeft(), fl_sub_layout.getPaddingTop(), 
				paddingRight, fl_sub_layout.getPaddingBottom());
	}
	
	public void setPaddingTop(int paddingTop) {
		fl_sub_layout.setPadding(fl_sub_layout.getPaddingLeft(), paddingTop, 
				fl_sub_layout.getPaddingRight(), fl_sub_layout.getPaddingBottom());
	}
	
	public void setPaddingBottom(int paddingBottom) {
		fl_sub_layout.setPadding(fl_sub_layout.getPaddingLeft(), fl_sub_layout.getPaddingTop(),
				fl_sub_layout.getPaddingRight(), paddingBottom);
	}
	
	public void setPadding(int paddingLeft, int paddingRight, int paddingTop, int paddingBottom) {
		fl_sub_layout.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
	}
	
	public void setBackground(Drawable drawable) {
		fl_sub_layout.setBackground(drawable);
	}
	
	public void setBackgroundColor(int bgColor) {
		fl_sub_layout.setBackgroundColor(bgColor);
	}
	
}
