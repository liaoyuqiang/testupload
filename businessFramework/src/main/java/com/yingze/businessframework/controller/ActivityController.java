/**
 * 控制activity的逻辑处理流程
 */
package com.yingze.businessframework.controller;

public abstract class ActivityController implements ViewController {
	/**
	 * 初始化方法调用
	 * <p> 在oncreate过程中执行指定的方法的调用 </p>
	 * <ul>
	 *  <li> injectControllerOnCreate 初始化自定义控制器的注入 </li>
	 *  <li> initTitleOnCreate 初始化标题控制器 </li>
	 *  <li> initWindowFeatureOnCreate 初始化窗口的特定方法处理 </li>
	 *  <li> initResourceViewOnCreate 初始化布局界面里面控件的绑定 </li>
	 *  <li> initTransactionOnCreate 初始化事务 </li>
	 *  <li> initListenerOnCreate 初始化监听器 </li>
	 *  <li> initHooksOnCreate 初始化钩子方法 </li>  
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void initOnCreate() {
		injectControllerOnCreate(null);
		initTitleOnCreate();
		initWindowFeatureOnCreate();
		initResourceViewOnCreate(null);
		initTransactionOnCreate();
		initListenerOnCreate();
		initHooksOnCreate();
	}

}
