package com.yingze.businessframework.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.yingze.businessframework.R;
import com.yingze.businessframework.component.LoadingLayout;
/**
 * 弹跳动画控制器
 * @author LiangZH
 * @date 2016/08/19 11:00
 * @version v0.0.1
 */
public class ElasticLoadingController extends LoadingController{
	@Override
	public void showProgress(final Context context, final ViewGroup group) {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if(!isEffectived) {
					return;
				}
				if(mGroup!=null && mLoadingView!=null) {
					mGroup.removeView(mLoadingView);
				}
				mGroup = group;
				mLayoutInflater = LayoutInflater.from(context);
				mLoadingView = mLayoutInflater.inflate(R.layout.appframework_loadingview, null);
				group.addView(mLoadingView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				LoadingLayout mLoadingLayout = (LoadingLayout) mLoadingView.findViewById(R.id.id_loading_view) ;
		        ((LoadingLayout)mLoadingLayout).startAnim();
			}
		});
	}
	
}
