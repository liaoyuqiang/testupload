package com.yingze.businessframework.controller;
/**
 * 导航栏控件
 */
import java.util.ArrayList;

import com.yingze.businessframework.R;
import com.yingze.businessframework.utils.InterpolatorUtils;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class NavigationbarController implements ViewController {
	private LinearLayout ll_navigation_container;
	private ImageView cursor;// 动画图片
	private int offset = 0;// 动画图片偏移量
	private int currIndex = 0;// 当前页卡编号
	
	private int[] tabOffsetArr;// 每个标签的起始偏移位置
	private int totalLength;// 控件的总长度
	private int cursorLength;// 游标的长度
	private int tabNumber;// 标签的数量
	
	private int[] tabLengthArr;// 标签的长度数组
	private int[] tabCursorLengthArr;// 标签的长度数组
	private ScrollerController mScrollerController;// 滑动计算控件
	
	private Interpolator mInterpolator = InterpolatorUtils.newOvershootInterpolator();
	
	public NavigationbarController() {
		
	}
	
	public NavigationbarController(Interpolator interpolator) {
		mInterpolator = interpolator;
	}
	
	@Override
	public int getLayoutResID() {
		return R.layout.appframework_navigation_bar;
	}

	@Override
	public void injectControllerOnCreate(
			ArrayList<ViewController> controllerArr) {
	}

	@Override
	public TitleController initTitleOnCreate() {
		return null;
	}

	@Override
	public void initWindowFeatureOnCreate() {
	}

	@Override
	public void initResourceViewOnCreate(View contentView) {
		ll_navigation_container = (LinearLayout) contentView.findViewById(R.id.ll_navigation_container);
		cursor = (ImageView) contentView.findViewById(R.id.cursor);
		
		View target = new View(contentView.getContext()) {
			@Override
			public void computeScroll() {
				mScrollerController.computeScroll();
				super.computeScroll();
			}
		};
		target.setWillNotDraw(false);
		ll_navigation_container.addView(target,new LinearLayout.LayoutParams(
				1, contentView.getContext().getResources().getDimensionPixelSize(
						R.dimen.nativation_gap_height)));
		
		mScrollerController = new ScrollerController(contentView.getContext(), target, mInterpolator);
		
//		mScrollerController = new ScrollerController(contentView.getContext(), target);
	}

	@Override
	public void initTransactionOnCreate() {
		offset = (totalLength / tabNumber - cursorLength) / 2;// 计算偏移量
		LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) cursor.getLayoutParams();
		params.leftMargin = offset;
		params.width = cursorLength;
		cursor.setLayoutParams(params);
		
		// 计算游标在每个标签的起始位置
		tabOffsetArr = new int[tabNumber];
		tabCursorLengthArr = new int[tabNumber];
		for(int i=0;i<tabNumber;i++) {
			tabCursorLengthArr[i] = cursorLength;
			tabOffsetArr[i] = totalLength / tabNumber * i + offset;
		}
	}
	
	@Override
	public void initListenerOnCreate() {

	}

	@Override
	public void initHooksOnCreate() {

	}
	
	public void init(int totalLength, int cursorLength, int tabNumber) {
		this.totalLength = totalLength;
		this.cursorLength = cursorLength;
		this.tabNumber = tabNumber;
	}
	
	/**
	 * 修改下划线的颜色
	 * @param Color
	 */
	public void setCursorColor(int Color){
		this.cursor.setBackgroundColor(Color);
	}
	
	/**
	 * 假设当前游标的长度不变
	 * @param tabLocaltionXArr 标签水平位置数组
	 * @param tabLengthArr 标签长度数组
	 * @param tabCursorLength 标签低下游标长度数组
	 * @param tabCursorLength
	 */
	public void setItemLength(int[] tabLocaltionXArr, int[] tabLengthArr, int[] tabCursorLength) {
		this.tabLengthArr = tabLengthArr;
		this.tabCursorLengthArr = tabCursorLength;
		for(int i=0;i<tabLengthArr.length;i++) {
			int offset = (tabLengthArr[i] - tabCursorLength[i]) / 2; // 计算指定标签的x偏移量
			tabOffsetArr[i] = tabLocaltionXArr[i] + offset;
		}
		
		LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) cursor.getLayoutParams();
		params.leftMargin = tabOffsetArr[0];
		params.width = tabCursorLength[0];
		cursor.setLayoutParams(params);
	}
	
//	/**
//	 * 设置当前光标的位置
//	 * @param index
//	 */
//	public void setCursorIndex(int index) {
////		LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) cursor.getLayoutParams();
////		params.width = tabCursorLengthArr[index];
////		cursor.setLayoutParams(params);
//		Animation animation = new TranslateAnimation(tabOffsetArr[currIndex], tabOffsetArr[index], 0, 0);
//		animation.setFillAfter(true);// True:图片停在动画结束位置
//		animation.setDuration(300);
//		cursor.startAnimation(animation);
//		currIndex = index;
//	}
	
	/**
	 * 设置当前光标的位置
	 * @param index
	 */
	public void setCursorIndex(final int index) {
		if(tabCursorLengthArr!=null){
		final int sourceCursorLength = tabCursorLengthArr[currIndex];
		final int sourceLocation = tabOffsetArr[currIndex];
		final boolean isToRight = index > currIndex ? true:false;
		mScrollerController.setScrollerControllerCallback(new ScrollerController.ScrollerControllerCallback(){
			private int increment = 0;
			private int lastCurrentX=-1;
			private int location = 0;
			private int width = 0;
			@Override
			public void scrolling(int currentX, int currentY) {
				float scale = 1;
				
				if (tabCursorLengthArr[index]<sourceCursorLength) {
					float value = ((float)tabCursorLengthArr[index]/sourceCursorLength);
					scale =1 - (1-value)*Math.abs((float)(currentX-sourceLocation)/(float)(tabOffsetArr[index]-sourceLocation));
				} else {
					float value = ((float)tabCursorLengthArr[index]/sourceCursorLength);
					scale =1 + (value - 1)*Math.abs((float)(currentX-sourceLocation)/(float)(tabOffsetArr[index]-sourceLocation));
				}
				
				if(lastCurrentX == -1) {
					lastCurrentX = currentX;
				}
				if (isToRight) { // 从左到右
					location = currentX;
					int bounceWidth = (int)(sourceCursorLength * scale) + increment;// 动态计算弹跳宽度
					increment += (currentX - lastCurrentX)*1/2;
					lastCurrentX = currentX;
					if (currentX+bounceWidth <= tabOffsetArr[index]+tabCursorLengthArr[index]) {
						width = bounceWidth;
					} else {
						width = tabOffsetArr[index]+tabCursorLengthArr[index] - currentX;
					}
					
				} else { // 从右到左
					int bounceLocation = currentX - increment;
					increment += Math.abs(currentX - lastCurrentX)*1/2;
					lastCurrentX = currentX;
					if (bounceLocation>tabOffsetArr[index]) {
						location = bounceLocation;
						width = Math.abs(location - currentX) + (int)(sourceCursorLength*scale);
					} else {
						location = tabOffsetArr[index];
//						width = Math.abs(location - currentX) + (int)(sourceCursorLength*scale);
						width = currentX - tabOffsetArr[index] + tabCursorLengthArr[index];
					}
				}
				
				LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) cursor.getLayoutParams();
				params.leftMargin = location;
				params.width = width;
				cursor.setLayoutParams(params);
			}
			@Override
			public void scrollFinish() {
				LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) cursor.getLayoutParams();
				params.leftMargin = tabOffsetArr[index];
				params.width = tabCursorLengthArr[index];
				Log.e("结束了", "++++++++++ scrolling:"+tabOffsetArr[index]);
				cursor.setLayoutParams(params);
			}
		});
		mScrollerController.setDuration(600);
		mScrollerController.smoothScrollTo(tabOffsetArr[currIndex], 0, tabOffsetArr[index], 0);
		currIndex = index;
		}
	}
	
	public ImageView getCursor() {
		return cursor;
	}

	public void setCursor(ImageView cursor) {
		this.cursor = cursor;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getCurrIndex() {
		return currIndex;
	}

	public void setCurrIndex(int currIndex) {
		this.currIndex = currIndex;
	}

	public int[] getTabOffsetArr() {
		return tabOffsetArr;
	}

	public void setTabOffsetArr(int[] tabOffsetArr) {
		this.tabOffsetArr = tabOffsetArr;
		
	}
}