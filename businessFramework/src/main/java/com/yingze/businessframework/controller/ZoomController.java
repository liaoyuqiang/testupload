/**
 * 控件缩放比例控制器
 */
package com.yingze.businessframework.controller;

import com.yingze.businessframework.R;
import com.yingzecorelibrary.utils.LogUtil;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;

@SuppressLint("Recycle")
public class ZoomController {
	private int FLAG_DEPEND_NORMAL = 0;
	private int FLAG_DEPEND_WIDTH = 1;
	private int FLAG_DEPEND_HEIGHT = 2;
	private int scaleType = FLAG_DEPEND_NORMAL;
	
	private int widthMeasureSpec;
	private int heightMeasureSpec;
	private float scale;

	public void initClassConstructor(Context context, AttributeSet attrs) {
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.zoomlayout);
		float scaleWidth = ta.getFloat(R.styleable.zoomlayout_scale_by_width, -1);
		float scaleHeight = ta.getFloat(R.styleable.zoomlayout_scale_by_height, -1);
		
		if(scaleWidth != -1) {
			scaleType = FLAG_DEPEND_WIDTH;
			scale = scaleWidth;
			LogUtil.set("^^^^^^^^^", "^^^^^^^^^^^^^^^ width : "+scale).verbose();
		} else if(scaleHeight != -1) {
			scaleType = FLAG_DEPEND_HEIGHT;
			scale = scaleHeight;
			LogUtil.set("^^^^^^^^^", "^^^^^^^^^^^^^^^ height : "+scale).verbose();
		} else {
			scaleType = FLAG_DEPEND_NORMAL;
		}
	}
	
	/**
	 * 按规则对测量尺寸进行处理
	 * @param widthMeasureSpec	宽度参考
	 * @param heightMeasureSpec	高度参考
	 */
	public void handleSizeOnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);  
		int sizeHeight =  MeasureSpec.getSize(heightMeasureSpec); 
		
		if (scaleType == FLAG_DEPEND_WIDTH) {
			this.widthMeasureSpec = widthMeasureSpec;
			this.heightMeasureSpec = MeasureSpec.makeMeasureSpec((int)(sizeWidth*scale), MeasureSpec.EXACTLY);
		} else if (scaleType == FLAG_DEPEND_HEIGHT) {
			this.widthMeasureSpec = MeasureSpec.makeMeasureSpec((int)(sizeHeight*scale), MeasureSpec.EXACTLY);
			this.heightMeasureSpec = heightMeasureSpec;
		} else {
			this.widthMeasureSpec = widthMeasureSpec;
			this.heightMeasureSpec = heightMeasureSpec;
		}
	}
	
	public int getWidthMeasureSpec() {
		return widthMeasureSpec;
	}

	public void setWidthMeasureSpec(int widthMeasureSpec) {
		this.widthMeasureSpec = widthMeasureSpec;
	}

	public int getHeightMeasureSpec() {
		return heightMeasureSpec;
	}

	public void setHeightMeasureSpec(int heightMeasureSpec) {
		this.heightMeasureSpec = heightMeasureSpec;
	}
	
}
