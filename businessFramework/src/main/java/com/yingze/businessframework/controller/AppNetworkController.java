/**
 * 网络请求控制器
 */
package com.yingze.businessframework.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.view.ViewGroup;
import com.yingze.corelibrary.network.BaseNetworkController;
import com.yingze.corelibrary.network.DeckerGetServer;
import com.yingze.corelibrary.network.DeckerPostServer;
import com.yingze.corelibrary.network.DeckerUploadServer;
import com.yingze.corelibrary.network.NetworkClient;
import com.yingze.corelibrary.network.ThreadPoolController;
import com.yingze.corelibrary.network.NetworkClient.NetworkClientCallback;
import com.yingze.corelibrary.network.ThreadPoolController.TaskController;
import com.yingze.corelibrary.network.UploadFileController;
import com.yingze.corelibrary.network.UploadMultipartController;
import com.yingze.corelibrary.network.UploadServerController;
import com.yingzecorelibrary.utils.LogUtil;

public class AppNetworkController {
	protected Context mContext;
	protected TaskController mTaskController;
	protected LoadingController mLoadingController;
	protected DialogCallback mDialogCallback;
	protected HttpRequestCallback mHttpRequestCallback;
	protected HttpResponseCallback mHttpResponseCallback;
	
	public AppNetworkController(Context context) {
		mContext = context;
	}
	
	public void initLoadingController(LoadingController loadingController) {
		mLoadingController = loadingController;
	}
	
	public void initDialogCallback(DialogCallback dialogCallback) {
		mDialogCallback = dialogCallback;
	}
	
	public void initHttpRequestCallback(HttpRequestCallback httpRequestCallback) {
		mHttpRequestCallback = httpRequestCallback;
	}

	public void initHttpResponseCallback(HttpResponseCallback httpResponseCallback) {
		mHttpResponseCallback = httpResponseCallback;
	}
	
	/**
	 * 打开网络请求
	 */
	public void openConnect() {
		if (mLoadingController!=null) {
			mLoadingController.showProgress(mContext, mDialogCallback.getRootLayout());
		}
		NetworkClient client = NetworkClient.newInstance();
		client.setNetworkController(new BaseNetworkController(
				mHttpRequestCallback.getServerUrl(),
				new NetworkClient.DownloadCallback() {
					@Override
					public void onFinish(String content) {
						mHttpResponseCallback.parsePageData(content);
						
						if (mLoadingController!=null) {
							mLoadingController.closeProgress();
						}
					}
					@Override
					public void onFail(Exception e, String message) {
						mHttpResponseCallback.onFail(e, message);
//						mLoadingController.showFailPrompts(
//								mContext, mDialogCallback.getRootLayout(), 
//							new LoadingController.OnRefreshListener() {
//								@Override
//								public void onRefresh(View view) {
//									requestServer();
//								}
//							});
					}
					@Override
					public void onDownloadProcess(int readbufferLength, int downloadFileLength,
							int totalFileLength) {
					}
					
				}){
			@Override
			public void uploadPostContent(OutputStream outputStream) {
				boolean isStarted = false;
				HashMap<String,String> property = new HashMap<String,String>();
				mHttpRequestCallback.submitDataToServer(property);
				StringBuilder sb = new StringBuilder();
				Iterator<Entry<String, String>> iterator = property.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<String, String> entry = iterator.next();
					String key = entry.getKey();
					String value = entry.getValue();
					if (!isStarted) {
						isStarted = true;
					} else {
						sb.append("&");
					}
					sb.append(String.format("%s%s%s", key, "=", value));
				}
				try {
					outputStream.write(sb.toString().getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		if(mHttpRequestCallback.getRequestType().equals(HttpRequestCallback.GET)) {
			mTaskController = new DeckerGetServer(client);
		} else {
			mTaskController = new DeckerPostServer(client);
		}
		ThreadPoolController.getInstance().submit(mTaskController);
	}
	
	public void post(String requestUrl, final Map<String,String> params, final NetworkClientCallback callback) {
		NetworkClient client = NetworkClient.newInstance();
		client.setNetworkController(
				new BaseNetworkController(requestUrl, 
						new NetworkClient.DownloadCallback() {
						@Override
						public void onFinish(String content) {
							callback.onFinish(content);
						}
						@Override
						public void onFail(Exception e, String message) {
							callback.onFail(e, message);
						}
						@Override
						public void onDownloadProcess(int readbufferLength, int downloadFileLength,
								int totalFileLength) {
						}
				}){
			@Override
			public void uploadPostContent(OutputStream outputStream) throws IOException {
				boolean isStarted = false;
				StringBuilder sb = new StringBuilder();
				Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<String, String> entry = iterator.next();
					String key = entry.getKey();
					String value = entry.getValue();
					if (!isStarted) {
						isStarted = true;
					} else {
						sb.append("&");
					}
					sb.append(String.format("%s%s%s", key, "=", value));
				}
				outputStream.write(sb.toString().getBytes());
			}
		});
		mTaskController = new DeckerPostServer(client);
		ThreadPoolController.getInstance().submit(mTaskController);
	}
	
	/**
	 * 上传本地文件(单个)
	 */
	public void upload(String localFilePath, String remoteSubmitPath, NetworkClientCallback callback, 
			Map<String,String> params) {
		NetworkClient client = NetworkClient.newInstance();
		client.setNetworkController(new UploadFileController(localFilePath, remoteSubmitPath, callback, params));
		mTaskController = new DeckerUploadServer(client);
		ThreadPoolController.getInstance().submit(mTaskController);
	}
	
	/**
	 * 上传图片(单个)
	 * @param fileName
	 * @param bitmap
	 * @param remoteSubmitPath
	 * @param callback
	 * @param params
	 */
	public void uplaod(String fileName, final Bitmap bitmap, String remoteSubmitPath, final NetworkClientCallback callback, 
			Map<String,String> params) {
		NetworkClient client = NetworkClient.newInstance();
		/*
		 * 对图片文件名进行添加后缀处理
		 */
		fileName = String.format("%s%s", fileName, ".png");
		client.setNetworkController(new UploadServerController(fileName, remoteSubmitPath, callback, params) {
			@Override
			public void uploadPostContent(OutputStream outputStream) {
//				bitmap.compress(CompressFormat.JPEG, 100, outputStream);
				try {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bitmap.compress(CompressFormat.PNG, 100, out);
					byte[] arr = out.toByteArray();
					int templength = 512;
					int i = 0;
					int j = arr.length/templength;
					int quyu = arr.length%templength;
					int sum = 0;
					while(i<j) {
						outputStream.write(arr, i*templength, templength);
						sum += templength;
						callback.onUploadProcess(templength, sum, arr.length);
						LogUtil.set( "upload", "已经上传:" + (sum*100/arr.length) + "%"
								+ "图片总长:" +arr.length).verbose();
						i++;
					}
					if(quyu>0) {
						outputStream.write(arr, i*templength, quyu);
						callback.onUploadProcess(quyu, arr.length, arr.length);
						LogUtil.set( "upload", "已经上传:" + "100%"
								+ "图片总长:" +arr.length).verbose();
					}
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		mTaskController = new DeckerUploadServer(client);
		ThreadPoolController.getInstance().submit(mTaskController);
	}
	
	/**
	 * 上传多个附件集合(支持本地file与bitmap的集合)
	 */
	public void upload(String remoteSubmitPath, NetworkClientCallback callback, 
			Map<String,String> params, ArrayList<UploadMultipartController.Annex> annexArr) {
		NetworkClient client = NetworkClient.newInstance();
		client.setNetworkController(new UploadMultipartController(annexArr, remoteSubmitPath, callback, params));
		mTaskController = new DeckerUploadServer(client);
		ThreadPoolController.getInstance().submit(mTaskController);
	}
	
	public void closeConnect() {
		ThreadPoolController.getInstance().cancel(mTaskController);
	}
	
	public static interface DialogCallback {
		public ViewGroup getRootLayout();
	}
	
	public static interface HttpRequestCallback {
		public static final String POST = "post";
		public static final String GET = "GET";
		public String getServerUrl();
		public String getRequestType();
		public void submitDataToServer(Map<String,String> property);
	}
	
	public static interface HttpResponseCallback {
		public void parsePageData(String content);
		public void onFail(Exception e, String message);
	}
	
	public static interface NetworkCallback extends AppNetworkController.HttpRequestCallback,
			AppNetworkController.HttpResponseCallback{
	}
	
	public static HashMap<String,String> NULL_PARAMS = new HashMap<String,String>();
	
}
