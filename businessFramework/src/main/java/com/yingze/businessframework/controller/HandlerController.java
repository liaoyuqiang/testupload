package com.yingze.businessframework.controller;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class HandlerController extends Handler implements Parcelable {
//	/**
//	 * Serializable default ID
//	 */
//	private static final long serialVersionUID = 1L;
	private HandlerControllerCallback mHandlerControllerCallback;
	
	public HandlerController(HandlerControllerCallback handlerControllerCallback) {
		mHandlerControllerCallback = handlerControllerCallback;
	}
	
	@Override
	public void handleMessage(Message msg) {
		mHandlerControllerCallback.handleMessage(msg);
//		int hGap = msg.arg1;
//    	if (hGap!=0) {
//    		mScrollLayout.setGap(-mScrollLayout.getLocalX() + hGap/5);
//    	}
	}
	
	public static interface HandlerControllerCallback {
		public void handleMessage(Message msg);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
	}
	
}