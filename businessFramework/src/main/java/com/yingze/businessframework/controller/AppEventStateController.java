package com.yingze.businessframework.controller;

import android.view.ViewConfiguration;

public class AppEventStateController {
	private final int STATE_FOCUS_GET = 0;// 获得焦点
	private final int STATE_FOCUS_LOST = 1;// 失去焦点
	private final int STATE_SLIDE_HORIZONTAL = 2;// 左右滑动
	private final int STATE_SLIDE_VERTICAL = 3;// 上下滑动
	private final int STATE_DATA_REFRESH = 4;// 滑动刷新
	private final int STATE_DATA_LOAD = 5;// 滑动加载
	private final int STATE_INTERCEP_TOUCHEVENT = 6;
	
	private int state = STATE_FOCUS_LOST;
	private float effectivePoint[];// 临界坐标
	
	private long downtime;
	private ViewConfiguration mViewConfiguration;
	
	public AppEventStateController(ViewConfiguration viewConfiguration) {
		effectivePoint = new float[2];
		mViewConfiguration = viewConfiguration;
	}
	
	public void setGetFoucusState() {
		state = STATE_FOCUS_GET;
	}
	public void setLostFocusState() {
		state = STATE_FOCUS_LOST;
	}
	public void setHorizontalSlideState() {
		state = STATE_SLIDE_HORIZONTAL;
	}
	public void setVerticalSlideState() {
		state = STATE_SLIDE_VERTICAL;
	}
	public void setDataRefreshState() {
		state = STATE_DATA_REFRESH;
	}
	public void setDataLoadState() {
		state = STATE_DATA_LOAD;
	}
	public void setInterceptTouchEventState() {
		state = STATE_INTERCEP_TOUCHEVENT;
	}
	public boolean isInGetFocusState() {
		return state == STATE_FOCUS_GET;
	}
	public boolean isInLostFocusState() {
		return state == STATE_FOCUS_LOST;
	}
	public boolean isInHorizontalSlideState() {
		return state == STATE_SLIDE_HORIZONTAL;
	}
	public boolean isInVerticalSlideState() {
		return state == STATE_SLIDE_VERTICAL;
	}
	public boolean isInDataRefreshState() {
		return state == STATE_DATA_REFRESH;
	}
	public boolean isInDataLoadState() {
		return state == STATE_DATA_LOAD;
	}
	public boolean isInInterceptTouchEventState() {
		return state == STATE_INTERCEP_TOUCHEVENT;
	}
	public void setDownTime(long downtime) {
		this.downtime = downtime;
	}
	
	public boolean isOverTime(long currentTime) {
		return currentTime-downtime >= ViewConfiguration.getGlobalActionKeyTimeout();
	}
	
	public void setEffectivePoint(float effectivePointX, float effectivePointY) {
		effectivePoint[0] = effectivePointX;
		effectivePoint[1] = effectivePointY;
	}
	
	public void updateTrackX(float x, long currentTime) {
		if(Math.abs(x - effectivePoint[0])>mViewConfiguration.getScaledTouchSlop()*3 &&
				currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
			setHorizontalSlideState();
		}
	}
	
	public void updateTrackY(float y, long currentTime) {
		if(Math.abs(y - effectivePoint[1])>mViewConfiguration.getScaledTouchSlop() &&
				currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
			setVerticalSlideState();
		}
	}
}
