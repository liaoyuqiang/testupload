/**
 * 标题控制器,可动态控制标题的样式
 */
package com.yingze.businessframework.controller;

import java.util.ArrayList;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.yingze.businessframework.R;
import com.yingze.businessframework.utils.TitleConfigUtils;

public abstract class TitleController implements ViewController {
	protected View v_title_bottomline;
	protected ImageView iv_title_left;
	protected TextView tv_title_left;
	protected ImageView iv_title_middle;
	protected TextView tv_title_middle;
	protected ImageView iv_title_right;
	protected TextView tv_title_right;
	protected LinearLayout ll_title_left;
	protected LinearLayout ll_title_middle;
	protected LinearLayout ll_title_right;
	protected RelativeLayout rl_title_root;
	
	public static TitleController newDefaultInstance() {
		return new TitleController() {
			@Override
			public void initTransactionOnCreate() {
			}
			@Override
			public void initListenerOnCreate() {
			}
			@Override
			public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {
			}
		};
	}
	
	@Override
	public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {	
	}
	
	@Override
	public void initWindowFeatureOnCreate() {
	}
	
	@Override
	public void initResourceViewOnCreate(View contentView) {
		v_title_bottomline = contentView.findViewById(R.id.v_title_bottomline);
		iv_title_left = (ImageView) contentView.findViewById(R.id.iv_title_left);
		tv_title_left = (TextView) contentView.findViewById(R.id.tv_title_left);
		iv_title_middle = (ImageView) contentView.findViewById(R.id.iv_title_middle);
		tv_title_middle = (TextView) contentView.findViewById(R.id.tv_title_middle);
		iv_title_right = (ImageView) contentView.findViewById(R.id.iv_title_right);
		tv_title_right = (TextView) contentView.findViewById(R.id.tv_title_right);
		ll_title_left = (LinearLayout) contentView.findViewById(R.id.ll_title_left);
		ll_title_middle = (LinearLayout) contentView.findViewById(R.id.ll_title_middle);
		ll_title_right = (LinearLayout) contentView.findViewById(R.id.ll_title_right);
		rl_title_root = (RelativeLayout) ll_title_left.getParent();

//		setTitleBackgroundColor(AppConfigUtil.titleBackgroundColor);
//		setTitleTextColor(AppConfigUtil.titleTextColor);
//		setTitleTextSize(AppConfigUtil.titleTextSize);
		
		/*
		 * 配置标题栏
		 */
		TitleConfigUtils.getInstance().configTitleController(this);
	}
	
	@Override
	public int getLayoutResID() {
		return 0;
	}
	
	@Override
	public TitleController initTitleOnCreate() {
		return null;
	}
	
	@Override
	public void initHooksOnCreate() {
	}
	
	/**
	 * 设置标题的背景颜色
	 * @param colors
	 */
	public void setTitleBackgroundColor(int colors) {
		rl_title_root.setBackgroundColor(colors);
	}
	
	/**
	 * 设置标题的文字颜色
	 * @param colors
	 */
	public void setTitleTextColor(int colors) {
		tv_title_left.setTextColor(colors);
		tv_title_middle.setTextColor(colors);
		tv_title_right.setTextColor(colors);
	}
	
	/**
	 * 设置标题的文字大小
	 * @param txtSize
	 */
	public void setTitleTextSize(int txtSize) {
		tv_title_left.setTextSize(txtSize);
		tv_title_middle.setTextSize(txtSize);
		tv_title_right.setTextSize(txtSize);
	}
	
	public View getTitleBottomLine() {
		return v_title_bottomline;
	}
	
	public TextView getTvTitleLeft() {
		return tv_title_left;
	}
	
	public TextView getTvTitleMiddle() {
		return tv_title_middle;
	}
	
	public TextView getTvTitleRight() {
		return tv_title_right;
	}
	
	public ImageView getIvTitleLeft() {
		return iv_title_left;
	}
	
	public ImageView getIvTitleMiddle() {
		return iv_title_middle;
	}
	
	public ImageView getIvTitleRight() {
		return iv_title_right;
	}
	
	public LinearLayout getLlTitleLeft() {
		return ll_title_left;
	}
	
	public LinearLayout getLlTitleMiddle() {
		return ll_title_middle;
	}
	
	public LinearLayout getLlTitleRight() {
		return ll_title_right;
	}
	
	public RelativeLayout getRlTitleRoot() {
		return rl_title_root;
	}
	
}
