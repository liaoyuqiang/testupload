/**
 * 页面生成绑定控制器
 */
package com.yingze.businessframework.controller;

import com.yingze.businessframework.component.CompatScrollView;
import com.yingze.businessframework.listener.ViewScrollCallback;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

public class LayoutInflaterController {
	protected Context mContext;
	protected CompatScrollView sc;
	
	public LayoutInflaterController(Context context) {
		mContext = context;
	}
	
	public View inflateRootLayout(int layoutId) {
		return LayoutInflater.from(mContext).inflate(layoutId, null);
	}
	
	public View findViewById(View contentView, int layoutId) {
		return contentView.findViewById(layoutId);
	}
	
	public void addSubLayoutChild(FrameLayout parent, View view) {
		parent.addView(view, new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.MATCH_PARENT, 
				FrameLayout.LayoutParams.MATCH_PARENT));
	}
	
	/**
	 * 添加滑动刷新
	 * @param parent
	 * @param view
	 * @param isScollPull
	 */
	public void addSubLayoutChild(Context context, FrameLayout parent, View view, 
			ViewScrollCallback viewScrollCallback) {
		if (viewScrollCallback!=null) {
			sc = new CompatScrollView(context);
			sc.setContent(view);
			sc.setScrollCallback(viewScrollCallback);
			sc.setPullEffectived(viewScrollCallback.isPullEffectived());
			sc.setLoadEffectived(viewScrollCallback.isLoadEffectived());
			parent.addView(sc, new FrameLayout.LayoutParams(
					FrameLayout.LayoutParams.MATCH_PARENT, 
					FrameLayout.LayoutParams.MATCH_PARENT));
		} else {
			parent.addView(view, new FrameLayout.LayoutParams(
					FrameLayout.LayoutParams.MATCH_PARENT, 
					FrameLayout.LayoutParams.MATCH_PARENT));
		}
	}
	
	public CompatScrollView getCompatScrollView() {
		return sc;
	}
	
}
