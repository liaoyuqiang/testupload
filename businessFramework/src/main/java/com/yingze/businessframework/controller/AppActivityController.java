/**
 * 控制activity的逻辑处理流程
 */
package com.yingze.businessframework.controller;

import java.util.ArrayList;

import android.view.View;

public class AppActivityController extends ActivityController {
	protected View mContentView;
	protected ViewController mViewController;
	protected TitleController mTitleController;
	protected ArrayList<ViewController> mControllerArr = new ArrayList<ViewController>();
	
	protected AppActivityController(ViewController viewController, View contentView) {
		mViewController = viewController;
		mContentView = contentView;
	}
	
	public static AppActivityController newInstance(ViewController viewController, View contentView) {
		return new AppActivityController(viewController, contentView);
	}
	
	@Override
	public int getLayoutResID() {
		return mViewController.getLayoutResID();
	}
	
	@Override
	public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {
		if(controllerArr != null) {
			mControllerArr = controllerArr;
		}
		mViewController.injectControllerOnCreate(mControllerArr);
	}
	
	@Override
	public TitleController initTitleOnCreate() {
		mTitleController = mViewController.initTitleOnCreate();
		if (mTitleController == null) {
			mTitleController = TitleController.newDefaultInstance();
		}
		mControllerArr.add(mTitleController);
		return mTitleController;
	}
	
	@Override
	public void initWindowFeatureOnCreate() {
		mViewController.initWindowFeatureOnCreate();
		for(ViewController controller : mControllerArr) {
			controller.initWindowFeatureOnCreate();
		}
	}

	@Override
	public void initResourceViewOnCreate(View contentView) {
		mViewController.initResourceViewOnCreate(mContentView);
		for(ViewController controller : mControllerArr) {
			controller.initResourceViewOnCreate(mContentView);
		}
	}
	
	@Override
	public void initTransactionOnCreate() {
		mViewController.initTransactionOnCreate();
		for(ViewController controller : mControllerArr) {
			controller.initTransactionOnCreate();
		}
	}
	
	@Override
	public void initListenerOnCreate() {
		mViewController.initListenerOnCreate();
		for(ViewController controller : mControllerArr) {
			controller.initListenerOnCreate();
		}
	}

	@Override
	public void initHooksOnCreate() {
		mViewController.initHooksOnCreate();
		for(ViewController controller : mControllerArr) {
			controller.initHooksOnCreate();
		}
	}
	
	public ArrayList<ViewController> getControllerArr() {
		return mControllerArr;
	}
	
	public TitleController getTitleController() {
		return mTitleController;
	}
	
	public static AppActivityController NULL = new AppActivityController(null, null) {
		@Override
		public void initOnCreate() {
		}
	};
	
}
