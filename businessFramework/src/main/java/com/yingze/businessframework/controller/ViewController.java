package com.yingze.businessframework.controller;
/**
 * 控件业务框架接口
 * @param controller
 * @author LiangZH
 * @version v0.0.1
 * @date 2016/7/19 8:48
 */

import java.util.ArrayList;
import android.view.View;

public interface ViewController {
	/**
	 * 获取activity布局的id
	 * <p> 通过获取activity主布局的id，实例化activity的主布局 </p>
	 * @param controller
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public int getLayoutResID();
	
	/**
	 * 注入逻辑控制器
	 * <p> 动态注入逻辑控制器，方便在activity的创建过程中业务逻辑控制 </p>
	 * <ul>
	 * 	<li>注入子布局的逻辑控制器，用于对子布局的状态、位置操作</li>
	 *  <li>注入dialog的逻辑控制器，用于弹出dialog时进行操作</li>
	 *  <li>注入自定义的逻辑控制器，用于对某些逻辑功能进行操作</li>
	 * </ul>
	 * @param controllerArr
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void injectControllerOnCreate(ArrayList<ViewController> controllerArr);
	
	/**
	 * 在创建时初始化标题
	 * <p> 用于控制标题中的控件的状态及行为方式 </p>
	 * @param null
	 * @return 返回title状态控制器
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public TitleController initTitleOnCreate();
	
	/**
	 * 在创建时初始化窗口的特征元素
	 * <p> 主要是在activity创建的时候，执行基本的变量的初始化工作 </p>
	 * <ul>
	 *  <li> 基本持久化实体的创建 </li>
	 *  <li> 基本变量的赋值 </li>
	 *  <li> 基本变量的赋值 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void initWindowFeatureOnCreate();
	
	/**
	 * 在activity创建时初始化view的引用
	 * <p> 通过传入的主布局控件，绑定当前activity的控件引用 </p>
	 * <ul>
	 *  <li> 绑定view的引用 </li>
	 *  <li> 绑定布局的引用 </li>
	 *  <li> 绑定其他控件的引用 </li>
	 * </ul>
	 * @param 传入主布局控件
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void initResourceViewOnCreate(View contentView);

	/**
	 * 在activity创建时初始化事务处理
	 * <p> 通过传入的主布局控件，绑定当前activity的控件引用 </p>
	 * <ul>
	 *  <li> view状态的更新赋值 </li>
	 *  <li> view的动作处理 </li>
	 *  <li> 其他逻辑处理 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void initTransactionOnCreate();
	
	/**
	 * 在activity创建时初始化监听
	 * <p> 监听activity内各种类型的事件 </p>
	 * <ul>
	 *  <li> view点击状态的监听 </li>
	 *  <li> view触摸的监听 </li>
	 *  <li> view滑动的监听 </li>
	 *  <li> 其他事件的监听 </li>
	 * </ul>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void initListenerOnCreate();
	
	/**
	 * 在创建时初始化钩子功能
	 * <p> 放置钩子功能，扩展子类的可配置功能 </p>
	 * @param null
	 * @return void
	 * @author LiangZH
	 * @version v0.0.1
	 * @date 2016/7/19 8:48
	 */
	public void initHooksOnCreate();
}
