/**
 * 统一封装了风格化的dialog的调用格式，子类可根据实际情况进行扩展
 */
package com.yingze.businessframework.dialog;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.yingze.businessframework.R;
import com.yingze.businessframework.controller.AppActivityController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;
import com.yingze.businessframework.utils.ActivityUtils;

public class IDialog extends Dialog {

	public IDialog(Context context) {
		super(context);
	}

	public IDialog(Context context, int theme) {
		super(context, theme);
	}
	
	@Override
	public void show() {
		try{
			if (ActivityUtils.isActivityEffectived(getContext())) {
				super.show();
			}
		} catch(Exception e) {
			e.printStackTrace();
			super.show();
		}
	}
	
	public static class Builder {
		private int windowAnimationsResId = -1;
		private int theme = R.style.Dialog;
		private Context context;
		private BaseDialogController viewController;
		
		public Builder(Context context) {
			this.context = context;
		}
		
		public Builder setDialogTheme(int theme) {
			this.theme = theme;
			return this;
		}
		
		public Builder setWindowAnimation(int resId) {
			windowAnimationsResId = resId;
			return this;
		}
		
		public Builder setBaseDialogController(BaseDialogController viewController) {
			this.viewController = viewController;
			return this;
		}
		
		public void dialogConfig(Dialog dialog) {
			Window dialogWindow = dialog.getWindow();
			WindowManager.LayoutParams lp = dialogWindow.getAttributes();
			lp.x = 0; // 新位置X坐标
	        lp.y = 0; // 新位置Y坐标
	        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度
	        lp.height = WindowManager.LayoutParams.MATCH_PARENT; // 高度
//	        lp.alpha = 0.0f; // 透明度
	        dialogWindow.setAttributes(lp);
		}
		
		public Dialog create() {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final IDialog dialog = new IDialog(context, theme);
			if(windowAnimationsResId != -1) {
				dialog.getWindow().setWindowAnimations(windowAnimationsResId);
			}
			
			View contentView = inflater.inflate(viewController.getLayoutResID(), null);
			dialog.setContentView(contentView);
			dialogConfig(dialog);
			viewController.setContext(context);
			viewController.setDialog(dialog);
			viewController.onCreate(contentView);
			return dialog;
		}
		
	}

	public static abstract class BaseDialogController implements ViewController, View.OnClickListener{
		protected Context mContext;
		protected TextView tv_dialog_info;
		protected Button btn_positiveButton;
		protected Button btn_negativeButton;
		protected View contentView;
		protected RelativeLayout rl_layer;
		protected Dialog dialog;
		protected AppActivityController mActivityController;
		
		protected abstract String getTitle();
		protected abstract String getContent();
		protected abstract void onPositiveButtonClick(Dialog dialog, View view);
		protected abstract void onNegativeButtonClick(Dialog dialog, View view);
		
		public void setContext(Context context) {
			this.mContext = context;
		}
		
		public void setDialog(Dialog dialog) {
			this.dialog = dialog;
		}
		
		public Dialog getDialog() {
			return dialog;
		}
		
		protected void onCreate(View contentView) {
			mActivityController = AppActivityController.newInstance(this, contentView);
			mActivityController.initOnCreate();
		}
		
		@Override
		public int getLayoutResID() {
			return R.layout.template_dialog_base_layout;
		};
		
		@Override
		public TitleController initTitleOnCreate() {
			return new TitleController() {
				@Override
				public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {
				}
				@Override
				public void initTransactionOnCreate() {
					v_title_bottomline.setVisibility(View.GONE);
					ll_title_left.setVisibility(View.GONE);
					ll_title_right.setVisibility(View.GONE);
					tv_title_middle.setText(getTitle());
					tv_title_middle.setTextColor(0xffffffff);
					rl_title_root.setBackgroundResource(R.drawable.ic_dialog_title_bg);
				}
				@Override
				public void initListenerOnCreate() {
				}
			};
		};
		
		@Override
		public void injectControllerOnCreate(ArrayList<ViewController> controllerArr) {
		}
		
		@Override
		public void initWindowFeatureOnCreate() {
		}
		
		@Override
		public void initResourceViewOnCreate(View contentView) {
			tv_dialog_info = (TextView)contentView.findViewById(R.id.tv_dialog_info);
			btn_positiveButton = (Button) contentView.findViewById(R.id.btn_positiveButton);
			btn_negativeButton = (Button) contentView.findViewById(R.id.btn_negativeButton);
			rl_layer = (RelativeLayout) contentView.findViewById(R.id.rl_layer);
		}
		
		@Override
		public void initTransactionOnCreate() {
			tv_dialog_info.setText(getContent());
		}
		
		@Override
		public void initListenerOnCreate() {
			btn_positiveButton.setOnClickListener(this);
			btn_negativeButton.setOnClickListener(this);
		}
		
		@Override
		public void initHooksOnCreate() {
		}
		
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_positiveButton) {
				onPositiveButtonClick(dialog, v);
			} else if(v.getId() == R.id.btn_negativeButton) {
				onNegativeButtonClick(dialog, v);
			}
		}
		
		public TitleController getTitleController() {
			return mActivityController.getTitleController();
		}
		
	}
}