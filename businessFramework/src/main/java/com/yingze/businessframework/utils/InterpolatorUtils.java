package com.yingze.businessframework.utils;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;

/**
 * 动画插值器
 * @author LiangZH
 */

public class InterpolatorUtils {
	
	/**
	 * 加速插值器
	 * @return
	 */
	public static DecelerateInterpolator newAccelerateInterpolator() {
		return new DecelerateInterpolator();
	}
	
	/**
	 * 减速插值器
	 * @return
	 */
	public static DecelerateInterpolator newDecelerateInterpolator() {
		return new DecelerateInterpolator();
	}
	
	/**
	 * AccelerateDecelerateInterpolator  加速减速插值器
	 */
	public static AccelerateDecelerateInterpolator newAccelerateDecelerateInterpolator() {
		return new AccelerateDecelerateInterpolator();
	}
	
	/**
	 * LinearInterpolator 线性插值器
	 */
	public static LinearInterpolator newLinearInterpolator() {
		return new LinearInterpolator();
	}
	
	/**
	 * BounceInterpolator 弹跳插值器
	 */
	public static BounceInterpolator newBounceInterpolator() {
		return new BounceInterpolator();
	}
	
	/**
	 * AnticipateInterpolator 回荡秋千插值器
	 */
	public static AnticipateInterpolator newAnticipateInterpolator() {
		return new AnticipateInterpolator();
	}
	
	/**
	 * AnticipateOvershootInterpolator
	 */
	public static AnticipateOvershootInterpolator newAnticipateOvershootInterpolator() {
		return new AnticipateOvershootInterpolator();
	}
	
	/**
	 * CycleInterpolator 正弦周期变化插值器
	 */
	public static CycleInterpolator newCycleInterpolator(float cycles) {
		return new CycleInterpolator(cycles);
	}
	
	/**
	 * OvershootInterpolator 
	 */
	public static OvershootInterpolator newOvershootInterpolator () {
		return new OvershootInterpolator ();
	}
	
}
