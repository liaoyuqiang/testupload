package com.yingze.businessframework.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

public class ActivityUtils {

	/**
	 * 判断当前activity是否有效
	 * @return
	 */
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	public static boolean isActivityEffectived(Context context) {
		Activity activity = scanForActivity(context);
		if (activity == null) {
			return false;
		}
		
		int sdk = android.os.Build.VERSION.SDK_INT; 
		if(sdk>=android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {     
			if (activity.isFinishing()) {
				return false;
			}
			if (activity.isDestroyed()) {
				return false;
			}
			return true;
		} else {    
			return true;
		}
	}
	
	private static Activity scanForActivity(Context cont) {  
	    if (cont == null) {
	        return null;  
	    } 	    
	    if (cont instanceof Activity) {
	        return (Activity) cont;  
	    } 
	    if (cont instanceof ContextWrapper) {
	        return scanForActivity(((ContextWrapper) cont).getBaseContext());  
	    } 
	    return null;  
	}  
	
}
