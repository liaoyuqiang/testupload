package com.yingze.businessframework.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * 字体控制器
 * @author LittleBird
 *
 */
public final class TypefaceUtils {
	
	private static TypefaceUtils instance = new TypefaceUtils();
	
	private TypefaceUtils() {
	}
	
	public static TypefaceUtils getInstance() {
		return instance;
	}
	
	/**
	 * 设置textview的字体样式
	 * @param context   上下文
	 * @param textView  需要设置字体的textview
	 * @param fontPath  字体文件路径
	 * <p>
	 * 		eg:
	 *  	resetTextTypeface(context, textView, "fonts/DroidSansThai.ttf");
	 * </p>
	 * 
	 */
	public void resetTextTypeface(Context context, TextView textView, String fontPath) {
		try{
			Typeface typeFace = Typeface.createFromAsset(context.getAssets(), fontPath);
			textView.setTypeface(typeFace);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
