/**
 * 标题配置管理器，主要功能包括：
 * 1、外部设置标题的公共值
 * 2、TitleController 需要从中获取配置的值
 * 符合外观、代理模式的特征
 */
package com.yingze.businessframework.utils;

import com.yingze.businessframework.controller.TitleController;
import com.yingzecorelibrary.utils.AppConfigUtil;

public final class TitleConfigUtils {
	private static TitleConfigUtils mTitleConfigUtils;
	private int titleBackgroundColor = AppConfigUtil.titleBackgroundColor;
	private int titleTxtColorLeft = AppConfigUtil.titleTextColor;
	private int titleTxtColorMiddle = AppConfigUtil.titleTextColor;
	private int titleTxtColorRight = AppConfigUtil.titleTextColor;
	
	private int titleTxtSizeLeft = AppConfigUtil.titleTextSize;
	private int titleTxtSizeMiddle = AppConfigUtil.titleTextSize;
	private int titleTxtSizeRight = AppConfigUtil.titleTextSize;

	private int titleRootImageRes = -1;
	private int titleLeftImageRes = -1;
	private int titleMiddleImageRes = -1;
	private int titleRightImageRes = -1;
	
	public static TitleConfigUtils getInstance() {
		synchronized (TitleConfigUtils.class) {
			if (mTitleConfigUtils == null) {
				mTitleConfigUtils = new TitleConfigUtils();
			}
			return mTitleConfigUtils;
		}
	}
	
	private TitleConfigUtils() {
		
	}
	
	public void configTitleController(TitleController titleController) {
		titleController.getTvTitleLeft().setTextSize(titleTxtSizeLeft);
		titleController.getTvTitleLeft().setTextColor(titleTxtColorLeft);
		titleController.getTvTitleMiddle().setTextSize(titleTxtSizeMiddle);
		titleController.getTvTitleMiddle().setTextColor(titleTxtColorMiddle);
		titleController.getTvTitleRight().setTextSize(titleTxtSizeRight);
		titleController.getTvTitleRight().setTextColor(titleTxtColorRight);
		titleController.setTitleBackgroundColor(titleBackgroundColor);
		
		if (titleLeftImageRes != -1) {
			titleController.getIvTitleLeft().setImageResource(titleLeftImageRes);
		}
		
		if(titleMiddleImageRes != -1) {
			titleController.getIvTitleMiddle().setImageResource(titleMiddleImageRes);
		}
		
		if (titleRightImageRes != -1) {
			titleController.getIvTitleRight().setImageResource(titleRightImageRes);
		}
		
		if (titleRootImageRes != -1) {
			titleController.getRlTitleRoot().setBackgroundResource(titleRootImageRes);
		}
	}

	/**
	 * 设置标题左边字体颜色
	 * @param titleTxtColorLeft
	 */
	public void setTitleTxtColorLeft(int titleTxtColorLeft) {
		this.titleTxtColorLeft = titleTxtColorLeft;
	}

	/**
	 * 设置标题中间字体颜色
	 * @param titleTxtColorMiddle
	 */
	public void setTitleTxtColorMiddle(int titleTxtColorMiddle) {
		this.titleTxtColorMiddle = titleTxtColorMiddle;
	}

	/**
	 * 设置标题右边字体颜色
	 * @param titleTxtColorRight
	 */
	public void setTitleTxtColorRight(int titleTxtColorRight) {
		this.titleTxtColorRight = titleTxtColorRight;
	}

	/**
	 * 设置标题背景颜色
	 * @param titleBackgroundColor 标题背景颜色
	 */
	public void setTitleBackgroundColor(int titleBackgroundColor) {
		this.titleBackgroundColor = titleBackgroundColor;
	}

	/**
	 * 设置标题左边字体大小
	 * @param titleTxtSizeLeft
	 */
	public void setTitleTxtSizeLeft(int titleTxtSizeLeft) {
		this.titleTxtSizeLeft = titleTxtSizeLeft;
	}

	/**
	 * 设置标题中间字体大小
	 * @param titleTxtSizeMiddle
	 */
	public void setTitleTxtSizeMiddle(int titleTxtSizeMiddle) {
		this.titleTxtSizeMiddle = titleTxtSizeMiddle;
	}

	/**
	 * 设置标题右边字体大小
	 * @param titleTxtSizeRight
	 */
	public void setTitleTxtSizeRight(int titleTxtSizeRight) {
		this.titleTxtSizeRight = titleTxtSizeRight;
	}

	public int getTitleRootImageRes() {
		return titleRootImageRes;
	}

	/**
	 * 设置根布局文件背景
	 * @param titleRootImageRes
	 */
	public void setTitleRootImageRes(int titleRootImageRes) {
		this.titleRootImageRes = titleRootImageRes;
	}
	
	public int getTitleLeftImageRes() {
		return titleLeftImageRes;
	}

	/**
	 * 设置标题栏左边图片控件的资源属性
	 * @param titleLeftImageRes
	 */
	public void setTitleLeftImageRes(int titleLeftImageRes) {
		this.titleLeftImageRes = titleLeftImageRes;
	}

	public int getTitleMiddleImageRes() {
		return titleMiddleImageRes;
	}

	/**
	 * 设置标题栏中间图片控件的资源属性
	 * @param titleLeftImageRes
	 */
	public void setTitleMiddleImageRes(int titleMiddleImageRes) {
		this.titleMiddleImageRes = titleMiddleImageRes;
	}

	public int getTitleRightImageRes() {
		return titleRightImageRes;
	}

	/**
	 * 设置标题栏右边图片控件的资源属性
	 * @param titleLeftImageRes
	 */
	public void setTitleRightImageRes(int titleRightImageRes) {
		this.titleRightImageRes = titleRightImageRes;
	}
	
}
