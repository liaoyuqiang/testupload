package com.yingze.businessframework.utils;

import android.content.Context;
import android.content.res.Resources;

public final class ResourcesUtils {
	private static ResourcesUtils mInstance;
	private Resources mResources;

	public static ResourcesUtils getInstance(Context context) {
		synchronized (ResourcesUtils.class) {
			if(mInstance == null) {
				mInstance = new ResourcesUtils(context);
			}
			return mInstance;
		}
	}
	
	private ResourcesUtils(Context context) {
		mResources = context.getResources();
	}
	
	public int getDimensionPixelSizeById(int id) {
		return mResources.getDimensionPixelSize(id);
	}
	
	public String getText(int id) {
		return mResources.getText(id).toString();
	}
	
}
