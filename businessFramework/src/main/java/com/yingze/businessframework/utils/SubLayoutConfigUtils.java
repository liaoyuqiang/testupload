/**
 * 子布局配置管理集合
 */
package com.yingze.businessframework.utils;

public final class SubLayoutConfigUtils {
	private int mPaddingLeft = 0;
	private int mPaddingRight = 0;
	private int mPaddingTop = 0;
	private int mPaddingBottom = 0;
	
	private int bgColor = 0xffffffff;
	private int backgroundResourceId = -1;
	
	private static SubLayoutConfigUtils instance;
	
	public static SubLayoutConfigUtils getInstance() {
		synchronized (SubLayoutConfigUtils.class) {
			if (instance == null) {
				instance = new SubLayoutConfigUtils();
			}
			return instance;
		}
	}

	public void setPadding(int paddingLeft, int paddingRight, int paddingTop, int paddingBottom) {
		mPaddingLeft = paddingLeft;
		mPaddingRight = paddingRight;
		mPaddingTop = paddingTop;
		mPaddingBottom = paddingBottom;
	}
	
	public int getPaddingLeft() {
		return mPaddingLeft;
	}

	public void setPaddingLeft(int paddingLeft) {
		this.mPaddingLeft = paddingLeft;
	}

	public int getPaddingRight() {
		return mPaddingRight;
	}

	public void setPaddingRight(int paddingRight) {
		this.mPaddingRight = paddingRight;
	}

	public int getPaddingTop() {
		return mPaddingTop;
	}

	public void setPaddingTop(int paddingTop) {
		this.mPaddingTop = paddingTop;
	}

	public int getPaddingBottom() {
		return mPaddingBottom;
	}

	public void setPaddingBottom(int paddingBottom) {
		this.mPaddingBottom = paddingBottom;
	}
	
	public int getBackgroundColor() {
		return bgColor;
	}
	
	public void setBackgroundColor(int bgColor) {
		this.bgColor = bgColor;
	}
	
	public int getBackgroundResourceId() {
		return backgroundResourceId;
	}
	
	public void setBackgroundResourceId(int resId) {
		this.backgroundResourceId = resId;
	}
}
