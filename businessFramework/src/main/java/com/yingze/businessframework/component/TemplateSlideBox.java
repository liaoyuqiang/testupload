package com.yingze.businessframework.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

import com.yingze.businessframework.R;
import com.yingze.businessframework.listener.InterceptTouchEventListener;
import com.yingzecorelibrary.utils.LogUtil;

/**
 * 自定义侧滑控件
 * @author littlebird
 *
 */
@SuppressLint("ClickableViewAccessibility")
public class TemplateSlideBox extends FrameLayout implements InterceptTouchEventListener{
	private FrameLayout mLeftSub;
	private SubLayout mMiddleSub;
	private FrameLayout mRightSub;
	private LayoutInflater inflater;
	private BehaviorStateController mBSController;
	AnimationController mAnimationController;
	private int interval = 120;
	private int durationMillis = 600;
	
	public TemplateSlideBox(Context context) {
		super(context);
		init();
	}
	
	public TemplateSlideBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void init() {
		mBSController = new BehaviorStateController();
		mAnimationController = new AnimationController();
		this.setClickable(true);
		
		mLeftSub = new FrameLayout(getContext());
		mRightSub = new FrameLayout(getContext());
		mMiddleSub = new SubLayout(getContext());
		mMiddleSub.setClickable(false);
		this.addView(mLeftSub);
		this.addView(mRightSub);
		this.addView(mMiddleSub);
	}
	
	public void test() {
		mLeftSub.setBackgroundColor(0x2fffff00);
		mRightSub.setBackgroundColor(0x2fff00ff);
		
//		inflater = LayoutInflater.from(getContext());
//		View page = inflater.inflate(R.layout.layout_main_page, null);
//		ListBox listview = (ListBox)page.findViewById(R.id.lv_main_page);
//		listview.setInterceptTouchEventListener(this);
//		BaseAdapter adapter = DataController.getInstance().getAdapter(getContext());
//		listview.setAdapter(adapter);
		TemplateContainer mRLBox = new TemplateContainer(getContext());
//		mRLBox.test();
		mRLBox.setInterceptTouchEventListener(this);
		mMiddleSub.setContentView(mRLBox);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(mBSController.isInAnimationState()) {
			return false;
		}
		if(ev.getAction() == MotionEvent.ACTION_DOWN || 
				ev.getAction() == MotionEvent.ACTION_UP || 
				ev.getAction() == MotionEvent.ACTION_CANCEL) {
			mBSController.setNoInterceptTouchEventState();
		}
		return super.dispatchTouchEvent(ev);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		LogUtil.set("slide-box", "onIntercepTouchEvent").info();
		if(mBSController.isInterceptTouchEventState()) {
			mBSController.setEffectivePoint(event.getX(), event.getY());
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mBSController.setEffectivePoint(event.getX(), event.getY());
			break;
		case MotionEvent.ACTION_MOVE:
			mBSController.updateTrack(event.getX(), event.getY());
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			mBSController.upHandle();
			break;
		}
		requestLayout();
		return super.onTouchEvent(event);
	}
	
	@Override
	public void onInterceptTouchEventHandle(boolean onInterceptTouchEvent, String type, MotionEvent event) {
		if(onInterceptTouchEvent) {
			mBSController.setInterceptTouchEventState();
		} else {
			mBSController.setNoInterceptTouchEventState();
		}
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		int[] mideleStartEdteArr = mBSController.getMiddleStartEdge();
		mMiddleSub.layout(left - SubLayout.EDGE_WIDTH + mideleStartEdteArr[0] , 0,
				right-left+SubLayout.EDGE_WIDTH + mideleStartEdteArr[0], bottom-top);
		mLeftSub.layout(left, 0, right, bottom-top);
		mRightSub.layout(left, 0, right, bottom-top);
		this.invalidate();
	}
	
	public void slideToRight() {
		if(mBSController.isInAnimationState()){
			return;
		}
		mBSController.setInAnimationState();
		mLeftSub.setVisibility(View.VISIBLE);
		mRightSub.setVisibility(View.GONE);
		float timeRatio = Math.abs(((float)getWidth()-interval-
				mBSController.getMiddleStartEdgeX())/(getWidth()-interval));
		mAnimationController
			.setTrack(0, getWidth()-interval-mBSController.getMiddleStartEdgeX(), 0, 0)
			.setDurationMillis((int)(timeRatio * durationMillis))
			.setContentView(mMiddleSub)
			.setAnimationListener(new ViewAnimationListener() {
				@Override
				public void feedback() {
					mMiddleSub.clearAnimation();
					mBSController.setMiddleStartEdgeX(getWidth()-interval);
					mBSController.setInNoAnimationState();
					requestLayout();
				}
		}).startAnimation();
	}
	
	public void slideToCenter() {
		if(mBSController.isInAnimationState()){
			return;
		}
		mBSController.setInAnimationState();
		float timeRatio = Math.abs(((float)-mBSController.getMiddleStartEdgeX())/(getWidth()-interval));
		mAnimationController
			.setTrack(0, -mBSController.getMiddleStartEdgeX(), 0, 0)
			.setDurationMillis((int)(timeRatio * durationMillis))
			.setContentView(mMiddleSub)
			.setAnimationListener(new ViewAnimationListener() {
				@Override
				public void feedback() {
					mMiddleSub.clearAnimation();
					mBSController.setMiddleStartEdgeX(0);
					mBSController.setInNoAnimationState();
					requestLayout();
				}
		}).startAnimation();
	}

	public void slideToLeft() {
		if(mBSController.isInAnimationState()){
			return;
		}
		mBSController.setInAnimationState();
		mLeftSub.setVisibility(View.GONE);
		mRightSub.setVisibility(View.VISIBLE);
		float timeRatio = Math.abs(((float)-getWidth()+interval-
				mBSController.getMiddleStartEdgeX())/(getWidth()-interval));
		mAnimationController
			.setTrack(0, -getWidth()+interval-mBSController.getMiddleStartEdgeX(), 0, 0)
			.setDurationMillis((int)(timeRatio * durationMillis))
			.setContentView(mMiddleSub)
			.setAnimationListener(new ViewAnimationListener() {
				@Override
				public void feedback() {
					mMiddleSub.clearAnimation();
					mBSController.setMiddleStartEdgeX(-getWidth()+interval);
					mBSController.setInNoAnimationState();
					requestLayout();
			}
		}).startAnimation();
	}
	
	/**
	 * 行为状态控制
	 * @author LittleBird
	 */
	class BehaviorStateController {
		private boolean isInterceptTouchEventState = false;
		private boolean isInAnimationState = false;
		private float effectivePoint[];
		private int mMiddleStartEdge[];

		private BehaviorStateController() {
			effectivePoint = new float[2];
			mMiddleStartEdge = new int[2];
		}
		
		public void setInterceptTouchEventState() {
			isInterceptTouchEventState = true;
		}
		
		public void setNoInterceptTouchEventState() {
			isInterceptTouchEventState = false;
		}
		
		public boolean isInterceptTouchEventState() {
			return isInterceptTouchEventState;
		}
		
		public void setInAnimationState() {
			isInAnimationState = true;
		}
		
		public void setInNoAnimationState() {
			isInAnimationState = false;
		}
		
		public boolean isInAnimationState() {
			return isInAnimationState;
		}
		
		public float[] getEffectivePoint() {
			return effectivePoint;
		}

		public void setEffectivePoint(float effectivePointX, float effectivePointY) {
			effectivePoint[0] = effectivePointX;
			effectivePoint[1] = effectivePointY;
		}
		
		public void updateTrack(float x, float y) {
			int xGap = (int)(x - effectivePoint[0]);
			int yGap = (int)(y - effectivePoint[1]);
			if(xGap != 0) {
				if(-getWidth()+interval<mMiddleStartEdge[0]+xGap &&
					mMiddleStartEdge[0]+xGap<getWidth()-interval){
					mMiddleStartEdge[0] += xGap;
					requestLayout();
				}
				effectivePoint[0] = x;
			}
			if(yGap != 0) {
				mMiddleStartEdge[1] += yGap;
				effectivePoint[1] = y;
				requestLayout();
			}
		}

		public int[] getMiddleStartEdge() {
			return mMiddleStartEdge;
		}

		public void setMiddleStartEdge(int[] mMiddleStartEdge) {
			this.mMiddleStartEdge = mMiddleStartEdge;
		}
		
		public int getMiddleStartEdgeX() {
			return mMiddleStartEdge[0];
		}
		
		public void setMiddleStartEdgeX(int middleStartEdgeX) {
			mMiddleStartEdge[0] = middleStartEdgeX;
		}
		
		public int getMiddleStartEdgeY() {
			return mMiddleStartEdge[1]; 
		}
		
		public void setMiddleStartEdgeY(int middleStartEdgeY) {
			mMiddleStartEdge[1] = middleStartEdgeY;
		}
		
		public void upHandle() {
			int width = getWidth();
			if(mMiddleStartEdge[0] > 0) {
				if(mMiddleStartEdge[0] > width/2) {
					slideToRight();
				} else {
					slideToCenter();
				}
			} else if(mMiddleStartEdge[0] < 0) {
				if(mMiddleStartEdge[0] > -width/2) {
					slideToCenter();
				} else {
					slideToLeft();
				}
			} else {
				mMiddleStartEdge[0] = 0;
				mMiddleStartEdge[1] = 0;
			}
			requestLayout();
		}

	}
	
	/**
	 * 动画控制类
	 * @author dell
	 */
	class AnimationController {
		private int durationMillis = 600;
		private float fromXDelta;
		private float toXDelta;
		private float fromYDelta;
		private float toYDelta;
		private View contentView;
		private ViewAnimationListener viewAnimationListener;
		
		public AnimationController setTrack(float fromXDelta, float toXDelta,
				float fromYDelta, float toYDelta) {
			this.fromXDelta = fromXDelta;
			this.toXDelta = toXDelta;
			this.fromYDelta = fromYDelta;
			this.toYDelta = toYDelta;
			return this;
		}
		
		public AnimationController setAnimationListener(ViewAnimationListener viewAnimationListener) {
			this.viewAnimationListener = viewAnimationListener;
			return this;
		}
		
		public AnimationController setContentView(View contentView) {
			this.contentView = contentView;
			return this;
		}
		
		public AnimationController setDurationMillis(int durationMillis) {
			this.durationMillis = durationMillis;
			return this;
		}
		
		public void startAnimation() {
			TranslateAnimation animation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta, toYDelta);
			animation.setDuration(durationMillis);
			animation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationEnd(Animation animation) {
					if(viewAnimationListener != null) {
						viewAnimationListener.feedback();
					}
				}
				@Override
				public void onAnimationRepeat(Animation animation) {}
				@Override
				public void onAnimationStart(Animation animation) {}
			});
			contentView.startAnimation(animation);
		}
		

	}
	
	interface ViewAnimationListener {
		public void feedback();
	}
	
	/**
	 * 主布局
	 * @author littlebird
	 */
	class SubLayout extends FrameLayout{
		private View leftEdgeView;
		private View rightEdgeView;
		private View contentView;
		private final static int EDGE_WIDTH = 12;
		
		public SubLayout(Context context) {
			super(context);
			leftEdgeView = new View(context);
			rightEdgeView = new View(context);
			leftEdgeView.setBackgroundResource(R.drawable.shade_gradient_left);
			rightEdgeView.setBackgroundResource(R.drawable.shade_gradient_right);
			addView(leftEdgeView);
			addView(rightEdgeView);
		}
		
		public void setContentView(View view) {
			contentView = view;
			this.addView(contentView);
		}
		
		@Override
		protected void onLayout(boolean changed, int left, int top, int right,
				int bottom) {
			leftEdgeView.layout(0, 0, EDGE_WIDTH, bottom-top);
			rightEdgeView.layout(right-left-EDGE_WIDTH, 0, right-left, bottom-top);
			if(contentView!=null){
				contentView.layout(EDGE_WIDTH, 0, right-left-EDGE_WIDTH, bottom-top);
			}
		}
	}
}
