package com.yingze.businessframework.component;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.yingze.businessframework.R;
import com.yingzecorelibrary.utils.LogUtil;

@SuppressLint({ "ClickableViewAccessibility", "DrawAllocation", "NewApi" })
public class DragBox extends FrameLayout{

	private int ANIMATION_DURATION = 400;
	private int mLastDragged = -1;
	private int mLastTarget = -1;
	private float mLastMotionX,mLastMotionY;
	private static final int DEFAULT_COL_COUNT = 2;
	private static final int DEFAULT_ROW_COUNT = 4;
	private static final int DEFAULT_GRID_GAP = 10; // gap between grids (dips)
	
	private int mPageCount;
	private int mGridItemWidth;
	private int mGridItemHeight;
	private int mPaddingLeft;
	private int mPaddingRight;
	private int mPaddingTop;
	private int mPaddingBottom;
	private int mColCount = DEFAULT_COL_COUNT;
	private int mRowCount = DEFAULT_ROW_COUNT;
	private int mPageSize = mColCount * mRowCount;
	private int mGridGap;
	private int mMaxOverScrollSize;
	private int mCurItem=0;
	private int mLastPosition = -1;
	/** 记录当前子项的最新位置 */
	private ArrayList<Integer> newPositionsArr = new ArrayList<Integer>();
	private Bitmap background;
	private float scale;
	
	public DragBox(Context context) {
		super(context);
		this.setLongClickable(true);
		this.setClickable(true);
		this.setChildrenDrawingOrderEnabled(true);
		this.setWillNotDraw(false);
	}
	
	public DragBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void test() {		
		final float density = getContext().getResources().getDisplayMetrics().density;
		mGridGap = (int) (DEFAULT_GRID_GAP * density);
		mPaddingLeft = (int)(20 * density);
		mPaddingRight = (int)(20 * density);
		mPaddingTop = (int)(40 * density);
		mPaddingBottom = (int)(40 * density);
		
		int[] idArr = new int[]{
//				R.drawable.ic_menu_emoticons,
//				R.drawable.ic_menu_notifications,
//				R.drawable.ic_menu_play_clip,
//				R.drawable.ic_menu_report_image,
//				R.drawable.ic_menu_search,
//				R.drawable.ic_volume_bluetooth_ad2p
				R.drawable.template_main_history,
				R.drawable.template_main_maintenance_records,
				R.drawable.template_registration_record_electricity,
				R.drawable.template_main_registration_record,
				R.drawable.template_main_reservation,
				R.drawable.template_main_person_info
				};
		String[] content = new String[]{"安全巡查","报修记录","安全管理","咨询查询","用户管理","烟草详情"};
		for(int i=0;i<6;i++) {
			LinearLayout layout = new LinearLayout(getContext());
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.setGravity(Gravity.CENTER);
			if(i==0 || i==3) {
				layout.setBackgroundColor(0xafff9829);
			} else {
				layout.setBackgroundColor(0xaf4eb7f8);
			}
			ImageView image = new ImageView(getContext());
			image.setLayoutParams(new LayoutParams((int)(80*density),(int)(80*density)));
			image.setScaleType(ScaleType.CENTER);
			image.setImageResource(idArr[i]);
			image.setScaleType(ScaleType.FIT_XY);
			layout.addView(image);
			
			TextView tv = new TextView(getContext());
			tv.setText(content[i]);
			tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
			tv.setTextColor(Color.WHITE);
			tv.setClickable(false);
			tv.setGravity(Gravity.CENTER);
			layout.addView(tv);
			addView(layout);
			
		}
	
		/*
		 * 处理背景滑动
		 */
		handleBackground();
	}
	
	private int translateX = 0;
	private int mGap = 0;
	public void handleBackground() {
		WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
		int boxWidth = wm.getDefaultDisplay().getWidth();
		int boxHeight = wm.getDefaultDisplay().getHeight();
		background = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.tianjingbg);
		
		int bmpWidth = background.getWidth();
		int bmpHeight = background.getHeight();
		
		int bmpNewHeight = boxHeight;
		scale = bmpNewHeight/(float)bmpHeight;
		int bmpNewWidth = (int) (bmpWidth * scale);
		
		mGap = bmpNewWidth - boxWidth;
		LogUtil.set("DragBox", scale+","+bmpWidth+","+bmpHeight+","+bmpNewWidth+","+bmpNewHeight).error();
//		this.setBackground(new BitmapDrawable(getContext().getResources(), background));
		
		matrix.setScale(scale, scale);
		handler.sendMessageDelayed(Message.obtain(), 2000);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mLastMotionX = event.getX();
			mLastMotionY = event.getY();
			mLastPosition = getPositionByXY((int) mLastMotionX, (int) mLastMotionY);
			if(mLastPosition==-1)return true;
			mLastDragged = mLastPosition;
			mLastTarget = mLastPosition;
			animateDragged();
			break;
		case MotionEvent.ACTION_MOVE:
			float gapX = event.getX() - mLastMotionX;
			float gapY = event.getY() - mLastMotionY;
			mLastMotionX = event.getX();
			mLastMotionY = event.getY();
			if (mLastDragged >= 0) {
				int target = getTargetByXY((int)event.getX(), (int) event.getY());
				if(target != -1 && target != mLastTarget) {
					animateTranslate(target);
					mLastTarget = target;	
				}
				View child = getChildAt(mLastDragged);
				child.setDrawingCacheEnabled(true);
				int left = child.getLeft()+(int)gapX;
				int top = child.getTop()+(int)gapY;
				child.layout(left, top, left+child.getWidth(), top + child.getHeight());
			}
			break;
		case MotionEvent.ACTION_UP:
			rearrange();
			mLastPosition = -1;
			mLastTarget = -1;
			mLastDragged = -1;
			break;
		}
		return true;
	}
	
	@Override
	public void onLayout(boolean changed, int left, int top, int right, int bottom) {
		final int childCount = getChildCount();
		mPageCount = (childCount + mPageSize - 1) / mPageSize;
		mGridItemWidth = (getWidth() - mPaddingLeft - mPaddingRight - (mColCount - 1) * mGridGap) / mColCount;
		mGridItemHeight = (getHeight() - mPaddingTop - mPaddingBottom - (mRowCount - 1) * mGridGap) / mRowCount;
		mGridItemWidth = mGridItemHeight = Math.min(mGridItemWidth, mGridItemHeight);
		mMaxOverScrollSize = mGridItemWidth / 2;
		newPositionsArr.clear();
		for (int i = 0; i < childCount; i++) {
			final View child = getChildAt(i);
			final Rect rect = getRectByPosition(i);
			child.measure(
					MeasureSpec.makeMeasureSpec(rect.width(), MeasureSpec.EXACTLY),
					MeasureSpec.makeMeasureSpec(rect.height(), MeasureSpec.EXACTLY));
			child.layout(rect.left, rect.top, rect.right, rect.bottom);
			newPositionsArr.add(-1);
		}
	}
	
	@Override
	protected int getChildDrawingOrder(int childCount, int i) {
		if (mLastDragged == -1) {
			return i;
		} else if (i == childCount - 1) {
			return mLastDragged;
		} else if (i >= mLastDragged) {
			return i + 1;
		}
		return i;
	}
	
	private Rect getRectByPosition(int position) {
		final int page = position / mPageSize;
		final int col = (position % mPageSize) % mColCount;
		final int row = (position % mPageSize) / mColCount;
		final int left = getWidth() * page + mPaddingLeft + col * (mGridItemWidth + mGridGap);
		final int top = mPaddingTop + row * (mGridItemHeight + mGridGap);
		return new Rect(left, top, left + mGridItemWidth, top + mGridItemHeight);
	}
	
	private int getPositionByXY(int x, int y) {
		final int col = (x - mPaddingLeft) / (mGridItemWidth + mGridGap);
		final int row = (y - mPaddingTop) / (mGridItemHeight + mGridGap);
		if (x < mPaddingLeft || 
				x >= (mPaddingLeft + col * (mGridItemWidth + mGridGap) + mGridItemWidth) ||
				y < mPaddingTop || 
				y >= (mPaddingTop + row * (mGridItemHeight + mGridGap) + mGridItemHeight) ||
				col < 0 || col >= mColCount || row < 0 || row >= mRowCount) {
			return -1;
		}
		final int position = mCurItem * mPageSize + row * mColCount + col;
		if (position < 0 || position >= getChildCount()) {
			return -1;
		}
		return position;
	}
	
	private int getTargetByXY(int x, int y) {
		final int position = getPositionByXY(x, y);
		if (position < 0) {
			return -1;
		}
		final Rect r = getRectByPosition(position);
		final int page = position / mPageSize;
		r.inset(r.width() / 4, r.height() / 4);
		r.offset(-getWidth() * page, 0);
		if (!r.contains(x, y)) {
			return -1;
		}
		return position;
	}

	public void animateTranslate(int target) {
		for(int i=0;i<this.getChildCount();i++) {
			View v = this.getChildAt(i);
			if (i == mLastDragged) {
				continue;
			}
			int newPos = i;
			if (mLastDragged < target && i > mLastDragged && i <= target) {
				newPos--;
			} else if (target < mLastDragged && i >= target && i < mLastDragged) {
				newPos++;
			}
			int oldPos = i;
			if (newPositionsArr.get(i) != -1) {
				oldPos = newPositionsArr.get(i);
			}
			if (oldPos == newPos) {
				continue;
			}
			final Rect oldRect = getRectByPosition(oldPos);
			final Rect newRect = getRectByPosition(newPos);
			oldRect.offset(-v.getLeft(), -v.getTop());
			newRect.offset(-v.getLeft(), -v.getTop());

			TranslateAnimation translate = new TranslateAnimation(
					oldRect.left, newRect.left,
					oldRect.top, newRect.top);
			translate.setDuration(ANIMATION_DURATION);
			DecelerateInterpolator it = new DecelerateInterpolator();
			translate.setInterpolator(it);
			translate.setFillEnabled(true);
			translate.setFillAfter(true);
			v.clearAnimation();
			v.startAnimation(translate);

			newPositionsArr.set(i, newPos);
		}
	}
	
	private void animateDragged() {
		if (mLastDragged >= 0) {
			final View v = getChildAt(mLastDragged);

			final Rect r = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
			r.inset(-r.width() / 20, -r.height() / 20);
			v.measure(MeasureSpec.makeMeasureSpec(r.width(), MeasureSpec.EXACTLY),
					MeasureSpec.makeMeasureSpec(r.height(), MeasureSpec.EXACTLY));
			v.layout(r.left, r.top, r.right, r.bottom);

			AnimationSet animSet = new AnimationSet(true);
			float finalX = 1f;
			float finalY = 1f;
			ScaleAnimation scale = new ScaleAnimation(0.9091f, finalX, 0.9091f, finalY, v.getWidth() / 2, v.getHeight() / 2);
			scale.setDuration(ANIMATION_DURATION);
			AlphaAnimation alpha = new AlphaAnimation(1, .7f);
			alpha.setDuration(ANIMATION_DURATION);

			animSet.addAnimation(scale);
			animSet.addAnimation(alpha);
			animSet.setFillEnabled(true);
			animSet.setFillAfter(true);

			v.clearAnimation();
			v.startAnimation(animSet);
		}
	}
	
	private void rearrange() {
		if (mLastDragged >= 0) {
			for (int i = 0; i < getChildCount(); i++) {
				getChildAt(i).clearAnimation();
			}
			if (mLastTarget >= 0 && mLastDragged != mLastTarget) {
				final View child = getChildAt(mLastDragged);
				removeViewAt(mLastDragged);
				addView(child, mLastTarget);
			}
			mLastDragged = -1;
			mLastTarget = -1;
			requestLayout();
			invalidate();
		}
	}
	
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		handler.removeCallbacksAndMessages(null);
	};
	
	int gap = 0;
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			matrix.reset();
			matrix.setScale(scale, scale);
			gap-=2;
			if(gap<-mGap) {
				gap = 0;
			}
			matrix.postTranslate(gap, 0);
			invalidate();
			Message message = Message.obtain();
			handler.sendMessageDelayed(message, 1000);
		}
	};
	
	Paint paint = new Paint();
	Matrix matrix = new Matrix();
	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawBitmap(background, matrix, paint);
		super.onDraw(canvas);
	}
}
