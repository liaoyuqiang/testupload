package com.yingze.businessframework.component;

import com.yingze.businessframework.component.TemplateListView.BehaviorStateController;
import com.yingze.businessframework.listener.InterceptTouchEventListener;
import com.yingzecorelibrary.utils.LogUtil;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.LinearLayout;

@SuppressLint("ClickableViewAccessibility")
public class TemplateLinearLayout extends LinearLayout implements TemplateContainerCallback{
	private long downtime;
	private ViewConfiguration config;
	private InterceptTouchEventListener mInterceptTouchEventListener;
	private BehaviorStateController mBSController;
	private int mTopEffectiveNumber = 0;// 顶部滑动次数
	private int mBottomEffectiveNumber = 0;// 底部滑动次数
	private final int FLAG_EFFECTIVE_LIMIT = 1;// 滑动临界值
	
	public TemplateLinearLayout(Context context) {
		super(context);
		config = ViewConfiguration.get(context);
		mBSController = new BehaviorStateController();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN) {
			downtime = System.currentTimeMillis();
			mBSController.setGetFoucusState();
			mBSController.setEffectivePoint(event.getRawX(), event.getRawY());
		} else {
			long currentTime = System.currentTimeMillis();
			if(!mBSController.isOverTime(currentTime)) {
				if(mBSController.isInGetFocusState()) {
					mBSController.updateTrackY(event.getRawY(), currentTime);
				}
				if(mBSController.isInGetFocusState()) {
					mBSController.updateTrackX(event.getRawX(), currentTime);
				}
				if(mBSController.isInHorizontalSlideState() &&
						mInterceptTouchEventListener != null) {
					mInterceptTouchEventListener.onInterceptTouchEventHandle(
						true, "intecept_fast_slide_horizontal", event);
					mBSController.setLostFocusState();
					onStateChanged(TemplateContainerCallback.STATE_LOST_FOCUS);
					return false;
				}
				if(mBSController.isInVerticalSlideState()) {
					onStateChanged(TemplateContainerCallback.STATE_LOST_FOCUS);
				}
			} 
		}
		return super.dispatchTouchEvent(event);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		LogUtil.set("template_linearlayout", "------------- mTopEffectiveNumber ：" + mTopEffectiveNumber).warm();
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			break;
		case MotionEvent.ACTION_MOVE:
			/* 
			 * 如果当前操作滑动到顶部，则执行<数据刷新状态>的判断
			 */
            if (getChildAt(0).getTop()>=0) {
            	LogUtil.set("listview", "------------- mTopEffectiveNumber ：" + mTopEffectiveNumber).warm();
				mTopEffectiveNumber+=1;
				if(mTopEffectiveNumber>FLAG_EFFECTIVE_LIMIT) {
					mBSController.setDataRefreshState();
					onStateChanged(TemplateContainerCallback.STATE_LOST_FOCUS);
					mInterceptTouchEventListener.onInterceptTouchEventHandle(true, "refresh", event);
				}
            } else if (getChildAt(getChildCount()-1).getBottom()<=getHeight()) {// 判断滚动到底部
				mBottomEffectiveNumber+=1;
				if(mBottomEffectiveNumber > FLAG_EFFECTIVE_LIMIT) {
					mBSController.setDataLoadState();
					onStateChanged(TemplateContainerCallback.STATE_LOST_FOCUS);
					mInterceptTouchEventListener.onInterceptTouchEventHandle(true, "load", event);
				}
            } else {
            	mBottomEffectiveNumber = 0;
            }
			break;
		}
//		mBSController.setDataRefreshState();
//		onStateChanged(TemplateContainerCallback.STATE_LOST_FOCUS);
//		mInterceptTouchEventListener.onInterceptTouchEventHandle(true, "refresh", event);
		return super.onTouchEvent(event);
	}
	
	@Override
	public void setInterceptTouchEventListener(
			InterceptTouchEventListener mInterceptTouchEventListener) {
		this.mInterceptTouchEventListener = mInterceptTouchEventListener;
	}
	
	@Override
	public void onStateChanged(int state) {
		mBottomEffectiveNumber = 0;
		if(state == TemplateContainerCallback.STATE_GET_FOCUS) {
			mTopEffectiveNumber = 0;
			mBSController.setVerticalSlideState();
		} else {
			cancelLongPress();
			setPressed(false);
//			getSelector().setState(StateSet.NOTHING);
		}
	}
	
	class BehaviorStateController {
		private final int STATE_FOCUS_GET = 0;// 获得焦点
		private final int STATE_FOCUS_LOST = 1;// 失去焦点
		private final int STATE_SLIDE_HORIZONTAL = 2;// 左右滑动
		private final int STATE_SLIDE_VERTICAL = 3;// 上下滑动
		private final int STATE_DATA_REFRESH = 4;// 滑动刷新
		private final int STATE_DATA_LOAD = 5;// 滑动加载
		private int state = STATE_FOCUS_LOST;
		private float effectivePoint[];// 临界坐标
		
		private BehaviorStateController() {
			effectivePoint = new float[2];
		}
		
		public void setGetFoucusState() {
			state = STATE_FOCUS_GET;
		}
		public void setLostFocusState() {
			state = STATE_FOCUS_LOST;
		}
		public void setHorizontalSlideState() {
			state = STATE_SLIDE_HORIZONTAL;
		}
		public void setVerticalSlideState() {
			state = STATE_SLIDE_VERTICAL;
		}
		public void setDataRefreshState() {
			state = STATE_DATA_REFRESH;
		}
		public void setDataLoadState() {
			state = STATE_DATA_LOAD;
		}
		public boolean isInGetFocusState() {
			return state == STATE_FOCUS_GET;
		}
		public boolean isInLostFocusState() {
			return state == STATE_FOCUS_LOST;
		}
		public boolean isInHorizontalSlideState() {
			return state == STATE_SLIDE_HORIZONTAL;
		}
		public boolean isInVerticalSlideState() {
			return state == STATE_SLIDE_VERTICAL;
		}
		public boolean isInDataRefreshState() {
			return state == STATE_DATA_REFRESH;
		}
		public boolean isInDataLoadState() {
			return state == STATE_DATA_LOAD;
		}
		
		public boolean isOverTime(long currentTime) {
			return currentTime-downtime >= ViewConfiguration.getGlobalActionKeyTimeout();
		}
		
		public void setEffectivePoint(float effectivePointX, float effectivePointY) {
			effectivePoint[0] = effectivePointX;
			effectivePoint[1] = effectivePointY;
		}
		
		public void updateTrackX(float x, long currentTime) {
			if(Math.abs(x - effectivePoint[0])>config.getScaledTouchSlop() &&
					currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
				setHorizontalSlideState();
			}
		}
		
		public void updateTrackY(float y, long currentTime) {
			if(Math.abs(y - effectivePoint[1])>config.getScaledTouchSlop() &&
					currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
				setVerticalSlideState();
			}
		}
	}
	
}
