/**
 * 模板容器
 * 用途 ： 下拉刷新，上推加载
 * author LiangZiHao
 */
package com.yingze.businessframework.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import com.yingze.businessframework.R;
import com.yingze.businessframework.listener.InterceptTouchEventListener;
import com.yingze.businessframework.utils.ResourcesUtils;
import com.yingzecorelibrary.utils.LogUtil;

/**
 * @author LittleBird
 */
public class TemplateContainer extends FrameLayout implements InterceptTouchEventListener {
	private boolean isPushEffective = true;
	private boolean isPullEffective = true;
	private int interval = 0;
	private int EDGE_WIDTH = 0;
	private int durationMillis = 600;
	private String mEventInterceptType;// 事件拦截的类型
	private FrameLayout mLeftSub;
	private SubLayout mMiddleSub;
	private FrameLayout mRightSub;
	private BehaviorStateController mBehaviorStateController;
	private AnimationController mAnimationController;
	private TemplateContainerCallback mTemplateContainerCallback;// 动作回调接口
	private InterceptTouchEventListener mInterceptTouchEventListener;// 事件拦截，用于与上层控件交互
	private TemplateContainerListener mTemplateContainerListener;// 刷新、加载监听器

	public TemplateContainer(Context context) {
		super(context);
		init();
	}
	
	public TemplateContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void init() {
		ResourcesUtils it = ResourcesUtils.getInstance(getContext());
		interval = it.getDimensionPixelSizeById(R.dimen.template_head_height);
		EDGE_WIDTH = it.getDimensionPixelSizeById(R.dimen.template_head_height);
		
		mBehaviorStateController = new BehaviorStateController();
		mAnimationController = new AnimationController();
		this.setClickable(true);
		
		mLeftSub = new FrameLayout(getContext());
		mRightSub = new FrameLayout(getContext());
		mMiddleSub = new SubLayout(getContext());
		mMiddleSub.setClickable(false);
		this.addView(mLeftSub);
		this.addView(mRightSub);
		this.addView(mMiddleSub);
		
		mLeftSub.setBackgroundColor(0x2fffff00);
		mRightSub.setBackgroundColor(0x2fff00ff);
	}
	
	/**
	 * 设置容器的内容控件
	 * @param view
	 */
	public void setContentView(View view) {
		mMiddleSub.setContentView(view);
	}
	
	/**
	 * 停止刷新数据
	 */
	public void stopRefreshingData() {
		slideToCenter();
	}
	
	/**
	 * 停止加载数据
	 */
	public void stopLoadingData() {
		slideToCenter();
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		/*
		 * 如果处于动画状态，则不进行事件处理
		 */
		if(mBehaviorStateController.isInAnimationState()) {
			return false;
		}
		/*
		 * 如果当前不是刷新加载状态，则重置当前状态为非事件拦截状态
		 */
		if(event.getAction() == MotionEvent.ACTION_DOWN && 
				!(mBehaviorStateController.isInRefreshDataState() || mBehaviorStateController.isInLoadDataState())) {
			mBehaviorStateController.setNoInterceptTouchEventState();
		}
		/*
		 * 由当前状态决定事件的分发处理
		 */
		if(mBehaviorStateController.isInNoInterceptTouchEventState()) {
			return mTemplateContainerCallback.dispatchTouchEvent(event);
		} else {
			mTemplateContainerCallback.onStateChanged(TemplateContainerCallback.STATE_LOST_FOCUS);
			return onTouchEvent(event);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			/*
			 * 如果处于原来的刷新加载状态，则需要重置状态
			 */
			if(event.getAction() == MotionEvent.ACTION_DOWN && 
					(mBehaviorStateController.isInRefreshDataState() || mBehaviorStateController.isInLoadDataState())) {
				/*
				 * 重置当前滑动刷新的临界值 
				 */
				mBehaviorStateController.updateCriticalPointInRefreshLoadState(event.getRawX(), event.getRawY());
				/*
				 * 初始化回调接口的事件分发 
				 */
				MotionEvent eventClone = MotionEvent.obtain(event);
				float gap = mBehaviorStateController.isInRefreshDataState() ? 
						Math.abs(mBehaviorStateController.middleStartEdge[1]) : 
							-Math.abs(mBehaviorStateController.middleStartEdge[1]);
				eventClone.setLocation(event.getX(), event.getY() - gap);
				mTemplateContainerCallback.dispatchTouchEvent(eventClone);
			} else {
				/* 
				 * 更新当前的点击的坐标
				 */
				mBehaviorStateController.setCriticalPoint(new float[]{event.getRawX(), event.getRawY()});
			}
			break;
		case MotionEvent.ACTION_MOVE:
			/* 
			 * 判断当前的滑动类型，包括下拉刷新 和 上推加载 
			 */
			if(mEventInterceptType.contentEquals("refresh")) {
				/* 
				 * 判断当前滑动是否到达<数据刷新模式>的临界值
				 */
				if(event.getRawY() <= mBehaviorStateController.criticalPoint[1]) {	
					/* 
					 * 如果当前滑动的坐标超过<数据刷新模式>的临界值，则进行状态转换
					 */
					LogUtil.set("refreshbox-touch", "++++++++ 父控件不拦截子项 ：" + mBehaviorStateController.middleStartEdge[1]).info();
					mBehaviorStateController.reset();
					mBehaviorStateController.setNoInterceptTouchEventState();
					mTemplateContainerCallback.onStateChanged(TemplateContainerCallback.STATE_GET_FOCUS);// 正常滑动模式
					mTemplateContainerCallback.dispatchTouchEvent(event);
				} else {
					/*
					 * 滑动
					 */
					mBehaviorStateController.onScroll(event.getRawX(), event.getRawY());
				}
			} else {
				/* 
				 * 判断当前滑动是否到达<数据加载模式>的临界值
				 */
				if(event.getRawY()>=mBehaviorStateController.criticalPoint[1]) {
					/* 
					 * 如果当前滑动的坐标超过限定的临界值，则进行状态转换
					 */
					mBehaviorStateController.reset();
					mBehaviorStateController.setNoInterceptTouchEventState();
					mTemplateContainerCallback.onStateChanged(TemplateContainerCallback.STATE_GET_FOCUS);// 正常滑动模式
					mTemplateContainerCallback.dispatchTouchEvent(event);
				} else {
					/*
					 * 滑动
					 */
					mBehaviorStateController.onScroll(event.getRawX(), event.getRawY());
				}
			}
			/*
			 * 判断下拉头控件是否进行状态变换 
			 */
			if(mBehaviorStateController.isInInterceptTouchEventState()) {
				if(mBehaviorStateController.getMiddleStartEdgeY()>EDGE_WIDTH) {
					mMiddleSub.onPreRefresh();
				} else {
					mMiddleSub.onNormal();
				}
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			mBehaviorStateController.layoutHandle();
			event.setAction(MotionEvent.ACTION_CANCEL);
			mTemplateContainerCallback.dispatchTouchEvent(event);
		case MotionEvent.ACTION_UP:
			mBehaviorStateController.layoutHandle();
			event.setAction(MotionEvent.ACTION_UP);
			mTemplateContainerCallback.dispatchTouchEvent(event);
			break;
		}
		requestLayout();
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		mLeftSub.layout(0, 0, right-left, bottom - top);
		mRightSub.layout(0, 0, right-left, bottom - top);
		mMiddleSub.layout(0, - EDGE_WIDTH + (int)mBehaviorStateController.getMiddleStartEdgeY(), 
				right - left, bottom - top + EDGE_WIDTH + (int)mBehaviorStateController.getMiddleStartEdgeY());
		invalidate();
	}
	
	@Override
	public void onInterceptTouchEventHandle(boolean onInterceptTouchEvent, String type, MotionEvent event) {
		this.mEventInterceptType = type;
		if(type.contentEquals("intecept_fast_slide_horizontal") && mInterceptTouchEventListener!=null) {
			mInterceptTouchEventListener.onInterceptTouchEventHandle(onInterceptTouchEvent, type, event);
		} else {
			if(onInterceptTouchEvent) {
				mBehaviorStateController.setInNoAnimationState();
				mBehaviorStateController.setInterceptTouchEventState();
				mBehaviorStateController.setCriticalPoint(new float[]{event.getRawX(), event.getRawY()});
			} else {
				mBehaviorStateController.setNoInterceptTouchEventState();
			}
		}
	}
	
	/**
	 * 滑动到顶部
	 */
	public void slideToTopArea() {
		if(mBehaviorStateController.isInAnimationState()){
			return;
		}
		mBehaviorStateController.setInAnimationState();
		float timeRatio = Math.abs(((float)-mBehaviorStateController.getMiddleStartEdgeY() + EDGE_WIDTH)/(getHeight()-interval));
		mAnimationController
			.setTrack(0, 0, 0, -mBehaviorStateController.getMiddleStartEdgeY() + EDGE_WIDTH)
			.setDurationMillis((int)(timeRatio * durationMillis))
			.setContentView(mMiddleSub)
			.setAnimationListener(new ViewAnimationListener() {
				@Override
				public void feedback() {
					mMiddleSub.clearAnimation();
					mMiddleSub.onRefresh();
					mBehaviorStateController.setMiddleStartEdgeY(EDGE_WIDTH);
					mBehaviorStateController.setInNoAnimationState();
					mBehaviorStateController.setInRefreshDataState();
					mTemplateContainerListener.onStartRefresh();
					requestLayout();
				}
		}).startAnimation();
	}
	
	/**
	 * 滑动到底部
	 */
	public void slideToBottomArea() {
		if(mBehaviorStateController.isInAnimationState()){
			return;
		}
		mBehaviorStateController.setInAnimationState();
		float timeRatio = Math.abs(((float)-mBehaviorStateController.getMiddleStartEdgeY() + EDGE_WIDTH)/(getHeight()-interval));
		mAnimationController
			.setTrack(0, 0, 0, -mBehaviorStateController.getMiddleStartEdgeY() - EDGE_WIDTH)
			.setDurationMillis((int)(timeRatio * durationMillis))
			.setContentView(mMiddleSub)
			.setAnimationListener(new ViewAnimationListener() {
				@Override
				public void feedback() {
					mMiddleSub.clearAnimation();
					mMiddleSub.onLoad();
					mBehaviorStateController.setMiddleStartEdgeY(-EDGE_WIDTH);
					mBehaviorStateController.setInNoAnimationState();
					mBehaviorStateController.setInLoadDataState();
					mTemplateContainerListener.onStartLoadMore();
					requestLayout();
				}
		}).startAnimation();
	}
	
	/**
	 * 滑动到中间
	 */
	public void slideToCenter() {
		if(mBehaviorStateController.isInAnimationState()) {
			return;
		}
		if(mBehaviorStateController.isInRefreshDataState()) {// 当前处于刷新状态
			mTemplateContainerListener.onStopRefresh();
		} else if(mBehaviorStateController.isInLoadDataState()) {// 当前处于加载状态
			mTemplateContainerListener.onStopLoadMore();
		}
		mBehaviorStateController.setInAnimationState();
		float timeRatio = Math.abs(((float)-mBehaviorStateController.getMiddleStartEdgeY())/(getHeight()-interval));
		int time = (int)(timeRatio * durationMillis*4);
		if(!isPullEffective && mBehaviorStateController.getMiddleStartEdgeY()>0 ||
				!isPushEffective && mBehaviorStateController.getMiddleStartEdgeY()<0) {
			time = 200;
		} 
		mAnimationController
			.setTrack(0, 0, 0, -mBehaviorStateController.getMiddleStartEdgeY())
			.setDurationMillis(time)
			.setContentView(mMiddleSub)
			.setAnimationListener(new ViewAnimationListener() {
				@Override
				public void feedback() {
					mMiddleSub.clearAnimation();
					mMiddleSub.onNormal();
					mBehaviorStateController.setMiddleStartEdgeY(0);
					mBehaviorStateController.setInNoAnimationState();
					mBehaviorStateController.setNoInterceptTouchEventState();
					requestLayout();
				}
		}).startAnimation();
	}
	
	public boolean isPushEffective() {
		return isPushEffective;
	}

	public void setPushEffective(boolean isPushEffective) {
		this.isPushEffective = isPushEffective;
		mMiddleSub.footer.setVisibility(isPushEffective ? View.VISIBLE:View.GONE);	
	}

	public boolean isPullEffective() {
		return isPullEffective;
	}

	public void setPullEffective(boolean isPullEffective) {
		this.isPullEffective = isPullEffective;
		mMiddleSub.header.setVisibility(isPullEffective ? View.VISIBLE:View.GONE);	
	}
	
	/**
	 * 设置templageContainer的回调接口，同时将自身设置到接口的实现类当中
	 * @param mTemplateContainerCallback
	 */
	public void setmTemplateContainerCallback(TemplateContainerCallback mTemplateContainerCallback) {
		this.mTemplateContainerCallback = mTemplateContainerCallback;
		mTemplateContainerCallback.setInterceptTouchEventListener(this);
	}
	
	public void setInterceptTouchEventListener(InterceptTouchEventListener interceptTouchEventListener) {
		mInterceptTouchEventListener = interceptTouchEventListener;
	}
	
	public InterceptTouchEventListener getInterceptTouchEventListener() {
		return mInterceptTouchEventListener;
	}
	
	public TemplateContainerListener getTemplateContainerListener() {
		return mTemplateContainerListener;
	}

	public void setTemplateContainerListener(TemplateContainerListener mTemplateContainerListener) {
		this.mTemplateContainerListener = mTemplateContainerListener;
	}
	
	/**
	 * 主布局
	 * @author littlebird
	 */
	class SubLayout extends FrameLayout {
		private TemplateContainerHeader header;
		private TemplateContainerFooter footer;
		private View contentView;

		public SubLayout(Context context) {
			super(context);
			ResourcesUtils it = ResourcesUtils.getInstance(getContext());
			interval = it.getDimensionPixelSizeById(R.dimen.template_head_height);
			header = new TemplateContainerHeader(context);
			header.setVisiableHeight(EDGE_WIDTH);
			footer = new TemplateContainerFooter(context);
			addView(header);
			addView(footer);
			onNormal();
		}
		
		public void setContentView(View view) {
			contentView = view;
			this.addView(contentView);
		}
		
		public void onNormal() {
			header.setState(TemplateContainerHeader.STATE_NORMAL);
			footer.normal();
		}
		
		public void onPreRefresh() {
			header.setState(TemplateContainerHeader.STATE_READY);
		}
		
		public void onRefresh() {
			header.setState(TemplateContainerHeader.STATE_REFRESHING);
		}
		
		public void onLoad() {
			footer.loading();
		}
		
		@Override
		protected void onLayout(boolean changed, int left, int top, int right,
				int bottom) {
			header.layout(0, 0, right-left, EDGE_WIDTH);
			footer.layout(0, bottom-top-EDGE_WIDTH, right-left, bottom-top);
			if(contentView!=null){
				contentView.layout(0,EDGE_WIDTH, right-left, bottom-top-EDGE_WIDTH);
			}
		}
	}
	
	/**
	 * 行为状态控制
	 * @author littlebird
	 */
	class BehaviorStateController {
		private boolean isInAnimationState = false;// 是否处于动画效果状态
		private final int STATE_NO_INTERCEPT_TOUCH_EVENT = 0;//非拦截模式
		private final int STATE_INTERCEPT_TOUCH_EVENT = -1;// 拦截模式
		private final int STATE_DATA_REFRESH = 1;// 拦截模式下的数据刷新模式
		private final int STATE_DATA_LOAD = 2;// 拦截模式下的数据加载模式
		private int state = STATE_NO_INTERCEPT_TOUCH_EVENT;
		private float middleStartEdge[];// 主布局的方位起始坐标
		private float criticalPoint[];// 父控件与子控件获取事件分发的临界坐标
		
		private BehaviorStateController() {
			middleStartEdge = new float[2];
			criticalPoint = new float[2];
		}
		
		/**
		 * 不拦截当前事件 
		 */
		public void setNoInterceptTouchEventState() {
			state = STATE_NO_INTERCEPT_TOUCH_EVENT;
		}
		
		/**
		 * 拦截当前事件 
		 */
		public void setInterceptTouchEventState() {
			state = STATE_INTERCEPT_TOUCH_EVENT;
		}
		
		/**
		 * 动画状态
		 */
		public void setInAnimationState() {
			isInAnimationState = true;
		}
		
		/**
		 * 不在动画状态
		 */
		public void setInNoAnimationState() {
			isInAnimationState = false;
		}
		
		/**
		 * 刷新数据状态
		 */
		public void setInRefreshDataState() {
			state = STATE_DATA_REFRESH;
		}
		
		/**
		 * 加载数据状态
		 */
		public void setInLoadDataState() {
			state = STATE_DATA_LOAD;
		}
		
		/**
		 * 是否 不拦截触摸事件状态
		 * @return
		 */
		public boolean isInNoInterceptTouchEventState() {
			return state == STATE_NO_INTERCEPT_TOUCH_EVENT;
		}
		
		/**
		 * 是否 拦截触摸事件状态
		 * @return
		 */
		public boolean isInInterceptTouchEventState() {
			return state == STATE_INTERCEPT_TOUCH_EVENT;
		}
		
		/**
		 * 是否 动画状态
		 * @return
		 */
		public boolean isInAnimationState() {
			return isInAnimationState;
		}
		
		/**
		 * 是否刷新数据状态
		 * @return
		 */
		public boolean isInRefreshDataState() {
			return state == STATE_DATA_REFRESH;
		}
		
		/**
		 * 是否数据加载状态
		 * @return
		 */
		public boolean isInLoadDataState() {
			return state == STATE_DATA_LOAD;
		}
		
		/**
		 * 设置事件分发的临界坐标
		 * @param criticalPoint
		 */
		public void setCriticalPoint(float[] criticalPoint) {
			this.criticalPoint = criticalPoint;
		}
		
		/**
		 * 根据当前状态，更新临界坐标
		 * @param criticalPointX
		 * @param cricitalPointY
		 */
		public void updateCriticalPointInRefreshLoadState(float criticalPointX, float cricitalPointY) {
			criticalPoint[0] = criticalPointX;
			if(middleStartEdge[1]>=0) {
				criticalPoint[1] = cricitalPointY - Math.abs(middleStartEdge[1]);
			} else {
				criticalPoint[1] = cricitalPointY + Math.abs(middleStartEdge[1]);
			}
		}
		
		/**
		 * 滑动
		 * @param x
		 * @param y
		 */
		public void onScroll(float x, float y) {
			middleStartEdge[1] = y-criticalPoint[1];
		}
		
		/**
		 * 布局处理
		 */
		public void layoutHandle() {
			if(middleStartEdge[1]>EDGE_WIDTH && isPushEffective) {
				slideToTopArea();
			} else if(middleStartEdge[1]<-EDGE_WIDTH && isPullEffective) {
				slideToBottomArea();
			} else {
				slideToCenter();
			}
			requestLayout();
		}

		/**
		 * 重置布局的参考距离
		 */
		public void reset() {
			middleStartEdge[0] = 0;
			middleStartEdge[1] = 0;
		}
		
		public float[] getMiddleStartEdge() {
			return middleStartEdge;
		}

		public void setMiddleStartEdge(float[] mMiddleStartEdge) {
			this.middleStartEdge = mMiddleStartEdge;
		}
		
		public float getMiddleStartEdgeX() {
			return middleStartEdge[0];
		}
		
		public void setMiddleStartEdgeX(int middleStartEdgeX) {
			middleStartEdge[0] = middleStartEdgeX;
		}
		
		public float getMiddleStartEdgeY() {
			return middleStartEdge[1]; 
		}
		
		public void setMiddleStartEdgeY(int middleStartEdgeY) {
			middleStartEdge[1] = middleStartEdgeY;
		}
		
	}
	
	/**
	 * 动画控制类
	 * @author littlebird
	 */
	class AnimationController {
		private int durationMillis = 600;
		private float fromXDelta;
		private float toXDelta;
		private float fromYDelta;
		private float toYDelta;
		private View contentView;
		private ViewAnimationListener viewAnimationListener;
		
		public AnimationController setTrack(float fromXDelta, float toXDelta,
				float fromYDelta, float toYDelta) {
			this.fromXDelta = fromXDelta;
			this.toXDelta = toXDelta;
			this.fromYDelta = fromYDelta;
			this.toYDelta = toYDelta;
			return this;
		}
		
		public AnimationController setAnimationListener(ViewAnimationListener viewAnimationListener) {
			this.viewAnimationListener = viewAnimationListener;
			return this;
		}
		
		public AnimationController setContentView(View contentView) {
			this.contentView = contentView;
			return this;
		}
		
		public AnimationController setDurationMillis(int durationMillis) {
			this.durationMillis = durationMillis;
			return this;
		}
		
		public void startAnimation() {
			TranslateAnimation animation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta, toYDelta);
			animation.setDuration(durationMillis);
			animation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationEnd(Animation animation) {
					if(viewAnimationListener != null) {
						viewAnimationListener.feedback();
					}
				}
				@Override
				public void onAnimationRepeat(Animation animation) {}
				@Override
				public void onAnimationStart(Animation animation) {}
			});
			contentView.startAnimation(animation);
		}
		
	}
	
	public static interface ViewAnimationListener {
		public void feedback();
	}
	
	public static interface TemplateContainerCallback {
		public final int STATE_GET_FOCUS = 1;
		public final int STATE_LOST_FOCUS = 2;
		public boolean dispatchTouchEvent(MotionEvent event);
		public void onStateChanged(int state);
		public void setInterceptTouchEventListener(InterceptTouchEventListener interceptTouchEventListener);
	}
	
	public static interface TemplateContainerListener {
		public void onStartRefresh();
		public void onStopRefresh();
		public void onStartLoadMore();
		public void onStopLoadMore();
	}
}