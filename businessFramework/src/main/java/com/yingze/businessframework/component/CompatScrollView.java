/**
 * 滑动滚动控件
 */
package com.yingze.businessframework.component;

import com.yingze.businessframework.R;
import com.yingze.businessframework.controller.ScrollerController;
import com.yingzecorelibrary.view.RotateableView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class CompatScrollView extends ScrollView {
	private int STATE_DEFAULT = 0;
	private int STATE_PREPARE_REFRESH = 1;
	private int STATE_READY_REFRESH = 2;
	private int STATE_REFRESH = 3;
	private int STATE_PREPARE_LOAD = 4;
	private int STATE_READY_LOAD = 5;
	private int STATE_LOAD = 6;
	
	private final float bound = 160;
	private float startY_pull;
	private float startY_load;
	private int topIndex = 0;
	private int bottomIndex = 0;
	private int gap = 0;
	private int mScaledTouchSlop = 0;
	private long downtime;
	private int mAnimationDuration =400;
	
	private boolean isInterceptEffectived = false;
	private boolean isPullEffectived = false;
	private boolean isLoadEffectived = false;
	
	private LinearLayout rooter;
	private ViewHeader header;
	private ViewFooter footer;
	private ViewBody body;
	private ViewGap viewGap;
	
	private StateController mStateController_pull = new StateController();
	private StateController mStateController_load = new StateController();
	private LoadController mLoadController = new LoadController();
	private ScrollCallback mScrollCallback;
	private Handler handler = new Handler();
	
	private BehaviorStateController mBSController;
	private ViewConfiguration config;
	
	
	 private ScrollerController mScrollerController;
	
	
	public CompatScrollView(Context context) {
		super(context);
		init();
	}

	public CompatScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void setScrollCallback(ScrollCallback scrollCallback) {
		mScrollCallback = scrollCallback;
	}
	
	public void init() {
		mScrollerController = new ScrollerController(getContext(), this);  
		config = ViewConfiguration.get(getContext());
		mScaledTouchSlop = config.getScaledTouchSlop();
		setOverScrollMode(View.OVER_SCROLL_NEVER);
		mBSController = new BehaviorStateController();
		
		/*
		 * 添加scrollview 的唯一子布局 
		 */
		rooter = new LinearLayout(getContext());
		rooter.setOrientation(LinearLayout.VERTICAL);
		rooter.setClickable(true);
		addView(rooter);
		
		/*
		 * 头控件
		 */
		header = new ViewHeader(getContext());
		LinearLayout.LayoutParams paramsHeader = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 0);
		rooter.addView(header, paramsHeader);
		
		/*
		 * 动态注入内容控件
		 */
		body = new ViewBody(getContext());
		body.setOrientation(LinearLayout.VERTICAL);
		rooter.addView(body);
		
		/*
		 * 空白位置控件
		 */
		viewGap = new ViewGap(getContext());
		LinearLayout.LayoutParams paramsGap = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 0);
		rooter.addView(viewGap, paramsGap);
		
		/*
		 * 底部控件
		 */
		footer = new ViewFooter(getContext());
		LinearLayout.LayoutParams paramsFooter = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 0);
		rooter.addView(footer, paramsFooter);
		
		header.updateDrawState();
		footer.updateDrawState();
	}

	public void setContent(View contentLayout) {
		body.addView(contentLayout);
	}
	
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		Log.e("onScroll-change", l+","+ t +","+ oldl+","+ oldt);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if (!mScrollerController.isScrolling()) {
			int bodyHeight = body.getHeight();
	        int rooterheight = rooter.getHeight();
	        int parentHeight = getHeight();
	        Log.e("scroll-pull-view", "body height:"+rooterheight+", parentHeight:"+parentHeight);
	        Log.e("scroll-pull-view", String.format("body height:%d, rooter height:%d, parentHeight:%d", 
	        		bodyHeight, rooterheight, parentHeight));

	        if (bodyHeight < parentHeight+2) {
	        	if (rooterheight != parentHeight+2) {
	    			/* 补全高度 */
	    			LinearLayout.LayoutParams  layoutParams = (LinearLayout.LayoutParams) viewGap.getLayoutParams();
	    			layoutParams.height = parentHeight - bodyHeight + 2;
	    			viewGap.setLayoutParams(layoutParams);
	    			setVerticalScrollBarEnabled(false);
	    			requestLayout();
	        	} else {
	        		return ;
	        	}
			} else {
				/* 去掉补全值，恢复原来的高度 */
				LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) viewGap.getLayoutParams();
				if (layoutParams.height!=0) {
					layoutParams.height = 0;
					viewGap.setLayoutParams(layoutParams);
					setVerticalScrollBarEnabled(false);
				} else {
					setVerticalScrollBarEnabled(true);
				}
			}
		}
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		if (mScrollerController.isScrolling()) {
			return super.dispatchTouchEvent(event);
		}
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			mRawXYController.isEffective_Refresh = false;
			mRawXYController.isEffective_Load = false;
			isTouchEffectived = true;
			downtime = System.currentTimeMillis();
			mBSController.setGetFoucusState();
			mBSController.setEffectivePoint(event.getRawX(), event.getRawY());
		} else {
			long currentTime = System.currentTimeMillis();
			if (!mBSController.isOverTime(currentTime)) {
				if (mBSController.isInGetFocusState()) {
					mBSController.updateTrackY(event.getRawY(), currentTime);
				}
				if (mBSController.isInGetFocusState()) {
					mBSController.updateTrackX(event.getRawX(), currentTime);
				}
				if (mBSController.isInVerticalSlideState()) {
					isInterceptEffectived = true;
					Log.e("快速滑动处理", "符合快速滑动处理的原则");
				}
			} 
		}
		if (event.getAction() == MotionEvent.ACTION_UP || 
				event.getAction() == MotionEvent.ACTION_CANCEL) {
			isInterceptEffectived = false;
			Log.e("快速滑动处理", "符合快速滑动处理的原则-----被取消了!!!");
		}
		
		return super.dispatchTouchEvent(event);
	}
	
	class RawXYController {
		public float yInRefresh;
		public float yInLoad;
		public boolean isEffective_Refresh = false;
		public boolean isEffective_Load = false;
	}
	
	RawXYController mRawXYController = new RawXYController();
	boolean isTouchEffectived = true;
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (mScrollerController.isScrolling()) {
			return true;
		}
		if (!isTouchEffectived)return true;
		
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			break;
		case MotionEvent.ACTION_MOVE:
			int scrollY = getScrollY();
			int height = getHeight();
			int scrollViewMeasuredHeight = getChildAt(0).getMeasuredHeight();
			int childHeight = getChildAt(0).getHeight();
			System.out.println("scroll is loading!!!");
			if (mStateController_pull.isInRefreshState() || mStateController_load.isInLoadState()) {
				/* 如果当前滑动控件处于数据刷新状态或着数据加载状态，则进行处理 */
				String str = "scrollY:"+scrollY+",height:"+height+",scrollViewMeasuredHeight:"+scrollViewMeasuredHeight+",childHeight:"+childHeight;
				if (mStateController_pull.isInRefreshState()) {	
					if (scrollY == 0) {
					Log.e("scroll-loading", "111:^^^^^^^^^^^^^^^^^^ scrollY:"+scrollY+",height:"+height+",scrollViewMeasuredHeight:"+scrollViewMeasuredHeight);	

						if (!mRawXYController.isEffective_Refresh) {
							mRawXYController.isEffective_Refresh = true;
							mRawXYController.yInRefresh = ev.getRawY();
						} else {
							if (ev.getRawY() < mRawXYController.yInRefresh) {
								header.updateParams((int)bound);
//								mRawXYController.isEffective_Refresh = false;
								requestLayout();
								return super.onTouchEvent(ev);
							} else {	
								header.updateParams((int)(bound + ev.getRawY() - mRawXYController.yInRefresh));
								requestLayout();
								return super.onTouchEvent(ev);
							}
						}

					}
					
					Log.e("scroll-loading", "222:"+str);
					if (footer.getLayoutParams().height==0) {
						if ((scrollY + height) == scrollViewMeasuredHeight ||
								height>=scrollViewMeasuredHeight) {
							Log.e("scroll-loading", "333:^^^^^^^^^^^^^^^^^^ ev.getRawY():"+ev.getRawY()+",mRawXYController.yInLoad:"+ mRawXYController.yInLoad);
	
							if (!mRawXYController.isEffective_Load) {
								mRawXYController.isEffective_Load = true;
								mRawXYController.yInLoad = ev.getRawY();
							} else {
								if (ev.getRawY()>mRawXYController.yInLoad) {
									footer.updateParams(0);
								} else {
									Log.e("scroll-loading", "4444:"+Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
									footer.updateParams((int)Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
								}
								
							}
							requestLayout();
							return super.onTouchEvent(ev);
						} else {
							
						}
					} else {
						if (ev.getRawY()>mRawXYController.yInLoad) {
							footer.updateParams(0);
							mRawXYController.isEffective_Load = false;
						} else {
							Log.e("scroll-loading", "4444:"+Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
							footer.updateParams((int)Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
						}
					}
				}
					
				// 处于加载模式
				if (mStateController_load.isInLoadState()) {
					if (scrollY == 0) {
					Log.e("scroll-loading", "111:^^^^^^^^^^^^^^^^^^ scrollY:"+scrollY+",height:"+height+",scrollViewMeasuredHeight:"+scrollViewMeasuredHeight);	

						if (!mRawXYController.isEffective_Refresh) {
							mRawXYController.isEffective_Refresh = true;
							mRawXYController.yInRefresh = ev.getRawY();
						} else {
							if (ev.getRawY() < mRawXYController.yInRefresh) {
								header.updateParams(0);
//								mRawXYController.isEffective_Refresh = false;
								requestLayout();
								return super.onTouchEvent(ev);
							} else {	
								header.updateParams((int)(ev.getRawY() - mRawXYController.yInRefresh));
								requestLayout();
								return super.onTouchEvent(ev);
							}
						}

					}
					
					if (footer.getLayoutParams().height==bound) {
						if ((scrollY + height) == scrollViewMeasuredHeight ||
								height>=scrollViewMeasuredHeight) {
							Log.e("scroll-loading", "333:^^^^^^^^^^^^^^^^^^ ev.getRawY():"+ev.getRawY()+",mRawXYController.yInLoad:"+ mRawXYController.yInLoad);
	
							if (!mRawXYController.isEffective_Load) {
								mRawXYController.isEffective_Load = true;
								mRawXYController.yInLoad = ev.getRawY();
							} else {
								if (ev.getRawY()>mRawXYController.yInLoad) {
									footer.updateParams((int)bound);
								} else {
									Log.e("scroll-loading", "4444:"+Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
									footer.updateParams((int)bound + (int)Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
								}
								
							}
							requestLayout();
							return super.onTouchEvent(ev);
						} else {
							
						}
					} else {
						if (ev.getRawY()>mRawXYController.yInLoad) {
							footer.updateParams((int)bound);
							mRawXYController.isEffective_Load = false;
						} else {
							Log.e("scroll-loading", "4444:"+Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
							footer.updateParams((int)bound + (int)Math.abs((ev.getRawY() - mRawXYController.yInLoad)));
						}
					}
					
				}
				
				requestLayout();
				return super.onTouchEvent(ev);
			} else {
				if (!mStateController_pull.isInRefreshState()) {
					if(!(height>=scrollViewMeasuredHeight && !mStateController_load.isInDefaultState())){
						if (scrollY == 0) {
							System.out.println("滑动到了顶部 scrollY=" + scrollY+","+(topIndex++));
							if (mStateController_pull.isInDefaultState()) {
								System.out.println("pull1");
								mStateController_pull.setState(STATE_PREPARE_REFRESH);
								startY_pull = ev.getRawY();
							} else {
								if (ev.getRawY() <= startY_pull) {
									System.out.println("pull2");
									header.updateParams(0);
									mStateController_pull.setState(STATE_DEFAULT);
								} else {
									if(ev.getRawY()-startY_pull > bound) {
										System.out.println("pull3");
										mStateController_pull.setState(STATE_READY_REFRESH);
										header.setState(ViewHeader.STATE_READY);
									} else {
										System.out.println("pull4");
										mStateController_pull.setState(STATE_PREPARE_REFRESH);
										header.setState(ViewHeader.STATE_NORMAL);
									}
									header.updateParams((int)(ev.getRawY() - startY_pull));
									requestLayout();
									Log.e("head", "+++++++++++++++ header height : "+header.getHeight()+","+
											ev.getRawY()+","+startY_pull+","+header.getLayoutParams().height);
									return true;
								}
							}
						} else {
							System.out.println("pull5");
							header.updateParams(0);
							header.setState(ViewHeader.STATE_NORMAL);
							if (!mStateController_pull.isInPrepareLoadState() &&
									!mStateController_pull.isInReadyLoadState() &&
									!mStateController_pull.isInLoadState()) {
								mStateController_pull.setState(STATE_DEFAULT);
							}
						}
					}
				}
				
				if (!mStateController_load.isInLoadState()) {
					if ((scrollY + height) == scrollViewMeasuredHeight || mLoadController.isLoad || height>=scrollViewMeasuredHeight) {
						if (mStateController_load.isInDefaultState()) {
							System.out.println("load1");
							mStateController_load.setState(STATE_PREPARE_LOAD);
							startY_load = ev.getRawY();
							mLoadController.isLoad = true;
						} else {
							if (ev.getRawY() >= startY_load) {
								System.out.println("load2");
								footer.updateParams(0);
								mStateController_load.setState(STATE_DEFAULT);
								mLoadController.isLoad = false;
							} else {
								System.out.println("滑动到了底部,高度=" + Math.abs((int)(ev.getRawY() - startY_load))+","+gap);
								if(ev.getRawY()-startY_load < -bound) {
									System.out.println("load3");
									mStateController_load.setState(STATE_READY_LOAD);
									footer.setState(ViewFooter.STATE_READY);
								} else {
									System.out.println("load4");
									mStateController_load.setState(STATE_PREPARE_LOAD);
									footer.setState(ViewFooter.STATE_NORMAL);
								}
								footer.updateParams(Math.abs((int)(ev.getRawY() - startY_load)));
							}
						}
					} else {
						System.out.println("load5");
						System.out.println("滑动到了底部,被重置了"+(scrollY+height)+","+scrollViewMeasuredHeight);
						footer.updateParams(0);
						footer.setState(ViewFooter.STATE_NORMAL);
						if (!mStateController_load.isInPrepareRefreshState() &&
								!mStateController_load.isInReadyRefreshState() &&
								!mStateController_load.isInRefreshState()) {
							mStateController_load.setState(STATE_DEFAULT);
						}
					}
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			if (mStateController_pull.isInRefreshState() || mStateController_load.isInLoadState()) {
				if(mStateController_pull.isInRefreshState()) {
					if (header.getLayoutParams().height>bound) {
						final int headerHeight = header.getLayoutParams().height;
						mScrollerController.setScrollerControllerCallback(
								new ScrollerController.ScrollerControllerCallback(){
									@Override
									public void scrolling(int currentX, int currentY) {
										header.updateParams(headerHeight - currentY);
										scrollTo(0, 0);
									}
									@Override
									public void scrollFinish() {
										header.updateParams((int)bound);
										scrollTo(0, 0);
										printMessage("1,"+headerHeight);
									}
						});
						mScrollerController.smoothScrollTo(0, 0, 0, headerHeight-(int)bound);
					} else if (footer.getLayoutParams().height>0) {
						final int footerHeight = footer.getLayoutParams().height;
						mScrollerController.setScrollerControllerCallback(
								new ScrollerController.ScrollerControllerCallback(){
									@Override
									public void scrolling(int currentX, int currentY) {
										footer.updateParams(footerHeight - currentY);
									}
									@Override
									public void scrollFinish() {
										footer.updateParams(0);
										printMessage("3,"+footerHeight);
									}
						});
						mScrollerController.smoothScrollTo(0, 0, 0, footerHeight);
					}
				}
				if (mStateController_load.isInLoadState()) {
					final int footerHeight = footer.getLayoutParams().height;
					if (footerHeight > bound) {
						mScrollerController.setScrollerControllerCallback(
								new ScrollerController.ScrollerControllerCallback(){
									@Override
									public void scrolling(int currentX, int currentY) {
										footer.updateParams(footerHeight - currentY);
									}
									@Override
									public void scrollFinish() {
										footer.updateParams((int)bound);
										printMessage("3,"+footerHeight);
									}
						});
						mScrollerController.smoothScrollTo(0, 0, 0, footerHeight-(int)bound);
					} else if(header.getLayoutParams().height>0) {
						final int headerHeight = header.getLayoutParams().height;
						mScrollerController.setScrollerControllerCallback(
								new ScrollerController.ScrollerControllerCallback(){
									@Override
									public void scrolling(int currentX, int currentY) {
										header.updateParams(headerHeight - currentY);
										scrollTo(0, 0);
									}
									@Override
									public void scrollFinish() {
										header.updateParams(0);
										scrollTo(0, 0);
										printMessage("1,"+headerHeight);
									}
						});
						mScrollerController.smoothScrollTo(0, 0, 0, headerHeight);
					}
				}
			}
			
			if (mStateController_pull.isInPrepareRefreshState() || (!isPullEffectived() && header.getHeight()>0)) {
				mStateController_pull.setState(STATE_DEFAULT);
				header.setState(ViewHeader.STATE_NORMAL);
				final int headerHeight = header.getLayoutParams().height;
				mScrollerController.setScrollerControllerCallback(
						new ScrollerController.ScrollerControllerCallback(){
							@Override
							public void scrolling(int currentX, int currentY) {
								header.updateParams(headerHeight - currentY);
								scrollTo(0, 0);
							}
							@Override
							public void scrollFinish() {
								header.updateParams(0);
								scrollTo(0, 0);
								printMessage("1,"+headerHeight);
							}
				});
				mScrollerController.smoothScrollTo(0, 0, 0, headerHeight);
			} else if (mStateController_pull.isInReadyRefreshState()) {
				mStateController_pull.setState(STATE_REFRESH);
				final int headerHeight = header.getLayoutParams().height;
				mScrollerController.setScrollerControllerCallback(
						new ScrollerController.ScrollerControllerCallback(){
							@Override
							public void scrolling(int currentX, int currentY) {
								header.updateParams(headerHeight - currentY);
								scrollTo(0, 0);
							}
							@Override
							public void scrollFinish() {
								header.updateParams((int)bound);
								scrollTo(0, 0);
								mScrollCallback.onStartRefresh();
								printMessage("2,"+headerHeight);
							}
				});
				mScrollerController.smoothScrollTo(0, 0, 0, headerHeight-(int)bound);
				header.setState(ViewHeader.STATE_REFRESHING);
			}
			
			if (header.getLayoutParams().height==0) {
				if(mStateController_load.isInPrepareLoadState() || !isLoadEffectived()) {
					mStateController_load.setState(STATE_DEFAULT);
					footer.setState(ViewFooter.STATE_NORMAL);
					final int footerHeight = footer.getLayoutParams().height;
					if (footerHeight > 0) {
						mScrollerController.setScrollerControllerCallback(
								new ScrollerController.ScrollerControllerCallback(){
									@Override
									public void scrolling(int currentX, int currentY) {
										footer.updateParams(footerHeight - currentY);
									}
									@Override
									public void scrollFinish() {
										footer.updateParams(0);
										printMessage("3,"+footerHeight);
									}
						});
						mScrollerController.smoothScrollTo(0, 0, 0, footerHeight);
					}
				} else if (mStateController_load.isInReadyLoadState()) {
					mStateController_load.setState(STATE_LOAD);
					final int footerHeight = footer.getLayoutParams().height;
					mScrollerController.setScrollerControllerCallback(
							new ScrollerController.ScrollerControllerCallback(){
								@Override
								public void scrolling(int currentX, int currentY) {
									footer.updateParams(footerHeight - currentY);
								}
								@Override
								public void scrollFinish() {
									footer.updateParams((int)bound);
									mScrollCallback.onStartLoad();
									printMessage("4,"+footerHeight);
								}
					});
					mScrollerController.smoothScrollTo(0, 0, 0, footerHeight-(int)bound);
					footer.setState(ViewFooter.STATE_LOADING);
				}
			}
			break;
		default:
			break;
		}
		requestLayout();
		return super.onTouchEvent(ev);
	}
	
	public void printMessage(Object str) {
//		Toast.makeText(getContext(), String.valueOf(str), Toast.LENGTH_SHORT).show();
	}
	
	@Override
    public void computeScroll() {  
		mScrollerController.computeScroll();
		super.computeScroll();
    }

	public void stopRefresh() {
		final int headerHeight = header.getHeight();
//		if (headerHeight == 0) return;
		isTouchEffectived = false;
		if (this.getScrollY()==0) { 
			mScrollerController.setScrollerControllerCallback(
					new ScrollerController.ScrollerControllerCallback(){
						@Override
						public void scrolling(int currentX, int currentY) {
							header.updateParams(headerHeight - currentY);
							scrollTo(0, 0);
						}
						@Override
						public void scrollFinish() {
							header.updateParams(0);
							scrollTo(0, 0);
							mScrollCallback.onStopRefresh();
							mStateController_pull.setState(STATE_DEFAULT);
						}
			});
			mScrollerController.smoothScrollTo(0, 0, 0, headerHeight);
		} else {
			header.updateParams(0);
			scrollTo(0, 0);
			mScrollCallback.onStopRefresh();
			mStateController_pull.setState(STATE_DEFAULT);
		}
	}
	
	public void stopLoad() {
//		final int footerHeight = footer.getHeight();
////		if (footerHeight == 0) return;
//		isTouchEffectived = false;
//		
//		int scrollY = getScrollY();
//		int height = getHeight();
//		int scrollViewMeasuredHeight = getChildAt(0).getMeasuredHeight();
//		
//		if ((scrollY + height) >= scrollViewMeasuredHeight) {
//			mScrollerController.setScrollerControllerCallback(
//					new ScrollerController.ScrollerControllerCallback() {
//						@Override
//						public void scrolling(int currentX, int currentY) {
//							footer.updateParams(footerHeight - currentY);
//						}
//						@Override
//						public void scrollFinish() {
//							Log.e("cs", "++++++++++++++++++++ 滚动结束");
//							footer.updateParams(0);
//							mStateController_load.setState(STATE_DEFAULT);
//							mScrollCallback.onStopLoad();
//						}
//			});
//			mScrollerController.smoothScrollTo(0, 0, 0, footerHeight);
//		} else {
//			Log.e("cs", "++++++++++++++++++++ 滚动结束");
//			footer.updateParams(0);
//			scrollTo(0, scrollViewMeasuredHeight - height);
//			mStateController_load.setState(STATE_DEFAULT);
//			mScrollCallback.onStopLoad();
//		}
		stopLoadImmediately();
	}
	
	public void stopLoadImmediately() {
		Log.e("cs", "++++++++++++++++++++ 滚动结束");
		footer.updateParams(0);
		mStateController_load.setState(STATE_DEFAULT);
		mScrollCallback.onStopLoad();
	}
	
	public boolean isPullEffectived() {
		return isPullEffectived;
	}

	public void setPullEffectived(boolean isPullEffectived) {
		this.isPullEffectived = isPullEffectived;
		header.updateDrawState();
		footer.updateDrawState();
	}

	public boolean isLoadEffectived() {
		return isLoadEffectived;
	}

	public void setLoadEffectived(boolean isLoadEffectived) {
		this.isLoadEffectived = isLoadEffectived;
		header.updateDrawState();
		footer.updateDrawState();
	}

	class ViewHeader extends LinearLayout {
		int modeHeight = 150;
		LinearLayout mContainer;
		TextView text;
		
		private ImageView mArrowImageView;
		private ProgressBar mProgressBar;
		private TextView mHintTextView;
		private RotateableView rv_header_refresh;
		private int mState = STATE_NORMAL;

		private Animation mRotateUpAnim;
		private Animation mRotateDownAnim;
		
		private final int ROTATE_ANIM_DURATION = 180;
		
		public final static int STATE_NORMAL = 0;
		public final static int STATE_READY = 1;
		public final static int STATE_REFRESHING = 2;
		
		public ViewHeader(Context context) {
			super(context);
			this.setGravity(Gravity.BOTTOM);
			initView(context);
		}
		
		private void initView(Context context) {
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, modeHeight);
			mContainer = (LinearLayout) LayoutInflater.from(context).inflate(
					R.layout.appframework_scroll_header, null);
			addView(mContainer, lp);
			setGravity(Gravity.BOTTOM);

			mArrowImageView = (ImageView)mContainer.findViewById(R.id.xlistview_header_arrow);
			mHintTextView = (TextView)mContainer.findViewById(R.id.xlistview_header_hint_textview);
			rv_header_refresh = (RotateableView) mContainer.findViewById(R.id.rv_header_refresh);
			
			mRotateUpAnim = new RotateAnimation(0.0f, -180.0f,
					Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			mRotateUpAnim.setDuration(ROTATE_ANIM_DURATION);
			mRotateUpAnim.setFillAfter(true);
			mRotateDownAnim = new RotateAnimation(-180.0f, 0.0f,
					Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			mRotateDownAnim.setDuration(ROTATE_ANIM_DURATION);
			mRotateDownAnim.setFillAfter(true);
		}
		
		public void setState(int state) {
			if (state == mState) return ;
			
			if (state == STATE_REFRESHING) {
				mArrowImageView.clearAnimation();
				mArrowImageView.setVisibility(View.INVISIBLE);
				rv_header_refresh.setVisibility(View.VISIBLE);
				rv_header_refresh.startAnimation();
			} else {
				mArrowImageView.setVisibility(View.VISIBLE);
				rv_header_refresh.setVisibility(View.INVISIBLE);
				rv_header_refresh.stopAnimation();
			}
			
			switch(state){
			case STATE_NORMAL:
				if (mState == STATE_READY) {
					mArrowImageView.startAnimation(mRotateDownAnim);
				}
				if (mState == STATE_REFRESHING) {
					mArrowImageView.clearAnimation();
				}
				mHintTextView.setText(R.string.xlistview_header_hint_normal);
				break;
			case STATE_READY:
				if (mState != STATE_READY) {
					mArrowImageView.clearAnimation();
					mArrowImageView.startAnimation(mRotateUpAnim);
					mHintTextView.setText(R.string.xlistview_header_hint_ready);
				}
				break;
			case STATE_REFRESHING:
				mHintTextView.setText(R.string.xlistview_header_hint_loading);
				break;
				default:
			}
			
			mState = state;
		}
		
		public void updateParams(int gap) {
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
			if (params == null) {
				params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,gap);
				setLayoutParams(params);
			} else {
				if (params.height != gap) {
					params.height = gap;
					setLayoutParams(params);
				}
			}
//			this.setLayoutParams(params);
		}
		
		public void updateDrawState() {
			if (!isPullEffectived()) {
				mContainer.setVisibility(View.GONE);
			} else {
				mContainer.setVisibility(View.VISIBLE);
			}
		}
		
	}
	
	class ViewFooter extends LinearLayout {
		public final static int STATE_NORMAL = 0;
		public final static int STATE_READY = 1;
		public final static int STATE_LOADING = 2;

		private int modeHeight = 150;
		private Context mContext;
		private View mContentView;
		private RotateableView rv_footer_loading;
		private TextView mHintView;
		private LinearLayout mContainer;
		
		public ViewFooter(Context context) {
			super(context);
			initView(context);
		}
		
		private void initView(Context context) {
			mContext = context;
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, modeHeight);
			mContainer = (LinearLayout)LayoutInflater.from(mContext).inflate(
					R.layout.appframework_scroll_footer, null);
			addView(mContainer,lp);

			mContentView = mContainer.findViewById(R.id.xlistview_footer_content);
			rv_footer_loading = (RotateableView) mContainer.findViewById(R.id.rv_footer_loading); 
			mHintView = (TextView)mContainer.findViewById(R.id.xlistview_footer_hint_textview);
		}
		
		public void setState(int state) {
			mHintView.setVisibility(View.INVISIBLE);
			mHintView.setVisibility(View.INVISIBLE);
			if (state == STATE_READY) {
				mHintView.setVisibility(View.VISIBLE);
				mHintView.setText(R.string.xlistview_footer_hint_ready);
			} else if (state == STATE_LOADING) {
				rv_footer_loading.setVisibility(View.VISIBLE);
				rv_footer_loading.startAnimation();
			} else {
				mHintView.setVisibility(View.VISIBLE);
				mHintView.setText(R.string.xlistview_footer_hint_normal);
				rv_footer_loading.setVisibility(View.INVISIBLE);
				rv_footer_loading.stopAnimation();
			}
		}
		
		public void updateParams(int gap) {
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
			if (params == null) {
				params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,gap);
				setLayoutParams(params);
			} else {
				if (params.height != gap) {
					params.height = gap;
					setLayoutParams(params);
				}
			}
//			this.setLayoutParams(params);
		}
		
		public void updateDrawState() {
			if (!isLoadEffectived()) {
				mContainer.setVisibility(View.GONE);
			} else {
				mContainer.setVisibility(View.VISIBLE);
			}
		}
	}
	
	class ViewBody extends LinearLayout {
		public ViewBody(Context context) {
			super(context);
		}
	}
	
	class ViewGap extends View {
	public ViewGap(Context context) {
		super(context);
	}
}
	
	class StateController {
		private int mState;
		
		public void setState(int state) {
			mState = state;
		}
		public boolean isInDefaultState() {
			return mState == STATE_DEFAULT;
		}
		public boolean isInPrepareRefreshState() {
			return mState == STATE_PREPARE_REFRESH;
		}
		public boolean isInReadyRefreshState() {
			return mState == STATE_READY_REFRESH;
		}
		public boolean isInRefreshState() {
			return mState == STATE_REFRESH;
		}
		public boolean isInPrepareLoadState() {
			return mState == STATE_PREPARE_LOAD;
		}
		public boolean isInReadyLoadState() {
			return mState == STATE_READY_LOAD;
		}
		public boolean isInLoadState() {
			return mState == STATE_LOAD;
		}
	}
	
	class BehaviorStateController {
		private final int STATE_FOCUS_GET = 0;// 获得焦点
		private final int STATE_FOCUS_LOST = 1;// 失去焦点
		private final int STATE_SLIDE_HORIZONTAL = 2;// 左右滑动
		private final int STATE_SLIDE_VERTICAL = 3;// 上下滑动
		private final int STATE_DATA_REFRESH = 4;// 滑动刷新
		private final int STATE_DATA_LOAD = 5;// 滑动加载
		private int state = STATE_FOCUS_LOST;
		private float effectivePoint[];// 临界坐标
		
		private BehaviorStateController() {
			effectivePoint = new float[2];
		}
		
		public void setGetFoucusState() {
			state = STATE_FOCUS_GET;
		}
		public void setLostFocusState() {
			state = STATE_FOCUS_LOST;
		}
		public void setHorizontalSlideState() {
			state = STATE_SLIDE_HORIZONTAL;
		}
		public void setVerticalSlideState() {
			state = STATE_SLIDE_VERTICAL;
		}
		public void setDataRefreshState() {
			state = STATE_DATA_REFRESH;
		}
		public void setDataLoadState() {
			state = STATE_DATA_LOAD;
		}
		public boolean isInGetFocusState() {
			return state == STATE_FOCUS_GET;
		}
		public boolean isInLostFocusState() {
			return state == STATE_FOCUS_LOST;
		}
		public boolean isInHorizontalSlideState() {
			return state == STATE_SLIDE_HORIZONTAL;
		}
		public boolean isInVerticalSlideState() {
			return state == STATE_SLIDE_VERTICAL;
		}
		public boolean isInDataRefreshState() {
			return state == STATE_DATA_REFRESH;
		}
		public boolean isInDataLoadState() {
			return state == STATE_DATA_LOAD;
		}
		
		public boolean isOverTime(long currentTime) {
			return currentTime-downtime >= ViewConfiguration.getGlobalActionKeyTimeout();
		}
		
		public void setEffectivePoint(float effectivePointX, float effectivePointY) {
			effectivePoint[0] = effectivePointX;
			effectivePoint[1] = effectivePointY;
		}
		
		public void updateTrackX(float x, long currentTime) {
			if(Math.abs(x - effectivePoint[0])>mScaledTouchSlop &&
					currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
				setHorizontalSlideState();
			}
		}
		
		public void updateTrackY(float y, long currentTime) {
			if(Math.abs(y - effectivePoint[1])>mScaledTouchSlop &&
					currentTime-downtime<ViewConfiguration.getGlobalActionKeyTimeout()) {
				setVerticalSlideState();
			}
		}
	}
	
	class LoadController {
		boolean isLoad = false;
	}
	
	public static interface ScrollCallback {
		public void onStartRefresh();
		public void onStopRefresh();
		public void onStartLoad();
		public void onStopLoad();
	}

}
