package com.yingze.businessframework.component;

import com.yingze.businessframework.R;
import com.yingze.businessframework.component.TemplateContainer.TemplateContainerCallback;

import android.app.Activity;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

public class SideActivity extends Activity{

	protected TemplateSlideBox layout;
	protected TemplateContainer mRLBox;
	
	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏 
		init2();
//		initDragBox();
//		registerBroadcastReceiver();
	};
	
	public void init() {
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		this.setContentView(mainLayout);
		
		LinearLayout titleLayout = new LinearLayout(this);
		Button button = new Button(this);
		button.setText("右滑");
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout.slideToRight();
			}
		});
		Button button1 = new Button(this);
		button1.setText("居中");
		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout.slideToCenter();
			}
		});
		Button button2 = new Button(this);
		button2.setText("左滑");
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				layout.slideToLeft();
			}
		});
		titleLayout.addView(button);
		titleLayout.addView(button1);
		titleLayout.addView(button2);
		mainLayout.addView(titleLayout);
		layout = new TemplateSlideBox(this);
		layout.test();
		mainLayout.addView(layout,new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
	}
	
	public void init2() {
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		this.setContentView(mainLayout);
		
		LinearLayout titleLayout = new LinearLayout(this);
		Button button = new Button(this);
		button.setText("右滑");
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				mRLBox.toBottom();
			}
		});
		Button button1 = new Button(this);
		button1.setText("居中");
		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mRLBox.slideToCenter();
			}
		});
		Button button2 = new Button(this);
		button2.setText("左滑");
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				mRLBox.toTop();
			}
		});
		titleLayout.addView(button);
		titleLayout.addView(button1);
		titleLayout.addView(button2);
		mainLayout.addView(titleLayout);
		
		/*
		 * 添加下拉刷新控件 
		 */
		mRLBox = new TemplateContainer(this);
		mainLayout.addView(mRLBox,new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));

		LayoutInflater inflater = LayoutInflater.from(this);
		View page = inflater.inflate(R.layout.template_main_page, null);
		mRLBox.setContentView(page);
		
		page.findViewById(R.id.lv_main_page_crossbar).setVisibility(View.GONE);
		
		TemplateContainerCallback mTemplateContainerCallback = (TemplateContainerCallback)page.findViewById(R.id.lv_main_page);
		BaseAdapter adapter = DataController.getInstance().getAdapter(this);
		((ListView)mTemplateContainerCallback).setAdapter(adapter);
		
		mRLBox.setmTemplateContainerCallback(mTemplateContainerCallback);
	}
	
	public void initDragBox() {
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		this.setContentView(mainLayout);
		
		DragBox box= new DragBox(this);
		mainLayout.addView(box,new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		box.test();
	}
	
//	CustomBroadcastReceiver mReceiver;
//	public void registerBroadcastReceiver() {
//		mReceiver = new CustomBroadcastReceiver(); 
//		IntentFilter filter = new IntentFilter();
//		filter.addAction("broadcast_action");
//		this.registerReceiver(mReceiver, filter);
//	}
//	
//	@Override
//	protected void onDestroy() {
//		this.unregisterReceiver(mReceiver);
//		super.onDestroy();
//	}
	
}
