package com.yingze.businessframework.component;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.WindowManager;

public class CustomDrawerLayout extends DrawerLayout {
	int width = 0;
	int height = 0;
    public CustomDrawerLayout(Context context) {
        super(context);
        getSize();
    }

    public CustomDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        getSize();
    }

    public CustomDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getSize();
    }

	public void getSize() {
		WindowManager wm = ((Activity) getContext()).getWindowManager();
		width = wm.getDefaultDisplay().getWidth();
		height = wm.getDefaultDisplay().getHeight();
	}
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(
                MeasureSpec.getSize(width), MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(
                MeasureSpec.getSize(height), MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

}
