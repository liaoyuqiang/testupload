/**
 * 滑动控件
 * @author LiangZH
 */
package com.yingze.businessframework.component;

import com.yingze.businessframework.R;
import com.yingze.businessframework.controller.AppEventStateController;
import com.yingze.businessframework.controller.ScrollerController;
import com.yingze.businessframework.listener.OnScrollLayoutControllerActionCallback;
import com.yingze.businessframework.utils.InterpolatorUtils;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

//@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class ScrollLayout extends FrameLayout implements OnScrollLayoutControllerActionCallback {
	public static int EVENT_STATE_KEEP_CURRENT_ACTIVITY = 0;
	public static int EVENT_STATE_RETURN_LAST_ACTIVITY = 1;
	private final float VALUE_MAX_X_VELOCITY = 500;
	private boolean isViewEffectived = true;

	private int width;
	private int height;
	private int mCurrentScrollOffsetHorizontal = 0;//当前滑动距离
	private int mLastScrollOffsetHorizontal = 0;//上一次的滑动距离
	private int mLayoutOffsetHorizontal;//当前布局的偏移距离
	private int mOnStopActivityScrollOffsetHorizontal = 0;//处于onStop的activity的滑动偏移
	private int mAnimationDuration = 360;//动画的持续时间
	private int mOffsetHorizontalMultiple=3;//偏移倍数
	private int mEdgeWidth = 40;// 渐变的边缘宽度
	private float mXVelocity = 0; // 水平轴的滑动速度
	private float mTouchEventDownX;//触摸事件的down事件的坐标
	
	private View edgeView;// 边缘控件
	private ScrollLayer mScrollLayer;// 底部组织布局
	private ScrollContentLayout mScrollContentLayout;
	private OnScrollLayoutCallback mOnScrollLayoutCallback = OnScrollLayoutCallback.NULL;
	
	private ScrollerController mScrollerController;
	private TrackerController mTrackerController = new TrackerController();
	private ScrollEffectivedController mScrollEffectivedController = new ScrollEffectivedController();
	private TouchLockController mTouchLockController = new TouchLockController();
	
	private AppEventStateController mAppEventStateController;
	private OnActivityStopController mOnActivityStopController = new OnActivityStopController();
	private OnActivityRestartController mOnActivityRestartController = new OnActivityRestartController();
	private OnActivityCreateController mOnActivityCreateController = new OnActivityCreateController();
	private OnActivityFinishController mOnActivityFinishController = new OnActivityFinishController();
	private OnBackActivityResetController mOnBackActivityResetController = new OnBackActivityResetController();
	private OnFrontActivityResetController mOnFrontActivityResetController = new OnFrontActivityResetController();

	public ScrollLayout(Context context) {
		super(context);
		mAppEventStateController = new AppEventStateController(ViewConfiguration.get(context));
		initLayoutOffsetHorizontal();
		this.setBackgroundColor(Color.TRANSPARENT);
		this.setClickable(true);
		this.setLongClickable(true);
		initViewContent();
		mScrollerController = new ScrollerController(getContext(), this, 
				InterpolatorUtils.newDecelerateInterpolator());
	}

	/**
	 * 初始化滑动距离
	 */
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void initLayoutOffsetHorizontal() {
		int sdk = android.os.Build.VERSION.SDK_INT; 
		if(sdk>=android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {    
			WindowManager wm = ((Activity) getContext()).getWindowManager();
			Point outSize = new Point();
			wm.getDefaultDisplay().getSize(outSize);
			width = outSize.x;
			height = outSize.y;
		} else {
			WindowManager wm = ((Activity) getContext()).getWindowManager();
			width = wm.getDefaultDisplay().getWidth();
			height = wm.getDefaultDisplay().getHeight();
		}

		mLayoutOffsetHorizontal = width/mOffsetHorizontalMultiple + mEdgeWidth;
		Log.e("scroll-layout", "scroll-layout size is : " + String.format(
				"%d%s%d", width, ",", height));
	}
	
	/**
	 * 初始化子内容
	 */
	public void initViewContent() {
		mScrollLayer = new ScrollLayer(getContext());
		edgeView = new View(getContext());
		edgeView.setBackgroundResource(R.drawable.shade_gradient_left);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mEdgeWidth, LayoutParams.MATCH_PARENT);
		mScrollLayer.addView(edgeView, params);
		
		mScrollContentLayout = new ScrollContentLayout(getContext());
		mScrollLayer.addView(mScrollContentLayout, new LayoutParams(width, LayoutParams.MATCH_PARENT));
		
		LayoutParams params_layer = new LayoutParams(width + mEdgeWidth, LayoutParams.MATCH_PARENT);
		params_layer.leftMargin = -mEdgeWidth;
		this.addView(mScrollLayer, params_layer);
	}

	@Override
	public void draw(Canvas canvas) {
		if (!isViewEffectived)return;
		super.draw(canvas);
	}
	
	@Override
	public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
		if (mScrollEffectivedController.isScrollEffectived()) {
			if (disallowIntercept) {
				mAppEventStateController.setInterceptTouchEventState();
			} else {
				mAppEventStateController.setGetFoucusState();
			}
		}
		super.requestDisallowInterceptTouchEvent(disallowIntercept);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if(mScrollEffectivedController.isScrollEffectived() && 
				mAppEventStateController.isInHorizontalSlideState()) {
			return true;
		}
		return super.onInterceptTouchEvent(ev);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		if (mTouchLockController.isTouchLocked()) {
			return true;
		}
		if (mScrollEffectivedController.isScrollEffectived()) {
			if (!mAppEventStateController.isInInterceptTouchEventState()) {
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
					mTrackerController.initTracker();
					mTouchEventDownX = event.getX();
					mAppEventStateController.setDownTime(System.currentTimeMillis());
					mAppEventStateController.setGetFoucusState();
					mAppEventStateController.setEffectivePoint(event.getRawX(), event.getRawY());
				} else {
					mTrackerController.addMovement(event);
					long currentTime = System.currentTimeMillis();
					if(!mAppEventStateController.isOverTime(currentTime)) {
						if(mAppEventStateController.isInGetFocusState()) {
							mAppEventStateController.updateTrackX(event.getRawX(), currentTime);
						}
						if(mAppEventStateController.isInGetFocusState()) {
							mAppEventStateController.updateTrackY(event.getRawY(), currentTime);
						}
		
						if(mAppEventStateController.isInHorizontalSlideState()) {
						} else if (mAppEventStateController.isInVerticalSlideState()) {
						}
					}
				}
			}
			if(event.getAction() == MotionEvent.ACTION_UP ||
					event.getAction() == MotionEvent.ACTION_CANCEL) {
				mAppEventStateController.setLostFocusState();
			}
		}
		return super.dispatchTouchEvent(event);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mTouchLockController.isTouchLocked()) {
			return true;
		}
		
		if (mScrollEffectivedController.isScrollEffectived()) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_MOVE:
				int dragDistance = mLastScrollOffsetHorizontal + (int) (event.getX() - mTouchEventDownX);
				if (0<=dragDistance && dragDistance<=width) {
					mCurrentScrollOffsetHorizontal = dragDistance;
				} else if (dragDistance<-mEdgeWidth) {
					mCurrentScrollOffsetHorizontal = 0;
				} else if (dragDistance>width) {
					mCurrentScrollOffsetHorizontal = width;
				}
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				mLastScrollOffsetHorizontal = mCurrentScrollOffsetHorizontal;  
				mTrackerController.computeCurrentVelocity(1000);
				mXVelocity = mTrackerController.getXVelocity();
				if (mTrackerController.getXVelocity() > VALUE_MAX_X_VELOCITY) {
					mOnScrollLayoutCallback.onEventActionState(EVENT_STATE_RETURN_LAST_ACTIVITY);
					return true;
				}
				break;
			}
			setCurrentScrollOffsetHorizontal(mCurrentScrollOffsetHorizontal);
			mOnStopActivityScrollOffsetHorizontal = mCurrentScrollOffsetHorizontal/mOffsetHorizontalMultiple;
			mOnScrollLayoutCallback.onScroll(mOnStopActivityScrollOffsetHorizontal);
			if (event.getAction() == MotionEvent.ACTION_UP) {
				if (mCurrentScrollOffsetHorizontal < width/2) {
					mOnScrollLayoutCallback.onEventActionState(EVENT_STATE_KEEP_CURRENT_ACTIVITY);
				} else {
					mOnScrollLayoutCallback.onEventActionState(EVENT_STATE_RETURN_LAST_ACTIVITY);
				}
			}
		} 
		
		requestLayout();
		return super.onTouchEvent(event);
	}
	
	@Override
	public void injectOnScrollLayoutControllerActionCallback(Object itself) {
		mOnScrollLayoutCallback = (OnScrollLayoutCallback) itself;
	}
	
	@Override
	public void onStopHandle() {
		mOnActivityStopController.handleEntryAnimation(this);
	}
	
	@Override
	public void onRestartHandle() {
		mOnActivityRestartController.handleExitAnimation(this);
	}
	
	@Override
	public void onCreateHandle() {
		mOnActivityCreateController.handleStartAnimation(this);
	}
	
	@Override
	public void onFinishHandle() {
		mOnActivityFinishController.handleColseAnimation(this);
	}
	
	@Override
	public void onRecoverHandle(int activityState) {
		if (activityState == OnScrollLayoutControllerActionCallback.STATE_ACTIVITY_BACKGROUND) {
			mOnBackActivityResetController.handleActivityResetAnimation(this);
		} else if (activityState == OnScrollLayoutControllerActionCallback.STATE_ACTIVITY_FOREGROUND) {
			mOnFrontActivityResetController.handleActivityResetAnimation(mScrollLayer);
		}
	}
	
	@Override
	public int getCurrentScrollOffsetHorizontal() {
		return mCurrentScrollOffsetHorizontal;
	}
	
	@Override
	public void setCurrentScrollOffsetHorizontal(final int currentScrollOffsetHorizontal) {
		mCurrentScrollOffsetHorizontal = currentScrollOffsetHorizontal;
		FrameLayout.LayoutParams params = (LayoutParams) mScrollLayer.getLayoutParams();
		if (params.leftMargin != mCurrentScrollOffsetHorizontal) {
			params.leftMargin = mCurrentScrollOffsetHorizontal;
			mScrollLayer.setLayoutParams(params);
		}
	}
	
	@Override
	public void resetCurrentScrollOffsetHorizontal() {
		FrameLayout.LayoutParams params = (LayoutParams) mScrollLayer.getLayoutParams();
		params.leftMargin = -mEdgeWidth;
		mScrollLayer.setLayoutParams(params);
	}
	
	@Override
	public int getLayoutOffsetHorizontal() {
		return mLayoutOffsetHorizontal;
	}
	
	@Override
	public void setLayoutOffsetHorizontal(int offset) {
		mLayoutOffsetHorizontal = offset;
	}
	
	@Override
	public void setScrollEffectived(boolean isScrollEffectived) {
		mScrollEffectivedController.setScrollEffective(isScrollEffectived);
	}
	
	@Override
    public void computeScroll() {  
		mScrollerController.computeScroll();
		super.computeScroll();
    } 
	
	public ScrollContentLayout getScrollContentLayout() {
		return mScrollContentLayout;
	}

	public class OnActivityStopController {
		public void handleEntryAnimation(View view) {	
		}
	}
	
	public class OnActivityRestartController {
		public void handleExitAnimation(View view) {
		}
	}
	
	public class OnActivityCreateController {
		public void handleStartAnimation(View view) {
			mScrollerController.setScrollerControllerCallback(new ScrollerController.ScrollerControllerCallback(){
				@Override
				public void scrolling(int currentX, int currentY) {
					setCurrentScrollOffsetHorizontal(currentX);
					float onStopActivityScrollOffsetHorizontal = -(((float)width)/(width+mEdgeWidth))*(width-currentX)/mOffsetHorizontalMultiple;
					Log.e("scroll", "@@@@@@@@@@@@@ "+onStopActivityScrollOffsetHorizontal);
					mOnScrollLayoutCallback.setLastScrollLayoutGap(-mEdgeWidth + (int)onStopActivityScrollOffsetHorizontal);
				}
				@Override
				public void scrollFinish() {
					setCurrentScrollOffsetHorizontal(-mEdgeWidth);
					mOnScrollLayoutCallback.setLastScrollLayoutGap(-mEdgeWidth - (width)/mOffsetHorizontalMultiple);
					mTouchLockController.setTouchLocked(false);
				}
			});
			mTouchLockController.setTouchLocked(true);
			mScrollerController.setDuration(mAnimationDuration);
			setCurrentScrollOffsetHorizontal(width);
			mScrollerController.smoothScrollTo(width, 0, -mEdgeWidth, 0);
		}
	}
	
	public class OnActivityFinishController {
		public void handleColseAnimation(View view) {
			mScrollerController.setScrollerControllerCallback(new ScrollerController.ScrollerControllerCallback(){
				@Override
				public void scrolling(int currentX, int currentY) {
					setCurrentScrollOffsetHorizontal(currentX);
					float onStopActivityScrollOffsetHorizontal = -(((float)width)/(width+mEdgeWidth))*(width-currentX)/mOffsetHorizontalMultiple;
					Log.e("scroll", "@@@@@@@@@@@@@ "+onStopActivityScrollOffsetHorizontal);
					mOnScrollLayoutCallback.setLastScrollLayoutGap(-mEdgeWidth + (int)onStopActivityScrollOffsetHorizontal);
					if(currentX>=width) {
						mScrollerController.interrupt();
					}
				}
				@Override
				public void scrollFinish() {
					setCurrentScrollOffsetHorizontal(width);
					mOnScrollLayoutCallback.setLastScrollLayoutGap(-mEdgeWidth + 0);
					mOnScrollLayoutCallback.onScrollLayoutFinish();
					mTrackerController.recycle();
					mTouchLockController.setTouchLocked(false);
				}
			});
			mTouchLockController.setTouchLocked(true);
			mScrollerController.setDuration(mAnimationDuration);
			setCurrentScrollOffsetHorizontal(getCurrentScrollOffsetHorizontal());
			mScrollerController.smoothScrollTo(getCurrentScrollOffsetHorizontal(), 0, width+20, 0);
//			if (mXVelocity>VALUE_MAX_X_VELOCITY) {
//				mScrollerController.fling(getCurrentScrollOffsetHorizontal(), 0, 
//						(int)mXVelocity, 0, (width+20)/10, width+20, 0, 0);
//			} else {
//				mScrollerController.smoothScrollTo(getCurrentScrollOffsetHorizontal(), 0, width+20, 0);
//			}
		}
	}
	
	public class OnBackActivityResetController {
		public void handleActivityResetAnimation(View view) {
		}
	}
	
	public class OnFrontActivityResetController {
		public void handleActivityResetAnimation(View view) {
			mScrollerController.setScrollerControllerCallback(new ScrollerController.ScrollerControllerCallback(){
				@Override
				public void scrolling(int currentX, int currentY) {
					setCurrentScrollOffsetHorizontal(currentX);
					float onStopActivityScrollOffsetHorizontal = -(width-currentX)/mOffsetHorizontalMultiple;
					Log.e("scroll", "@@@@@@@@@@@@@ "+onStopActivityScrollOffsetHorizontal);
					mOnScrollLayoutCallback.setLastScrollLayoutGap(-mEdgeWidth + (int)onStopActivityScrollOffsetHorizontal);
				}
				@Override
				public void scrollFinish() {
					mLastScrollOffsetHorizontal = 0;
					setCurrentScrollOffsetHorizontal(-mEdgeWidth);
					mOnScrollLayoutCallback.setLastScrollLayoutGap(-mEdgeWidth - (width)/mOffsetHorizontalMultiple);
					mTouchLockController.setTouchLocked(false);
				}
			});
			mTouchLockController.setTouchLocked(true);
			mScrollerController.setDuration(mAnimationDuration);
			setCurrentScrollOffsetHorizontal(getCurrentScrollOffsetHorizontal());
			mScrollerController.smoothScrollTo(getCurrentScrollOffsetHorizontal(), 0, -mEdgeWidth, 0);
		}
	}

	public class TrackerController {
		VelocityTracker vTracker = VelocityTracker.obtain();
		
		public void initTracker() {   
			vTracker.clear();    
		}
		public void recycle() {
			if (vTracker != null) {
				vTracker.recycle();
			}
		}
		public void addMovement(MotionEvent event) {
			vTracker.addMovement(event);
		}
		public void computeCurrentVelocity(int gap) {
			vTracker.computeCurrentVelocity(gap);
		}
		public float getXVelocity() {
			return vTracker.getXVelocity();
		}
		public float getYVelocity() {
			return vTracker.getYVelocity();
		}
	}
	
	public class ScrollEffectivedController {
		boolean isScrollEffectived = true;
		public void setScrollEffective(boolean isScrollEffectived) {
			this.isScrollEffectived = isScrollEffectived;
		}
		public boolean isScrollEffectived() {
			return isScrollEffectived;
		}
	}
	
	public class TouchLockController {
		boolean isTouchLocked = false;
		public void setTouchLocked(boolean isTouchLocked) {
			this.isTouchLocked = isTouchLocked; 
		}
		public boolean isTouchLocked() {
			return isTouchLocked;
		}
	}
	
	public static class ScrollLayer extends LinearLayout {
		public ScrollLayer(Context context) {
			super(context);
			this.setOrientation(LinearLayout.HORIZONTAL);
		}
	}
	
	public static class ScrollContentLayout extends FrameLayout {
		public ScrollContentLayout(Context context) {
			super(context);
		}
	}
	
	public static interface OnScrollLayoutCallback {
		public void onScroll(int gap);
		public void onEventActionState(int actionType);
		public void onScrollLayoutFinish();
		public void setLastScrollLayoutGap(int gap);
		
		public static OnScrollLayoutCallback NULL = new OnScrollLayoutCallback() {
			@Override
			public void onScroll(int gap) {
			}
			@Override
			public void onEventActionState(int actionType) {
			}
			@Override
			public void onScrollLayoutFinish() {
			}
			@Override
			public void setLastScrollLayoutGap(int gap) {
			}
		};
	}

}
