package com.yingze.businessframework.component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yingze.businessframework.R;
import com.yingzecorelibrary.utils.LogUtil;

public class DataController {

	private static DataController it;
	
	public synchronized static DataController getInstance() {
		if(it == null) {
			it = new DataController();
		}
		return it;
	}
	
	public BaseAdapter getAdapter(Context context) {
		List<PictureInfo> list = new ArrayList<PictureInfo>();
		for(int i=0;i<15;i++) {
			PictureInfo info = new PictureInfo();
			list.add(info);
		}
		MyAdapter adapter = new MyAdapter(context, list);
		return adapter;
	}
	
	class MyAdapter extends BaseAdapter{
		private LayoutInflater inflater;
		private Context mContext = null; 
		List<PictureInfo> infoList=null;
		
		public MyAdapter(Context context,List<PictureInfo> list) {
			this.infoList=list;
			mContext=context;
			inflater=LayoutInflater.from(mContext);
		}
		
		public void update(List<PictureInfo> infoList) {
			this.infoList = infoList;
			this.notifyDataSetChanged();
		}
		
		@Override
		public int getItemViewType(int position) {
			return position==0 ? 0:1;
		}
		
		@Override
		public int getViewTypeCount() {
			// TODO Auto-generated method stub
			return 2;
		}
		
		@Override
		public int getCount() {
			return infoList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			VCursor vCursor;
			PictureInfo areaInfo=infoList.get(position);
			if(getItemViewType(position)==1) {
				if(convertView == null){
					convertView = inflater.inflate(R.layout.item_main_page, null);
					vCursor = new VCursor();
					convertView.setTag(vCursor);
				} else {
					vCursor = (VCursor) convertView.getTag();
				}
			} else {
				if(convertView == null){
					convertView = inflater.inflate(R.layout.item_main_page_picture, null);
					vCursor = new VCursor();
					convertView.setTag(vCursor);
				}
			}
			return convertView;
		}
		
		class VCursor{
			 public TextView departmentName;  
		}
	}

//	listbox -> listview -> AbsListView -> AdapterView -> ViewGroup
	public void invoke(Object instance, MotionEvent event){
	    try {
	    	instance.getClass().getDeclaredClasses();
		    Class<?> targetClass = instance.getClass()
		    		.getSuperclass().getSuperclass().getSuperclass();
//	    	Class<?> targetClass = Class.forName("android.view.ViewGroup");
		    
	    	Method[] ms = targetClass.getMethods();
//	    	resetTouchState
//		    ViewGroup superInst = (ViewGroup)targetClass.cast(instance);
		    Method method_onTouch = targetClass.getMethod("onTouchEvent", MotionEvent.class);
		    Method method_onTouch2 = targetClass.getDeclaredMethod("dispatchTransformedGenericPointerEvent", MotionEvent.class, View.class);
		   
			Method method_resetTouchState = targetClass.getDeclaredMethod("resetTouchState");
			Method method_cancelAndClearTouchTargets = targetClass.getDeclaredMethod("cancelAndClearTouchTargets", new Class[]{MotionEvent.class});
			
			LogUtil.set("method_resetTouchState", method_resetTouchState.getName()).error();
			LogUtil.set("method_cancelAndClearTouchTargets", method_cancelAndClearTouchTargets.getName()).error();
			
//			method_resetTouchState.invoke(superInst);
//			method_cancelAndClearTouchTargets.invoke(superInst, event);
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
