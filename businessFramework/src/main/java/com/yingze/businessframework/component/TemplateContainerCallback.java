package com.yingze.businessframework.component;

import android.view.MotionEvent;

import com.yingze.businessframework.listener.InterceptTouchEventListener;

public interface TemplateContainerCallback {
	public final int STATE_GET_FOCUS = 1;
	public final int STATE_LOST_FOCUS = 2;
	public boolean dispatchTouchEvent(MotionEvent event);
	public void onStateChanged(int state);
	public void setInterceptTouchEventListener(InterceptTouchEventListener interceptTouchEventListener);

}
