package com.yingze.businessframework.component;

import com.yingze.businessframework.controller.ZoomController;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

@SuppressLint("Recycle")
public class ZoomContainer extends FrameLayout {
	private ZoomController mZoomContainer = new ZoomController();
	
	public ZoomContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
		mZoomContainer.initClassConstructor(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		mZoomContainer.handleSizeOnMeasure(widthMeasureSpec, heightMeasureSpec);
		super.onMeasure(mZoomContainer.getWidthMeasureSpec(), mZoomContainer.getHeightMeasureSpec());
	}
	
}
