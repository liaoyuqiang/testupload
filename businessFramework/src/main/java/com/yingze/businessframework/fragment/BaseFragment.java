/**
 * 顶层Fragment框架，一般用于第三方功能嵌套或定义一些执行步骤方法供调用
 */
package com.yingze.businessframework.fragment;

import com.umeng.analytics.MobclickAgent;
import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {
	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(getActivity());
	}
	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(getActivity());
	}
}
