package com.yingze.businessframework.fragment;

import com.yingze.businessframework.controller.AppActivityController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class AppFragment extends BaseFragment implements ViewController {
	protected View mContentView;
	protected AppActivityController mActivityController = AppActivityController.NULL;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = LayoutInflater.from(getActivity()).inflate(getLayoutResID(), null);
		return mContentView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mActivityController = AppActivityController.newInstance(this, mContentView);
		mActivityController.initOnCreate();
	}
	
	@Override
	public void onDestroy() {
		cleanController();
		super.onDestroy();
	}
	
	/**
	 * 动态添加控制器
	 * @param controller
	 */
	public void addController(ViewController controller) {
		mActivityController.getControllerArr().add(controller);
	}
	
	/**
	 * 清空控制器
	 */
	public void cleanController() {
		mActivityController.getControllerArr().clear();
	}
	
	/**
	 * 返回标题控制器
	 * @return
	 */
	public TitleController getTitleController() {
		return mActivityController.getTitleController();
	}
}