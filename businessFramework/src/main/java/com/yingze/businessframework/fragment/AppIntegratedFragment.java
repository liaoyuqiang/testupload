/**
 * 集成了标题控制、标题栏下内容布局控制、网络加载、弹出进度框控制的综合界面
 */
package com.yingze.businessframework.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.yingze.businessframework.R;
import com.yingze.businessframework.component.CompatScrollView;
import com.yingze.businessframework.controller.AppActivityController;
import com.yingze.businessframework.controller.AppIntegratedActivityController;
import com.yingze.businessframework.controller.AppNetworkController;
import com.yingze.businessframework.controller.LayoutInflaterController;
import com.yingze.businessframework.controller.LoadingController;
import com.yingze.businessframework.controller.SubLayoutController;
import com.yingze.businessframework.controller.TitleController;
import com.yingze.businessframework.controller.ViewController;
import com.yingze.businessframework.controller.AppNetworkController.NetworkCallback;
import com.yingze.businessframework.listener.ViewScrollCallback;

public abstract class AppIntegratedFragment extends BaseFragment implements ViewController, AppNetworkController.DialogCallback{
	protected View mContentView;
	protected AppIntegratedActivityController mActivityController = AppIntegratedActivityController.NULL;
	protected AppNetworkController mAppNetworkController;
	protected LayoutInflaterController mLayoutInflaterController;
	protected LoadingController mLoadingController = new LoadingController();// 数据加载控制器

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		 * 生成根布局控件
		 */
		mLayoutInflaterController = new LayoutInflaterController(getActivity());
		mContentView = mLayoutInflaterController.inflateRootLayout(R.layout.template_app_activity_facade);
		/*
		 * 将子布局动态绑定到根布局中 
		 */
		View subLayout = mLayoutInflaterController.findViewById(mContentView, R.id.fl_sub_layout);
		View subContent = mLayoutInflaterController.inflateRootLayout(getLayoutResID());
		mLayoutInflaterController.addSubLayoutChild(getActivity(), (FrameLayout)subLayout, subContent, getViewScrollCallback());
//		mLayoutInflaterController.addSubLayoutChild((FrameLayout)subLayout, subContent);
		mAppNetworkController = new AppNetworkController(getActivity());
		return mContentView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mActivityController = AppIntegratedActivityController.newInstance(this, mContentView);
		mActivityController.initOnCreate();
	}
	
	@Override
	public ViewGroup getRootLayout() {
		return (ViewGroup) mContentView.findViewById(R.id.fl_sub_layout);
	}
	
	@Override
	public void onDestroy() {
		cleanController();
		super.onDestroy();
	}
	
	/**
	 * 钩子方法，用于网络请求
	 */
	public void doNetworkAction(NetworkCallback networkCallback) {
		mAppNetworkController.initLoadingController(mLoadingController);
		mAppNetworkController.initDialogCallback(this);
		mAppNetworkController.initHttpRequestCallback(networkCallback);
		mAppNetworkController.initHttpResponseCallback(networkCallback);
		mAppNetworkController.openConnect();
	}
	
	/**
	 * 动态添加控制器
	 * @param controller
	 */
	public void addController(ViewController controller) {
		mActivityController.getControllerArr().add(controller);
	}
	
	/**
	 * 清空控制器
	 */
	public void cleanController() {
		mActivityController.getControllerArr().clear();
	}
	
	/**
	 * 返回标题控制器
	 * @return
	 */
	public TitleController getTitleController() {
		return mActivityController.getTitleController();
	}
	
	/**
	 * 返回页面内容布局控制器
	 * @return
	 */
	public SubLayoutController getSubLayoutController() {
		return mActivityController.getSubLayoutController();
	}
	
	/**
	 * 是否需要滑动刷新
	 * @return
	 */
	public ViewScrollCallback getViewScrollCallback() {
		return new ViewScrollCallback.NullCallback();
	}
	
	public CompatScrollView getCompatScrollView() {
		return mLayoutInflaterController.getCompatScrollView();
	}
	
	/**
	 * 返回activity的子布局
	 * @return
	 */
	public ViewGroup getSubContentView() {
		return (ViewGroup) mLayoutInflaterController.findViewById(mContentView, R.id.fl_sub_layout);
	}
}