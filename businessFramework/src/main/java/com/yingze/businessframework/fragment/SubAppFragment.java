package com.yingze.businessframework.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yingze.businessframework.R;

public abstract class SubAppFragment extends AppFragment {
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_nopic)
				.showImageForEmptyUri(R.drawable.ic_nopic)
				.showImageOnFail(R.drawable.ic_nopic)
				.imageScaleType(ImageScaleType.EXACTLY)
				.cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return super.onCreateView(inflater, container, savedInstanceState);

	}
}
