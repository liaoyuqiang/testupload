package com.yingze.photobrowser.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.util.Log;

@SuppressLint("UseValueOf")
public class Tools {

	// byte[] 与 short[] 的相互转化，采取合并及拆分的方式进行

	public byte[] shortToByte(short[] shortArr) {
		if(shortArr == null)return null;
		byte[] byteArr = new byte[shortArr.length * 2];
		for (int i = 0; i < shortArr.length; i++) {
			byte[] byteArr_tmp = shortToByte(shortArr[i]);
			byteArr[i * 2] = byteArr_tmp[0];
			byteArr[i * 2 + 1] = byteArr_tmp[1];
		}
		return byteArr;
	}

	public short[] byteToShort(byte[] byteArr) {
		if(byteArr == null)return null;
		short[] shortArr = new short[byteArr.length/2];
		for(int i=0;i<byteArr.length/2;i++) {
			shortArr[i] = byteToShort(byteArr, i*2);
		}
		return shortArr;
	}
		
	public byte[] shortToByte(short number) {
		int temp = number;
		byte[] b = new byte[2];
		for (int i = 0; i < b.length; i++) {
			b[i] = new Integer(temp & 0xff).byteValue();// 将最低位保存在最低位
			temp = temp >> 8; // 向右移8位
		}
		return b;
	}

	public short byteToShort(byte[] b,int index) { 
        short s = 0; 
        short s0 = (short) (b[index] & 0xff);// 最低位 
        short s1 = (short) (b[index + 1] & 0xff); 
        s1 <<= 8; 
        s = (short) (s0 | s1); 
        return s; 
    }
	
	public ArrayList<Short> arrayToList_short(short[] array) {
		ArrayList<Short> list = new ArrayList<Short>();
		for(short item:array) {
			list.add(item);
		}
		return list;
	}
	
	public String getCurrentTime(long tome) {
		Date d = new Date(tome); 
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");//其中yyyy-MM-dd是你要表示的格式 
		// 可以任意组合，不限个数和次序；具体表示为：MM-month,dd-day,yyyy-year;kk-hour,mm-minute,ss-second; 
		String str=sdf.format(d); 
		return str; 
	}
	
	public String getTime(long time) {
		Date d = new Date(time); 
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");//其中yyyy-MM-dd是你要表示的格式 
		// 可以任意组合，不限个数和次序；具体表示为：MM-month,dd-day,yyyy-year;kk-hour,mm-minute,ss-second; 
		String str=sdf.format(d); 
		return str; 
	}
	
	public String getPictrueNameByTime(long time) {
		Date d = new Date(time); 
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");//其中yyyy-MM-dd是你要表示的格式 
		// 可以任意组合，不限个数和次序；具体表示为：MM-month,dd-day,yyyy-year;kk-hour,mm-minute,ss-second; 
		String str=sdf.format(d); 
		return str; 
	}
	
	public long getSystemTime() {
		return System.currentTimeMillis();
	}
	
	public static String getCharset(File file) {
        String charset = "GBK";
        byte[] first3Bytes = new byte[3];
        try {
            boolean checked = false;
            BufferedInputStream bis = new BufferedInputStream(
                  new FileInputStream(file));
            bis.mark(0);
            int read = bis.read(first3Bytes, 0, 3);
            if (read == -1)
                return charset;
            if (first3Bytes[0] == (byte) 0xFF && first3Bytes[1] == (byte) 0xFE) {
                charset = "UTF-16LE";
                checked = true;
            } else if (first3Bytes[0] == (byte) 0xFE && first3Bytes[1]
                == (byte) 0xFF) {
                charset = "UTF-16BE";
                checked = true;
            } else if (first3Bytes[0] == (byte) 0xEF && first3Bytes[1]
                    == (byte) 0xBB
                    && first3Bytes[2] == (byte) 0xBF) {
                charset = "UTF-8";
                checked = true;
            }
            bis.reset();
            if (!checked) {
                int loc = 0;
                while ((read = bis.read()) != -1) {
                    loc++;
                    if (read >= 0xF0)
                        break;
                    //单独出现BF以下的，也算是GBK
                    if (0x80 <= read && read <= 0xBF)
                        break;
                    if (0xC0 <= read && read <= 0xDF) {
                        read = bis.read();
                        if (0x80 <= read && read <= 0xBF)// 双字节 (0xC0 - 0xDF)
                            // (0x80 -
                            // 0xBF),也可能在GB编码内
                            continue;
                        else
                            break;
                     // 也有可能出错，但是几率较小
                    } else if (0xE0 <= read && read <= 0xEF) {
                        read = bis.read();
                        if (0x80 <= read && read <= 0xBF) {
                            read = bis.read();
                            if (0x80 <= read && read <= 0xBF) {
                                charset = "UTF-8";
                                break;
                            } else
                                break;
                        } else
                            break;
                    }
                }
                System.out.println(loc + " " + Integer.toHexString(read));
            }
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return charset;
    }
	
	public boolean saveBitmapToFile(Bitmap bitmap, String _file) throws IOException {
		BufferedOutputStream os = null;
		try {
			File file = new File(_file);
			// String _filePath_file.replace(File.separatorChar +
			// file.getName(), "");
			int end = _file.lastIndexOf(File.separator);
			String _filePath = _file.substring(0, end);
			File filePath = new File(_filePath);
			if (!filePath.exists()) {
				filePath.mkdirs();
			}
			file.createNewFile();
			os = new BufferedOutputStream(new FileOutputStream(file));
			return bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					Log.e("save bitmap", e.getMessage(), e);
				}
			}
			
		}
	}
	
	public String resdFile(String path) throws IOException {
		
		File file = new File(path);
		FileInputStream fs = new FileInputStream(file);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		
		byte[] buffer = new byte[512];
		int length = 0;
		
		while((length=fs.read(buffer))!=-1) {
			os.write(buffer, 0, length);
		}
		fs.close();
		os.close();
		String str = os.toString();
		return str;
	}
	
}
