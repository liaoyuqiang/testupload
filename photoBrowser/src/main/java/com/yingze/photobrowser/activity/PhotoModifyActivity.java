/**
 * 图片修改界面，控制各种特效的生成
 */
package com.yingze.photobrowser.activity;

import java.io.Serializable;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.fedorvlasov.lazylist.FileCache;
import com.fedorvlasov.lazylist.ImageLoader;
import com.yingze.corelibrary.zoom.ZoomImageView;
import com.yingze.photobrowser.imageloader.R;
import com.yingzecorelibrary.utils.LogUtil;

public class PhotoModifyActivity extends Activity implements View.OnClickListener {
	private ImageView iv_close;
	private TextView iv_photo_edit;
	private Button btn_photo_edit;
	private String mImageUrl;
	private ImageLoader mImageLoader;
	private ZoomImageView ziv_imageViewDialog_image;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		initResourceView();
		initTransaction();
		initListener();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if(intent!=null && intent.getExtras()!=null) {
			mImageUrl = intent.getExtras().getString("image_url");
			Bitmap bitmap = getImageBitmap(mImageUrl, 600, 600);
			ziv_imageViewDialog_image.reset();
			ziv_imageViewDialog_image.requestLayout();
			ziv_imageViewDialog_image.invalidate();
			ziv_imageViewDialog_image.setImageBitmap(bitmap);
		}
	}
	
	public void initResourceView() {
		this.setContentView(R.layout.activity_photo_modify);
		
		iv_close = (ImageView) findViewById(R.id.iv_close);
		iv_photo_edit = (TextView) findViewById(R.id.tv_photo_sure);
		btn_photo_edit = (Button) findViewById(R.id.btn_photo_edit);
	}
	
	public void initTransaction() {
		Intent intent = getIntent();
		if(intent!=null && intent.getExtras()!=null) {
			mImageUrl = intent.getExtras().getString("image_url");
		}
		
		FileCache.downloadFolder = "";
		mImageLoader = new ImageLoader(this);
		
		ziv_imageViewDialog_image = (ZoomImageView)findViewById(R.id.ziv_imageViewDialog_image);
		Bitmap bitmap = getImageBitmap(mImageUrl, 600, 600);
		ziv_imageViewDialog_image.setImageBitmap(bitmap);
	}
	
	public void initListener() {
		iv_close.setOnClickListener(this);
		iv_photo_edit.setOnClickListener(this);
		btn_photo_edit.setOnClickListener(this);
	}
	
	public Bitmap getImageBitmap(String fileSavePath ,int image_width ,int image_height){
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(fileSavePath, options);
		
		int bitmap_width = options.outWidth;
		int bitmap_height = options.outHeight;
		int inSampleSize = 1;
		if (bitmap_width > image_width || bitmap_height > image_height) {
			int scale_width = bitmap_width / image_width;
			int scale_height = bitmap_height / image_height;
			if (scale_width > scale_height) {
				inSampleSize = scale_width;
			}else {
				inSampleSize = scale_height;
			}
		}
		options.inSampleSize = inSampleSize;
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeFile(fileSavePath, options);
		return bitmap;
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.iv_close) {
			finish();
		} else if(v.getId() == R.id.tv_photo_sure) {
			Intent intent = new Intent();			
			Bundle bundle = getIntent().getExtras();
			PhotoModifyController controller = (PhotoModifyController)bundle.getSerializable(PhotoModifyController.getID());
			controller.setImageUrl(mImageUrl);
			LogUtil.set("photo_modify", "+++++++++++ : "+mImageUrl).verbose();
			bundle.putSerializable(PhotoModifyController.getID(), controller);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), controller.getRedirectionName());
			startActivity(intent);
			finish();
		} else if(v.getId() == R.id.btn_photo_edit) {
			Intent intent = new Intent();
			intent.setClass(this, PhotoSpecialEffectsActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("image_url", mImageUrl);
			intent.putExtras(bundle);
			startActivity(intent);
		}
	}
	
	public static class PhotoModifyController implements Serializable {
		private static long serialVersionUID = 1L;
		private String mRedirectionActivityName;
		private String imageUrl;
		
		public static String getID() {
			return String.format("%d", serialVersionUID);
		}
		public static void setID(long id){
			serialVersionUID = id;
		}
		public PhotoModifyController(Activity activity) {
			mRedirectionActivityName = activity.getClass().getName();
		}
		
		public String getRedirectionName() {
			return mRedirectionActivityName;
		}
		
		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}
		
		public String getImageUrl() {
			return imageUrl;
		}
	}
}
