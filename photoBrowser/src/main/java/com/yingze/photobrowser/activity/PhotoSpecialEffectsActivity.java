package com.yingze.photobrowser.activity;

import java.io.File;
import java.io.IOException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.fedorvlasov.lazylist.FileCache;
import com.fedorvlasov.lazylist.ImageLoader;
import com.yingze.photobrowser.bean.BorderView;
import com.yingze.photobrowser.bean.BorderView.ZoomCallback;
import com.yingze.photobrowser.imageloader.R;
import com.yingze.photobrowser.utils.Tools;
import com.yingzecorelibrary.utils.ToastUtil;

@SuppressLint("NewApi")
public class PhotoSpecialEffectsActivity extends Activity implements View.OnClickListener {
	private String imageFloat="Photo_Folder";
	private ImageView iv_close;
	private TextView tv_photo_save;
	private Button btn_photo_cut;
	private Button btn_photo_type_cut;
	private BorderView bv_photo_sepcial_effects;
	private RelativeLayout rl_content;
	private String mImageUrl;
	private ImageLoader mImageLoader;
	private Bitmap sourceBmp;
	private Bitmap resultBmp;
	private Handler mHandler = new Handler();
	private Tools tools = new Tools();
	private int body_width=0;
	private int body_height=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		initResourceView();
		initTransaction();
		initListener();
	}
	
	public void initResourceView() {
		this.setContentView(R.layout.activity_photo_special_effects);
		
		btn_photo_cut = (Button) findViewById(R.id.btn_photo_cut);
		btn_photo_type_cut = (Button) findViewById(R.id.btn_photo_type_cut);
		iv_close = (ImageView) findViewById(R.id.iv_close);
		tv_photo_save = (TextView) findViewById(R.id.tv_photo_save);
		rl_content = (RelativeLayout) findViewById(R.id.rl_content);
		bv_photo_sepcial_effects = (BorderView)findViewById(R.id.bv_photo_sepcial_effects);
	}
	
	public void initTransaction() {
		Intent intent = getIntent();
		if(intent!=null && intent.getExtras()!=null) {
			mImageUrl = intent.getExtras().getString("image_url");
		}
	}
	
	public void initListener() {
		btn_photo_cut.setOnClickListener(this);
		iv_close.setOnClickListener(this);
		tv_photo_save.setOnClickListener(this);

		ViewTreeObserver vto = rl_content.getViewTreeObserver();  
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener(){
		    @Override 
		    public void onGlobalLayout() { 
		    	rl_content.getViewTreeObserver().removeOnGlobalLayoutListener(this);  
		    	body_width = rl_content.getMeasuredWidth();  
		    	body_height = rl_content.getMeasuredHeight(); 
		    	getPicture();
		    } 
		});
		
		bv_photo_sepcial_effects.setZoomCallback(new ZoomCallback(){
			@Override
			public void onSizeChanged(float x, float y) {
				int imageWidth = resultBmp.getWidth();
				int imageHeight = resultBmp.getHeight();
				
				int currentBorderWidth = (int) (imageWidth*x);
				int currentBorderHeight = (int) (imageHeight*y);
				btn_photo_type_cut.setText(String.format("%d*%d", currentBorderWidth,
						currentBorderHeight));
			}
		});
	}
	
	public void getPicture() {
		FileCache.downloadFolder = "";
		mImageLoader = new ImageLoader(this);
		
		// 测试
//		mImageUrl=Environment.getExternalStorageDirectory()+"/Download/slideview_item_2.png";
		sourceBmp = getImageBitmap(mImageUrl, body_width, body_height);
		resultBmp = sourceBmp;

		resetImageLayoutParams(resultBmp);
		bv_photo_sepcial_effects.setImageBitmap(resultBmp);
	}
	
	public void resetImageLayoutParams(Bitmap sourceBmp) {
		int bmpWidth = sourceBmp.getWidth();
		int bmpHeight = sourceBmp.getHeight();
		
		float scale = 1;
		float scale_x = bmpWidth / (float)body_width;
		float scale_y = bmpHeight / (float)body_height;
		
		// 选大的
		scale = scale_x>scale_y?scale_x:scale_y;
		int photoScaleWidth =(int)(bmpWidth/scale);
		int photoScaleHeight = (int)(bmpHeight/scale);
		
		LayoutParams params = (LayoutParams) bv_photo_sepcial_effects.getLayoutParams();
		params.width = photoScaleWidth;
		params.height = photoScaleHeight;
		bv_photo_sepcial_effects.setLayoutParams(params);
	}
	
	public Bitmap getImageBitmap(String fileSavePath ,int image_width ,int image_height) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(fileSavePath, options);
		
		int bitmap_width = options.outWidth;
		int bitmap_height = options.outHeight;
		int inSampleSize = 1;
		if (bitmap_width > image_width || bitmap_height > image_height) {
			int scale_width = bitmap_width / image_width;
			int scale_height = bitmap_height / image_height;
			if (scale_width > scale_height) {
				inSampleSize = scale_width;
			}else {
				inSampleSize = scale_height;
			}
		}
		options.inSampleSize = inSampleSize;
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeFile(fileSavePath, options);
		return bitmap;
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.iv_close) {
			finish();
		} else if(v.getId() == R.id.tv_photo_save) {
			try {
				File file = new File(Environment.getExternalStorageDirectory().getPath()
						+File.separator+imageFloat);
				if(!file.exists()) {
					file.mkdirs();
				}
				String filePath =  file+ File.separator + tools.getPictrueNameByTime(
						System.currentTimeMillis())+".png";
				boolean result = tools.saveBitmapToFile(resultBmp, filePath);
				if(result) {
					mImageUrl = filePath;
					ToastUtil.set(PhotoSpecialEffectsActivity.this, "图片保存成功").show();
				} else {
					ToastUtil.set(PhotoSpecialEffectsActivity.this, "图片保存失败").show();
				}
				
				Intent intent = new Intent(PhotoSpecialEffectsActivity.this, PhotoModifyActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("image_url", mImageUrl);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if(v.getId() == R.id.btn_photo_cut) {
			float[] borderLocation=bv_photo_sepcial_effects.getBorderLocation();
			float[] borderSize = bv_photo_sepcial_effects.getBorderScale();
			float[] viewSize = bv_photo_sepcial_effects.getViewSize();
			
			float localScale = resultBmp.getWidth()/viewSize[0];
			// 方框跟控件大小的缩放比例
			float border_scale_x = borderSize[0];
			float border_scale_y = borderSize[1];
			
			int[] act_location = new int[]{(int)(borderLocation[0]*localScale), 
					(int)(borderLocation[1]*localScale)};
			int[] act_size = new int[]{(int)(resultBmp.getWidth()* border_scale_x), 
					(int)(resultBmp.getHeight()*border_scale_y)};
			
			int[] pixels = new int[act_size[0] * act_size[1]];

			resultBmp.getPixels(pixels, 0, act_size[0], act_location[0], act_location[1], 
					act_size[0], act_size[1]);
			resultBmp = Bitmap.createBitmap(pixels, act_size[0], act_size[1], Config.ARGB_8888);
			
			resetImageLayoutParams(resultBmp);
			
			bv_photo_sepcial_effects.setImageBitmap(resultBmp);
			bv_photo_sepcial_effects.reset();
			bv_photo_sepcial_effects.requestLayout();
		}
	}
	
}
