package com.yingze.photobrowser.bean;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.yingzecorelibrary.utils.LogUtil;

public class BorderView extends ImageView {
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;
	private int mode = NONE;

	private static final int DRAG_FREE = 111;
	private static final int DRAG_POINT_NIGHT_OCLOCK=9;// 9点钟方向
	private static final int DRAG_POINT_TWELVE_OCLOCK=12;// 12点钟方向
	private static final int DRAG_POINT_THREE_OCLOCK=3;// 3点钟方向
	private static final int DRAG_POINT_SIX_OCLOCK=6;// 6点钟方向
	
	private static final int DRAG_POINT_ELEVEN_OCLOCK = 11;// 11点钟方向
	private static final int DRAG_POINT_ONE_OCLOCK = 1;// 1点钟方向
	private static final int DRAG_POINT_FIVE_OCLOCK = 5;// 5点钟方向
	private static final int DRAG_POINT_SEVEN_OCLOCK = 7;// 7点钟方向
	
	private int dragMode = DRAG_FREE;
	
    private float[] mMatrixValues = new float[9];
    private float mWidth = 0;
    private float mHeight = 0;
    private float mOldDist = 1f; 
	
	private PointF start = new PointF(); 
    private PointF mid = new PointF(); 
       
    private Paint mPaint = new Paint();
    private Paint mPaintLayer = new Paint();
    private Paint mPaintCircle = new Paint();
    private RectF mRect = new RectF();
    
	private Matrix mImageMatrix = new Matrix();
	private Matrix matrix = new Matrix();
	private Matrix mSavedMatrix = new Matrix();

	private ZoomCallback mZoomCallback;
	
	public BorderView(Context context) {
		super(context);
		this.setClickable(true);
		init();
	}
	
	public BorderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setClickable(true);
		init();
	}

	public void init() {
		mPaint.setAntiAlias(true);
		mPaint.setColor(0xffffffff);
		mPaint.setStyle(Style.STROKE);
		mPaint.setStrokeWidth(2);
		
		mPaintLayer.setAntiAlias(true);
		mPaintLayer.setColor(0xcc000000);
		
		mPaintCircle.setAntiAlias(true);
		mPaintCircle.setColor(0xffffffff);
	}
	
	public void initSize() {
		mWidth = getWidth();
		mHeight = getHeight();
	}
	
	public void reset() {
		start = new PointF(); 
	    mid = new PointF(); 

		mImageMatrix = new Matrix();
		matrix = new Matrix();
		mSavedMatrix = new Matrix();
	}
	
	public float[] getBorderLocation() {
		return new float[]{
				getValue(matrix, Matrix.MTRANS_X),
				getValue(matrix, Matrix.MTRANS_Y)};
	}
	
	public float[] getBorderScale() {
		return new float[]{getValue(matrix, Matrix.MSCALE_X),
				getValue(matrix, Matrix.MSCALE_Y)};
	}
	
	public float[] getViewSize(){
		return new float[]{mWidth, mHeight};
	}
	
	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		
		float matrix_tran_x = this.getValue(matrix, Matrix.MTRANS_X);
		float matrix_tran_y = this.getValue(matrix, Matrix.MTRANS_Y);
		float matrix_scale_x = this.getValue(matrix, Matrix.MSCALE_X);
		float matrix_scale_y = this.getValue(matrix, Matrix.MSCALE_Y);
		
		Log.v("borderview", String.format("%f %f %f %f", matrix_tran_x, matrix_tran_y,
				matrix_scale_x, matrix_scale_y));

		mRect.left = matrix_tran_x;
		mRect.top = matrix_tran_y;
		mRect.right = mRect.left+mWidth*matrix_scale_x;
		mRect.bottom = mRect.top + mHeight*matrix_scale_y;
		
		// 画覆盖层
		canvas.drawRect(new RectF(0, 0, getWidth(), mRect.top), mPaintLayer);
		canvas.drawRect(new RectF(0, mRect.bottom, getWidth(), getHeight()), mPaintLayer);
		
		canvas.drawRect(new RectF(0, mRect.top, mRect.left, mRect.bottom), mPaintLayer);
		canvas.drawRect(new RectF(mRect.right, mRect.top, getWidth(), mRect.bottom), mPaintLayer);
		
		// 画白色方框
		canvas.drawRect(mRect, mPaint);
		
		// 画白色实心圆圈
		int row=0, col=0, len=3,cx=0,cy=0;
		for(int i=0;i<len*len;i++) {
			if(i==len*len/2)continue;
			row = i/len;
			col = i%len;
			cx = (int) (matrix_tran_x + col*mWidth*matrix_scale_x/(len-1));
			cy = (int) (matrix_tran_y + row*mHeight*matrix_scale_y/(len-1));
			drawCircle(canvas, cx, cy);
		}
	}
	
	public void drawCircle(Canvas canvas,float cx, float cy) {
		canvas.drawCircle(cx, cy, 8, mPaintCircle);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			matrix.set(mImageMatrix);
			mSavedMatrix.set(matrix);
			start.set(event.getX(), event.getY());
			mode = DRAG;
			
			handleDragEvent(event);
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			mOldDist = spacing(event);
			if (mOldDist > 10f) {
				mSavedMatrix.set(matrix);
				midPoint(mid, event);
				mode = ZOOM;
			}
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			dragMode = DRAG_FREE;
			mode = NONE;
			mImageMatrix.set(matrix);
			break;
		case MotionEvent.ACTION_MOVE:
			if (mode == DRAG) {
				matrix.set(mSavedMatrix);
				
				float matrix_tran_x = this.getValue(matrix, Matrix.MTRANS_X);
				float matrix_tran_y = this.getValue(matrix, Matrix.MTRANS_Y);
				float matrix_scale_x = this.getValue(matrix, Matrix.MSCALE_X);
				float matrix_scale_y = this.getValue(matrix, Matrix.MSCALE_Y);
				
				float left = matrix_tran_x;
				float top = matrix_tran_y;
				float right = left + mWidth*matrix_scale_x;
				float bottom = top + mHeight*matrix_scale_y;
				
				switch(dragMode) {
				case DRAG_FREE:
					matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
					break;
				case DRAG_POINT_NIGHT_OCLOCK:
					float gap = (event.getX() - start.x)/(mWidth*matrix_scale_x);
					float finalGap = 1 - gap;
					matrix.postScale(finalGap, 1, right, top);
					break;
				case DRAG_POINT_TWELVE_OCLOCK:
					gap = (event.getY() - start.y)/(mHeight*matrix_scale_y);
					finalGap = 1 - gap;
					matrix.postScale(1, finalGap, left, bottom);
					break;
				case DRAG_POINT_THREE_OCLOCK:
					gap = (event.getX() - start.x)/(mWidth*matrix_scale_x);
					finalGap = 1 + gap;
					matrix.postScale(finalGap, 1, left, top);
					break;
				case DRAG_POINT_SIX_OCLOCK:
					gap = (event.getY() - start.y)/(mHeight*matrix_scale_y);
					finalGap = 1 + gap;
					matrix.postScale(1, finalGap, left, top);
					break;
					
				case DRAG_POINT_ELEVEN_OCLOCK:
					float gap_x = (event.getX() - start.x)/(mWidth*matrix_scale_x);
					float gap_y = (event.getY() - start.y)/(mHeight*matrix_scale_y);
					float finalGap_x = 1 - gap_x;
					float finalGap_y = 1 - gap_y;
					matrix.postScale(finalGap_x, finalGap_y, right, bottom);
					break;
					
				case DRAG_POINT_FIVE_OCLOCK:
					gap_x = (event.getX() - start.x)/(mWidth*matrix_scale_x);
					gap_y = (event.getY() - start.y)/(mHeight*matrix_scale_y);
					finalGap_x = 1 + gap_x;
					finalGap_y = 1 + gap_y;
					matrix.postScale(finalGap_x, finalGap_y, left, top);
					break;
					
				case DRAG_POINT_ONE_OCLOCK:
					gap_x = (event.getX() - start.x)/(mWidth*matrix_scale_x);
					gap_y = (event.getY() - start.y)/(mHeight*matrix_scale_y);
					finalGap_x = 1 + gap_x;
					finalGap_y = 1 - gap_y;
					matrix.postScale(finalGap_x, finalGap_y, left, bottom);
					break;
					
				case DRAG_POINT_SEVEN_OCLOCK:
					gap_x = (event.getX() - start.x)/(mWidth*matrix_scale_x);
					gap_y = (event.getY() - start.y)/(mHeight*matrix_scale_y);
					finalGap_x = 1 - gap_x;
					finalGap_y = 1 + gap_y;
					matrix.postScale(finalGap_x, finalGap_y, right, top);
					break;
				}
				
				checkBorder(matrix);
			} else if (mode == ZOOM) {
				float newDist = spacing(event);
				if (newDist > 10f) {
					matrix.set(mSavedMatrix);
					float scale = newDist / mOldDist;
					LogUtil.set("scale", "缩放比例:"+scale);
					matrix.postScale(scale, scale, mid.x, mid.y);
					checkBorder(matrix);
				}
			}
			break;
		}
		mZoomCallback.onSizeChanged(getValue(matrix, Matrix.MSCALE_X), 
				getValue(matrix, Matrix.MSCALE_Y));
		invalidate();
		return true;
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		initSize();
	}
	
	public void handleDragEvent(MotionEvent event) {
		float matrix_tran_x = this.getValue(matrix, Matrix.MTRANS_X);
		float matrix_tran_y = this.getValue(matrix, Matrix.MTRANS_Y);
		float matrix_scale_x = this.getValue(matrix, Matrix.MSCALE_X);
		float matrix_scale_y = this.getValue(matrix, Matrix.MSCALE_Y);
		
		int row=0, col=0, len=3,cx=0,cy=0, index = -1;
		for(int i=0;i<len*len;i++) {
			if(i==len*len/2)continue;
			row = i/len;
			col = i%len;
			cx = (int) (matrix_tran_x + col*mWidth*matrix_scale_x/(len-1));
			cy = (int) (matrix_tran_y + row*mHeight*matrix_scale_y/(len-1));
			if(checkLocation(cx, cy, event.getX(), event.getY())) {
				index = i;
				break;
			}
		}
		
		if(index==0) {
			dragMode = DRAG_POINT_ELEVEN_OCLOCK;
		} else if(index==1){
			dragMode = DRAG_POINT_TWELVE_OCLOCK;
		} else if(index==2) {
			dragMode = DRAG_POINT_ONE_OCLOCK;
		} else if(index==3) {
			dragMode = DRAG_POINT_NIGHT_OCLOCK;
		} else if(index==5) {
			dragMode = DRAG_POINT_THREE_OCLOCK;
		} else if(index==6) {
			dragMode = DRAG_POINT_SEVEN_OCLOCK;
		} else if(index==7) {
			dragMode = DRAG_POINT_SIX_OCLOCK;
		} else if(index==8) {
			dragMode = DRAG_POINT_FIVE_OCLOCK;
		} else {
			dragMode = DRAG_FREE;
		}
		
	}
	
	public boolean checkLocation(float cx, float cy, float eventX, float eventY) {
		float x = cx - eventX;
		float y = cy - eventY;
		float gap =  FloatMath.sqrt(x * x + y * y);
		if(gap<30) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 边界检测
	 * @param matrix
	 */
	private void checkBorder(Matrix matrix) {
		// 获取当前变量
		float matrix_tran_x = this.getValue(matrix, Matrix.MTRANS_X);
		float matrix_tran_y = this.getValue(matrix, Matrix.MTRANS_Y);
		float matrix_scale_x = this.getValue(matrix, Matrix.MSCALE_X);
		float matrix_scale_y = this.getValue(matrix, Matrix.MSCALE_Y);
		
		if(matrix_scale_x>1 || matrix_scale_y>1) {
			float[] values = new float[9];
			matrix.getValues(values);
			values[Matrix.MTRANS_X] = matrix_tran_x;
			values[Matrix.MTRANS_Y] = matrix_tran_y;
			values[Matrix.MSCALE_X] = matrix_scale_x>1 ? 1:matrix_scale_x;
			values[Matrix.MSCALE_Y] = matrix_scale_y>1 ? 1:matrix_scale_y;
			matrix.setValues(values);
		}
		
		// 刷新当前变量
		matrix_tran_x = this.getValue(matrix, Matrix.MTRANS_X);
		matrix_tran_y = this.getValue(matrix, Matrix.MTRANS_Y);
		matrix_scale_x = this.getValue(matrix, Matrix.MSCALE_X);
		matrix_scale_y = this.getValue(matrix, Matrix.MSCALE_Y);
		
		if(matrix_tran_x<0 || matrix_tran_y<0) {
			float[] values = new float[9];
			matrix.getValues(values);
			values[Matrix.MTRANS_X] = matrix_tran_x>0 ? matrix_tran_x:0;
			values[Matrix.MTRANS_Y] = matrix_tran_y>0 ? matrix_tran_y:0;
			values[Matrix.MSCALE_X] = matrix_scale_x;
			values[Matrix.MSCALE_Y] = matrix_scale_y;
			matrix.setValues(values);
		}
		
		// 刷新当前变量
		matrix_tran_x = this.getValue(matrix, Matrix.MTRANS_X);
		matrix_tran_y = this.getValue(matrix, Matrix.MTRANS_Y);
		matrix_scale_x = this.getValue(matrix, Matrix.MSCALE_X);
		matrix_scale_y = this.getValue(matrix, Matrix.MSCALE_Y);
		
		if(matrix_tran_x+mWidth*matrix_scale_x>mWidth || matrix_tran_y+mHeight*matrix_scale_y>mHeight) {
			float[] values = new float[9];
			matrix.getValues(values);
			values[Matrix.MTRANS_X] = matrix_tran_x+mWidth*matrix_scale_x>mWidth ? 
					mWidth-mWidth*matrix_scale_x:matrix_tran_x;
			values[Matrix.MTRANS_Y] = matrix_tran_y+mHeight*matrix_scale_y>mHeight?
					mHeight-mHeight*matrix_scale_y:matrix_tran_y;
			values[Matrix.MSCALE_X] = matrix_scale_x;
			values[Matrix.MSCALE_Y] = matrix_scale_y;
			matrix.setValues(values);
		}
	}
	
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	protected float getValue(Matrix matrix, int whichValue) {
        matrix.getValues(mMatrixValues);
        return mMatrixValues[whichValue];
    }
	
	
	
	public ZoomCallback getZoomCallback() {
		return mZoomCallback;
	}

	public void setZoomCallback(ZoomCallback zoomCallback) {
		this.mZoomCallback = zoomCallback;
	}

	public static interface ZoomCallback {
		public void onSizeChanged(float x, float y);
	}

}
